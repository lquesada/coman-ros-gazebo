#include "gazebo/physics/physics.hh"
#include <functional>
#include <gazebo/gui/GuiIface.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"
#include "gazebo/rendering/rendering.hh"
#include "gazebo/sensors/sensors.hh"
#include <iostream>
#include "ignition/math4/ignition/math.hh"
#include <vector>
#include <string>
#include "StiffJoint.hh"
#include <chrono>
#include <thread>

const int N = 31; // Usual number of joints

// The spawning plugin

namespace gazebo
{
class Spawning : public WorldPlugin
{
private:
  //***** Simulation settings

  const bool walkingExp = false; // We want to spawn walking experiment of carrying?
  bool coupled;                  //->File | Do we want to do a trial where both coman are coupled?
  double coupledN;               //->File | The value coming from the parameter file
  int iters;                     //->File | Number of iteration for the solver
  double sor;                    //->File | The SOR of the solver

  // Joint attach between hands and table

  bool customJoint = true; // Use the custom joint or not

  double linearStiffness;     //->File
  double linearDamping;       //->File
  double rotationalStiffness; //->File
  double rotationalDamping;   //->File

  // Phase lag control

  bool phaseLag = true;    // Enable/disable phase lag control
  double phase;            // ->File | 1 = No phase (Pace), 0 = Full cycle phase (Trot), can go continuously from 0 to 1
  double pushForce = 50;   // Force that is used to impose initial phase lag
  bool startingCom = true; // -> File | Which coman starts first?

  // Perturbation

  bool objInc = false;  // Incrementable increase (can't be both inc and step)
  bool objStep = false; // Step increase (can't be both inc and step)

  double initialMass; //->File | The initial mass of the object
  double newMass;     //->File | The new mass in case of the step trial (mass goes from initialMass to newMass at a given time)
  double massType;    //0: None , 1:Inc, 2:Step ->File

  double force[3] = {}; //->File | The force applied on the table (in N)
  double forceSpan = 0; //->File | The during how much time the force is applied (in seconds)

  // Associated file

  std::ifstream inputParameter; // File to read parameters

  //**** Initializing useful variables

  gazebo::physics::WorldPtr world;
  gazebo::event::ConnectionPtr updateConnection;
  bool firstLoop = true;
  std::vector<event::ConnectionPtr> connections;
  double simTime;

  physics::ModelPtr gazCom;
  physics::ModelPtr gazCom1;
  physics::ModelPtr gazTable;
  physics::LinkPtr object;

  StiffJoint *backRHJ;
  StiffJoint *backLHJ;
  StiffJoint *frontRHJ;
  StiffJoint *frontLHJ;

  std::vector<gazebo::physics::JointPtr> joints1;
  std::vector<gazebo::physics::JointPtr> joints2;

  physics::PhysicsEnginePtr physicsEng;

  // Initial position of the robots

  double qknee0 = 0.2, qP0 = -0.1, qR0 = 0.055 * 1;
  const double RIGHT_ELBOW_YAW = 0, LEFT_ELBOW_YAW = 0, HIP_YAW = 0;

  double bWaistYaw = 0;
  double bWaistSag = 0;
  double bWaistLat = 0;
  double bhipSag = qP0;
  double bhipLat = -qR0;
  double bhipYaw = 0;
  double bkneeSag = qknee0;
  double bankSag = qP0 * 1;
  double bankLat = qR0;
  double bshSag = 0.01;
  double bshLat = -0.23;
  double bshYaw = 0.015;
  double belbj = -0.3;
  double bforearmPlate = 0.06;
  double bWrj1 = -0.001;
  double bWrj2 = -0.06481;

  double fWaistYaw = 0;
  double fWaistSag = 0.175;
  double fWaistLat = 0;
  double fhipSag = qP0;
  double fhipLat = -qR0;
  double fhipYaw = 0;
  double fkneeSag = qknee0;
  double fankSag = qP0 * 1;
  double fankLat = qR0;
  double fshSag = 0.2;
  double fshLat = -0.2;
  double fshYaw = -0.06398;
  double felbj = -0.46973;
  double fforearmPlate = 0.1725;
  double fWrj1 = 0.2;
  double fWrj2 = 0.05;

  double qInitCOMAN1[N] = {bWaistYaw, bWaistSag, bWaistLat, bhipSag, bhipSag, bhipLat, bhipYaw, bkneeSag, bankSag, bankLat, -bhipLat, bhipYaw, bkneeSag, bankSag, -bankLat, bshSag, bshLat, bshYaw, belbj, bshSag, -bshLat, -bshYaw, belbj, bforearmPlate, bWrj1, bWrj2, -bforearmPlate, bWrj1, -bWrj2, 0, 0};
  double qInitCOMAN2[N] = {fWaistYaw, fWaistSag, fWaistLat, fhipSag, fhipSag, fhipLat, fhipYaw, fkneeSag, fankSag, fankLat, -fhipLat, fhipYaw, fkneeSag, fankSag, -fankLat, fshSag, fshLat, fshYaw, felbj, fshSag, -fshLat, -fshYaw, felbj, fforearmPlate, fWrj1, fWrj2, -fforearmPlate, fWrj1, -fWrj2, 0, 0};

public:
  // To be modified in case it is used in a different computer
  // Adapt the path to where the models are located

  std::string pathComan = "/home/biorob/.gazebo/models/coman/model.sdf"; // Path to the coman model.sdf
  std::string pathTable = "/home/biorob/.gazebo/models/table/model.sdf"; // Path to the table model.sdf

  std::string paramPath = "/home/biorob/Parameters/"; // Path were the parameter files are located

  std::string plugWalk = "walking_plugin";   // Name of the walking plugin
  std::string plugCarry = "carrying_plugin"; // name of the carrying plugin

  std::vector<std::string> jointsNames =
      {
          "WaistYaw", "WaistSag", "WaistLat", "RHipSag", "LHipSag", "RHipLat", "RHipYaw", "RKneeSag", "RAnkSag", "RAnkLat", "LHipLat", "LHipYaw", "LKneeSag", "LAnkSag", "LAnkLat", "RShSag", "RShLat", "RShYaw", "RElbj", "LShSag", "LShLat", "LShYaw", "LElbj", "RForearmPlate", "RWrj1", "RWrj2", "LForearmPlate", "LWrj1", "LWrj2", "r_handj", "l_handj"};

  // Function that is called during the loading of the world
  // Essentially loads the simulation parameters

  void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/)
  {
    // Gather pointers to worls and models

    world = _parent;

    physicsEng = world->Physics();

    gazCom = world->ModelByName("coman");

    if (!walkingExp)
    {
      gazCom1 = world->ModelByName("coman1");
    }

    //-- Loading all the parameters for the simulation

    inputParameter.open(paramPath + "stiffness.txt");
    inputParameter >> linearStiffness;
    inputParameter.close();

    //

    inputParameter.open(paramPath + "damping.txt");
    inputParameter >> linearDamping;
    inputParameter.close();

    //

    inputParameter.open(paramPath + "rotStiffness.txt");
    inputParameter >> rotationalStiffness;
    inputParameter.close();

    //

    inputParameter.open(paramPath + "rotDamping.txt");
    inputParameter >> rotationalDamping;
    inputParameter.close();

    //

    inputParameter.open(paramPath + "initMass.txt");
    inputParameter >> initialMass;
    inputParameter.close();

    //

    inputParameter.open(paramPath + "newMass.txt");
    inputParameter >> newMass;
    inputParameter.close();

    //

    inputParameter.open(paramPath + "typeMass.txt");
    inputParameter >> massType;
    inputParameter.close();

    objInc = (massType == 1);
    objStep = (massType == 2);

    if (massType != 0)
    {
      object = gazTable->GetLink("object"); // Taking object pointer in case of a perturbation trial
    }

    //

    inputParameter.open(paramPath + "phaseLag.txt");
    inputParameter >> phase;
    inputParameter.close();

    if (phase <= 0)
    {
      startingCom = false;
      phase = -phase;
    }

    //

    inputParameter.open(paramPath + "coupled.txt");
    inputParameter >> coupledN;
    inputParameter.close();

    coupled = (coupledN == 1);

    if (coupled)
    {
      gazTable = world->ModelByName("table"); // Taking the table pointer in case robots are coupled
    }

    //

    inputParameter.open(paramPath + "iters.txt");
    inputParameter >> iters;
    inputParameter.close();

    //

    inputParameter.open(paramPath + "sor.txt");
    inputParameter >> sor;
    inputParameter.close();

    //

    inputParameter.open(paramPath + "force.txt");
    inputParameter >> force[0];
    inputParameter >> force[1];
    inputParameter >> force[2];
    inputParameter.close();

    //

    inputParameter.open(paramPath + "forceSpan.txt");
    inputParameter >> forceSpan;
    inputParameter.close();

    // Event trigger to attach a new loop to the OnUpdate function

    this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&Spawning::OnUpdate, this));
  }

  // Function that is called a each loop of the simulation

  void OnUpdate()
  {
    simTime = world->SimTime().Double(); // Reading simulation time

    if (firstLoop == true && !walkingExp) // Setting the carrying scenario at the first loop
    {
      firstLoop = false;

      // Setting mass of the object on the table (if it exists)

      if (object != NULL && coupled)
      {
        object->GetInertial()->SetMass(initialMass);
        object->UpdateMass();
      }

      // Setting of the initiale position (added for the carrying experiment)

      for (int i = 0; i < jointsNames.size(); i++) // Gathering joints pointers in the right order
      {
        joints1.push_back(gazCom->GetJoint(jointsNames[i]));
        joints2.push_back(gazCom1->GetJoint(jointsNames[i]));
      }

      for (int i = 0; i < joints1.size(); i++) // Setting the  joints positions
      {
        joints1[i]->SetPosition(0, qInitCOMAN1[i]);
        joints2[i]->SetPosition(0, qInitCOMAN2[i]);
      }

      // Attaching links together with a joint

      if (!customJoint && coupled) // If using gazebo native joints
      {
        std::string typeOfJoint = "fixed"; // Type of joint to use (revolute, ball, fixed)

        physics::JointPtr jback_RH = physicsEng->CreateJoint(typeOfJoint, gazCom); // Creating a joint with gazCom (Coman 1) as a parent
        physics::LinkPtr clback_RH = gazTable->GetChildLink("back_RH");            // Setting which link in the table to join
        physics::LinkPtr plback_RH = gazCom->GetChildLink("RSoftHand");            // Setting which link on the coman to join
        jback_RH->Attach(plback_RH, clback_RH);                                    // Attach the two joints
        jback_RH->Load(plback_RH, clback_RH, ignition::math::Pose3d());            // Set the distance between the two links
        jback_RH->SetModel(gazTable);                                              // Setting which model the joint belongs to

        physics::JointPtr jback_LH = physicsEng->CreateJoint(typeOfJoint, gazCom);
        physics::LinkPtr clback_LH = gazTable->GetChildLink("back_LH");
        physics::LinkPtr plback_LH = gazCom->GetChildLink("LSoftHand");
        jback_LH->Attach(plback_LH, clback_LH);
        jback_LH->Load(plback_LH, clback_LH, ignition::math::Pose3d());
        jback_LH->SetModel(gazTable);

        physics::JointPtr jfront_RH = physicsEng->CreateJoint(typeOfJoint, gazCom1);
        physics::LinkPtr clfront_RH = gazTable->GetChildLink("front_RH");
        physics::LinkPtr plfront_RH = gazCom1->GetChildLink("RSoftHand");
        jfront_RH->Attach(plfront_RH, clfront_RH);
        jfront_RH->Load(plfront_RH, clfront_RH, ignition::math::Pose3d());
        jfront_RH->SetModel(gazTable);

        physics::JointPtr jfront_LH = physicsEng->CreateJoint(typeOfJoint, gazCom1);
        physics::LinkPtr clfront_LH = gazTable->GetChildLink("front_LH");
        physics::LinkPtr plfront_LH = gazCom1->GetChildLink("LSoftHand");
        jfront_LH->Attach(plfront_LH, clfront_LH);
        jfront_LH->Load(plfront_LH, clfront_LH, ignition::math::Pose3d());
        jfront_LH->SetModel(gazTable);
      }
      else if (coupled) // If using custom spring-damper joints
      {
        // Getting the link that will be attached

        physics::LinkPtr clback_RH = gazTable->GetChildLink("back_RH");
        physics::LinkPtr plback_RH = gazCom->GetChildLink("RSoftHand");

        physics::LinkPtr clback_LH = gazTable->GetChildLink("back_LH");
        physics::LinkPtr plback_LH = gazCom->GetChildLink("LSoftHand");

        physics::LinkPtr clfront_RH = gazTable->GetChildLink("front_RH");
        physics::LinkPtr plfront_RH = gazCom1->GetChildLink("RSoftHand");

        physics::LinkPtr clfront_LH = gazTable->GetChildLink("front_LH");
        physics::LinkPtr plfront_LH = gazCom1->GetChildLink("LSoftHand");

        // Instanciating the custom joint (see the StiffJoint class)

        backRHJ = new StiffJoint(clback_RH, plback_RH);
        backLHJ = new StiffJoint(clback_LH, plback_LH);
        frontRHJ = new StiffJoint(clfront_RH, plfront_RH);
        frontLHJ = new StiffJoint(clfront_LH, plfront_LH);

        // Logging

        // std::cerr << clback_RH->WorldPose() - plback_RH->WorldPose() << std::endl;
        // std::cerr << clback_LH->WorldPose() - plback_LH->WorldPose() << std::endl;
        // std::cerr << clfront_RH->WorldPose() - plfront_RH->WorldPose() << std::endl;
        // std::cerr << clfront_LH->WorldPose() - plfront_LH->WorldPose() << std::endl;

        // Setting the values for stiffness and damping

        backRHJ->linearStiffness = this->linearStiffness;
        backLHJ->linearStiffness = this->linearStiffness;
        frontRHJ->linearStiffness = this->linearStiffness;
        frontLHJ->linearStiffness = this->linearStiffness;

        backRHJ->linearDamping = this->linearDamping;
        backLHJ->linearDamping = this->linearDamping;
        frontRHJ->linearDamping = this->linearDamping;
        frontLHJ->linearDamping = this->linearDamping;

        backRHJ->rotationalStiffness = this->rotationalStiffness;
        backLHJ->rotationalStiffness = this->rotationalStiffness;
        frontRHJ->rotationalStiffness = this->rotationalStiffness;
        frontLHJ->rotationalStiffness = this->rotationalStiffness;

        backRHJ->rotationalDamping = this->rotationalDamping;
        backLHJ->rotationalDamping = this->rotationalDamping;
        frontRHJ->rotationalDamping = this->rotationalDamping;
        frontLHJ->rotationalDamping = this->rotationalDamping;

        // Logging

        // std::cerr << "Stifness: " << this->linearStiffness << " Damping: " << this->linearDamping << std::endl;
        // std::cerr << "Stifness: " << this->rotationalStiffness << " Damping: " << this->rotationalDamping << std::endl;
      }
    }

    // Custom joint managment during the simulation

    if (customJoint && !walkingExp && coupled)
    {
      if (simTime == 0.002) // Initialise the custom joint (after 2 ms to make sure everything is in place in gazebo)
      {
        backRHJ->Initialize();
        backLHJ->Initialize();
        frontRHJ->Initialize();
        frontLHJ->Initialize();
      }

      if (simTime > 0.002) // Applying the joint at each timestep
      {
        backRHJ->Apply();
        backLHJ->Apply();
        frontRHJ->Apply();
        frontLHJ->Apply();
      }
    }

    //*** The below blocks are used in order to act on the scenario depending on the parameters

    //** Perturbation trials

    // Changing the mass of the object in a step manere

    if (simTime == 45 && objStep && object != NULL)
    {
      object->GetInertial()->SetMass(newMass);
      object->UpdateMass();
    }

    // Changing the mass of the object in a incremental manere

    if (simTime >= 45 && objInc && object != NULL)
    {
      double mass = initialMass + (simTime - 45) * 0.1;
      object->GetInertial()->SetMass(mass);
      object->UpdateMass();
    }

    // Force on the table

    if (simTime >= 45 && simTime < 45 + forceSpan && gazTable != NULL)
    {
      gazTable->GetLink("plank")->AddForce(ignition::math::Vector3d(force[0], force[1], force[2]));
    }

    //** Phase lag
    // This will set the initial phase lag

    if (phaseLag && !walkingExp)
    {
      if (simTime <= 24)
      {
        gazCom->GetLink("DWYTorso")->AddForce(ignition::math::Vector3d(0, 10, 0));
        gazCom1->GetLink("DWYTorso")->AddForce(ignition::math::Vector3d(0, 10, 0));
      }
      else if (simTime >= 24 && simTime < 24.2)
      {
        gazCom->GetLink("DWYTorso")->AddForce(ignition::math::Vector3d(0, pushForce, 0));
        gazCom1->GetLink("DWYTorso")->AddForce(ignition::math::Vector3d(0, pushForce, 0));
      }
      else if (simTime >= 24.2 && simTime <= 24.2 + 0.45 * (1 - phase))
      {
        if (startingCom)
        {
          gazCom1->GetLink("DWYTorso")->AddForce(ignition::math::Vector3d(0, pushForce, 0));
        }
        else
        {
          gazCom->GetLink("DWYTorso")->AddForce(ignition::math::Vector3d(0, pushForce, 0));
        }
      }
    }

    // Time logging

    // std::cerr << "Time : " << simTime << std::endl;
  }
};

GZ_REGISTER_WORLD_PLUGIN(Spawning)
} // namespace gazebo
