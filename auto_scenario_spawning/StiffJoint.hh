#include "gazebo/physics/physics.hh"
#include "gazebo/gazebo.hh"
#include "ignition/math4/ignition/math.hh"
#include <iostream>
#include <math.h>

class StiffJoint
{
private:
  gazebo::physics::LinkPtr link1;
  gazebo::physics::LinkPtr link2;

  ignition::math::Pose3d initialOffset;

  // Linear offset variables

  ignition::math::Vector3d initialPosOffset;
  ignition::math::Vector3d currentPosOffset;
  ignition::math::Vector3d offsetPosOffset;

  ignition::math::Vector3d linearVelocityLink1;
  ignition::math::Vector3d linearVelocityLink2;

  ignition::math::Vector3d forceStiff;

  ignition::math::Vector3d forceVel1;
  ignition::math::Vector3d forceVel2;

  ignition::math::Vector3d forceLink1;
  ignition::math::Vector3d forceLink2;

  // Rotationnal offset variables

  ignition::math::Quaterniond rotLink1;
  ignition::math::Quaterniond rotLink2;

  ignition::math::Quaterniond initialRotOffset;
  ignition::math::Quaterniond currentRotOffset;
  ignition::math::Quaterniond offsetRotOffset;

  ignition::math::Vector3d angularVel1;
  ignition::math::Vector3d angularVel2;

  ignition::math::Vector3d torqueStiff;

  ignition::math::Vector3d torqueVelLink1;
  ignition::math::Vector3d torqueVelLink2;

  ignition::math::Vector3d torqueLink1;
  ignition::math::Vector3d torqueLink2;

  // Frame alignment variables

  ignition::math::Vector3d alignedForceStiffLink1;
  ignition::math::Vector3d alignedForceStiffLink2;
  ignition::math::Vector3d alignedForceVelLink1;
  ignition::math::Vector3d alignedForceVelLink2;

  ignition::math::Vector3d alignedTorqueStiffLink1;
  ignition::math::Vector3d alignedTorqueStiffLink2;
  ignition::math::Vector3d alignedTorqueVelLink1;
  ignition::math::Vector3d alignedTorqueVelLink2;

public:
  StiffJoint(gazebo::physics::LinkPtr link1_, gazebo::physics::LinkPtr link2_);
  ignition::math::Vector3d stiffnessLawLinear(ignition::math::Vector3d offsetVec);
  ignition::math::Vector3d dampingLawLinear(ignition::math::Vector3d velocityVec);
  ignition::math::Vector3d stiffnessLawRotational(ignition::math::Vector3d offsetVec);
  ignition::math::Vector3d dampingLawRotational(ignition::math::Vector3d velocityVec);
  void Initialize();
  void Apply();

  //

  double linearStiffness = 100000;
  double linearDamping = 5;
  double rotationalStiffness = 107;
  double rotationalDamping = 3;
};
