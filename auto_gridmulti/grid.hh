#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <bits/stdc++.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctime>
#include <thread>
#include <chrono>
#include <math.h>

using namespace std;

// Structure used to easily pass simulation parameters from on function to another

struct parameters
{
  // Scenario

  bool coupled = true; // Is there a coupling (when running simulation without object between the two robots)
  int typeMass = 0;    // Type of mass perturbation, see spawning.cc

  double initMass = 0.1; // Initial mass of the object
  double newMass = 0.1;  // Final mass of the object

  std::vector<double> force{0, 0, 0}; // Force applied to the table for the force perturbation
  double forceSpan = 0;               // Duration of the force

  double phaseLag = 0.85; // Starting phase lag

  // Robots

  std::vector<double> gait1{0.45, 0.043}; // Gait of back coman {Period, Step Length}
  std::vector<double> gait2{0.45, 0.043}; // Gait of front coman {Period, Step Length}

  std::vector<double> pid1{220.249, -14.3756, 2.03844, -15.7112}; // PID for the back coman legs (used in WalkingController3.cc::EvalTorques)
  std::vector<double> pid2{220.249, -14.3756, 2.03844, -15.7112}; // PID for the front coman legs (used in WalkingController3.cc::EvalTorques)

  // Custom joints between hands and handles of the table (spring-damper)

  double damping = 5;        // Linear damping
  double stiffness = 100000; // Linear stiffness
  double rotDamping = 3;     // Rotationnal damping
  double rotStiffness = 107; // Rotationnal stiffness

  // Simulation

  int threadNb = -1;  // On which thread the simulation is executed, also commands the name of the savefile: DATA/COMANX/carrying#threadNb#.txt
  double iters = 100; // Maximum number of iteration for the physical engine solver
  double sor = 0.3;   // Relaxation coefficient of the solver

  // Navigation

  int activateNav = 0;                  // Do we use the navigation in this simulation ? (turning/following trajectories)
  std::vector<double> pidNav{1.1, 1.7}; // The PD gains for the back COMAN (its yaw follows the table one according to this PD)
};

// The grid class

class Grid
{
private:
  int totalThread;
  std::string gazURI;
  int initPort;

public:
  Grid(void);
  void writeFile(std::string fileName, std::vector<double> params);
  void setParameters(parameters params);
  void execSimulation(std::string simPath, std::string expPath, int type, int count, int threadNb);
  int freeThread();
  int waitForThread();
  void threadExec(std::string simPath, std::string expPath, int type, int count, int threadNb);
  void initializeThreadFiles();
  void waitAllFinished();
  void saveParams(std::ofstream &file, parameters &params);

  // Path where the parameter files are located

  std::string paramPath = "/home/biorob/Parameters/"; //-> To be modified

  // Other paths

  std::string execPath = paramPath + "ExecFiles/";
  std::string threadPath = paramPath + "ThreadFiles/";
};