#include "grid.hh"

/****** The grid class
 *  This class allows you to launch multiple simulation at the same time
 *  It makes it easier setting all the parameters and minimize the amount of manual things you have to do
*******/

Grid::Grid(void)
{
    totalThread = 10; // Number of simulations running in parallel
    initPort = 46;

    initializeThreadFiles();
};

// Used to write a set of parameters in its corresponding file
// Note: Don't forget that for even when there a single parameter in a set, it as to be a vector

void Grid::writeFile(std::string fileName, std::vector<double> params)
{
    std::ofstream paramFile;
    paramFile.open(paramPath + fileName + ".txt", std::ios_base::out | std::ios_base::trunc);

    for (int i = 0; i < params.size(); i++)
    {
        paramFile << params[i] << std::endl;
    }

    paramFile.close();
}

// Write all the parameters in all the files

void Grid::setParameters(parameters params)
{
    std::ofstream paramFile;

    // Scenario

    writeFile("coupled", vector<double>(1, (params.coupled ? 1 : 0)));
    writeFile("typeMass", vector<double>(1, params.typeMass));

    writeFile("initMass", vector<double>(1, params.initMass));
    writeFile("newMass", vector<double>(1, params.newMass));

    writeFile("force", params.force);
    writeFile("forceSpan", vector<double>(1, params.forceSpan));

    writeFile("phaseLag", vector<double>(1, params.phaseLag));

    // Robots

    writeFile("gait1", params.gait1);
    writeFile("gait2", params.gait2);

    writeFile("pid1", params.pid1);
    writeFile("pid2", params.pid2);

    // Joints

    writeFile("damping", vector<double>(1, params.damping));
    writeFile("stiffness", vector<double>(1, params.stiffness));

    writeFile("rotDamping", vector<double>(1, params.rotDamping));
    writeFile("rotStiffness", vector<double>(1, params.rotStiffness));

    // Simulation

    writeFile("threadNb", vector<double>(1, params.threadNb));
    writeFile("iters", vector<double>(1, params.iters));
    writeFile("sor", vector<double>(1, params.sor));

    // Navigation

    writeFile("activateNav", vector<double>(1, params.activateNav));
    writeFile("yawNavGain", params.pidNav);
}

// Execute a simulation on a free thread
// Gazebo can't be started two times on the same port, so basically each time we launch a simulation we are looking
// for a free port to launch on.
// When we set this port as an environment variable
// This function has to be called in a new thread, then detached
// The thread status files are a bunch of files, one per thread, which contain a 1 or a 0 depending on the availability of the thread
// Using files allows to asynchronized the process in a easy way, it can sure be replaced by something safer!

void Grid::execSimulation(std::string simPath, std::string expPath, int type, int count, int threadNb)
{
    int resSim = 0;
    std::ofstream threadStatusFile;

    // Setting the thread as taken

    threadStatusFile.open(threadPath + "thread" + std::to_string(threadNb) + ".txt", std::ios_base::out | std::ios_base::trunc);
    threadStatusFile << 0;
    threadStatusFile.close();

    // Setting the Gazebo port in the environment

    std::string env = "http://localhost:113" + std::to_string(initPort + threadNb);
    setenv("GAZEBO_MASTER_URI", env.c_str(), true);

    // This part redirects gazebo errors in /dev/null
    // Because at the end of the simulation gazebo don't know how to close without displaying an error :/
    // It is just to make the command line cleaner

    int devnull_fd, duperr_fd;
    devnull_fd = open("/dev/null", O_WRONLY | O_APPEND);
    duperr_fd = dup(STDERR_FILENO);
    dup2(devnull_fd, STDERR_FILENO);

    // Launching the simulation depending on the type of trial

    switch (type)
    {
    case 0: // Coupled without object
        resSim = system((execPath + "runCoupled.sh").c_str());
        break;

    case 1: // Decoupled
        resSim = system((execPath + "runDecoupled.sh").c_str());
        break;

    case 2: // Coupled with object
        resSim = system((execPath + "runObject.sh").c_str());
        break;

    case 3: // Round foots
        resSim = system((execPath + "runRound.sh").c_str());
        break;

    case 4: // Virtual Controller
        resSim = system((execPath + "runVirtual.sh").c_str());
        break;
    }

    std::cout << "Simulations " << count << " has finished and returned " << resSim << std::endl;

    // Restore the error display

    dup2(duperr_fd, STDERR_FILENO);
    close(duperr_fd);
    close(devnull_fd);

    // Copying data save files

    std::string com1 = "cp " + simPath + "COMAN1/carrying" + std::to_string(threadNb) + ".txt " + expPath + "COMAN1/carrying" + std::to_string(count) + ".txt";
    std::string com2 = "cp " + simPath + "COMAN2/carrying" + std::to_string(threadNb) + ".txt " + expPath + "COMAN2/carrying" + std::to_string(count) + ".txt";

    auto res1 = system(com1.c_str());
    auto res2 = system(com2.c_str());

    // when the simulation is finished, set the thread status as free

    threadStatusFile.open(threadPath + "thread" + std::to_string(threadNb) + ".txt", std::ios_base::out | std::ios_base::trunc);
    threadStatusFile << 1;
    threadStatusFile.close();
}

// Returns the number of the first free thread that it finds

int Grid::freeThread()
{
    std::ifstream threaFile;
    int currentStatus = 0;

    for (int i = 0; i < this->totalThread; i++)
    {
        threaFile.open(threadPath + "thread" + std::to_string(i) + ".txt");
        threaFile >> currentStatus;
        threaFile.close();

        if (currentStatus == 1)
        {
            std::cout << "Found one free thread: " << i << std::endl;
            return i;
        }
    }

    return -1;
}

// Wait while a thread is freed

int Grid::waitForThread()
{
    int freeThd = freeThread();

    if (freeThd != -1)
    {
        return freeThd;
    }

    std::cout << "Waiting for a free thread..." << std::endl;

    while (freeThd == -1)
    {
        std::this_thread::sleep_for(std::chrono::seconds(2));
        freeThd = freeThread();
    }

    return freeThd;
}

// Execute simulation on a different thread and detach it to continue computation

void Grid::threadExec(std::string simPath, std::string expPath, int type, int count, int threadNb)
{
    std::thread simulation(&Grid::execSimulation, this, simPath, expPath, type, count, threadNb);
    simulation.detach();
}

// Initialize thread files or create them in case they don't exist yet

void Grid::initializeThreadFiles()
{
    std::ofstream threaFile;

    for (int i = 0; i < this->totalThread; i++)
    {
        threaFile.open(threadPath + "thread" + std::to_string(i) + ".txt", std::ios_base::out | std::ios_base::trunc);
        threaFile << 1;
        threaFile.close();
    }
}

// To be called last
// Idle while all the thread aren't finished

void Grid::waitAllFinished()
{
    std::ifstream threaFile;
    int currentStatus = 0;
    bool finished = false;

    while (!finished)
    {
        finished = true;
        for (int i = 0; i < this->totalThread; i++)
        {
            threaFile.open(threadPath + "thread" + std::to_string(i) + ".txt");
            threaFile >> currentStatus;
            threaFile.close();

            if (currentStatus == 0)
            {
                finished = false;
            }
        }

        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
}

// Save all the parameters in the given file
// It is useful when exploit simulation results on matlab, with this you know all the parameters for each simulation ran
// It is important to keep in mind the order in which the parameters are saved in order to extract them

void Grid::saveParams(std::ofstream &file, parameters &params)
{
    file << params.coupled 
         << " " << params.typeMass
         << " " << params.initMass
         << " " << params.newMass;

    for (int i = 0; i < 3; i++)
    {
        file << " " << params.force[i];
    }

    file << " " << params.forceSpan
         << " " << params.phaseLag;

    for (int i = 0; i < 2; i++)
    {
        file << " " << params.gait1[i];
    }

    for (int i = 0; i < 2; i++)
    {
        file << " " << params.gait2[i];
    }

    for (int i = 0; i < 4; i++)
    {
        file << " " << params.pid1[i];
    }

    for (int i = 0; i < 4; i++)
    {
        file << " " << params.pid2[i];
    }

    file << " " << params.damping
         << " " << params.stiffness
         << " " << params.rotDamping
         << " " << params.rotStiffness
         << " " << params.threadNb
         << " " << params.iters
         << " " << params.sor
         << " " << params.activateNav
         << " " << params.pidNav[0]
         << " " << params.pidNav[1]
         << " " << std::endl;
}