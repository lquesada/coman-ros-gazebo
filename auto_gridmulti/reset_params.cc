#include "grid.hh"

// This programm only serves the purpose to reset all the parameters to their default values in case you changed
// them manually and want to go back

int main()
{
    Grid *grid;
    grid = new Grid();

    parameters params;
    params = parameters();

    grid->setParameters(params);
}