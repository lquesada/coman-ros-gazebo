#include "grid.hh"

int main()
{
    //*** Variables

    //** Timy thingies

    time_t overallTStart, overallTEnd, overallTDiff;

    //** Files

    std::ofstream paramsSaveFile; // File where the the association between simulation number and parameter will be saved

    //** Paths

    std::string expPath; // Path where the files will be saved
    std::string simPath; // Path were the running simulations save data
    // The data will be transfered from the simPath to the expPath
    // The simPath has to be the same path as the one in carrying.cc for saving data

    //** Variable initialisations

    parameters params;

    int iteNb = 0;              // Iteration number
    int type = 0;               // Type of scenario (see grid.cc)
    double timeDiff = 0;        // Time measurment variable
    double overallTimeDiff = 0; // Time measurment variable
    int threadNb = 0;           // Thread number

    double initT = 0.45;   // Init walking period
    double initSL = 0.043; // Init step length

    double initStiff = 100000; // Init stiffness of the spring-damper joint
    double initRotStiff = 107; // Init rotationnal stiffness of the spring-damper joint

    double beg = 0;
    double end = 0;
    double step = 0;

    int totNb = 0;  // Total number of simulations to be ran
    int currNb = 0; // Current count of ran simulations

    Grid *grid; // Grid object allowing multi-threading and other tools
    grid = new Grid();

    //*** Welcome message

    overallTStart = time(0);

    simPath = "/home/biorob/SimulationData/Fresh/Data/"; //-> Path where the simulation files are originaly stored, must correspond to the one in carrying.cc

    std::cout << "##### Automatic grid simulation #####" << std::endl;
    std::cout << "Source path: " << simPath << std::endl;
    std::cout << "# Starting computation... " << std::endl;

    // //*** Template of a parallel grid run

    // Set the paths where the grid and data will be saved
    // /!\ The parent folders have to be created first !!  /!\ 
    // In this exemple, /home/biorob/SimulationData/Experiments/Synch/ has to exist

    expPath = "/home/biorob/SimulationData/Experiments/Asynch/RepeatabilitySORHighGran/"; //-> To be modified

    // Setting the type of trial (see Grid::execSimulation() )

    type = 0; //-> To be modified accordingly to your wishes

    // Setting the starting number (usually 0), set the file numbering when saving data for each trial
    // Useful when you interompted a batch of simulation and want to start again from where you stopped
    // /!\ You have to change the beginning parameter acordingly /!\ 

    iteNb = 0; //-> To be modified (optionnal)

    // Setting the grid parameters beg:step:end

    beg = 0;     //-> To be modified accordingly to your wishes
    end = 2;     //-> To be modified accordingly to your wishes
    step = 0.02; //-> To be modified accordingly to your wishes

    // Create the directories
    // Will return 0 if everything goes fine
    // Will return -1 if something went wrong or the folder already existed

    std::cerr << "Creating main folder: " << mkdir(expPath.c_str(), S_IRWXU | S_IRWXG | S_IRWXO) << std::endl;
    std::cerr << "Creating data folder: " << mkdir((expPath + "Data").c_str(), S_IRWXU | S_IRWXG | S_IRWXO) << std::endl;
    std::cerr << "Creating COMAN1 folder: " << mkdir((expPath + "Data/COMAN1").c_str(), S_IRWXU | S_IRWXG | S_IRWXO) << std::endl;
    std::cerr << "Creating COMAN2 folder: " << mkdir((expPath + "Data/COMAN2").c_str(), S_IRWXU | S_IRWXG | S_IRWXO) << std::endl;

    // Create the grid saving file

    paramsSaveFile.open(expPath + "paramsGrid.txt", std::ios_base::out | std::ios_base::trunc);

    // Computing total number of simulations

    totNb = (int)round((end - beg) / step) * 10;

    // Creating new default parameter set

    params = parameters();
    params.gait1[0] = initT * 1.06;
    params.gait1[1] = initSL * 1.06;

    // The loop running all the simulations
    // Note: You can create several intricated loops in order to have multi-dimension grid if you want
    // alway be aware of the amount of hard drive space and time you will need, you would be surprised
    // Actually on my setup with parallel simulation: 1 simulation = 6 Mo of space and 1 minute of time

    for (double param = beg; param <= end; param += step)
    {
        // Setting parametters
        // Set as many parameters as you want (see grid.hh to understand the parameters structure)
        //-> To be modified
        for (int i = 0; i < 10; i++)
        {

            params.sor = param;

            //--- The below code doesn't need editing

            // Waiting for a thread

            params.threadNb = grid->waitForThread();

            // Recording parameters

            grid->saveParams(paramsSaveFile, params);

            // Execute simulation

            grid->setParameters(params);                                                  // Setting the parameters of the simulation
            grid->threadExec(simPath, (expPath + "Data/"), type, iteNb, params.threadNb); // Executing it on a parallele thread

            // Display infos

            currNb = (int)round((param - beg) / step);
            currNb = iteNb+1;
            std::cout << "# Simulation test: " << currNb << " out of " << totNb
                      << " launched | SOR = " << param << std::endl;

            //

            iteNb++;
            std::this_thread::sleep_for(std::chrono::seconds(5));
        }
    }

    // Resetting all parameters to default and waiting all simulations to finish

    params = parameters();
    grid->setParameters(params);
    grid->waitAllFinished();
    paramsSaveFile.close();

    //*** End of the template

    //*** Computing how much time it tooks, just for kicks

    overallTEnd = time(0);
    overallTDiff = difftime(overallTEnd, overallTStart);

    std::cout << "# Done in " << int(overallTDiff / 3600 / 24) << " days, "
              << int(overallTDiff / 3600 % 24) << " hours, "
              << int(overallTDiff / 60 % 60) << " minutes, and "
              << int(overallTDiff % 60) << " seconds" << std::endl;
}