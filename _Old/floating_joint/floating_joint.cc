#include "gazebo/physics/physics.hh"
#include <functional>
#include <gazebo/gui/GuiIface.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"
#include "gazebo/rendering/rendering.hh"
#include "gazebo/sensors/sensors.hh"
#include <iostream>
#include "ignition/math4/ignition/math.hh"
#include <vector>
#include <string>
#include <math.h>
#include "StiffJoint.hh"

// The spawning plugin

struct modelParam
{
  std::string name;
  ignition::math::Pose3d pose;
  //double *Q0[N];
};

namespace gazebo
{
class floating_joint : public WorldPlugin
{
private:
  gazebo::physics::WorldPtr world;
  gazebo::event::ConnectionPtr updateConnection;
  bool firstLoop = true;
  std::vector<event::ConnectionPtr> connections;

  physics::ModelPtr sphere;
  physics::ModelPtr box;

  physics::LinkPtr boxLink;
  physics::LinkPtr sphereLink;

  double simTime = 0;
  ignition::math::Pose3d sphereInitPose;
  ignition::math::Pose3d boxInitPose;
  ignition::math::Pose3d movPose;

  ignition::math::Pose3d initialOffset;
  StiffJoint *stiffJoint;

public:
  // Function that is called during the loading of the world

  void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/)
  {
    world = _parent;

    boxInitPose.Set(0, 0, 1, 0, 0, 0);
    sphereInitPose.Set(0, 0, 1, 0, 0, 0);

    modelParam paramBox{"box", boxInitPose};
    modelParam paramsphere{"sphere", sphereInitPose};
    AddModel("/home/biorob/.gazebo/models/box/model.sdf", paramBox);
    AddModel("/home/biorob/.gazebo/models/sphere/model.sdf", paramsphere);

    // world->InsertModelFile("/home/biorob/.gazebo/models/sphere/model.sdf");

    // Event trigger to attach a new loop to the OnUpdate function

    this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&floating_joint::OnUpdate, this));
  }
  // Insert a model in the simulation depending of the parameters giving (see the modelParam structure)

  void AddModel(std::string fileName, modelParam param)
  {
    // Get the model SDF

    sdf::SDFPtr modelSDF = sdf::readFile(fileName);
    sdf::ElementPtr model = modelSDF->Root()->GetElement("model");

    // Setting the name and the position in the world

    model->GetAttribute("name")->SetFromString(param.name);
    model->GetElement("pose")->GetValue()->Set(param.pose);

    // Inserting the model into the world

    world->InsertModelString(modelSDF->ToString());
  }

  // Function that is called a each loop of the simulation

  void OnUpdate()
  {
    simTime = world->SimTime().Double();

    if (firstLoop == true) // Setting the carrying scenario at the first loop
    {
      firstLoop = false;

      // Gathering models pointers

      box = world->ModelByName("box");
      sphere = world->ModelByName("sphere");

      // Gathering links

      boxLink = box->GetLink("link");
      sphereLink = sphere->GetLink("link");

      // Setting of the initiale position
      stiffJoint = new StiffJoint(boxLink, sphereLink);
      stiffJoint->Initialize();

      std::cerr << sphereLink->WorldPose() - boxLink->WorldPose() << std::endl;
    }

    // movPose.Set(cos(simTime) - 1, sin(simTime), cos(simTime), 2 * simTime, 2 * simTime, 2 * simTime);
    // movPose.Set(0, 0, 1, 0, 0, 0);

    // customJoint(initialOffset, boxLink, sphereLink);
    stiffJoint->Apply();

    // box->SetWorldPose(movPose);
    // sphere->SetWorldPose(ignition::math::Pose3d(0, 0, 1, 0, 0, 0));

    // std::cerr << offsetOffset << std::endl;

    // Others

    std::cerr << "----------------- Time : " << world->SimTime() << std::endl;
  }

  void customJoint(ignition::math::Pose3d initialOffset_, physics::LinkPtr link1, physics::LinkPtr link2)
  {
    // Linear offset

    ignition::math::Vector3d initialPosOffset = initialOffset_.Pos();
    ignition::math::Vector3d currentPosOffset = link2->WorldPose().Pos() - link1->WorldPose().Pos();
    ignition::math::Vector3d offsetPosOffset = currentPosOffset - initialPosOffset;
    ignition::math::Vector3d forceStiff = offsetPosOffset;

    ignition::math::Vector3d forceLink1;
    ignition::math::Vector3d forceLink2;

    ignition::math::Vector3d linearVelocityLink1 = link1->WorldLinearVel();
    ignition::math::Vector3d linearVelocityLink2 = link2->WorldLinearVel();

    ignition::math::Vector3d forceVel1 = dampingLawLinear(linearVelocityLink1);
    ignition::math::Vector3d forceVel2 = dampingLawLinear(linearVelocityLink2);

    forceStiff = stiffnessLawLinear(forceStiff);

    // Rotationnal offset

    ignition::math::Quaterniond rotLink1 = link1->WorldPose().Rot();
    ignition::math::Quaterniond rotLink2 = link2->WorldPose().Rot();

    ignition::math::Quaterniond initialRotOffset = initialOffset_.Rot();
    ignition::math::Quaterniond currentRotOffset = rotLink1 * rotLink2.Inverse();
    ignition::math::Quaterniond offsetRotOffset = initialRotOffset * currentRotOffset.Inverse();

    ignition::math::Vector3d torqueLink1;
    ignition::math::Vector3d torqueLink2;

    offsetRotOffset.Normalize();

    ignition::math::Vector3d torqueStiff = offsetRotOffset.Euler();

    ignition::math::Vector3d angularVel1 = link1->WorldAngularVel();
    ignition::math::Vector3d angularVel2 = link2->WorldAngularVel();

    torqueStiff = stiffnessLawRotational(torqueStiff);
    ignition::math::Vector3d torqueVelLink1 = dampingLawRotational(angularVel1);
    ignition::math::Vector3d torqueVelLink2 = dampingLawRotational(angularVel2);

    // Frame alignments

    ignition::math::Vector3d alignedForceStiffLink1;
    ignition::math::Vector3d alignedForceStiffLink2;
    ignition::math::Vector3d alignedForceVelLink1;
    ignition::math::Vector3d alignedForceVelLink2;

    alignedForceStiffLink1 = rotLink1.RotateVectorReverse(forceStiff);
    alignedForceStiffLink2 = -rotLink2.RotateVectorReverse(forceStiff);
    alignedForceVelLink1 = rotLink1.RotateVectorReverse(forceVel1);
    alignedForceVelLink2 = rotLink2.RotateVectorReverse(forceVel2);

    forceLink1 = alignedForceStiffLink1 + alignedForceVelLink1;
    forceLink2 = alignedForceStiffLink2 + alignedForceVelLink2;

    ignition::math::Vector3d alignedTorqueStiffLink1;
    ignition::math::Vector3d alignedTorqueStiffLink2;
    ignition::math::Vector3d alignedTorqueVelLink1;
    ignition::math::Vector3d alignedTorqueVelLink2;

    // alignedTorqueStiffLink1 = rotLink1.RotateVectorReverse(torqueStiff);
    // alignedTorqueStiffLink2 = rotLink2.RotateVectorReverse(-torqueStiff);
    alignedTorqueVelLink1 = rotLink1.RotateVectorReverse(torqueVelLink1);
    alignedTorqueVelLink2 = rotLink2.RotateVectorReverse(torqueVelLink2);

    alignedTorqueStiffLink1 = torqueStiff;
    alignedTorqueStiffLink2 = -torqueStiff;
    // alignedTorqueVelLink1 = torqueVelLink1;
    // alignedTorqueVelLink2 = torqueVelLink2;

    torqueLink1 = alignedTorqueStiffLink1 + alignedTorqueVelLink1;
    torqueLink2 = alignedTorqueStiffLink2 + alignedTorqueVelLink2;

    // Force and torques applications

    link1->AddLinkForce(forceLink1);
    link2->AddLinkForce(forceLink2);

    link1->AddTorque(torqueLink1);
    link2->AddTorque(torqueLink2);

    //

    std::cerr << "Initial offset: " << initialPosOffset << std::endl;
    std::cerr << "Pos link1: " << link1->WorldPose() << std::endl;
    std::cerr << "Pos link2: " << link2->WorldPose() << std::endl;
    std::cerr << "Offset offset: " << offsetPosOffset << std::endl;
    std::cerr << "Force link 1: " << forceLink1 << std::endl;
    std::cerr << "Force link 2: " << forceLink2 << std::endl;
    std::cerr << "-----" << std::endl;

    //

    std::cerr << "Initial rot offset: " << initialRotOffset << std::endl;
    std::cerr << "Rot link1: " << rotLink1 << std::endl;
    std::cerr << "Rot link2: " << rotLink2 << std::endl;
    std::cerr << "Current offset: " << currentRotOffset << std::endl;
    std::cerr << "Offset offset: " << offsetRotOffset.Euler() << std::endl;
    std::cerr << "Torque link 1: " << torqueLink1 << std::endl;
    std::cerr << "Torque link 2: " << torqueLink2 << std::endl;
  }

  ignition::math::Vector3d stiffnessLawLinear(ignition::math::Vector3d vec)
  {
    double k = 10000;
    return k * vec;
  }

  ignition::math::Vector3d dampingLawLinear(ignition::math::Vector3d velocity)
  {
    double c = -10;
    return c * velocity;
  }

  ignition::math::Vector3d stiffnessLawRotational(ignition::math::Vector3d vec)
  {
    double k = 10000;

    for (int i = 0; i < 3; i++)
    {
      vec[i] = k * vec[i] / 1.57;
    }

    return vec;
  }

  ignition::math::Vector3d dampingLawRotational(ignition::math::Vector3d vec)
  {
    double c = -100;

    for (int i = 0; i < 3; i++)
    {
      vec[i] = c * vec[i] / 1.57;
    }

    return vec;
  }
};

// Register this plugin with the simulator

GZ_REGISTER_WORLD_PLUGIN(floating_joint)

} // namespace gazebo