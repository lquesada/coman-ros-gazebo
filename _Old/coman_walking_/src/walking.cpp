//#include "HeaderFiles.hh"
#include "walking.hpp"

using namespace gazebo;

// Load function, called at the spawn of the robot

void WalkingPlugin::Load(gazebo::physics::ModelPtr _parent, sdf::ElementPtr modelSDF)
{
    //

    std::cerr << "Loading Walking plugin" << std::endl;

    // Store the pointer to the model

    this->gazModel = _parent;

    // Get simulation time

    this->gazWorld = this->gazModel->GetWorld();

    simTime = this->gazWorld->SimTime().Double();

    // Open log file

    outputLogFile.open("/home/biorob/Scripts/walktest.txt", std::ios_base::out | std::ios_base::trunc);

    // Store the pointers of the joints

    GetJointsInOrder();

    // IMU Sensor
    double num = (gazModel->GetName() == "coman") ? 0 : 1;
    imuSensorBasePtr = sensors::SensorManager::Instance()->GetSensor("imu_sensor_" + gazModel->GetName());
    imuSensor = std::dynamic_pointer_cast<gazebo::sensors::ImuSensor>(imuSensorBasePtr);

    // Initialize joints states and torque
    control = new Control();
    Initialize();

    // Listen to the update event. This event is broadcast every
    // simulation iteration.

    this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&WalkingPlugin::OnUpdate, this)); // Time update

    //
}

// Called by the world update start event, it is the main loop

void WalkingPlugin::OnUpdate()
{
    // Get simulation time and compute timestep

    std::cerr << "Plugin " << control->whichComan_ << std::endl;

    simTime = this->gazWorld->SimTime().Double();

    if (dt != 0)
        dt = simTime - prevTime;
    else
        dt += simTime;

    prevTime = simTime;

    std::cerr << "For init pos" << std::endl;

    // For init_pos

    if (begin_time == -1)
    {
        // Initialize tauDes to 0
        initTorques();

        // set begin_time
        begin_time = simTime;

        // Set QInit to qSens for init_pos
        for (int i = 0; i < NUM; i++)
        {
            Q0[i] = qSens[i]; // TODO: Change qSens to vector
        }
    }

    simTime -= begin_time;

    // Gather IMU data

    std::cerr << "Imu data" << std::endl;

    ReadImu();

    // Gather Force/Torque data

    std::cerr << "Read FT" << std::endl;

    ReadFT();

    // Gather joints state data

    std::cerr << "Read JS" << std::endl;

    ReadJS();

    // Compute controller

    std::cerr << "Controller" << std::endl;

    if (simTime <= TIME2WALK && begin_time != -1) // Go to init_pos
    {
        std::cerr << "Init pos" << std::endl;
        init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, control.whichComan_);

        /*SaveVars( initOutput, simTime, qSens, dqSens, forceRightAnkle, forceLeftAnkle,
                        torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,
                        ImuAngRates, ImuAccelerations, tauDes, trans, euler[1], euler[0]);*/
    }
    else // Walking controller
    {
        std::cerr << "LowerBody" << std::endl;
        varsOut = control->LowerBody(
            simTime, Q0, qSens, qSensAbs, dqSens, tauSens,
            forceRightAnkle, forceLeftAnkle, torqueRightAnkle,
            torqueLeftAnkle, forceRightHand, forceLeftHand,
            trans, ImuAngRates, ImuAccelerations, h, dh,
            hD, dhD, tauDes, vals, dt, euler);

        //control.SaveVars(controlOutput); // Have an impact on the sim
    }

    // Send command to joints

    std::cerr << "Write joints" << std::endl;

    // if (gazModel->GetName() == "coman" or (simTime <= TIME2WALK && begin_time != -1))
    // {
    WriteJC();
    // }

    // Record

    std::cerr << "Output" << std::endl;

    if (simTime < 100)
    {
        outputLogFile << simTime << " "                                                 // 1
                      << euler[0] << " " << euler[1] << " " << euler[2] << " "          // 2
                      << trans[0][0] << " " << trans[0][1] << " " << trans[0][2] << " " // 5
                      << trans[1][0] << " " << trans[1][1] << " " << trans[1][2] << " "
                      << trans[2][0] << " " << trans[2][1] << " " << trans[2][2] << " "
                      << ImuAngRates[0] << " " << ImuAngRates[1] << " " << ImuAngRates[2] << " "                 // 14
                      << ImuAccelerations[0] << " " << ImuAccelerations[1] << " " << ImuAccelerations[2] << " "  // 17
                      << forceRightAnkle[0] << " " << forceRightAnkle[1] << " " << forceRightAnkle[2] << " "     // 20
                      << torqueRightAnkle[0] << " " << torqueRightAnkle[1] << " " << torqueRightAnkle[2] << " "; // 23

        for (int i = 0; i < 31; i++) // 26
        {
            outputLogFile << tauDes[i] << " ";
        }

        for (int i = 0; i < 31; i++) // 26+ N
        {
            outputLogFile << h[i] << " ";
        }

        for (int i = 0; i < 31; i++) // 26 +2N
        {
            outputLogFile << hD[i] << " ";
        }

        outputLogFile << varsOut.s_ << " ";    // 26 + 3N
        outputLogFile << varsOut.f0_ << " ";   // 27 + 3N
        outputLogFile << varsOut.side_ << " "; // 28 + 3N
        outputLogFile << varsOut.k_ << " ";    // 29 + 3N

        outputLogFile << std::endl;
    }
}

// Get the joints in the same order as in the controller
// The function model::GetJoints() doesn't return joints in the same order as in the urdf

void WalkingPlugin::Initialize()
{
    //

    std::cerr << "Initialize" << std::endl;

    // Initialisatize torques to zero

    std::cerr << "Torques" << std::endl;

    initTorques();
    WriteJC();

    // Gather joints state

    std::cerr << "Joints states" << std::endl;

    ReadJS();

    // Set QInit to qSens for init_pos

    std::cerr << "Qinit" << std::endl;

    for (int i = 0; i < N; i++)
    {
        Q0[i] = qSens[i];
    }

    // Set begin time trigger

    begin_time = -1;

    std::cerr << "Gait" << std::endl;

    control->STEP_LENGTH = 0.025;
    control->T = 0.5;

    //control->whichComan_ = (gazModel->GetName() == "coman") ? 1 : 2;
}

void WalkingPlugin::GetJointsInOrder()
{
    std::vector<std::string> jointsNames =
        {
            "WaistYaw", "WaistSag", "WaistLat", "RHipSag", "LHipSag", "RHipLat", "RHipYaw", "RKneeSag", "RAnkSag", "RAnkLat", "LHipLat", "LHipYaw", "LKneeSag", "LAnkSag", "LAnkLat", "RShSag", "RShLat", "RShYaw", "RElbj", "LShSag", "LShLat", "LShYaw", "LElbj", "RForearmPlate", "RWrj1", "RWrj2", "LForearmPlate", "LWrj1", "LWrj2", "r_handj", "l_handj"};

    for (int i = 0; i < jointsNames.size(); i++)
    {
        joints.push_back(gazModel->GetJoint(jointsNames[i]));
    }
}

void WalkingPlugin::ReadImu()
{
    ignition::math::Vector3d imuAngRate = imuSensor->AngularVelocity();
    ignition::math::Vector3d imuLinAcc = imuSensor->LinearAcceleration();
    ignition::math::Quaterniond imuOrient = imuSensor->Orientation();

    // Converting them to euler's angles

    double QW = imuOrient.W();
    double QX = imuOrient.Z();
    double QY = imuOrient.X();
    double QZ = imuOrient.Y();

    trans[0][0] = 2 * ((QX * QX) + (QW * QW)) - 1;
    trans[0][1] = 2 * ((QX * QY) - (QW * QZ));
    trans[0][2] = 2 * ((QX * QZ) + (QW * QY));
    trans[1][0] = 2 * ((QX * QY) + (QW * QZ));
    trans[1][1] = 2 * ((QW * QW) + (QY * QY)) - 1;
    trans[1][2] = 2 * ((QY * QZ) - (QW * QX));
    trans[2][0] = 2 * ((QX * QZ) - (QW * QY));
    trans[2][1] = 2 * ((QY * QZ) + (QW * QX));
    trans[2][2] = 2 * ((QW * QW) + (QZ * QZ)) - 1;

    imuData.get_Orientation(trans, cdPR, euler);

    // Angular acceleration

    ImuAngRates[0] = imuAngRate.Z();
    ImuAngRates[1] = imuAngRate.X();
    ImuAngRates[2] = imuAngRate.Y();

    imuData.get_AngRates(ImuAngRates, cdPR, ImuAngRates);

    // Linear acceleration

    ImuAccelerations[0] = imuLinAcc.Z();
    ImuAccelerations[1] = imuLinAcc.X();
    ImuAccelerations[2] = imuLinAcc.Y();

    double gravArray[3] = {0, 0, -9.81};

    imuData.get_Accelerations(gravArray, cdPR, gravArray);

    imuData.get_Accelerations(ImuAccelerations, cdPR, ImuAccelerations);

    ImuAccelerations[0] += gravArray[0];
    ImuAccelerations[1] += gravArray[1];
    ImuAccelerations[2] += gravArray[2];
}

// Function that read and store Force/Torque sensors

void WalkingPlugin::ReadFT()
{
    gazebo::physics::JointWrench RightAnkleWrench = gazModel->GetJoint("RAnkSag")->GetForceTorque(0);
    gazebo::physics::JointWrench LeftAnkleWrench = gazModel->GetJoint("LAnkSag")->GetForceTorque(0);
    gazebo::physics::JointWrench RightForearmWrench = gazModel->GetJoint("RForearmPlate")->GetForceTorque(0);
    gazebo::physics::JointWrench LeftForearmWrench = gazModel->GetJoint("LForearmPlate")->GetForceTorque(0);

    ignition::math::Vector3d vecForceRightAnkle = RightAnkleWrench.body2Force;
    ignition::math::Vector3d vecForceLeftAnkle = LeftAnkleWrench.body2Force;
    ignition::math::Vector3d vecForceRightHand = RightForearmWrench.body2Force;
    ignition::math::Vector3d vecForceLeftHand = LeftForearmWrench.body2Force;
    ignition::math::Vector3d vecTorqueRightAnkle = RightAnkleWrench.body2Torque;
    ignition::math::Vector3d vecTorqueLeftAnkle = LeftAnkleWrench.body2Torque;

    forceRightAnkle[0] = vecForceRightAnkle.X();
    forceRightAnkle[1] = vecForceRightAnkle.Y();
    forceRightAnkle[2] = vecForceRightAnkle.Z();

    forceLeftAnkle[0] = vecForceLeftAnkle.X();
    forceLeftAnkle[1] = vecForceLeftAnkle.Y();
    forceLeftAnkle[2] = vecForceLeftAnkle.Z();

    forceRightHand[0] = vecForceRightHand.X();
    forceRightHand[1] = vecForceRightHand.Y();
    forceRightHand[2] = vecForceRightHand.Z();

    forceLeftHand[0] = vecForceLeftHand.X();
    forceLeftHand[1] = vecForceLeftHand.Y();
    forceLeftHand[2] = vecForceLeftHand.Z();

    torqueRightAnkle[0] = vecTorqueRightAnkle.X();
    torqueRightAnkle[1] = vecTorqueRightAnkle.Y();
    torqueRightAnkle[2] = vecTorqueRightAnkle.Z();

    torqueLeftAnkle[0] = vecTorqueLeftAnkle.X();
    torqueLeftAnkle[1] = vecTorqueLeftAnkle.Y();
    torqueLeftAnkle[2] = vecTorqueLeftAnkle.Z();
}

// Read and store joints states

void WalkingPlugin::ReadJS()
{
    for (int i = 0; i < joints.size(); i++)
    {
        qSens[i] = joints[i]->Position();
        dqSens[i] = joints[i]->GetVelocity(0);
    }
}

// Write joints commands

void WalkingPlugin::WriteJC()
{
    for (int i = 0; i < joints.size(); i++)
    {
        joints[i]->SetForce(0, tauDes[i]);
    }
}

// Initialise torques to zero

void WalkingPlugin::initTorques()
{
    for (int i = 0; i < N; i++)
    {
        tauDes[i] = 0.0;
    }
}

// Save the data in the specified output file

void WalkingPlugin::SaveVars(
    std::ofstream &outputLogFile, double tm_, double *qSens, double *dqSens,
    double *forceRightAnkle, double *forceLeftAnkle, double *torqueRightAnkle,
    double *torqueLeftAnkle, double *forceRightHand, double *forceLeftHand,
    double imuAngRates[], double imuAccelerations[], double tauDes[],
    double trans[][3], double thp, double thr)
{
    // Save Data  tme, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,forceSensors, trans, imuAngRates, imuAccelerations

    outputLogFile << tm_;
    // start_id = 2
    for (int i = 0; i < N; i++)
    {
        outputLogFile << " " << qSens[i];
    }
    // start_id = 2 + N
    for (int i = 0; i < N; i++)
    {
        outputLogFile << " " << dqSens[i];
    }
    // 2 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << forceRightAnkle[i];
    }
    // start_id = 5 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << forceLeftAnkle[i];
    }
    // start_id = 8 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << torqueRightAnkle[i];
    }
    // start_id = 11 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << torqueLeftAnkle[i];
    }
    // start_id = 14 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << forceRightHand[i];
    }
    // start_id = 17 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << forceLeftHand[i];
    }
    // start_id = 20 + 2N
    for (int i = 0; i < N; i++)
    {
        outputLogFile << " " << tauDes[i];
    }
    // start_id = 20 + 3N
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            outputLogFile << " " << trans[i][j];
        }
    }
    // start_id = 29 + 3N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << imuAngRates[i];
    }
    // start_id = 32 + 3N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << imuAccelerations[i];
    }
    outputLogFile // start_id = 35 + 3N
        << " " << thp
        << " " << thr // 36 + 3N
        << std::endl;
}