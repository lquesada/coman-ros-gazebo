#include <math.h>
template <class S>


void LowPassFilter(std::vector<S> &vec, double freq, double timestep)
{
    double puls = 2*M_PI*freq;
    vec.back() = (1/(1+puls*timestep))*(puls*timestep*vec.back()+vec.front());
}