#pragma once

#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/sensors/sensors.hh>
#include <gazebo/sensors/ImuSensor.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>
#include <fstream>
#include <iostream>
#include <ignition/math4/ignition/math.hh>

#include "init_pos.hh"
#include "Control.hh"
#include "imu_data.hh"

#include <fstream>
#include <boost/bind.hpp>
#include <iostream>
#include <stack>
#include <ctime>
#include <numeric>

#define N 31

using namespace gazebo;

class WalkingPlugin : public gazebo::ModelPlugin
{
  public:
    void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/);
    void OnUpdate();

  private:
    bool isInit = false;

    const double TIME2WALK = 10;

    unsigned int whichComan_ = 1;

    double dt = 0;
    double _tm = 0;
    double Q0[N];
    double qknee0 = 0.2, qP0 = -0.1, qR0 = 0.055 * 1;
    const double RIGHT_ELBOW_YAW = 0, LEFT_ELBOW_YAW = 0, HIP_YAW = 0;
    double qInit[N] = {0, 0.075, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1.4, qR0, qR0 * 1, 0, qknee0, qP0 * 1.4, -qR0 * 1, 0.45, -0.2, 0.0, -1.75, 0.45, 0.2, 0.0, -1.75, RIGHT_ELBOW_YAW, 0.0, 0.0, LEFT_ELBOW_YAW, 0, 0, 0, 0};

    double vals[N], qSens[N], dqSens[N],
        tauSens[N], qSensAbs[N], tauDes[N];
    double trans[3][3], testTrans[3][3];
    double ImuAngRates[3], ImuAccelerations[3];
    double forceRightAnkle[3], torqueRightAnkle[3],
        forceLeftAnkle[3], torqueLeftAnkle[3],
        forceRightHand[3], forceLeftHand[3];
    double h[N], dh[N], hD[N], dhD[N];
    double euler[3], testOrientation[3], cdPR[3][3];

    Control *control;

    double QW, QX, QY, QZ;
    imu_data imuData;

    std::ofstream outputLogFile;
    // std::ofstream initOutput("/home/lucas/Scripts/init.txt", std::ios_base::out | std::ios_base::trunc);
    // std::ofstream controlOutput("/home/lucas/Scripts/control.txt", std::ios_base::out | std::ios_base::trunc);

    state_vars varsOut;

    double hipAmp1 = 0.35;

    double kneeAmp2 = 0.3;
    double hipLatAmp2 = 0.15;
    double shSagAmp2 = -0.4;
    double hipSagAmp2 = 0.3;

    double hipYawAmp3 = 0.5;

    // Pointer to the model
    gazebo::physics::ModelPtr gazModel;

    // Pointer to the world

    gazebo::physics::WorldPtr gazWorld;

    // Pointer vector to the joints

    std::vector<gazebo::physics::JointPtr> joints;

    // IMU Sensor

    gazebo::sensors::SensorPtr imuSensorBasePtr;
    gazebo::sensors::ImuSensorPtr imuSensor;

    // Pointer to the update event connection

    gazebo::event::ConnectionPtr updateConnection;
    gazebo::event::ConnectionPtr resetWorld;

    // Time

    double prevTime;
    double simTime;
    double begin_time = -1;

    // Private functions

    void Initialize();
    void GetJointsInOrder();
    void ReadImu();
    void ReadFT();
    void ReadJS();
    void WriteJC();
    void initTorques();
    void SaveVars(
        std::ofstream &outputLogFile, double tm_, double *qSens, double *dqSens,
        double *forceRightAnkle, double *forceLeftAnkle, double *torqueRightAnkle,
        double *torqueLeftAnkle, double *forceRightHand, double *forceLeftHand,
        double ImuAngRates[], double ImuAccelerations[], double tauDes[],
        double trans[][3], double thp, double thr);
};

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(WalkingPlugin)
