#pragma once

#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/sensors/sensors.hh>
#include <gazebo/sensors/ImuSensor.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>
#include <fstream>
#include <iostream>
#include <ignition/math4/ignition/math.hh>

#include "init_pos.hh"
#include "Control.hh"
#include "imu_data.hh"

#include <fstream>
#include <boost/bind.hpp>
#include <iostream>
#include <stack>
#include <ctime>
#include <numeric>

#define N 31

// Variable declarations

bool isInit = false;

const double TIME2WALK = 10;

unsigned int whichComan_ = 1;

static double dt = 0;
static double _tm = 0;
static double Q0[N];
static double qknee0 = 0.2, qP0 = -0.1, qR0 = 0.055 * 1;
const double RIGHT_ELBOW_YAW = 0, LEFT_ELBOW_YAW = 0, HIP_YAW = 0;
static double qInit[] = {0, 0.075, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1.4, qR0, qR0 * 1, 0, qknee0, qP0 * 1.4, -qR0 * 1, 0.45, -0.2, 0.0, -1.75, 0.45, 0.2, 0.0, -1.75, RIGHT_ELBOW_YAW, 0.0, 0.0, LEFT_ELBOW_YAW, 0, 0, 0, 0};
//static double                   qInit[] = {0,0.0,0,qP0,qP0,-qR0,0,qknee0,qP0*1,qR0,qR0*1,0,qknee0,qP0*1,-qR0*1, 0.45, -0.2 , 0.0, -1.75, 0.45, 0.2, 0.0, -1.75, RIGHT_ELBOW_YAW,0.0,0.0,LEFT_ELBOW_YAW,0,0,0,0};

double vals[N], qSens[N], dqSens[N],
    tauSens[N], qSensAbs[N], tauDes[N];
double Trans[3][3], testTrans[3][3];
double ImuAngRates[3], ImuAccelerations[3];
double forceRightAnkle[3], torqueRightAnkle[3],
    forceLeftAnkle[3], torqueLeftAnkle[3],
    forceRightHand[3], forceLeftHand[3];
double h[N], dh[N], hD[N], dhD[N];
double euler[3], testOrientation[3], cdPR[3][3];

Control control;
double prevTime;

double QW, QX, QY, QZ;
imu_data imuData;

std::ofstream outputLogFile;
std::ofstream initOutput("/home/lucas/Scripts/init.txt", std::ios_base::out | std::ios_base::trunc);
std::ofstream controlOutput("/home/lucas/Scripts/control.txt", std::ios_base::out | std::ios_base::trunc);

state_vars varsOut;

static double hipAmp1 = 0.35;

static double kneeAmp2 = 0.3;
static double hipLatAmp2 = 0.15;
static double shSagAmp2 = -0.4;
static double hipSagAmp2 = 0.3;

static double hipYawAmp3 = 0.5;

static double qInit1[] = {
    0, 0, 0, hipAmp1, hipAmp1, 0, 0,  // 0 - 6
    0, 0, 0, 0, 0,                    // 7 - 11
    0, 0, 0,                          // 12 - 14
    /* 15 onwards */ 0, 0, 0.0, 0, 0, // 15 - 19
    0, 0.0, 0, 0,                     // 20 - 23
    0.0, 0.0, 0, 0, 0, 0, 0           // 24 - 30
};

// Function that read, convert, and store IMU data

void ReadImu(gazebo::sensors::ImuSensorPtr imuSensor_)
{
    ignition::math::Vector3d imuAngRate = imuSensor_->AngularVelocity();
    ignition::math::Vector3d imuLinAcc = imuSensor_->LinearAcceleration();
    ignition::math::Quaterniond imuOrient = imuSensor_->Orientation();

    // Converting them to euler's angles

    QW = imuOrient.W();
    QX = imuOrient.Z();
    QY = imuOrient.X();
    QZ = imuOrient.Y();

    Trans[0][0] = 2 * ((QX * QX) + (QW * QW)) - 1;
    Trans[0][1] = 2 * ((QX * QY) - (QW * QZ));
    Trans[0][2] = 2 * ((QX * QZ) + (QW * QY));
    Trans[1][0] = 2 * ((QX * QY) + (QW * QZ));
    Trans[1][1] = 2 * ((QW * QW) + (QY * QY)) - 1;
    Trans[1][2] = 2 * ((QY * QZ) - (QW * QX));
    Trans[2][0] = 2 * ((QX * QZ) - (QW * QY));
    Trans[2][1] = 2 * ((QY * QZ) + (QW * QX));
    Trans[2][2] = 2 * ((QW * QW) + (QZ * QZ)) - 1;

    imuData.get_Orientation(Trans, cdPR, euler);

    // Angular acceleration

    ImuAngRates[0] = imuAngRate.Z();
    ImuAngRates[1] = imuAngRate.X();
    ImuAngRates[2] = imuAngRate.Y();

    imuData.get_AngRates(ImuAngRates, cdPR, ImuAngRates);

    // Linear acceleration

    ImuAccelerations[0] = imuLinAcc.Z();
    ImuAccelerations[1] = imuLinAcc.X();
    ImuAccelerations[2] = imuLinAcc.Y();

    double gravArray[3] = {0, 0, -9.81};
    imuData.get_Accelerations(gravArray, cdPR, gravArray);

    imuData.get_Accelerations(ImuAccelerations, cdPR, ImuAccelerations);

    ImuAccelerations[0] += gravArray[0];
    ImuAccelerations[1] += gravArray[1];
    ImuAccelerations[2] += gravArray[2];
}

// Function that read and store Force/Torque sensors

void ReadFT(const gazebo::physics::ModelPtr model_)
{
    gazebo::physics::JointWrench RightAnkleWrench = model_->GetJoint("RAnkSag")->GetForceTorque(0);
    gazebo::physics::JointWrench LeftAnkleWrench = model_->GetJoint("LAnkSag")->GetForceTorque(0);
    gazebo::physics::JointWrench RightForearmWrench = model_->GetJoint("RForearmPlate")->GetForceTorque(0);
    gazebo::physics::JointWrench LeftForearmWrench = model_->GetJoint("LForearmPlate")->GetForceTorque(0);

    ignition::math::Vector3d vecForceRightAnkle = RightAnkleWrench.body2Force;
    ignition::math::Vector3d vecForceLeftAnkle = LeftAnkleWrench.body2Force;
    ignition::math::Vector3d vecForceRightHand = RightForearmWrench.body2Force;
    ignition::math::Vector3d vecForceLeftHand = LeftForearmWrench.body2Force;
    ignition::math::Vector3d vecTorqueRightAnkle = RightAnkleWrench.body2Torque;
    ignition::math::Vector3d vecTorqueLeftAnkle = LeftAnkleWrench.body2Torque;

    forceRightAnkle[0] = vecForceRightAnkle.X();
    forceRightAnkle[1] = vecForceRightAnkle.Y();
    forceRightAnkle[2] = vecForceRightAnkle.Z();

    forceLeftAnkle[0] = vecForceLeftAnkle.X();
    forceLeftAnkle[1] = vecForceLeftAnkle.Y();
    forceLeftAnkle[2] = vecForceLeftAnkle.Z();

    forceRightHand[0] = vecForceRightHand.X();
    forceRightHand[1] = vecForceRightHand.Y();
    forceRightHand[2] = vecForceRightHand.Z();

    forceLeftHand[0] = vecForceLeftHand.X();
    forceLeftHand[1] = vecForceLeftHand.Y();
    forceLeftHand[2] = vecForceLeftHand.Z();

    torqueRightAnkle[0] = vecTorqueRightAnkle.X();
    torqueRightAnkle[1] = vecTorqueRightAnkle.Y();
    torqueRightAnkle[2] = vecTorqueRightAnkle.Z();

    torqueLeftAnkle[0] = vecTorqueLeftAnkle.X();
    torqueLeftAnkle[1] = vecTorqueLeftAnkle.Y();
    torqueLeftAnkle[2] = vecTorqueLeftAnkle.Z();
}

// Read and store joints states

void ReadJS(std::vector<gazebo::physics::JointPtr> joints)
{
    for (int i = 0; i < joints.size(); i++)
    {
        qSens[i] = joints[i]->Position();
        dqSens[i] = joints[i]->GetVelocity(0);
    }
}

// Write joints commands

void WriteJC(std::vector<gazebo::physics::JointPtr> joints)
{
    for (int i = 0; i < joints.size(); i++)
    {
        joints[i]->SetForce(0, tauDes[i]);
    }
}

// Initialise torques to zero

void init(double torques[])
{
    for (int i = 0; i < N; i++)
    {
        torques[i] = 0.0;
    }
}

// Save the data in the specified output file

void SaveVars(
    std::ofstream &outputLogFile, double tm_, double *qSens, double *dqSens,
    double *forceRightAnkle, double *forceLeftAnkle, double *torqueRightAnkle,
    double *torqueLeftAnkle, double *forceRightHand, double *forceLeftHand,
    double imuAngRates[], double imuAccelerations[], double tauDes[],
    double trans[][3], double thp, double thr)
{
    // Save Data  tme, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,forceSensors, trans, imuAngRates, imuAccelerations

    outputLogFile << tm_;
    // start_id = 2
    for (int i = 0; i < N; i++)
    {
        outputLogFile << " " << qSens[i];
    }
    // start_id = 2 + N
    for (int i = 0; i < N; i++)
    {
        outputLogFile << " " << dqSens[i];
    }
    // 2 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << forceRightAnkle[i];
    }
    // start_id = 5 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << forceLeftAnkle[i];
    }
    // start_id = 8 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << torqueRightAnkle[i];
    }
    // start_id = 11 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << torqueLeftAnkle[i];
    }
    // start_id = 14 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << forceRightHand[i];
    }
    // start_id = 17 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << forceLeftHand[i];
    }
    // start_id = 20 + 2N
    for (int i = 0; i < N; i++)
    {
        outputLogFile << " " << tauDes[i];
    }
    // start_id = 20 + 3N
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            outputLogFile << " " << trans[i][j];
        }
    }
    // start_id = 29 + 3N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << imuAngRates[i];
    }
    // start_id = 32 + 3N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << imuAccelerations[i];
    }
    outputLogFile // start_id = 35 + 3N
        << " " << thp
        << " " << thr // 36 + 3N
        << std::endl;
}