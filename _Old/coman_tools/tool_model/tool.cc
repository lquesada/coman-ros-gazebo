#include "gazebo/physics/physics.hh"
#include <functional>
#include <gazebo/gui/GuiIface.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"
#include "gazebo/rendering/rendering.hh"
#include <iostream>
#include "ignition/math4/ignition/math.hh"
#include <vector>
#include <string>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>

namespace gazebo
{
class ToolModel : public ModelPlugin
{
  gazebo::physics::ModelPtr model;
  std::vector<gazebo::physics::JointPtr> joints;
  gazebo::event::ConnectionPtr updateConnection;
  double jointPos[31];

  transport::NodePtr node;
  transport::SubscriberPtr jointsCmd;

  void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
  {
    model = _parent;

    GetJointsInOrder();

    this->node = transport::NodePtr(new transport::Node());
    this->node->Init();

    jointsCmd = this->node->Subscribe("~/pos_from_gui", &ToolModel::UpdateJoints, this);

    this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&ToolModel::OnUpdate, this));

    for (int i = 0; i < 31; i++)
    {
      jointPos[i] = 0;
    }
  }

  void OnUpdate()
  {
    WriteJC();
    model->SetWorldPose(ignition::math::Pose3d(0, 0, 0.517, 0, 0, 0));
  }

private:
  void UpdateJoints(ConstVector2dPtr &msg)
  {
    std::cerr << "Received message" << std::endl;
    jointPos[(int)msg->x()] = msg->y();
  }

  void GetJointsInOrder()
  {
    std::vector<std::string> jointsNames =
        {
            "WaistYaw", "WaistSag", "WaistLat", "RHipSag", "LHipSag", "RHipLat", "RHipYaw", "RKneeSag", "RAnkSag", "RAnkLat", "LHipLat", "LHipYaw", "LKneeSag", "LAnkSag", "LAnkLat", "RShSag", "RShLat", "RShYaw", "RElbj", "LShSag", "LShLat", "LShYaw", "LElbj", "RForearmPlate", "RWrj1", "RWrj2", "LForearmPlate", "LWrj1", "LWrj2", "r_handj", "l_handj"};

    for (int i = 0; i < jointsNames.size(); i++)
    {
      joints.push_back(model->GetJoint(jointsNames[i]));
    }
  }

  void WriteJC()
  {
    for (int i = 0; i < 31; i++)
    {
      joints[i]->SetPosition(0, jointPos[i]);
    }
  }
};

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(ToolModel)
} // namespace gazebo