#include <sstream>
#include <gazebo/msgs/msgs.hh>
#include <iostream>
#include "gazebo/physics/physics.hh"
#include "tool.hh"

using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_GUI_PLUGIN(coman_tool)

coman_tool::coman_tool() : GUIPlugin()
{
    // Set the frame background and foreground colors
    this->setStyleSheet(
        "QFrame { background-color : rgba(100, 100, 100, 255); color : white; }");

    // Create a push button, and connect it to the OnButton function
    QPushButton *button = new QPushButton(tr("Spawn Sphere"));
    connect(button, &QAbstractButton::clicked, this, [this] { OnButton(42); });
    frameLayout->addWidget(button);
    LoadGui();

    // Add the button to the frame's layout

    // Add frameLayout to the frame
    mainFrame->setLayout(frameLayout);

    // Add the frame to the main layout
    mainLayout->addWidget(mainFrame);

    // Remove margins to reduce space
    frameLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setContentsMargins(0, 0, 0, 0);

    this->setLayout(mainLayout);

    // Position and resize this widget
    this->move(10, 10);
    this->resize(300, 1000);

    // Create a node for transportation
    this->node = transport::NodePtr(new transport::Node());
    this->node->Init();
    this->factoryPub = this->node->Advertise<msgs::Factory>("~/factory");

    this->jointPub = this->node->Advertise<msgs::Vector2d>("~/pos_from_gui");
}

void coman_tool::Load(sdf::ElementPtr sdf)
{
    LoadModel();
}

void coman_tool::LoadGui()
{
    std::vector<std::string> jointsNames =
        {
            "WaistYaw", "WaistSag", "WaistLat", "RHipSag", "LHipSag", "RHipLat", "RHipYaw", "RKneeSag", "RAnkSag", "RAnkLat", "LHipLat", "LHipYaw", "LKneeSag", "LAnkSag", "LAnkLat", "RShSag", "RShLat", "RShYaw", "RElbj", "LShSag", "LShLat", "LShYaw", "LElbj", "RForearmPlate", "RWrj1", "RWrj2", "LForearmPlate", "LWrj1", "LWrj2", "r_handj", "l_handj"};

    for (int i = 0; i < 31; i++)
    {
        QGroupBox *groupBox = new QGroupBox;
        QLayout *layout = new QHBoxLayout();

        QLabel *label = new QLabel();
        label->setText(QString::fromStdString(jointsNames[i]));

        QSlider *slider = new QSlider(Qt::Horizontal);
        slider->setMinimum(0);
        slider->setMaximum(360);
        slider->setGeometry(10, 30, 80, 20);

        // QLabel *slidValue = new QLabel();
        // slidValue->setText("0");

        layout->addWidget(label);
        // layout->addWidget(slider);
        // layout->addWidget(slidValue);

        groupBox->setContentsMargins(0, 0, 0, 0);
        groupBox->setLayout(layout);
        frameLayout->addWidget(groupBox);
    }
}

void coman_tool::LoadModel()
{
    // Load an edit the model

    std::string pathComan = "/home/biorob/.gazebo/models/coman/model.sdf";
    sdf::SDFPtr modelSDF = sdf::readFile(pathComan);
    sdf::ElementPtr modelElem = modelSDF->Root()->GetElement("model");

    modelElem->AddElement("plugin");
    modelElem->GetElement("plugin")->GetAttribute("name")->SetFromString("libcoman_tool_model");
    modelElem->GetElement("plugin")->GetAttribute("filename")->SetFromString("libcoman_tool_model.so");

    // Send the model to the gazebo server

    msgs::Model model;
    model.set_name("coman");

    msgs::Set(model.mutable_pose(), ignition::math::Pose3d(0, 0, 0.517, 0, 0, 0));

    msgs::Factory msg;
    msg.set_sdf(modelSDF->ToString());
    this->factoryPub->Publish(msg);
}

void coman_tool::OnButton(int test)
{
    msgs::Vector2d msg;
    std::cerr << test << std::endl;

    double qknee0 = 0.2, qP0 = -0.1, qR0 = 0.055 * 1;
    const double RIGHT_ELBOW_YAW = 0, LEFT_ELBOW_YAW = 0, HIP_YAW = 0;
    // double qInit[31] = {0, 0.075, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1.4, qR0, qR0 * 1, 0, qknee0, qP0 * 1.4, -qR0 * 1, 0.45, -0.2, 0.0, -1.75, 0.45, 0.2, 0.0, -1.75, RIGHT_ELBOW_YAW, 0.0, 0.0, LEFT_ELBOW_YAW, 0, 0, 0, 0};
    double qInit[31] = {};
    qInit[27] = 1.57;
    qInit[24] = 1.57;
    for (int i = 0; i < 31; i++)
    {
        msg.set_x(i);
        msg.set_y(qInit[i]);
        jointPub->Publish(msg);
    }
}