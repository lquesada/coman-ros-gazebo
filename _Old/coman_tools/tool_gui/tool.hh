#include <gazebo/common/Plugin.hh>
#include <gazebo/gui/GuiPlugin.hh>
#ifndef Q_MOC_RUN // See: https://bugreports.qt-project.org/browse/QTBUG-22829
#include <gazebo/transport/transport.hh>
#include <gazebo/gui/gui.hh>
#include <vector>
#endif

namespace gazebo
{
class GAZEBO_VISIBLE coman_tool : public GUIPlugin
{
  Q_OBJECT

  // Create the main layout
  QHBoxLayout *mainLayout = new QHBoxLayout;

  // Create the frame to hold all the widgets
  QFrame *mainFrame = new QFrame();

  // Create the layout that sits inside the frame
  QVBoxLayout *frameLayout = new QVBoxLayout();

  /// \brief Constructor
  /// \param[in] _parent Parent widget
public:
  coman_tool();
  void Load(sdf::ElementPtr sdf);
  void LoadGui();
  void LoadModel();

  /// \brief Callback trigged when the button is pressed.
protected slots:
  void OnButton(int test);

  /// \brief Node used to establish communication with gzserver.
private:
  transport::NodePtr node;

  /// \brief Publisher of factory messages.
private:
  transport::PublisherPtr factoryPub;
  transport::PublisherPtr jointPub;
};
} // namespace gazebo