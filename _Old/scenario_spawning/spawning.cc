#include "gazebo/physics/physics.hh"
#include <functional>
#include <gazebo/gui/GuiIface.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"
#include "gazebo/rendering/rendering.hh"
#include "gazebo/sensors/sensors.hh"
#include <iostream>
#include "ignition/math4/ignition/math.hh"
#include <vector>
#include <string>
#include "StiffJoint.hh"
#include <chrono>
#include <thread>

const int N = 31;
const bool walkingExp = false; // We want to spawn walking experiment of carrying?

// Structure to store model parameters for the spawn

struct modelParam
{
  std::string name;
  ignition::math::Pose3d pose;
  std::string plugName;
  //double *Q0[N];
};

// The spawning plugin

namespace gazebo
{
class Spawning : public WorldPlugin
{
private:
  gazebo::physics::WorldPtr world;
  gazebo::event::ConnectionPtr updateConnection;
  bool firstLoop = true;
  bool customJoint = true;
  std::vector<event::ConnectionPtr> connections;

  physics::ModelPtr gazCom;
  physics::ModelPtr gazCom1;
  physics::ModelPtr gazTable;

  StiffJoint *backRHJ;
  StiffJoint *backLHJ;
  StiffJoint *frontRHJ;
  StiffJoint *frontLHJ;

public:
  std::string pathComan = "/home/biorob/.gazebo/models/coman/model.sdf"; // Path to the coman model.sdf
  std::string pathTable = "/home/biorob/.gazebo/models/table/model.sdf"; // Path to the table model.sdf

  std::string plugWalk = "walking_plugin";   // Name of the walking plugin
  std::string plugCarry = "carrying_plugin"; // name of the carrying plugin

  // Function that is called during the loading of the world

  void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/)
  {
    world = _parent;

    if (walkingExp) // Only walking experiment
    {

      modelParam paramComan{"coman", ignition::math::Pose3d(0, -1, 0.517, 0, 0, 0), plugCarry};
      // modelParam paramComan1{"coman1", ignition::math::Pose3d(0, 1, 0.517, 0, 0, 0), plugWalk};
      AddModel(pathComan, paramComan);
      // AddModel(pathComan, paramComan);
    }
    else // Carrying experiment
    {
      modelParam paramComan{"coman", ignition::math::Pose3d(0, 0, 0.52, 0, 0, 0), plugCarry};
      modelParam paramComan1{"coman1", ignition::math::Pose3d(1.105880, 0, 0.52, 0, 0, 0), plugCarry};
      modelParam paramTable{"table", ignition::math::Pose3d(0.579591, 0, 0.403337, 0, 0, 0), ""};

      AddModel(pathComan, paramComan);
      AddModel(pathComan, paramComan1);
      AddModel(pathTable, paramTable);
    }

    // Event trigger to attach a new loop to the OnUpdate function

    this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&Spawning::OnUpdate, this));
  }

  // Insert a model in the simulation depending of the parameters giving (see the modelParam structure)

  void AddModel(std::string fileName, modelParam param)
  {
    // Get the model SDF

    sdf::SDFPtr modelSDF = sdf::readFile(fileName);
    sdf::ElementPtr model = modelSDF->Root()->GetElement("model");

    // Setting the name and the position in the world

    model->GetAttribute("name")->SetFromString(param.name);
    model->GetElement("pose")->GetValue()->Set(param.pose);

    // Other settings

    if (param.plugName != "") // If we want to associate a plugin to this model
    {
      double num = (param.name == "coman") ? 0 : 1;

      // Setting the pluging attached to the model

      model->AddElement("plugin");
      model->GetElement("plugin")->GetAttribute("name")->SetFromString(param.plugName + "_" + param.name);
      model->GetElement("plugin")->GetAttribute("filename")->SetFromString("lib" + param.plugName + ".so");

      // Searching for sensors and giving a unique name to the sensors

      sdf::ElementPtr jointElement = model->GetElement("link");

      while (jointElement) // Searching in joints
      {
        const std::string jointName = jointElement->GetAttribute("name")->GetAsString();

        sdf::ElementPtr sensorElement = jointElement->GetElementImpl("sensor");

        if (sensorElement)
        {
          const std::string sensorName = sensorElement->GetAttribute("name")->GetAsString();
          sensorElement->GetAttribute("name")->SetFromString(sensorName + "_" + param.name);

          std::cerr << "Changed the name of sensor \"" << sensorName << "\" into \"" << sensorName << "_" << param.name << " in " << jointName << " joint" << std::endl;
        }

        jointElement = jointElement->GetNextElement("joint");
      }

      model->GetElement("link")->GetElement("sensor")->GetAttribute("name")->SetFromString("imu_sensor_" + param.name);

      // Verification of the used plugin

      std::cerr << model->GetElement("plugin")->GetAttribute("filename")->GetAsString() << std::endl;
    }

    // Inserting the model into the world

    world->InsertModelString(modelSDF->ToString());
  }

  // Function that is called a each loop of the simulation

  void OnUpdate()
  {
    if (firstLoop == true && !walkingExp) // Setting the carrying scenario at the first loop
    {
      firstLoop = false;

      std::cerr << "First loop" << std::endl;

      // Gathering models pointers

      gazCom = world->ModelByName("coman");
      gazCom1 = world->ModelByName("coman1");
      gazTable = world->ModelByName("table");
      physics::PhysicsEnginePtr physicsEng;
      physicsEng = world->Physics();

      // Setting of the initiale position (added for the carrying experiment)

      std::vector<gazebo::physics::JointPtr> joints1;
      std::vector<gazebo::physics::JointPtr> joints2;
      double qknee0 = 0.2, qP0 = -0.1, qR0 = 0.055 * 1;
      const double RIGHT_ELBOW_YAW = 0, LEFT_ELBOW_YAW = 0, HIP_YAW = 0;

      double bWaistYaw = 0;
      double bWaistSag = 0;
      double bWaistLat = 0;
      double bhipSag = qP0;
      double bhipLat = -qR0;
      double bhipYaw = 0;
      double bkneeSag = qknee0;
      double bankSag = qP0 * 1;
      double bankLat = qR0;
      double bshSag = 0.01;
      double bshLat = -0.23;
      double bshYaw = 0.015;
      double belbj = -0.3;
      double bforearmPlate = 0.06;
      double bWrj1 = -0.001;
      double bWrj2 = -0.06481;

      double fWaistYaw = 0;
      double fWaistSag = 0.175;
      double fWaistLat = 0;
      double fhipSag = qP0;
      double fhipLat = -qR0;
      double fhipYaw = 0;
      double fkneeSag = qknee0;
      double fankSag = qP0 * 1;
      double fankLat = qR0;
      double fshSag = 0.2;
      double fshLat = -0.2;
      double fshYaw = -0.06398;
      double felbj = -0.46973;
      double fforearmPlate = 0.1725;
      double fWrj1 = 0.2;
      double fWrj2 = 0.05;

      double qInitCOMAN1[N] = {bWaistYaw, bWaistSag, bWaistLat, bhipSag, bhipSag, bhipLat, bhipYaw, bkneeSag, bankSag, bankLat, -bhipLat, bhipYaw, bkneeSag, bankSag, -bankLat, bshSag, bshLat, bshYaw, belbj, bshSag, -bshLat, -bshYaw, belbj, bforearmPlate, bWrj1, bWrj2, -bforearmPlate, bWrj1, -bWrj2, 0, 0};
      double qInitCOMAN2[N] = {fWaistYaw, fWaistSag, fWaistLat, fhipSag, fhipSag, fhipLat, fhipYaw, fkneeSag, fankSag, fankLat, -fhipLat, fhipYaw, fkneeSag, fankSag, -fankLat, fshSag, fshLat, fshYaw, felbj, fshSag, -fshLat, -fshYaw, felbj, fforearmPlate, fWrj1, fWrj2, -fforearmPlate, fWrj1, -fWrj2, 0, 0};
      // double qInitCOMAN2[N] = {bWaistYaw, bWaistSag, bWaistLat, bhipSag, bhipSag, bhipLat, bhipYaw, bkneeSag, bankSag, bankLat, -bhipLat, bhipYaw, bkneeSag, bankSag, -bankLat, bshSag, bshLat, bshYaw, belbj, bshSag, -bshLat, -bshYaw, belbj, bforearmPlate, bWrj1, bWrj2, -bforearmPlate, bWrj1, -bWrj2, 0, 0};

      // double qInitCOMAN1[N] = {0, 0.075, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1.4, qR0, qR0 * 1, 0, qknee0, qP0 * 1.4, -qR0 * 1, 0.45, -0.2, 0.0, -1.75, 0.45, 0.2, 0.0, -1.75, RIGHT_ELBOW_YAW, 0.0, 0.0, LEFT_ELBOW_YAW, 0, 0, 0, 0};

      // double qInitCOMAN1[N] = {0, 0.0, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1, qR0, qR0 * 1, 0, qknee0, qP0 * 1, -qR0 * 1, 0.01, -0.23, 0.015, -0.3, 0.01, 0.17, -0.015, -0.3, 0.06, -0.001, 0.06481, 0.06, -0.001, -0.06481, 0, 0};
      // double qInitCOMAN2[N] = {0, 0.175, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1, qR0, qR0 * 1, 0, qknee0, qP0 * 1, -qR0 * 1, 0.2, -0.16, -0.06398, -0.46973, 0.2, 0.2, -0.06398, -0.46973, -0.01725, 0.2, 0.05, 0.01725, 0.2, -0.05, 0, 0};
      // Oscillation test
      // double qInitCOMAN1[N] = {0, 0.0, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1, qR0, qR0 * 1, 0, qknee0, qP0 * 1, -qR0 * 1, 0.01, -0.23, 0.015, -0.3, 0.01, 0.17, -0.015, -0.3, 0.06, -0.001, 0.06481, 0.06, -0.001, -0.06481, 0, 0};
      // double qInitCOMAN2[N] = {0, 0.2, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1, qR0, qR0 * 1, 0, qknee0, qP0 * 1, -qR0 * 1, 0.2, -0.16, -0.06398, -0.46973, 0.2, 0.2, -0.06398, -0.46973, -0.01725, 0.2, 0.05, 0.01725, 0.2, -0.05, 0, 0};

      std::vector<std::string> jointsNames =
          {
              "WaistYaw", "WaistSag", "WaistLat", "RHipSag", "LHipSag", "RHipLat", "RHipYaw", "RKneeSag", "RAnkSag", "RAnkLat", "LHipLat", "LHipYaw", "LKneeSag", "LAnkSag", "LAnkLat", "RShSag", "RShLat", "RShYaw", "RElbj", "LShSag", "LShLat", "LShYaw", "LElbj", "RForearmPlate", "RWrj1", "RWrj2", "LForearmPlate", "LWrj1", "LWrj2", "r_handj", "l_handj"};

      for (int i = 0; i < jointsNames.size(); i++) // Gathering joints ptrs in the right order
      {
        joints1.push_back(gazCom->GetJoint(jointsNames[i]));
        joints2.push_back(gazCom1->GetJoint(jointsNames[i]));
      }

      for (int i = 0; i < joints1.size(); i++) // Setting the  joints positions
      {
        joints1[i]->SetPosition(0, qInitCOMAN1[i]);
        joints2[i]->SetPosition(0, qInitCOMAN2[i]);
      }

      // Attaching links together with a joint

      std::string typeOfJoint = "fixed"; // Type of joint to use (revolute, ball, fixed)

      if (!customJoint)
      {
        physics::JointPtr jback_RH = physicsEng->CreateJoint(typeOfJoint, gazCom);
        physics::LinkPtr clback_RH = gazTable->GetChildLink("back_RH");
        physics::LinkPtr plback_RH = gazCom->GetChildLink("RSoftHand");
        jback_RH->Attach(plback_RH, clback_RH);
        jback_RH->Load(plback_RH, clback_RH, ignition::math::Pose3d());
        jback_RH->SetModel(gazTable);

        physics::JointPtr jback_LH = physicsEng->CreateJoint(typeOfJoint, gazCom);
        physics::LinkPtr clback_LH = gazTable->GetChildLink("back_LH");
        physics::LinkPtr plback_LH = gazCom->GetChildLink("LSoftHand");
        jback_LH->Attach(plback_LH, clback_LH);
        jback_LH->Load(plback_LH, clback_LH, ignition::math::Pose3d());
        jback_LH->SetModel(gazTable);

        physics::JointPtr jfront_RH = physicsEng->CreateJoint(typeOfJoint, gazCom1);
        physics::LinkPtr clfront_RH = gazTable->GetChildLink("front_RH");
        physics::LinkPtr plfront_RH = gazCom1->GetChildLink("RSoftHand");
        jfront_RH->Attach(plfront_RH, clfront_RH);
        jfront_RH->Load(plfront_RH, clfront_RH, ignition::math::Pose3d());
        jfront_RH->SetModel(gazTable);

        physics::JointPtr jfront_LH = physicsEng->CreateJoint(typeOfJoint, gazCom1);
        physics::LinkPtr clfront_LH = gazTable->GetChildLink("front_LH");
        physics::LinkPtr plfront_LH = gazCom1->GetChildLink("LSoftHand");
        jfront_LH->Attach(plfront_LH, clfront_LH);
        jfront_LH->Load(plfront_LH, clfront_LH, ignition::math::Pose3d());
        jfront_LH->SetModel(gazTable);
      }
      else
      {
        physics::LinkPtr clback_RH = gazTable->GetChildLink("back_RH");
        physics::LinkPtr plback_RH = gazCom->GetChildLink("RSoftHand");

        physics::LinkPtr clback_LH = gazTable->GetChildLink("back_LH");
        physics::LinkPtr plback_LH = gazCom->GetChildLink("LSoftHand");

        physics::LinkPtr clfront_RH = gazTable->GetChildLink("front_RH");
        physics::LinkPtr plfront_RH = gazCom1->GetChildLink("RSoftHand");

        physics::LinkPtr clfront_LH = gazTable->GetChildLink("front_LH");
        physics::LinkPtr plfront_LH = gazCom1->GetChildLink("LSoftHand");

        backRHJ = new StiffJoint(clback_RH, plback_RH);
        backLHJ = new StiffJoint(clback_LH, plback_LH);
        frontRHJ = new StiffJoint(clfront_RH, plfront_RH);
        frontLHJ = new StiffJoint(clfront_LH, plfront_LH);

        std::cerr << plback_RH->WorldPose() - plback_RH->WorldPose() << std::endl;
        std::cerr << plback_LH->WorldPose() - plback_LH->WorldPose() << std::endl;
        std::cerr << plfront_RH->WorldPose() - plfront_RH->WorldPose() << std::endl;
        std::cerr << plfront_LH->WorldPose() - plfront_LH->WorldPose() << std::endl;
      }
    }
    else if (firstLoop == true) // Setting the carrying scenario at the first loop
    {
      firstLoop = false;

      std::cerr << "First loop" << std::endl;

      // Gathering models pointers

      gazCom = world->ModelByName("coman");
      physics::PhysicsEnginePtr physicsEng;
      physicsEng = world->Physics();

      // Setting of the initiale position (added for the carrying experiment)

      std::vector<gazebo::physics::JointPtr> joints1;
      double qknee0 = 0.2, qP0 = -0.1, qR0 = 0.055 * 1;
      const double RIGHT_ELBOW_YAW = 0, LEFT_ELBOW_YAW = 0, HIP_YAW = 0;

      double bWaistYaw = 0;
      double bWaistSag = 0;
      double bWaistLat = 0;
      double bhipSag = qP0;
      double bhipLat = -qR0;
      double bhipYaw = 0;
      double bkneeSag = qknee0;
      double bankSag = qP0 * 1;
      double bankLat = qR0;
      double bshSag = 0.01;
      double bshLat = -0.23;
      double bshYaw = 0.015;
      double belbj = -0.3;
      double bforearmPlate = 0.06;
      double bWrj1 = -0.001;
      double bWrj2 = -0.06481;

      double fWaistYaw = 0;
      double fWaistSag = 0.175;
      double fWaistLat = 0;
      double fhipSag = qP0;
      double fhipLat = -qR0;
      double fhipYaw = 0;
      double fkneeSag = qknee0;
      double fankSag = qP0 * 1;
      double fankLat = qR0;
      double fshSag = 0.2;
      double fshLat = -0.2;
      double fshYaw = -0.06398;
      double felbj = -0.46973;
      double fforearmPlate = 0.1725;
      double fWrj1 = 0.2;
      double fWrj2 = 0.05;

      double qInitCOMAN1[N] = {bWaistYaw, bWaistSag, bWaistLat, bhipSag, bhipSag, bhipLat, bhipYaw, bkneeSag, bankSag, bankLat, -bhipLat, bhipYaw, bkneeSag, bankSag, -bankLat, bshSag, bshLat, bshYaw, belbj, bshSag, -bshLat, -bshYaw, belbj, bforearmPlate, bWrj1, bWrj2, -bforearmPlate, bWrj1, -bWrj2, 0, 0};

      std::vector<std::string> jointsNames =
          {
              "WaistYaw", "WaistSag", "WaistLat", "RHipSag", "LHipSag", "RHipLat", "RHipYaw", "RKneeSag", "RAnkSag", "RAnkLat", "LHipLat", "LHipYaw", "LKneeSag", "LAnkSag", "LAnkLat", "RShSag", "RShLat", "RShYaw", "RElbj", "LShSag", "LShLat", "LShYaw", "LElbj", "RForearmPlate", "RWrj1", "RWrj2", "LForearmPlate", "LWrj1", "LWrj2", "r_handj", "l_handj"};

      for (int i = 0; i < jointsNames.size(); i++) // Gathering joints ptrs in the right order
      {
        joints1.push_back(gazCom->GetJoint(jointsNames[i]));
      }

      for (int i = 0; i < joints1.size(); i++) // Setting the  joints positions
      {
        joints1[i]->SetPosition(0, qInitCOMAN1[i]);
      }
    }

    if (customJoint == true && !walkingExp)
    {
      if (world->SimTime().Double() == 0.002)
      {
        backRHJ->Initialize();
        backLHJ->Initialize();
        frontRHJ->Initialize();
        frontLHJ->Initialize();
      }

      if (world->SimTime().Double() > 0.002)
      {
        backRHJ->Apply();
        backLHJ->Apply();
        frontRHJ->Apply();
        frontLHJ->Apply();
      }
    }

    // Others

    std::cerr << "----------------- Time : " << world->SimTime().Double() << std::endl;
  }
}; // namespace gazebo

// Register this plugin with the simulator

GZ_REGISTER_WORLD_PLUGIN(Spawning)
} // namespace gazebo