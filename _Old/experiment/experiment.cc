#include "gazebo/physics/physics.hh"
#include <functional>
#include <gazebo/gui/GuiIface.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"
#include "gazebo/rendering/rendering.hh"
#include "gazebo/sensors/sensors.hh"
#include <iostream>
#include "ignition/math4/ignition/math.hh"
#include <vector>
#include <string>
#include <math.h>
#include "StiffJoint.hh"

// The spawning plugin

struct modelParam
{
  std::string name;
  ignition::math::Pose3d pose;
  //double *Q0[N];
};

namespace gazebo
{
class experiment : public WorldPlugin
{
private:
  gazebo::physics::WorldPtr world;
  gazebo::event::ConnectionPtr updateConnection;
  bool firstLoop = true;
  std::vector<event::ConnectionPtr> connections;

  physics::ModelPtr sphere;
  physics::ModelPtr box;
  physics::ModelPtr strud;

  physics::LinkPtr boxLink;
  physics::LinkPtr sphereLink;
  physics::LinkPtr strudLinkR;
  physics::LinkPtr strudLinkL;

  double simTime = 0;
  ignition::math::Pose3d sphereInitPose;
  ignition::math::Pose3d boxInitPose;
  ignition::math::Pose3d strudInitPose;
  ignition::math::Pose3d movPose;

  ignition::math::Pose3d initialOffset;
  StiffJoint *stiffJointR;
  StiffJoint *stiffJointL;

public:
  // Function that is called during the loading of the world

  void Load(physics::WorldPtr _parent, sdf::ElementPtr /*_sdf*/)
  {
    world = _parent;

    boxInitPose.Set(1, 0, 0, 0, 0, 0);
    sphereInitPose.Set(0, 0, 0, 0, 0, 0);
    strudInitPose.Set(0.5, 0, 0, 0, 0, 0);

    modelParam paramBox{"box", boxInitPose};
    modelParam paramsphere{"sphere", sphereInitPose};
    modelParam paramStrud{"strud", strudInitPose};
    AddModel("/home/biorob/.gazebo/models/box/model.sdf", paramBox);
    AddModel("/home/biorob/.gazebo/models/sphere/model.sdf", paramsphere);
    AddModel("/home/biorob/.gazebo/models/strud/model.sdf", paramStrud);

    // world->InsertModelFile("/home/biorob/.gazebo/models/sphere/model.sdf");

    // Event trigger to attach a new loop to the OnUpdate function

    this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&experiment::OnUpdate, this));
  }
  // Insert a model in the simulation depending of the parameters giving (see the modelParam structure)

  void AddModel(std::string fileName, modelParam param)
  {
    // Get the model SDF

    sdf::SDFPtr modelSDF = sdf::readFile(fileName);
    sdf::ElementPtr model = modelSDF->Root()->GetElement("model");

    // Setting the name and the position in the world

    model->GetAttribute("name")->SetFromString(param.name);
    model->GetElement("pose")->GetValue()->Set(param.pose);

    // Inserting the model into the world

    world->InsertModelString(modelSDF->ToString());
  }

  // Function that is called a each loop of the simulation

  void OnUpdate()
  {
    simTime = world->SimTime().Double();

    if (firstLoop == true) // Setting the carrying scenario at the first loop
    {
      firstLoop = false;

      // Gathering models pointers

      box = world->ModelByName("box");
      sphere = world->ModelByName("sphere");
      strud = world->ModelByName("strud");

      // Gathering links

      boxLink = box->GetLink("link");
      sphereLink = sphere->GetLink("link");
      strudLinkR = strud->GetLink("linkR");
      strudLinkL = strud->GetLink("linkL");

      // Setting of the initiale position
      stiffJointL = new StiffJoint(sphereLink, strudLinkL);
      stiffJointR = new StiffJoint(strudLinkR, boxLink);
      stiffJointL->Initialize();
      stiffJointR->Initialize();

      std::cerr << sphereLink->WorldPose() - boxLink->WorldPose() << std::endl;
    }

    // movPose.Set(cos(simTime) - 1, sin(simTime), cos(simTime), 2 * simTime, 2 * simTime, 2 * simTime);
    // movPose.Set(0, 0, 1, 0, 0, 0);

    boxLink->AddLinkForce(ignition::math::Vector3d(2, 0, 0));
    sphereLink->AddLinkForce(ignition::math::Vector3d(2, 0, 0));

    if (simTime > 2 && simTime < 5)
    {
      boxLink->AddTorque(ignition::math::Vector3d(0, 0, -0.4));
    }
    else if (simTime > 5 && simTime < 8)
    {
      boxLink->AddTorque(ignition::math::Vector3d(0, 0, 0.4));
    }

    // customJoint(initialOffset, boxLink, sphereLink);
    stiffJointR->Apply();
    stiffJointL->Apply();

    // box->SetWorldPose(movPose);
    // sphere->SetWorldPose(ignition::math::Pose3d(0, 0, 1, 0, 0, 0));

    // std::cerr << offsetOffset << std::endl;

    // Others

    std::cerr << "----------------- Time : " << world->SimTime() << std::endl;
  }
  // Register this plugin with the simulator
};
GZ_REGISTER_WORLD_PLUGIN(experiment)

} // namespace gazebo