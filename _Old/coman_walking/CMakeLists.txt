cmake_minimum_required(VERSION 2.8.3)
project(coman_control_plugin)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

## Load catkin and all the dependencies

find_package(catkin REQUIRED COMPONENTS
  gazebo_ros
  roscpp
)

## Depen on system install of gazebo

find_package(gazebo REQUIRED)

link_directories(${GAZEBO_LIBRARY_DIRS})
include_directories(${Boost_INCLUDE_DIR} ${catkin_INCLUDE_DIRS} ${GAZEBO_INCLUDE_DIRS})

file(GLOB includeAllSrc
    "include/walking_control/includeall/*.cc"
)

add_library(walking_plugin src/walking.cpp 
    src/walking_control/init_pos.cpp 
    src/walking_control/Control.cc
    ${includeAllSrc}
)




target_link_libraries(walking_plugin ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES})

catkin_package(
  DEPENDS
    roscpp
    gazebo_ros
)


include_directories(
  include
  include/walking_control
  include/walking_control/includeall
  include/walking_control/includeall/state_estimation_include
  ${catkin_INCLUDE_DIRS}
  src/walking_control
)


