//#include "HeaderFiles.hh"
#include "walking.hpp"

using namespace gazebo;

// Load function, called at the spawn of the robot

void WalkingPlugin::Load(gazebo::physics::ModelPtr _parent, sdf::ElementPtr modelSDF)
{

    std::cerr << "Loading Walking plugin" << std::endl;

    // Store the pointer to the model

    this->gazModel = _parent;

    std::cerr << gazModel->GetName() << std::endl;

    // Get simulation time

    this->gazWorld = this->gazModel->GetWorld();

    simTime = this->gazWorld->SimTime().Double();

    // Parameters for the automation

    // int itNb = 0;

    // inputIteration.open("/home/biorob/Automation/iteration.txt", std::ios_base::in);

    // inputIteration >> itNb;
    // inputIteration.close();

    // std::cerr << "Iteration number: " << itNb << std::endl;

    // inputParams.open("/home/biorob/Automation/Params/ParamsComan" + std::to_string(itNb) + ".txt", std::ios_base::in);
    // outputLogFile.open("/home/biorob/Automation/Logs/ComanTest" + std::to_string(itNb) + ".txt", std::ios_base::out | std::ios_base::trunc);

    // double dT = 0;
    // double dSL = 0;

    // inputParams >> dT;
    // inputParams >> dSL;

    // std::cerr << "Params: " << dT << " | " << dSL << std::endl;

    // inputParams.close();

    // Not automation

    // inputParams.open("/home/biorob/Automation/ParamsComanManu.txt", std::ios_base::in);
    outputLogFile.open("/home/biorob/Scripts/walktest.txt", std::ios_base::out | std::ios_base::trunc);

    // double dT = 0;
    // double dSL = 0;

    // inputParams >> dT;
    // inputParams >> dSL;

    // inputParams.close();

    // Store the pointers of the joints

    GetJointsInOrder();

    // IMU Sensor
    double num = (gazModel->GetName() == "coman") ? 0 : 1;
    imuSensorBasePtr = sensors::SensorManager::Instance()->GetSensor("imu_sensor_" + gazModel->GetName());
    // imuSensorBasePtr = sensors::SensorManager::Instance()->GetSensor("imu_sensor");
    imuSensor = std::dynamic_pointer_cast<gazebo::sensors::ImuSensor>(imuSensorBasePtr);

    control = new Control();
    control->SetGait(0.5, 0.025);

    // Initialize joints states and torque

    Initialize();

    // Listen to the update event. This event is broadcast every
    // simulation iteration.

    this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&WalkingPlugin::OnUpdate, this)); // Time update

    //
}

// Called by the world update start event, it is the main loop

void WalkingPlugin::OnUpdate()
{
    // Get simulation time and compute timestep

    std::cerr << "Plugin " << control->whichComan_ << std::endl;

    simTime = this->gazWorld->SimTime().Double();

    if (dt != 0)
        dt = simTime - prevTime;
    else
        dt += simTime;

    prevTime = simTime;

    std::cerr << "For init pos" << std::endl;

    // For init_pos

    if (begin_time == -1)
    {
        // Initialize tauDes to 0
        initTorques();

        // set begin_time
        begin_time = simTime;

        // Set QInit to qSens for init_pos
        for (int i = 0; i < NUM; i++)
        {
            Q0[i] = qSens[i]; // TODO: Change qSens to vector
        }
    }

    simTime -= begin_time;

    // Gather IMU data

    std::cerr << "Imu data" << std::endl;

    ReadImu();

    // Gather Force/Torque data

    std::cerr << "Read FT" << std::endl;

    ReadFT();

    // Gather joints state data

    std::cerr << "Read JS" << std::endl;

    ReadJS();

    // Compute controller

    std::cerr << "Controller" << std::endl;

    if (simTime <= TIME2WALK && begin_time != -1) // Go to init_pos
    {
        std::cerr << "Init pos" << std::endl;
        init_pos(simTime, Q0, qInit, qSens, dqSens, tauDes, whichComan_);
    }
    else // Walking controller
    {
        std::cerr << "LowerBody" << std::endl;
        control->LowerBody(
            simTime, Q0, qSens, qSensAbs, dqSens, tauSens,
            forceRightAnkle, forceLeftAnkle, torqueRightAnkle,
            torqueLeftAnkle, forceRightHand, forceLeftHand,
            trans, ImuAngRates, ImuAccelerations, h, dh,
            hD, dhD, tauDes, vals, dt, euler);
        // control->SaveVars(outputLogFile);
    }

    // Send command to joints

    std::cerr << "Write joints" << std::endl;

    WriteJC();

    linVel = gazModel->WorldLinearVel();
    pos = gazModel->WorldPose().Pos();

    // Record

    std::cerr << "Output" << std::endl;

    if (simTime > 1 && true) // Usual simulations data
    {
        outputLogFile << this->gazWorld->SimTime().Double() << " " << hD[3] << " " << hD[4] << " " << qSens[3] << " " << qSens[4] << std::endl;
        ;
    }

    std::cerr << "Stopping? " << control->stopSimulation << std::endl;

    if (control->stopSimulation || simTime > 100)
    {
        gazWorld->Stop();
    }
}

// Get the joints in the same order as in the controller
// The function model::GetJoints() doesn't return joints in the same order as in the urdf

void WalkingPlugin::Initialize()
{
    //

    std::cerr << "Initialize" << std::endl;

    // Initialisatize torques to zero

    initTorques();
    WriteJC();

    // Gather joints state

    ReadJS();

    // Set QInit to qSens for init_pos

    for (int i = 0; i < N; i++)
    {
        Q0[i] = qSens[i];
    }

    std::cerr << "Initialize" << std::endl;

    // Set begin time trigger

    begin_time = -1;

    //control->whichComan_ = (gazModel->GetName() == "coman") ? 1 : 2;
}

void WalkingPlugin::GetJointsInOrder()
{
    std::vector<std::string> jointsNames =
        {
            "WaistYaw", "WaistSag", "WaistLat", "RHipSag", "LHipSag", "RHipLat", "RHipYaw", "RKneeSag", "RAnkSag", "RAnkLat", "LHipLat", "LHipYaw", "LKneeSag", "LAnkSag", "LAnkLat", "RShSag", "RShLat", "RShYaw", "RElbj", "LShSag", "LShLat", "LShYaw", "LElbj", "RForearmPlate", "RWrj1", "RWrj2", "LForearmPlate", "LWrj1", "LWrj2", "r_handj", "l_handj"};

    for (int i = 0; i < jointsNames.size(); i++)
    {
        joints.push_back(gazModel->GetJoint(jointsNames[i]));
    }
}

void WalkingPlugin::ReadImu()
{
    ignition::math::Vector3d imuAngRate = imuSensor->AngularVelocity();
    ignition::math::Vector3d imuLinAcc = imuSensor->LinearAcceleration();
    ignition::math::Quaterniond imuOrient = imuSensor->Orientation();

    // Converting them to euler's angles

    double QW = imuOrient.W();
    double QX = imuOrient.Z();
    double QY = imuOrient.X();
    double QZ = imuOrient.Y();

    trans[0][0] = 2 * ((QX * QX) + (QW * QW)) - 1;
    trans[0][1] = 2 * ((QX * QY) - (QW * QZ));
    trans[0][2] = 2 * ((QX * QZ) + (QW * QY));
    trans[1][0] = 2 * ((QX * QY) + (QW * QZ));
    trans[1][1] = 2 * ((QW * QW) + (QY * QY)) - 1;
    trans[1][2] = 2 * ((QY * QZ) - (QW * QX));
    trans[2][0] = 2 * ((QX * QZ) - (QW * QY));
    trans[2][1] = 2 * ((QY * QZ) + (QW * QX));
    trans[2][2] = 2 * ((QW * QW) + (QZ * QZ)) - 1;

    imuData.get_Orientation(trans, cdPR, euler);

    // Angular acceleration

    ImuAngRates[0] = imuAngRate.Z();
    ImuAngRates[1] = imuAngRate.X();
    ImuAngRates[2] = imuAngRate.Y();

    imuData.get_AngRates(ImuAngRates, cdPR, ImuAngRates);

    // Linear acceleration

    ImuAccelerations[0] = imuLinAcc.Z();
    ImuAccelerations[1] = imuLinAcc.X();
    ImuAccelerations[2] = imuLinAcc.Y();

    double gravArray[3] = {0, 0, -9.81};

    imuData.get_Accelerations(gravArray, cdPR, gravArray);

    imuData.get_Accelerations(ImuAccelerations, cdPR, ImuAccelerations);

    ImuAccelerations[0] += gravArray[0];
    ImuAccelerations[1] += gravArray[1];
    ImuAccelerations[2] += gravArray[2];
}

// Function that read and store Force/Torque sensors

void WalkingPlugin::ReadFT()
{
    gazebo::physics::JointWrench RightAnkleWrench = gazModel->GetJoint("RAnkSag")->GetForceTorque(0);
    gazebo::physics::JointWrench LeftAnkleWrench = gazModel->GetJoint("LAnkSag")->GetForceTorque(0);
    gazebo::physics::JointWrench RightForearmWrench = gazModel->GetJoint("RForearmPlate")->GetForceTorque(0);
    gazebo::physics::JointWrench LeftForearmWrench = gazModel->GetJoint("LForearmPlate")->GetForceTorque(0);

    ignition::math::Vector3d vecForceRightAnkle = RightAnkleWrench.body2Force;
    ignition::math::Vector3d vecForceLeftAnkle = LeftAnkleWrench.body2Force;
    ignition::math::Vector3d vecForceRightHand = RightForearmWrench.body2Force;
    ignition::math::Vector3d vecForceLeftHand = LeftForearmWrench.body2Force;
    ignition::math::Vector3d vecTorqueRightAnkle = RightAnkleWrench.body2Torque;
    ignition::math::Vector3d vecTorqueLeftAnkle = LeftAnkleWrench.body2Torque;

    forceRightAnkle[0] = vecForceRightAnkle.X();
    forceRightAnkle[1] = vecForceRightAnkle.Y();
    forceRightAnkle[2] = vecForceRightAnkle.Z();

    forceLeftAnkle[0] = vecForceLeftAnkle.X();
    forceLeftAnkle[1] = vecForceLeftAnkle.Y();
    forceLeftAnkle[2] = vecForceLeftAnkle.Z();

    forceRightHand[0] = vecForceRightHand.X();
    forceRightHand[1] = vecForceRightHand.Y();
    forceRightHand[2] = vecForceRightHand.Z();

    forceLeftHand[0] = vecForceLeftHand.X();
    forceLeftHand[1] = vecForceLeftHand.Y();
    forceLeftHand[2] = vecForceLeftHand.Z();

    torqueRightAnkle[0] = vecTorqueRightAnkle.X();
    torqueRightAnkle[1] = vecTorqueRightAnkle.Y();
    torqueRightAnkle[2] = vecTorqueRightAnkle.Z();

    torqueLeftAnkle[0] = vecTorqueLeftAnkle.X();
    torqueLeftAnkle[1] = vecTorqueLeftAnkle.Y();
    torqueLeftAnkle[2] = vecTorqueLeftAnkle.Z();
}

// Read and store joints states

void WalkingPlugin::ReadJS()
{
    for (int i = 0; i < joints.size(); i++)
    {
        qSens[i] = joints[i]->Position();
        dqSens[i] = joints[i]->GetVelocity(0);
    }
}

// Write joints commands

void WalkingPlugin::WriteJC()
{
    for (int i = 0; i < joints.size(); i++)
    {
        joints[i]->SetForce(0, tauDes[i]);
    }
}

// Initialise torques to zero

void WalkingPlugin::initTorques()
{
    for (int i = 0; i < N; i++)
    {
        tauDes[i] = 0.0;
    }
}

// Save the data in the specified output file

void WalkingPlugin::SaveVars(
    std::ofstream &outputLogFile, double tm_, double *qSens, double *dqSens,
    double *forceRightAnkle, double *forceLeftAnkle, double *torqueRightAnkle,
    double *torqueLeftAnkle, double *forceRightHand, double *forceLeftHand,
    double imuAngRates[], double imuAccelerations[], double tauDes[],
    double trans[][3], double thp, double thr)
{
    // Save Data  tme, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,forceSensors, trans, imuAngRates, imuAccelerations

    outputLogFile << tm_;
    // start_id = 2
    for (int i = 0; i < N; i++)
    {
        outputLogFile << " " << qSens[i];
    }
    // start_id = 2 + N
    for (int i = 0; i < N; i++)
    {
        outputLogFile << " " << dqSens[i];
    }
    // 2 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << forceRightAnkle[i];
    }
    // start_id = 5 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << forceLeftAnkle[i];
    }
    // start_id = 8 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << torqueRightAnkle[i];
    }
    // start_id = 11 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << torqueLeftAnkle[i];
    }
    // start_id = 14 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << forceRightHand[i];
    }
    // start_id = 17 + 2N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << forceLeftHand[i];
    }
    // start_id = 20 + 2N
    for (int i = 0; i < N; i++)
    {
        outputLogFile << " " << tauDes[i];
    }
    // start_id = 20 + 3N
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            outputLogFile << " " << trans[i][j];
        }
    }
    // start_id = 29 + 3N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << imuAngRates[i];
    }
    // start_id = 32 + 3N
    for (int i = 0; i < 3; i++)
    {
        outputLogFile << " " << imuAccelerations[i];
    }
    outputLogFile // start_id = 35 + 3N
        << " " << thp
        << " " << thr // 36 + 3N
        << std::endl;
}