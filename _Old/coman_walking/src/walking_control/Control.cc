#include "Control.hh"
#include <numeric>
#include "WhichSide.hh"
#include "forward_kinematics_pelvis1.hh"
#include "forward_kinematics_swing_foot.hh"
#include "imu_data.hh"
#include "bezier.hh"
#include "saturate.hh"
//#include "init_pos.hh"
#include <math.h>
#include "StFtToPelvisFK.hh"
#include "SwapArrays.hh"
#include "EraseVectors.hh"
#include "DesiredFtPos.hh"
//#include "ControlLowerBody.hh"
//#include "AvgFilter.hh" to add
#include "MedianFilter.h"
#include "StackAsVector.hh"
#include "R2Euler.hh"
#include <fstream>

// Eigen headers
#include </usr/include/eigen3/Eigen/Dense>
using namespace Eigen;
//! The class of Cmatrix abreviated from Eigen
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Cmatrix;
//! The class of Cvector abreviated from Eigen VectorXd
#define Cvector Eigen::VectorXd
//! The class of Cvector3 abreviated from Eigen Vector3d
#define Cvector3 Eigen::Vector3d
#define Cvector4 Eigen::Vector4d
#define zero_v3 Cvector3(0.0, 0.0, 0.0)

//std::ofstream outputFile("/home/lucas/Scripts/control.txt", std::ios_base::out | std::ios_base::trunc);

Control::Control()
{
    qSensAbsVec = std::vector<std::vector<double>>(NUM, std::vector<double>(0.0));
    pyMidVec = std::vector<double>(0.0);
}

void Control::SetGait(double dT, double dSL)
{
    this->T = dT;
    this->STEP_LENGTH = dSL;
}

state_vars Control::LowerBody(double tm, double *Q0, double *qSens, double *qSensAbs,
                              double *dqSens, double *tauSens, double *forceRightAnkle,
                              double *forceLeftAnkle, double *torqueRightAnkle,
                              double *torqueLeftAnkle, double *forceRightHand,
                              double *forceLeftHand, double trans[][3], double angRates[3], double aPelvis[3],
                              double *h, double *dh, double *hD,
                              double *dhD, double *tauDes, double *vals, double DTm, double *euler)
{
    StackAsVector(tmVec, tm, M);

    DTm = 0.001;

    // Which Coman
    walkingController3.whichComan_ = this->whichComan_;

    thr = euler[0];
    thp = euler[1];
    thy = euler[2];

    for (int i = 0; i < NUM; i++)
    {
        MedianFilter(qSensAbsVec[i], qSensAbsMed[i], qSensAbs[i], M2);
    }

    thPelvis[0] = thp;
    thPelvis[1] = thr;

    forward_kinematics_pelvis1(qSt, pPelvis, trans, side);
    forward_kinematics_pelvis1(qSt, pPelvisTest, trans, side);
    forward_kinematics_swing_foot(qSw_, pSwFtInH, orSwFt, trans, 1 - side);
    forward_kinematics_pelvis1(qStAbs, pPelvisAbs, trans, side);
    forward_kinematics_pelvis1(qStAbsMed, pPelvisAbsMed, trans, side);
    StFtToPelvisFK(qStAbs, dqSt, side, pPelvisFK, vPelvisFk);

    // const double IMU_TO_PELVIS = 0.072;
    double pPelvisAbsTmpX = pPelvisAbs[0];
    //    if (side == 1){
    //        pPelvisAbs[0] = p_pelvis_right[0] + IMU_TO_PELVIS;
    //        pPelvisAbsMed[0] = p_pelvis_right[0] + IMU_TO_PELVIS;
    //        pxAbs = p_pelvis_right[0] + IMU_TO_PELVIS;
    //    }
    //    else{
    //        pPelvisAbs[0] = p_pelvis_left[0] + IMU_TO_PELVIS;
    //        pPelvisAbsMed[0] = p_pelvis_left[0] + IMU_TO_PELVIS;
    //        pxAbs = p_pelvis_left[0] + IMU_TO_PELVIS;
    //    }

    lM = tmVec.size() == 1 ? 1 : tmVec.back() - tmVec[0];
    // lM = vecPxAbs.size() == 0 ? 1 : (DTm * vecPxAbs.size());
    AvgFilter(pxVec, px, pPelvis[0], M);
    AvgFilter(pxAbsVec, pxAbs, pPelvisAbs[0], M);
    AvgFilter(pxAbsMedVec, pxAbsMed, pPelvisAbsMed[0], M);
    pxAbs = pxAbsMed;
    AvgFilter(pyVec, py, pPelvis[1], M);
    AvgFilter(pyAbsVec, pyAbs, pPelvisAbs[1], M);
    AvgFilter(pxswfVec, pxswf, pSwFtInH[0], M4);
    AvgFilter(pyswfVec, pyswf, pSwFtInH[1], M4);
    AvgFilter(vxVec, vx, (px - px_old) / (DTm * M), M2);
    AvgFilter(vxAbsVec, vxAbs, (pxAbs - pxAbs_old) / lM, M2);
    AvgFilter(vxAbsVecF, vxAbsF, (pxAbs - pxAbs_old) / lM, M);
    AvgFilter(vxFkVec, vxFK, vPelvisFk[0], M4);
    AvgFilter(vyVec, vy, (py - py_old) / (DTm * M), M2);
    AvgFilter(vyAbsVec, vyAbs, (pyAbs - pyAbs_old) / (DTm * M), M2);
    AvgFilter(vyswfVec, vyswf, (pyswf - pyswf_old) / (DTm * M5), M5);
    AvgFilter(vxswfVec, vxswf, (pxswf - pxswf_old) / (DTm * M5), M5);
    AvgFilter(axVec, ax, aPelvis[0], M4); //
    AvgFilter(thpFVec, thpF, thp, M5);
    AvgFilter(dthpFVec, dthpF, angRates[1], M5);
    AvgFilter(thrFVec, thrF, thr, M5);
    AvgFilter(dthrFVec, dthrF, angRates[0], M5);
    AvgFilter(swFtPitchVec, swFtPitch, orSwFt[1], M4);
    AvgFilter(dswFtPitchVec, dswFtPitch, (swFtPitch - swFtPitch_old) / (DTm * M3), M2);
    AvgFilter(swFtRollVec, swFtRoll, orSwFt[0], M4);
    AvgFilter(dswFtRollVec, dswFtRoll, (swFtRoll - swFtRoll_old) / (DTm * M3), M2);

    aPelvis[0] = ax;

    for (int i(0); i < 3; i++)
    {
        MedianFilter(frcRVec[i], forceRightAnkleF[i], forceRightAnkle[i], M5);
        MedianFilter(frcLVec[i], forceLeftAnkleF[i], forceLeftAnkle[i], M5);
        MedianFilter(trqRVec[i], torqueRightAnkleF[i], torqueRightAnkle[i], M5);
        MedianFilter(trqLVec[i], torqueLeftAnkleF[i], torqueLeftAnkle[i], M5);
    }

    for (int i(0); i < 3; i++)
    {
        AvgFilter(pPelvisAbsVec[i], pPelvisAbsF[i], pPelvisAbsMed[i], M);
        StackAsVector(pPelvisAbsFVec[i], pPelvisAbsF[i], M);
    }

    unsigned int M_FORCE_FILT;
    double K_FXY = 0;

    if (whichComan_ == 1)
    {
        M_FORCE_FILT = 1;
        K_FXY = 1;
    }
    else
    {
        M_FORCE_FILT = 2 * M2;
        K_FXY = 0; // this coefficient is to make the effect of FX and FY of the ankle force sensors zero in COMAN2 (IIT coman) because they are very noisy with very high spikes
    }

    double forceLeftAnkleZ = forceLeftAnkleF[2];
    double forceRightAnkleZ = forceRightAnkleF[2];
    MedianFilter(forceLeftAnkleZVec, forceLeftAnkleZMed, forceLeftAnkleZ, M_FORCE_FILT);                                        // coman 2
    MedianFilter(forceRightAnkleZVec, forceRightAnkleZMed, forceRightAnkleZ, M_FORCE_FILT);                                     // coman 2
    double f1 = pow((pow(K_FXY * forceRightAnkle[0], 2) + pow(K_FXY * forceRightAnkle[1], 2) + pow(forceRightAnkleZ, 2)), 0.5); // coman 2
    double f2 = pow((pow(K_FXY * forceLeftAnkle[0], 2) + pow(K_FXY * forceLeftAnkle[1], 2) + pow(forceLeftAnkleZ, 2)), 0.5);    // coman 2
    kRaw = f1 / (f1 + f2 + EPSILON);

    AvgFilter(kVec, kF, kRaw, M3);
    // kF = 0.5 * cos(2 * M_PI * (tm + 0.25)) + 0.5;
    kR = kF;
    kL = 1 - kF;
    /* //updated kR, kL toCheck
    if (kR < 0.5){
        kR = 2.5 * kR - 0.25;
    }
    else
    {
        kR = 1;
    }
    if (kR < 0.1){
        kR = 0;
    }


    if (kL < 0.5){
        kL = 2.5 * kL - 0.25;
    }
    else
    {
        kL = 1;
    }
    if (kL < 0.1){
        kL = 0;
    }
    */
    if (f_d == 0)
    {
        kR = 1;
        kL = 1;
    }

    dorSwFt[1] = tInStep > DTm * M4 ? dswFtPitch : 0;
    thp = thpF;
    thr = thrF;
    orSwFt[1] = swFtPitch;
    thPelvis[0] = thp;
    thPelvis[1] = thr;
    pSwFtInH[0] = pxswf;
    pSwFtInH[1] = pyswf;

    count1 = (count1 + 1) % M;
    count2 = (count2 + 1) % M2;
    count3 = (count3 + 1) % M3;
    count4 = (count4 + 1) % M4;
    count5 = (count5 + 1) % M5;
    px_old = vec_px[count1];
    vec_px[count1] = px;
    pxAbs_old = vec_pxAbs[count1];
    if (vecPxAbs.size() < M)
    {
        vecPxAbs.push_back(pxAbs);
        pxAbs_old = vecPxAbs[0];
    }
    else
    {
        pxAbs_old = vecPxAbs[count1];
        vecPxAbs[count1] = pxAbs;
    }
    vec_pxAbs[count1] = pxAbs;
    py_old = vec_py[count1];
    vec_py[count1] = py;
    pyAbs_old = vec_pyAbs[count1];
    vec_pyAbs[count1] = pyAbs;
    pxswf_old = vec_pxswf[count5];
    vec_pxswf[count5] = pxswf;
    pyswf_old = vec_pyswf[count5];
    vec_pyswf[count5] = pyswf;
    swFtPitch_old = vec_swFtPitch[count3];
    vec_swFtPitch[count3] = swFtPitch;
    swFtRoll_old = vec_swFtRoll[count3];
    vec_swFtRoll[count3] = swFtRoll;

    pxAbs_old = pPelvisAbsFVec[0][0];
    pyAbs_old = pPelvisAbsFVec[1][0];
    //  pxswf_old = pxswfVec[0];
    //  pyswf_old = pyswfVec[0];
    //  swFtPitch_old = swFtPitchVec[0];
    //  swFtRoll_old = swFtRollVec[0];

    pPelvis[0] = px;
    pPelvis[1] = py;
    vPelvis[0] = vx;
    vPelvis[1] = vy;
    pPelvisAbs[0] = pxAbs;
    pPelvisAbsMed[0] = pxAbsMed;
    pPelvisAbs[1] = pyAbs;
    vPelvisAbs[0] = vxAbs;
    vPelvisAbs[1] = vyAbs;

    const double H = 0.05;
    double xCopR = forceRightAnkleF[2] < 50 ? -(torqueRightAnkleF[1] + forceRightAnkleF[0] * H) / 50 : -(torqueRightAnkleF[1] + forceRightAnkleF[0] * H) / forceRightAnkleF[2];
    double xCopL = forceLeftAnkleF[2] < 50 ? -(torqueLeftAnkleF[1] + forceLeftAnkleF[0] * H) / 50 : -(torqueLeftAnkleF[1] + forceLeftAnkleF[0] * H) / forceLeftAnkleF[2];

    t_ = tm - TIME_WALK;
    double tw = 0;
    if (t_ >= TIME_WALK && t_ <= TIME_WALK + 0.1)
    {
        f_d = 1 / T;
        tw = t_ - TIME_WALK;
        f0 = tw <= 0.1 ? 10 * tw * f_d : f_d; // fast but continuously go to f_d from f0  = 0;
    }
    // if tm > 2 * TIME_WALK start walking
    if (t_ >= TIME_WALK && startWalkingFlag == 0)
    {
        startWalkingFlag = 1;
        for (int i = 0; i < NUM; i++)
        {
            qSensInit[i] = qSens[i];
        }
        for (int i = 0; i < 3; i++)
        {
            thPelvisInit[i] = thPelvis[i];
            swFtPosInit[i] = pSwFtInH[i];
            orSwFtInit[i] = orSwFt[i];
        }
        thpF_init = thpF;
        thrF_init = thrF;
        px0 = pxAbs;
    }

    if (flagMidStep == false && vyAbs > 0)
    {
        pyMidVec.push_back(pyAbs);
        if (pyMidVec.size() > 2)
        {
            pyMidVec.erase(pyMidVec.begin());
            frontalBias = (pyMidVec[0] + pyMidVec[1]) / 2;
        }
        flagMidStep = true;
    }

    if (side != oldSide)
    {
        begsteptime = tm;
        ++stepNumber;
        SwapArrays(indexSt, indexSw, 6);
        EraseVectors();
        flagMidStep = false;
        QswKMid = QswKMid_INIT;
    }
    tInStep = tm - begsteptime;
    for (int i = 0; i < 6; i++)
    {
        qSt[i] = qSens[indexSt[i]];
        dqSt[i] = dqSens[indexSt[i]];
        qStAbs[i] = qSensAbs[indexSt[i]];
        qStAbsMed[i] = qSensAbsMed[indexSt[i]];
        qSw_[i] = qSens[indexSw[i]];
    }
    // Find initial values at the beginning of the step
    if (tInStep < ZERO_S)
    {
        for (int i = 0; i < NUM; i++)
        {
            qSensInit[i] = qSens[i];
        }
        for (int i = 0; i < 3; i++)
        {
            thPelvisInit[i] = thPelvis[i];
            swFtPosInit[i] = pSwFtInH[i];
            orSwFtInit[i] = orSwFt[i];
        }
        thpF_init = thpF;
        thrF_init = thrF;
        px0 = pxAbs;
    }

    // Determine the stance side and sign (sg)
    oldSide = side;
    WhichSide(kF, s, side, sg);
    if (side != oldSide)
    {
        //            if (stepNumber >= 15 && stepNumber < 30){
        //                STEP_LENGTH += 0.002;
        //                f_d += .02;
        //                if (stepNumber == 15){
        //                    freqVec[0] = f_d;
        //                    freqVec[1] = f_d;
        //                    QswKMid += 0.1;
        //                }
        //                f0 += 0.02;
        //                Q_INIT[5] = -0.04;
        //                Q_INIT[10] = 0.04;
        //            }
        //            if (stepNumber >= 30 && stepNumber < 35){
        //                STEP_LENGTH += DTm;
        //                f_d +=.02;
        //                if (stepNumber == 30){
        //                    freqVec[0] = f_d;
        //                    freqVec[1] = f_d;
        //                    QswKMid += 0.1;
        //                }
        //                f0 += 0.02;
        //                Q_INIT[5] = -0.03;
        //                Q_INIT[10] = 0.03;
        //            }
        if (stepNumber == firstStopN)
        {
            STEP_LENGTH = 2 * STEP_LENGTH / 3;
        }
        if (stepNumber == firstStopN + 1)
        {
            STEP_LENGTH = STEP_LENGTH / 3;
        }
        if (stepNumber == firstStopN + 2)
        {
            STEP_LENGTH = 0;
        }
        x0 = (stepNumber > 2 && t_ > 1) ? STEP_LENGTH : 0;
        vxDes = STEP_LENGTH == 0 ? 0 : 2 * x0 * f_d;
        pyEnd = pyAbs;
        if (stepNumber > 1 && tInStep > 0.15)
        {
            freqVec.push_back(1 / tInStep);
            freqVec.erase(freqVec.begin());
            avgFreq = (freqVec[0] + freqVec[1]) / 2;
            last_step_freq = freqVec[1];
            f0 = f0 - 0.1 * (avgFreq - f_d); // for fd = 2.5 the coeff was set to 0.05
            f0 = saturate(f0, 3, 1.35);      // added check ****
        }
        if (stepNumber >= firstStopN + stepsToStop + 2 && abs(pSwFtInH[0] + pPelvisAbs[0]) < 0.03 && abs(vxAbsF) < 0.1 && s > 0.25 && flagStopCommand == false)
        {
            flagStopCommand = true;
            f0AtStop = f0;
            f_dAtStop = f_d;
            timeAtStop = tm;
        }
    }

    if (flagStopCommand == true)
    {
        f_d = saturatel(f_dAtStop - 10 * (tm - timeAtStop), 0);
        // drives the stance outputs to the end of the step (s = 1)
        if (f_d == 0 && tm - timeAtStop > 2 * T)
        {
            f0 = f_d;
        }
    }

    if (f0 == 0 && tm > timeAtStop + 4 * T)
    { // *** changed 2-3 to 4
        if (pPelvisAbs[0] > 0.1 || pPelvisAbs[0] < -0.06 || kF < 0.1 || kF > 0.9)
        {
            f_d = 1 / T;
            f0 = f_d;
            STEP_LENGTH = 0;
            stepNumber = 1;
            firstStopN = 2;
            side = kF < 0.5 ? 0 : 1;
            if (pPelvisAbs[0] < -0.06)
            {
                QswKMid = 1.3;
            }
            else
            {
                QswKMid = 1.1;
            }
            flagStopCommand = false;
        }
    }

    s = saturate((tInStep)*f0, 1, 0);
    //        kR = pow(kF, pow(f0 / 3, 0.3));
    //        kL = pow(1 - kF, pow(f0 / 3, 0.3));
    if (tInStep > DTm * (M4 + M2 + M5))
    {
        //            pSwFtInH[0] = pxswf;
        //            pSwFtInH[1] = pyswf;
        vSwFtInH[0] = vxswf;
        vSwFtInH[1] = vyswf;
    }
    else if (tInStep > DTm * (M5 + M4))
    {
        vSwFtInH[0] = (pxswf - pxswf_old) / (DTm * M5);
        dswFtPitch = (swFtPitch - swFtPitch_old) / (DTm * M5);
        dswFtRoll = (swFtRoll - swFtRoll_old) / (DTm * M5);
    }
    for (int i = 0; i < 3; i++)
    {
        swFtPos[i] = pSwFtInH[i];
        dswFtPos[i] = vSwFtInH[i];
    }

    // High-level foot adjustment
    DesiredFtPos(pxAbs, pyAbs, tInStep, px0, vxDes, vxAbs, vxAbsF, vyAbs, sg, deltaX, deltaY, kv, frontalBias, s, T);

    if (forceRightAnkleZMed < AirTresh && forceLeftAnkleZMed < AirTresh)
    {
        stepNumber = 1;
        side = SIDE_INITIAL;
        oldSide = side;
        inAir = 1;
        if (timeInAir == 0)
        {
            timeInAir = tm;
            for (int i = 0; i < NUM; i++)
            {
                Q0[i] = qSens[i];
            }
        }
        init_pos(tm - timeInAir, Q0, Q_INIT, qSens, dqSens, tauDes, whichComan_);
        //   cout << tm - timeInAir << endl;
    }
    else
    {
        if (inAir == 1)
        {
            t0 = tm;
            inAir = 0;
            timeInAir = 0;
        }

        if (tm - t0 < TIME_REWALK)
        {
            init_pos(tm - t0, Q_INIT, Q_INIT, qSens, dqSens, tauDes, whichComan_);
            // cout << tm - t0 << endl;
        }
        else
        {
            // std::cerr<< tm << ": s=" << s << " f_d=" << f_d << " f0=" << f0 << std::endl;
            // s=0;
            // f0 = 0;
            walkingController3.EvalOutputs(s, f0, Q_INIT, qSens, dqSens, kR, kL, indexSt, indexSw, thp, dthpF, thr, dthrF, x0, deltaX, deltaY, qSensInit,
                                           swFtPosInit, thpF_init, thrF_init, px0, pSwFtInH, vSwFtInH, orSwFt, dorSwFt, orSwFtInit, h, dh, hD, dhD, STEP_LENGTH, QswKMid);
            walkingController3.EvalTorques(s, tInStep, f_d, f0, x0, px0, Q_INIT, qSens, dqSens, kR, kL, orSwFt, tauAnkTorque, forceRightAnkleF, forceLeftAnkleF, torqueRightAnkleF, torqueLeftAnkleF,
                                           pPelvisAbs, vxAbsF, h, dh, hD, dhD, tauDes, vals);
        }
    }

    // std::cerr << f0 << " | " << f_d << std::endl;
    // std::cerr << inAir<< std::endl;
    // std::cerr << std::isnan(f0)<< std::endl;
    // std::cerr << std::isnan(f_d)<< std::endl;
    // std::cerr << std::isinf(f0)<< std::endl;
    // std::cerr << std::isinf(f_d)<< std::endl;

    if (tm > 15 && (inAir == 1 || (f0 == 0 && f_d == 0) || (std::isnan(f0) || std::isnan(f_d)) || (std::isinf(f0) || std::isinf(f_d))))
    {
        std::cerr << "Stop" << std::endl;
        stopSimulation = true;
    }

    forceRightAnkle = forceRightAnkleF;

    // Var for Output
    varsOut.tm_ = tm;
    varsOut.n_ = NUM;
    varsOut.qSens_ = qSens;
    varsOut.qSensAbs_ = qSensAbs;
    varsOut.dqSens_ = dqSens;
    varsOut.pPelvis_ = pPelvis;
    varsOut.pPelvisAbs_ = pPelvisAbs;
    varsOut.vPelvis_ = vPelvis;
    varsOut.vPelvisAbs_ = vPelvisAbs;
    varsOut.aPelvis_ = aPelvis;
    varsOut.forceRightAnkle_ = forceRightAnkle;
    varsOut.forceLeftAnkle_ = forceLeftAnkle;
    varsOut.torqueRightAnkle_ = torqueRightAnkle;
    varsOut.torqueLeftAnkle_ = torqueLeftAnkle;
    varsOut.forceRightHand_ = forceRightHand;
    varsOut.forceLeftHand_ = forceLeftHand;
    varsOut.orSwFt_ = orSwFt;
    varsOut.dorSwFt_ = dorSwFt;
    varsOut.pSwFtInH_ = pSwFtInH;
    varsOut.angRates_ = angRates;
    varsOut.h_ = h;
    varsOut.hD_ = hD;
    varsOut.pPelvisFK_ = pPelvisFK;
    varsOut.pPelvisTest_ = pPelvisTest;
    varsOut.vPelvisFk_ = vPelvisFk;
    varsOut.tauSens_ = tauSens;
    varsOut.tauDes_ = tauDes;
    varsOut.tauAnkTorque_ = tauAnkTorque;
    varsOut.k_ = kF;
    varsOut.s_ = s;
    varsOut.tInStep_ = tInStep;
    varsOut.deltaX_ = deltaX;
    varsOut.deltaY_ = deltaY;
    varsOut.thp_ = thp;
    varsOut.thpF_init_ = thpF_init;
    varsOut.thr_ = thr;
    varsOut.side_ = side;
    varsOut.vxFK_ = vxFK;
    varsOut.kv_ = kv;
    varsOut.px0_ = px0;
    varsOut.kOrg_ = kRaw;
    varsOut.vxDes_ = vxDes;
    varsOut.x0_ = x0;
    varsOut.T_ = T;
    memcpy(varsOut.trans_, trans, sizeof(varsOut.trans_)); // check values
    //varsOut.trans_=trans;  //???? error with types???
    varsOut.kR_ = kR;
    varsOut.kL_ = kL;
    varsOut.avgFreq_ = avgFreq;
    varsOut.f0_ = f0;
    varsOut.last_step_freq_ = last_step_freq;
    varsOut.frontalBias_ = frontalBias;
    varsOut.vxAbsF_ = vxAbsF;
    varsOut.qSensAbsMed_ = qSensAbsMed;
    varsOut.pPelvisAbsMed_ = pPelvisAbsMed;
    varsOut.qSt_ = qSt;
    varsOut.indexSt_ = indexSt;
    varsOut.pxAbsOld_ = pxAbs_old;
    varsOut.pPelvisAbsF_ = pPelvisAbsF;

    return varsOut;
}

void Control::EraseVectors()
{
    pxVec.clear();
    pxVec.clear();
    pyVec.clear();
    pxswfVec.clear();
    pyswfVec.clear();
    //    vxVec.clear();
    //    vyVec.clear();
    vxswfVec.clear();
    vyswfVec.clear();
    axVec.clear();
    thpFVec.clear();
    thrFVec.clear();
    dthpFVec.clear();
    dthrFVec.clear();
    kVec.clear();
    pxAbsVec.clear();
    pxAbsMedVec.clear();
    pyAbsVec.clear();
    //    vxFkVec.clear();
    //    vxAbsVec.clear();
    //    vyAbsVec.clear();
    swFtPitchVec.clear();
    dswFtPitchVec.clear();
    swFtRollVec.clear();
    dswFtRollVec.clear();
    vecPxAbs.clear();
    //    tmVec.clear();
    //    forceLeftAnkleZVec.clear();
    //    forceRightAnkleZVec.clear();
    for (int i(0); i < 3; i++)
    {
        pPelvisAbsVec[i].clear();
        pPelvisAbsFVec[i].clear();
        //    frcRVec[i].clear();
        //    frcLVec[i].clear();
        //    trqRVec[i].clear();
        //    trqLVec[i].clear();
    }
}

void Control::SaveVars(std::ofstream &outputFile){
    // Save Data  tme, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,forceSensors, trans, imuAngRates, imuAccelerations

    outputFile << varsOut.tm_;
            // start_id = 2
            for (int i = 0; i < NUM; i++){
                outputFile << " " << varsOut.qSens_[i];
            }
            //  start_id = 2 + NUM
            for (int i = 0; i < NUM; i++){
                outputFile << " " << varsOut.qSensAbs_[i];
            }
            // start_id = 2 + 2N
            for (int i = 0; i < NUM; i++)
            {
                outputFile << " " << varsOut.dqSens_[i];
            }
            // start_id = 2 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.pPelvis_[i];
            }
            // start_id = 5 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.pPelvisAbs_[i];
            }
            // start_id = 8 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.vPelvis_[i];
            }
            // start_id = 11 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.vPelvisAbs_[i];
            }
            // start_id = 14 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.aPelvis_[i];
            }
            // start_id = 17 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.forceRightAnkle_[i];
            }
            // start_id = 20 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.forceLeftAnkle_[i];
            }
            // start_id = 23 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.torqueRightAnkle_[i];
            }
            // start_id = 26 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.torqueLeftAnkle_[i];
            }
            // start_id = 29 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.forceRightHand_[i];
            }
            // start_id = 32 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.forceLeftHand_[i];
            }
            // start_id = 35 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.orSwFt_[i];
            }
            // start_id = 38 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.dorSwFt_[i];
            }
            // start_id = 41 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.pSwFtInH_[i];
            }
            // start_id = 44 + 3N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.angRates_[i];
            }
            // start_id = 47 + 3N
            for (int i = 0; i < NUM; i++)
            {
                outputFile << " " << varsOut.h_[i];
            }
            // start_id = 47 + 4N
            for (int i = 0; i < NUM; i++)
            {
                outputFile << " " << varsOut.hD_[i];
            }
            // start_id = 47 + 5N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.pPelvisFK_[i];
            }
            // start_id = 50 + 5N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.pPelvisTest_[i];
            }
            // start_id = 53 + 5N
            for (int i = 0; i < 3; i++)
            {
                outputFile << " " << varsOut.vPelvisFk_[i];
            }
            // start_id = 56 + 5N
            for (int i = 0; i < NUM; i++)
            {
                outputFile << " " << varsOut.tauSens_[i];
            }
            // start_id = 56 + 6N
            for (int i = 0; i < NUM; i++)
            {
                outputFile << " " << varsOut.tauDes_[i];
            }
            // start_id = 56 + 7N
            for (int i = 0; i < 2; i++)
            {
                outputFile << " " << varsOut.tauAnkTorque_[i];
            }
            // start_id = 58 + 7N
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    outputFile << " " << varsOut.trans_[i][j];
                }
            }
            // start_id = 73 + 7N
            for (int i = 0; i < NUM; i++){
                outputFile << " " << varsOut.qSensAbsMed_[i];
            }
            // start_id = 73 + 8N
            for (int i = 0; i < 3; i++){
                outputFile << " " << varsOut.pPelvisAbsMed_[i];
            }
            // start_id = 76 + 8N
            for (int i = 0; i < 3; i++){
                outputFile << " " << varsOut.pPelvisAbsF_[i];
            }
    //        for (int i = 0; i < 800; i++){
    //            outputFile << " " << varsOut.velTimeWind_[i];
    //        }
            outputFile // start_id = 76 + 8N
                      << " " << varsOut.k_ // 76 + 8N
                      << " " << varsOut.s_ // 77 + 8N
                      << " " << varsOut.tInStep_ // 78 + 8N
                      << " " << varsOut.deltaX_ // 79 + 8N
                      << " " << varsOut.deltaY_ // 80 + 8N
                      << " " << varsOut.thp_ // 81 + 8N
                      << " " << varsOut.thpF_init_ // 82 + 8N
                      << " " << varsOut.thr_ // 83 + 8N
                      << " " << varsOut.side_ // 84 + 8N
                      << " " << varsOut.vxFK_ // 85 + 8N
                      << " " << varsOut.kv_ // 86 + 8N
                      << " " << varsOut.px0_ // 87 + 8N
                      << " " << varsOut.kOrg_ // 88 + 8N
                      << " " << varsOut.vxDes_ * varsOut.tInStep_ + varsOut.px0_ // 89 + 8N
                      << " " << varsOut.x0_ // 87 + 7N
                      << " " << varsOut.T_
                      << " " << varsOut.kR_
                      << " " << varsOut.kL_
                      << " " << varsOut.avgFreq_
                      << " " << varsOut.f0_
                      << " " << varsOut.last_step_freq_
                      << " " << varsOut.frontalBias_
                      << " " << varsOut.vxAbsF_
                      << " " << varsOut.pxAbsOld_
                      << std::endl;
}