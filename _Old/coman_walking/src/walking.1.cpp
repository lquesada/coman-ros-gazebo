#include <walking.hpp>

namespace gazebo
{
class WalkingPlugin : public ModelPlugin
{

    // Pointer to the model
  private:
    physics::ModelPtr model;

    // Pointer to the world

  private:
    gazebo::physics::WorldPtr world;

    // Pointer vector to the joints

  private:
    std::vector<gazebo::physics::JointPtr> joints_;

    // IMU Sensor

  private:
    sensors::SensorPtr imuSensorBasePtr;

  private:
    gazebo::sensors::ImuSensorPtr imuSensor_;

    // Pointer to the update event connection

  private:
    event::ConnectionPtr updateConnection;

  private:
    event::ConnectionPtr resetWorld;

    // Time

  private:
    double simTime;

  private:
    double begin_time = -1;

    // Load function, called at the spawn of the robot

  public:
    void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
        //

        std::cerr << "LOAD" << std::endl;

        // Store the pointer to the model

        this->model = _parent;

        // Get simulation time

        this->world = this->model->GetWorld();

        simTime = this->world->SimTime().Double();

        // Open log file

        outputLogFile.open("/home/lucas/Scripts/gazlog.txt", std::ios_base::out | std::ios_base::trunc);

        // Store the pointers of the joints

        GetJointsInOrder();

        // IMU Sensor

        imuSensorBasePtr = sensors::SensorManager::Instance()->GetSensor("imu_sensor");
        imuSensor_ = std::dynamic_pointer_cast<gazebo::sensors::ImuSensor>(imuSensorBasePtr);

        // Initialize joints states and torque

        Initialize();

        // Initialize pose

        model->SetWorldPose(ignition::math::Pose3d(0, 0, 0.517, 0, 0, 0));

        // Listen to the update event. This event is broadcast every
        // simulation iteration.

        this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&WalkingPlugin::OnUpdate, this)); // Time update
        this->resetWorld = event::Events::ConnectWorldReset(std::bind(&WalkingPlugin::Initialize, this));           // World reset
    }

    // Called by the world update start event

  public:
    void OnUpdate()
    {
        // std::cerr << "  ------------ " << simTime << "  ------------" << std::endl;

        // Get simulation time and compute timestep

        simTime = this->world->SimTime().Double();

        if (dt != 0)
            dt = simTime - prevTime;
        else
            dt += simTime;

        prevTime = simTime;

        // For init_pos

        if (begin_time == -1)
        {
            // Initialize tauDes to 0
            init(tauDes);

            // set begin_time
            begin_time = simTime;

            // Set QInit to qSens for init_pos
            for (int i = 0; i < NUM; i++)
            {
                Q0[i] = qSens[i]; // TODO: Change qSens to vector
            }
        }

        simTime -= begin_time;

        // Gather IMU data

        ReadImu(imuSensor_);

        // Gather Force/Torque data

        ReadFT(model);

        // Gather joints state data

        ReadJS(joints_);

        // Compute controller

        if (simTime <= TIME2WALK && begin_time != -1) // Go to init_pos
        {
            init_pos(simTime, Q0, qInit, qSens, dqSens, tauDes, whichComan_);

            /*SaveVars( initOutput, simTime, qSens, dqSens, forceRightAnkle, forceLeftAnkle,
                        torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,
                        ImuAngRates, ImuAccelerations, tauDes, Trans, euler[1], euler[0]);*/
        }
        else // Walking controller
        {
            varsOut = control.LowerBody(
                simTime, Q0, qSens, qSensAbs, dqSens, tauSens,
                forceRightAnkle, forceLeftAnkle, torqueRightAnkle,
                torqueLeftAnkle, forceRightHand, forceLeftHand,
                Trans, ImuAngRates, ImuAccelerations, h, dh,
                hD, dhD, tauDes, vals, dt, euler);

            control.SaveVars(controlOutput); // Have an impact on the sim
        }

        // Send command to joints

        WriteJC(joints_);

        // Record

        if (simTime < 100)
        {
            outputLogFile << simTime << " "                                                 // 1
                          << euler[0] << " " << euler[1] << " " << euler[2] << " "          // 2
                          << Trans[0][0] << " " << Trans[0][1] << " " << Trans[0][2] << " " // 5
                          << Trans[1][0] << " " << Trans[1][1] << " " << Trans[1][2] << " "
                          << Trans[2][0] << " " << Trans[2][1] << " " << Trans[2][2] << " "
                          << ImuAngRates[0] << " " << ImuAngRates[1] << " " << ImuAngRates[2] << " "                 // 14
                          << ImuAccelerations[0] << " " << ImuAccelerations[1] << " " << ImuAccelerations[2] << " "  // 17
                          << QW << " " << QX << " " << QY << " " << QZ << " "                                        // 20
                          << forceRightAnkle[0] << " " << forceRightAnkle[1] << " " << forceRightAnkle[2] << " "     // 24
                          << torqueRightAnkle[0] << " " << torqueRightAnkle[1] << " " << torqueRightAnkle[2] << " "; // 27

            for (int i = 0; i < 31; i++) // 30
            {
                outputLogFile << tauDes[i] << " ";
            }

            for (int i = 0; i < 31; i++) // 30+ N
            {
                outputLogFile << h[i] << " ";
            }

            for (int i = 0; i < 31; i++) // 30 +2N
            {
                outputLogFile << hD[i] << " ";
            }

            outputLogFile << varsOut.s_ << " ";    // 30 + 3N
            outputLogFile << varsOut.f0_ << " ";   // 4N
            outputLogFile << varsOut.side_ << " "; // 1 + 4N

            outputLogFile << std::endl;
        }
    }

    // Get the joints in the same order as in the controller
    // The function model::GetJoints() doesn't return joints in the same order as in the urdf

    void Initialize()
    {
        //

        std::cerr << "Initialize" << std::endl;

        // Initialisatize torques to zero

        init(tauDes);
        WriteJC(joints_);

        // Gather joints state

        ReadJS(joints_);

        // Set QInit to qSens for init_pos

        for (int i = 0; i < N; i++)
        {
            Q0[i] = qSens[i];
        }

        // Set begin time trigger

        begin_time = -1;
    }

    void GetJointsInOrder()
    {
        std::vector<std::string> jointsNames =
            {
                "WaistYaw", "WaistSag", "WaistLat", "RHipSag", "LHipSag", "RHipLat", "RHipYaw", "RKneeSag", "RAnkSag", "RAnkLat", "LHipLat", "LHipYaw", "LKneeSag", "LAnkSag", "LAnkLat", "RShSag", "RShLat", "RShYaw", "RElbj", "LShSag", "LShLat", "LShYaw", "LElbj", "RForearmPlate", "RWrj1", "RWrj2", "LForearmPlate", "LWrj1", "LWrj2", "r_handj", "l_handj"};

        for (int i = 0; i < jointsNames.size(); i++)
        {
            joints_.push_back(model->GetJoint(jointsNames[i]));
        }
    }
};

// Register this plugin with the simulator

GZ_REGISTER_MODEL_PLUGIN(WalkingPlugin)
} // namespace gazebo