#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <ctime>

#include </usr/include/eigen3/Eigen/Dense>

using namespace Eigen;
//! The class of Cmatrix abreviated from Eigen
typedef Eigen::Matrix<double,
                      Eigen::Dynamic,
                      Eigen::Dynamic,
                      Eigen::RowMajor>
    Cmatrix;

#include "AvgFilter.hh"
#include "includeall/state_vars.hh"
#include "init_pos.hh"
#include "WalkingController3.hh"

//! The class of Cmatrix abreviated from Eigen
//! The class of Cvector abreviated from Eigen VectorXd
#define Cvector Eigen::VectorXd
//! The class of Cvector3 abreviated from Eigen Vector3d
#define Cvector3 Eigen::Vector3d
#define Cvector4 Eigen::Vector4d
#define zero_v3 Cvector3(0.0, 0.0, 0.0)

#define M 50
#define M2 10
#define M3 15
#define M4 10
#define M5 5
#define AirTresh 50
#define LEFT 0
#define RIGHT 1
#define LOWER_BODY_N 15
#define UPPER_BODY_N 16
#define N_OF_ITERATIONS 10
#define EPSILON 0.001
#define ALPHA 1
#define N_C 3
#define N_J 23
#define ALPHA_ 1

const int NUM = 31;

class Control
{

  public:
  Control();

  state_vars LowerBody(double tm, double *Q0, double *qSens, double *qSensAbs,
                       double *dqSens, double *tauSens, double *forceRightAnkle,
                       double *forceLeftAnkle, double *torqueRightAnkle,
                       double *torqueLeftAnkle, double *forceRightHand,
                       double *forceLeftHand, double trans[][3], double angRates[3], double aPelvis[3],
                       double *h, double *dh, double *hD,
                       double *dhD, double *tauDes, double *vals, double DTm, double *euler);
  //Eigen::ArrayXXf outputtest;
  state_vars varsOut;

  //    state_vars lb_vars;
  void SaveVars(std::ofstream &outputFile);
  void EraseVectors();
  void SetGait(double dT, double dSL);

  unsigned int whichComan_ = 1;
  // double T=0.5, STEP_LENGTH=0.05;
  bool stopSimulation = false;

private:
  double qknee0 = 0.2, qP0 = -0.1, qR0 = 0.055 * 1;
  double RIGHT_ELBOW_YAW = 0, LEFT_ELBOW_YAW = 0;
  // double Q_INIT[NUM] = {0, 0.0, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1, qR0, qR0 * 1, 0, qknee0, qP0 * 1, -qR0 * 1, 0.45, -0.2, 0.0, -1.75, 0.45, 0.2, 0.0, -1.75, RIGHT_ELBOW_YAW, 0.0, 0.0, LEFT_ELBOW_YAW, 0, 0, 0, 0};
  double Q_INIT[NUM] = {0, 0.075, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1.4, qR0, qR0 * 1, 0, qknee0, qP0 * 1.4, -qR0 * 1, 0.45, -0.2, 0.0, -1.75, 0.45, 0.2, 0.0, -1.75, RIGHT_ELBOW_YAW, 0.0, 0.0, LEFT_ELBOW_YAW, 0, 0, 0, 0};

  int count1 = 0, count2 = 0, count3 = 0, count4 = 0, count5 = 0;

  // Lower-body variables
  double pxDes = {}, kR= {}, kL= {}, forceRightAnkleFx= {};
  double pPelvis[3]= {}, vPelvis[3]= {};
  double pSwFtInH[3]= {}, vSwFtInH[3]= {};
  double orSwFt[3]= {}, dorSwFt[3]= {};
  double qSt[6]= {}, qStAbs[6]= {}, qStAbsMed[6]= {}, dqSt[6]= {}, qSw_[6]= {}, qSensAbsMed[NUM]= {}, tauAnkTorque[2]= {};
  double imuOrientation[3]= {}, angRates[3]= {}, aPelvis[3]= {}, thr=0, thp=0, thy=0;
  double pPelvisTest[3]= {}, pPelvisAbs[3]= {}, pPelvisAbsMed[3]= {}, vPelvisAbs[3]= {}, pPelvisFK[3]= {}, vPelvisFk[3]= {};
  double t_=0, s=0, tInStep=0, tmeTemp=0;
  double kv=0, x0=0, vxDes=0, vxOld=0;
  double dVx = 0; //to check
  std::vector<std::vector<double>> qSensAbsVec;
  double lM=0;
  double forceRightAnkleF[3]= {}, forceLeftAnkleF[3]= {}, torqueRightAnkleF[3]= {}, torqueLeftAnkleF[3]= {};
  double kRaw=0;
  std::vector<double> pyMidVec;
  double thPelvis[3]= {}, swFtPos[3]= {}, dswFtPos[3]= {};
  double TIME_WALK = 10, ZERO_S = 0.01, TIME_REWALK = 10, DTm;
  double T = 0;
  double pyEnd= 0;
  WalkingController3 walkingController3;

  double f_d = 2;

  bool flagStopCommand = false;
  double f0AtStop = f_d;
  double timeAtStop = 0, f_dAtStop = f_d;
  double timeInAir = 0;
  double STEP_LENGTH = 0; // 0.07
  bool flagStart = false;
  int sg = -1;
  int stepNumber = 1;
  int indexSt[6] = {4, 10, 11, 12, 13, 14}, indexSw[6] = {3, 5, 6, 7, 8, 9};
  unsigned int side = 0, oldSide = side;
  double qSensInit[NUM]= {};
  double thpF= {}, thrF= {}, thpF_init= {}, thrF_init= {}, thPelvisInit[3]= {}, swFtPosInit[3]= {}, orSwFtInit[3]= {};
  double px= 0, px0= 0, py= 0, pxswf= 0, pyswf= 0, vx= 0, vy= 0, vxswf= 0, vyswf= 0, ax= 0, px_old= 0, py_old= 0, pxswf_old= 0, pyswf_old= 0, dthpF= 0, dthrF= 0, swFtPitch= 0, swFtPitch_old= 0, dswFtPitch= 0, swFtRoll= 0, swFtRoll_old= 0, dswFtRoll= 0;
  double k_vec[M5]= {}, sumk= 0, forceLeftAnkleZMed= 0, forceRightAnkleZMed= 0;
  double vec_px[M]= {}, vec_py[M]= {}, vec_pxswf[M5]= {}, vec_pyswf[M5]= {}, vec_swFtPitch[M3]= {}, vec_swFtRoll[M3]= {};
  double k= 0, pxAbs= 0, pxAbsMed= 0, pyAbs= 0, vxAbs= 0, vxAbsF= 0, vyAbs= 0, pxAbs_old= 0, pyAbs_old= 0, vec_pxAbs[M], vec_pyAbs[M]= {}, vxFK= 0, avgFreq = f_d;
  double deltaX = 0, deltaY = 0;
  double t0 = 0;
  int inAir = 0;
  bool flagMidStep = false;
  double frontalBias = 0;
  double QswKMid_INIT = 0.8, QswKMid = QswKMid_INIT;
  int firstStopN = 60, stepsToStop = 3;
  std::vector<double> freqVec{f_d, f_d};
  double SIDE_INITIAL = 0;
  double f0 = 0;
  bool startWalkingFlag = false;
  double last_step_freq = f0;
  //double handRightPosFilt; // to check if the value persists

  double pPelvisAbsF[3]= {};

  double p_pelvis_left[3]= {};
  double p_pelvis_right[3]= {};
  double velocity_straight[3]= {};

  std::vector<double> pxVec, pyVec, pxswfVec, pyswfVec, vxVec, vyVec, vxswfVec, vyswfVec, axVec, thpFVec, thrFVec, dthpFVec, dthrFVec, kVec, pxAbsVec, pyAbsVec, vxFkVec, vxAbsVec, vyAbsVec, swFtPitchVec, dswFtPitchVec, swFtRollVec, dswFtRollVec, vecPxAbs, pxAbsMedVec, vxAbsVecF, frcRVec[3], frcLVec[3], trqRVec[3], trqLVec[3];

  std::vector<double> FRxVec, FRyVec, FRzVec, FLxVec, FLyVec, FLzVec, pHand, vHandVec, pHandVectTemp, relVelVec, velTimeWind, intForceVec, forceLeftAnkleZVec, forceRightAnkleZVec, sensorExtForceVec, tmVec; // frcRyVec, frcRzVec, frcLyVec, frcLzVec, trqRyVec, trqRzVec, trqLyVec, trqLzVec

  std::vector<double> pPelvisAbsVec[3], pPelvisAbsFVec[3];

  double kF=0;
  double begsteptime = TIME_WALK;

};
