#include "tuning.hh"

const double desiredSL = 0.09;

// int main()
// {
//     // double actualPID1[8] = {198.947, 0, 11.4712, 0, 199.552, 0, 17.9922, 0};
//     // double actualPID2[8] = {198.832, 0, 20.5581, 0, 199.076, 0, 20.5581, 0};

//     // execSim(actualPID1, actualPID2);
//     double score = (readScore(1, 100) + readScore(2, 100)) / 2;
//     std::cout << "Score: " << score << std::endl;
//     // saveResults(actualPID1, actualPID2, score, true);
// }

int main()
{
    std::ofstream resOutput;
    std::ifstream dpFile;
    std::ofstream scoreFile;

    resOutput.open("/home/biorob/forceTuning/allresults.txt", std::ios_base::out | std::ios_base::app);
    resOutput << "******** Double coman stiff joint *********" << std::endl;
    resOutput.close();

    resOutput.open("/home/biorob/forceTuning/results.txt", std::ios_base::out | std::ios_base::app);
    resOutput << "******** Double coman stiff joint *********" << std::endl;
    resOutput.close();

    double params[4] = {100000,
                        5,
                        100,
                        3};

    double score = 100000;
    double iterations = 0;
    double dp[4] = {};

    VectorXd grad(4);
    VectorXd dpid(4);
    MatrixXd A(4, 4);

    while (iterations <= 1000 && score > 0.001 && true)
    {

        // Simulation execution

        execSim(params);
        score = readScore(score);
        saveResults(params, score, true);

        std::cout << "##########################" << std::endl;
        std::cout << "#### Simulation #" << iterations << ": " << score << std::endl;

        // Gradient calculation

        grad = calcGradient(params, score); // It puts the gradient in grad.txt

        std::cout << "Gradient: " << grad << std::endl;

        scoreFile.open("/home/biorob/forceTuning/score.txt", std::ios_base::out | std::ios_base::trunc);
        scoreFile << score; // Put the main score in score.txt
        scoreFile.close();

        system("/home/biorob/forceTuning/calcDP.sh"); // compute dp with grad.txt and score.txt, puts it in dp.txt

        dpFile.open("/home/biorob/forceTuning/dp.txt"); // Loads the dp
        dpFile >> dp[0];
        dpFile >> dp[1];
        dpFile >> dp[2];
        dpFile >> dp[3];

        std::cout << "dp: " << dp[0] << " " << dp[1] << " " << dp[2] << " " << dp[3] << std::endl;

        std::cout << "Actual params: ";
        for (int i = 0; i < 4; i += 1)
        {
            std::cout << params[i] << " ";
        }
        std::cout << std::endl;

        std::cout << "New params: ";
        for (int i = 0; i < 4; i += 1)
        {
            params[i] += dpid(i);
            std::cout << params[i] << " ";
        }

        iterations++;
    }
}

void modifyParam(double params[4])
{

    std::ofstream paramsOutput;
    paramsOutput.open("/home/biorob/Parameters/stiffness.txt", std::ios_base::out | std::ios_base::trunc);
    paramsOutput << params[0];
    paramsOutput.close();
    paramsOutput.open("/home/biorob/Parameters/damping.txt", std::ios_base::out | std::ios_base::trunc);
    paramsOutput << params[1];
    paramsOutput.close();
    paramsOutput.open("/home/biorob/Parameters/rotStiffness.txt", std::ios_base::out | std::ios_base::trunc);
    paramsOutput << params[2];
    paramsOutput.close();
    paramsOutput.open("/home/biorob/Parameters/rotDamping.txt", std::ios_base::out | std::ios_base::trunc);
    paramsOutput << params[3];
    paramsOutput.close();
}

double readScore(double baseScore)
{
    system("/home/biorob/forceTuning/calcScore.sh");
    std::ifstream scoreFile;
    scoreFile.open("score.txt");
    double score = 0;

    scoreFile >> score;

    return score;
}

void saveResults(double params[], double score, bool mainSim)
{
    std::ofstream resOutput;
    resOutput.open("/home/biorob/forceTuning/allresults.txt", std::ios_base::out | std::ios_base::app);

    resOutput << score << " ";

    for (int i = 0; i < 4; i++)
    {
        resOutput << params[i] << " ";
    }

    resOutput << std::endl;
    resOutput.close();

    if (mainSim)
    {
        std::ofstream resOutput;
        resOutput.open("/home/biorob/forceTuning/results.txt", std::ios_base::out | std::ios_base::app);

        resOutput << score << " ";

        for (int i = 0; i < 4; i++)
        {
            resOutput << params[i] << " ";
        }

        resOutput << std::endl;
        resOutput.close();
    }
}

void execSim(double params[4])
{
    modifyParam(params);
    system("/home/biorob/forceTuning/rungaz.sh");
}

Eigen::VectorXd calcGradient(double params[4], double baseScore)
{
    VectorXd grad(4);

    double newParams[4] = {};

    double dp = 0;
    double newScore = 0;

    for (int i = 0; i < 4; i++)
    {
        newParams[i] = params[i];
    }

    for (int i = 0; i < 4; i += 1)
    {
        dp = newParams[i] * 0.01;
        newParams[i] += dp;

        execSim(newParams);
        newScore = readScore(baseScore);
        saveResults(newParams, newScore, false);

        grad(i) = (newScore - baseScore) / dp;

        newParams[i] = params[i];

        std::cout << "### Gradient calculation: " << i + 1 << " out of 4; Score: " << newScore << std::endl;
    }

    std::ofstream gradFile;
    gradFile.open("/home/biorob/forceTuning/grad.txt", std::ios_base::out | std::ios_base::trunc);
    gradFile << grad[0] << " " << grad[1] << " " << grad[2] << " " << grad[3];
    gradFile.close();
    return grad;
}
