#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <vector>
#include <cmath>

#define MAXBUFSIZE  ((int) 1e6)

using namespace std;
using namespace Eigen;

typedef Matrix< double, Dynamic, 1, ColMajor > EVector;

int main();
void modifyParam(double params[4]);
double readScore(double baseScore);
void saveResults(double params[], double score, bool mainSim);
void execSim(double params[4]);
Eigen::VectorXd calcGradient(double params[4], double baseScore);