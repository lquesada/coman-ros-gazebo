#include "tuning.hh"

const double desiredSL = 0.08;

// int main()
// {
//     // double actualPID1[8] = {198.947, 0, 11.4712, 0, 199.552, 0, 17.9922, 0};
//     // double actualPID2[8] = {198.832, 0, 20.5581, 0, 199.076, 0, 20.5581, 0};

//     // execSim(actualPID1, actualPID2);
//     double score = (readScore(1, 100) + readScore(2, 100)) / 2;
//     std::cout << "Score: " << score << std::endl;
//     // saveResults(actualPID1, actualPID2, score, true);
// }

int main()
{
    double actualPID1[8] = {199.435, 0, 12.3232, 0, 199.386, 0, 15.1758, 0};
    double actualPID2[8] = {200.649, 0, 21.5551, 0, 199.42, 0, 24.2702, 0};

    double score = 100000;
    double iterations = 0;

    VectorXd grad(8);
    VectorXd dpid(8);
    MatrixXd A(8, 8);

    while (iterations <= 1000 && score > 0.001 && true)
    {
        execSim(actualPID1, actualPID2);
        score = (readScore(1, score) + readScore(2, score)) / 2;
        saveResults(actualPID1, actualPID2, score, true);

        std::cout << "##########################" << std::endl;
        std::cout << "#### Simulation #" << iterations << ": " << score << std::endl;

        grad = calcGradient(actualPID1, actualPID2, score);

        std::cout << "Gradient: " << grad << std::endl;

        A = grad * grad.transpose();

        std::cout << "A: " << A << std::endl;

        dpid = -0.1 * score * A.bdcSvd(ComputeThinU | ComputeThinV).solve(grad);

        std::cout << "dp: " << dpid << std::endl;

        std::cout << "Actual PID1: ";
        for (int i = 0; i < 4; i += 1)
        {
            std::cout << actualPID1[i] << " ";
        }
        std::cout << std::endl;

        std::cout << "New PID1: ";
        for (int i = 0; i < 8; i += 1)
        {
            actualPID1[i] += dpid(int(i / 2));
            std::cout << actualPID1[i] << " ";
        }

        // std::cout << "Actual PID2: ";
        // for (int i = 0; i < 8; i += 1)
        // {
        //     std::cout << actualPID2[i] << " ";
        // }
        // std::cout << std::endl;

        // std::cout << "New PID2: ";
        // for (int i = 0; i < 4; i += 1)
        // {
        //     actualPID2[i] += dpid(i) + 4);
        //     std::cout << actualPID2[i] << " ";
        // }
        // std::cout << std::endl;

        iterations++;
    }
}

void modifyPID(double pid1[], double pid2[])
{
    std::ofstream paramsOutput;
    paramsOutput.open("/home/biorob/Tuning/pid1.txt", std::ios_base::out | std::ios_base::trunc);

    for (int i = 0; i < 8; i++)
    {
        paramsOutput << pid1[i] << std::endl;
    }

    paramsOutput.close();

    paramsOutput.open("/home/biorob/Tuning/pid2.txt", std::ios_base::out | std::ios_base::trunc);

    for (int i = 0; i < 8; i++)
    {
        paramsOutput << pid2[i] << std::endl;
    }

    paramsOutput.close();
}

double readScore(int numComan, double baseScore)
{
    MatrixXd data = readMatrix("/home/biorob/Tuning/data" + std::to_string(numComan) + ".txt");

    VectorXd timeVec = data.col(0);
    VectorXd followVec = data.col(1);
    VectorXd posVec = data.col(2);
    VectorXd pPelvVec = data.col(3);
    VectorXd sideVec = data.col(4);

    double Nmax = timeVec.size();

    if (Nmax < 10000)
    {
        std::cout << "Bad simulation" << std::endl;
        return baseScore * 1.1;
    }

    int currentS = sideVec[1];

    double firstPPos = 0;
    double secondPPos = 0;

    std::vector<double> oddSteps;
    std::vector<double> evenSteps;

    int oddEven = 1;

    for (int i = 1; i < Nmax - 4; i++)
    {
        if (sideVec[i] != currentS)
        {
            currentS = sideVec[i];

            secondPPos = pPelvVec[i];

            if (oddEven == 1)
            {
                oddSteps.push_back((secondPPos - firstPPos));
            }
            else
            {
                evenSteps.push_back((secondPPos - firstPPos));
            }
            oddEven = 1 - oddEven;

            firstPPos = pPelvVec[i + 3];
        }
    }

    if (oddSteps.size() < 20 || evenSteps.size() < 20)
    {
        std::cout << "Bad simulation" << std::endl;
        return baseScore * 1.1;
    }

    double meanOdd = std::accumulate(oddSteps.begin() + 13, oddSteps.end() - 5, 0.0) / (oddSteps.size() - 18);
    double meanEven = std::accumulate(evenSteps.begin() + 13, evenSteps.end() - 5, 0.0) / (evenSteps.size() - 18);

    double scoreSym = (1 - meanOdd / meanEven) * 100;
    double scoreOdd = (desiredSL - meanOdd) / desiredSL * 100;
    double scoreEven = (desiredSL - meanEven) / desiredSL * 100;

    double scoreTot = sqrt(pow((0.5 * scoreEven + 0.5 * scoreOdd), 2) + pow(scoreSym, 2));

    // std::cout << "meanOdd: " << meanOdd << std::endl
    //           << "meanEven: " << meanEven << std::endl
    //           << "scoreSym: " << scoreSym << std::endl
    //           << "scoreOdd: " << scoreOdd << std::endl
    //           << "scoreEven: " << scoreEven << std::endl;

    return scoreTot;
}

void saveResults(double pid1[], double pid2[], double score, bool mainSim)
{
    std::ofstream resOutput;
    resOutput.open("/home/biorob/Tuning/allresults.txt", std::ios_base::out | std::ios_base::app);

    resOutput << score << " ";

    for (int i = 0; i < 8; i++)
    {
        resOutput << pid1[i] << " ";
    }

    for (int i = 0; i < 8; i++)
    {
        resOutput << pid2[i] << " ";
    }

    resOutput << std::endl;

    resOutput << std::endl;
    resOutput.close();

    if (mainSim)
    {
        std::ofstream resOutput;
        resOutput.open("/home/biorob/Tuning/results.txt", std::ios_base::out | std::ios_base::app);

        resOutput << score << " ";

        for (int i = 0; i < 8; i++)
        {
            resOutput << pid1[i] << " ";
        }

        for (int i = 0; i < 8; i++)
        {
            resOutput << pid2[i] << " ";
        }

        resOutput << std::endl;
        resOutput.close();
    }
}

void execSim(double pid1[], double pid2[])
{
    modifyPID(pid1, pid2);
    system("/home/biorob/Tuning/rungaz.sh");
}

Eigen::MatrixXd readMatrix(std::string filename)
{
    int cols = 0, rows = 0;
    double buff[MAXBUFSIZE];

    // Read numbers from file into buffer.
    ifstream infile;
    infile.open(filename);
    while (!infile.eof())
    {
        string line;
        getline(infile, line);

        int temp_cols = 0;
        stringstream stream(line);
        while (!stream.eof())
            stream >> buff[cols * rows + temp_cols++];

        if (temp_cols == 0)
            continue;

        if (cols == 0)
            cols = temp_cols;

        rows++;
    }

    infile.close();

    rows--;

    // Populate matrix with numbers.
    MatrixXd result(rows, cols);
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < cols; j++)
            result(i, j) = buff[cols * i + j];

    return result;
}

Eigen::VectorXd calcGradient(double pid1[], double pid2[], double baseScore)
{
    VectorXd grad(8);

    double newPID1[8] = {};
    double newPID2[8] = {};

    double dp = 0;
    double newScore = 0;

    for (int i = 0; i < 8; i++)
    {
        newPID1[i] = pid1[i];
        newPID2[i] = pid2[i];
    }

    for (int i = 0; i < 8; i += 2)
    {
        dp = newPID1[i] * 0.01;
        newPID1[i] += dp;

        execSim(newPID1, newPID2);
        newScore = (readScore(1, baseScore) + readScore(2, baseScore)) / 2;
        saveResults(newPID1, newPID2, newScore, false);

        grad(int(i / 2)) = (newScore - baseScore) / dp;

        newPID1[i] = pid1[i];

        std::cout << "### Gradient calculation: " << int(i / 2) + 1 << " out of 8; Score: " << newScore << std::endl;
    }

    for (int i = 0; i < 8; i += 2)
    {
        dp = newPID2[i] * 0.01;
        newPID2[i] += dp;

        execSim(newPID1, newPID2);
        newScore = (readScore(1, baseScore) + readScore(2, baseScore)) / 2;
        saveResults(newPID1, newPID2, newScore, false);

        grad(int(i / 2) + 4) = (newScore - baseScore) / dp;

        newPID2[i] = pid2[i];

        std::cout << "### Gradient calculation: " << int(i / 2) + 5 << " out of 8; Score: " << newScore << std::endl;
    }

    return grad;
}
