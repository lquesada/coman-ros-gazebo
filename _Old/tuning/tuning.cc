#include "tuning.hh"

const double desiredSL = 0.086;

// int main()
// {
//     // double actualPID1[8] = {198.947, 0, 11.4712, 0, 199.552, 0, 17.9922, 0};
//     // double actualPID2[8] = {198.832, 0, 20.5581, 0, 199.076, 0, 20.5581, 0};

//     // execSim(actualPID1, actualPID2);
//     double score = (readScore(1, 100) + readScore(2, 100)) / 2;
//     std::cout << "Score: " << score << std::endl;
//     // saveResults(actualPID1, actualPID2, score, true);
// }

int main()
{
    std::ofstream resOutput;
    resOutput.open("/home/biorob/Tuning/allresults.txt", std::ios_base::out | std::ios_base::app);
    resOutput << "******** Double coman PID *********" << std::endl;
    resOutput.close();

    resOutput.open("/home/biorob/Tuning/results.txt", std::ios_base::out | std::ios_base::app);
    resOutput << "******** Double coman PID *********" << std::endl;
    resOutput.close();

    // double actualPID[8] = {220.236, -14.1136, 2.02636, -15.3459, 220.236, -14.1136, 2.02636, -15.3459};
    double actualPID[8] = {218.537,-13.6738,13.2296,-11.5768,220.286,-10.2987,-1.21776,-14.2277}; 


    double score = 100000;
    double iterations = 0;

    VectorXd grad(8);
    VectorXd dpid(8);
    MatrixXd A(8, 8);

    while (iterations <= 1000 && score > 0.001 && true)
    {
        execSim(actualPID);
        score = (readScore(1, score) + readScore(2, score)) / 2;
        saveResults(actualPID, score, true);

        std::cout << "##########################" << std::endl;
        std::cout << "#### Simulation #" << iterations << ": " << score << std::endl;

        grad = calcGradient(actualPID, score);

        std::cout << "Gradient: " << grad << std::endl;

        A = grad * grad.transpose();

        std::cout << "A: " << A << std::endl;

        dpid = -1 * score * A.bdcSvd(ComputeThinU | ComputeThinV).solve(grad);

        std::cout << "dp: " << dpid << std::endl;

        std::cout << "Actual PID: ";
        for (int i = 0; i < 8; i += 1)
        {
            std::cout << actualPID[i] << " ";
        }
        std::cout << std::endl;

        std::cout << "New PID1: ";
        for (int i = 0; i < 8; i += 1)
        {
            actualPID[i] += dpid(i);
            std::cout << actualPID[i] << " ";
        }

        iterations++;
    }
}

void modifyPID(double pid[])
{
    std::ofstream paramsOutput;
    paramsOutput.open("/home/biorob/Parameters/pid1.txt", std::ios_base::out | std::ios_base::trunc);

    for (int i = 0; i < 4; i++)
    {
        paramsOutput << pid[i] << std::endl;
    }

    paramsOutput.close();

    paramsOutput.open("/home/biorob/Parameters/pid2.txt", std::ios_base::out | std::ios_base::trunc);

    for (int i = 4; i < 8; i++)
    {
        paramsOutput << pid[i] << std::endl;
    }

    paramsOutput.close();
}

double readScore(int comNum, double baseScore)
{
    MatrixXd data = readMatrix("/home/biorob/SimulationData/tuningData" + std::to_string(comNum) + ".txt");

    VectorXd timeVec = data.col(0);
    VectorXd pPelvVec = data.col(1);
    VectorXd sideVec = data.col(2);

    double Nmax = timeVec.size();

    if (Nmax < 20000)
    {
        std::cout << std::endl
                  << "!!!!!! Bad simulation !!!!!!" << std::endl
                  << "Time for coman " + std::to_string(comNum) + ": " << timeVec(Nmax - 1) << std::endl;
        return baseScore * 1.1;
    }

    int currentS = sideVec[1];

    double firstPPos = 0;
    double secondPPos = 0;

    std::vector<double> oddSteps;
    std::vector<double> evenSteps;

    int oddEven = 1;

    for (int i = 10000; i < Nmax - 10000; i++)
    {
        if (sideVec[i] != currentS)
        {
            currentS = sideVec[i];

            secondPPos = pPelvVec[i];

            if (oddEven == 1)
            {
                oddSteps.push_back((secondPPos - firstPPos));
            }
            else
            {
                evenSteps.push_back((secondPPos - firstPPos));
            }
            oddEven = 1 - oddEven;

            firstPPos = pPelvVec[i + 3];
        }
    }

    double meanOdd = std::accumulate(oddSteps.begin(), oddSteps.end(), 0.0) / oddSteps.size();
    double meanEven = std::accumulate(evenSteps.begin(), evenSteps.end(), 0.0) / evenSteps.size();

    double scoreSym = (1 - meanOdd / meanEven) * 100;
    double scoreOdd = (desiredSL - meanOdd) / desiredSL * 100;
    double scoreEven = (desiredSL - meanEven) / desiredSL * 100;

    double scoreTot = sqrt(pow((0.5 * scoreEven + 0.5 * scoreOdd), 2) + pow(scoreSym, 2));

    // std::cout << "meanOdd: " << meanOdd << std::endl
    //           << "meanEven: " << meanEven << std::endl
    //           << "scoreSym: " << scoreSym << std::endl
    //           << "scoreOdd: " << scoreOdd << std::endl
    //           << "scoreEven: " << scoreEven << std::endl;

    return scoreTot;
}

void saveResults(double pid[], double score, bool mainSim)
{
    std::ofstream resOutput;
    resOutput.open("/home/biorob/Tuning/allresults.txt", std::ios_base::out | std::ios_base::app);

    resOutput << score << " ";

    for (int i = 0; i < 8; i++)
    {
        resOutput << pid[i] << " ";
    }

    resOutput << std::endl;
    resOutput.close();

    if (mainSim)
    {
        std::ofstream resOutput;
        resOutput.open("/home/biorob/Tuning/results.txt", std::ios_base::out | std::ios_base::app);

        resOutput << score << " ";

        for (int i = 0; i < 8; i++)
        {
            resOutput << pid[i] << " ";
        }

        resOutput << std::endl;
        resOutput.close();
    }
}

void execSim(double pid[])
{
    modifyPID(pid);
    system("/home/biorob/Parameters/rungaz.sh");
}

Eigen::MatrixXd readMatrix(std::string filename)
{
    int cols = 0, rows = 0;
    double buff[MAXBUFSIZE];

    // Read numbers from file into buffer.
    ifstream infile;
    infile.open(filename);
    while (!infile.eof())
    {
        string line;
        getline(infile, line);

        int temp_cols = 0;
        stringstream stream(line);
        while (!stream.eof())
            stream >> buff[cols * rows + temp_cols++];

        if (temp_cols == 0)
            continue;

        if (cols == 0)
            cols = temp_cols;

        rows++;
    }

    infile.close();

    rows--;

    // Populate matrix with numbers.
    MatrixXd result(rows, cols);
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < cols; j++)
            result(i, j) = buff[cols * i + j];

    return result;
}

Eigen::VectorXd calcGradient(double pid[], double baseScore)
{
    VectorXd grad(8);

    double newPID[8] = {};

    double dp = 0;
    double newScore = 0;

    for (int i = 0; i < 8; i++)
    {
        newPID[i] = pid[i];
    }

    for (int i = 0; i < 8; i += 1)
    {
        dp = newPID[i] * 0.1;
        newPID[i] += dp;

        execSim(newPID);
        newScore = (readScore(1, baseScore) + readScore(2, baseScore)) / 2;
        saveResults(newPID, newScore, false);

        grad(i) = (newScore - baseScore) / dp;

        newPID[i] = pid[i];

        std::cout << "### Gradient calculation: " << i + 1 << " out of 8; Score: " << newScore << std::endl;
    }

    return grad;
}
