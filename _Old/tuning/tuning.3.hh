#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <vector>
#include <cmath>

#define MAXBUFSIZE  ((int) 1e6)

using namespace std;
using namespace Eigen;

int main();
void modifyParam(double param);
double readScore(int comNum, double baseScore);
void saveResults(double param, double score);
void execSim(double param);
Eigen::MatrixXd readMatrix(std::string filename);