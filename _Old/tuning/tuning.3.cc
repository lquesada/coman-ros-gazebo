#include "tuning.hh"

const double desiredSL = 0.09;

// int main()
// {
//     // double actualPID1[8] = {198.947, 0, 11.4712, 0, 199.552, 0, 17.9922, 0};
//     // double actualPID2[8] = {198.832, 0, 20.5581, 0, 199.076, 0, 20.5581, 0};

//     // execSim(actualPID1, actualPID2);
//     double score = (readScore(1, 100) + readScore(2, 100)) / 2;
//     std::cout << "Score: " << score << std::endl;
//     // saveResults(actualPID1, actualPID2, score, true);
// }

int main()
{
    double score = 100000;
    int iterations = 0;

    for (double rotDamping = 0; rotDamping < 2; rotDamping += 0.1)
    {
        execSim(rotDamping);
        score = (readScore(1, score) + readScore(2, score)) / 2;
        saveResults(rotDamping, score);

        std::cout << "##########################" << std::endl;
        std::cout << "#### Simulation #" << iterations << ": " << score << std::endl;
        std::cout << "#### Rotational Damping = " << rotDamping << std::endl;
        iterations++;
    }
}

void modifyParam(double param)
{
    std::ofstream paramsOutput;
    paramsOutput.open("/home/biorob/Tuning/rotDamping.txt", std::ios_base::out | std::ios_base::trunc);
    paramsOutput << param;
    paramsOutput.close();
}

double readScore(int comNum, double baseScore)
{
    MatrixXd data = readMatrix("/home/biorob/Tuning/data" + std::to_string(comNum) + ".txt");

    VectorXd timeVec = data.col(0);
    VectorXd followVec = data.col(1);
    VectorXd posVec = data.col(2);
    VectorXd pPelvVec = data.col(3);
    VectorXd sideVec = data.col(4);

    double Nmax = timeVec.size();

    if (Nmax < 10000)
    {
        std::cout << std::endl
                  << "!!!!!! Bad simulation !!!!!!" << std::endl
                  << "Time for coman " + std::to_string(comNum) + ": " << timeVec(Nmax - 1) << std::endl;
        return baseScore * 1.1;
    }

    int currentS = sideVec[1];

    double firstPPos = 0;
    double secondPPos = 0;

    std::vector<double> oddSteps;
    std::vector<double> evenSteps;

    int oddEven = 1;

    for (int i = 1; i < Nmax - 4; i++)
    {
        if (sideVec[i] != currentS)
        {
            currentS = sideVec[i];

            secondPPos = pPelvVec[i];

            if (oddEven == 1)
            {
                oddSteps.push_back((secondPPos - firstPPos));
            }
            else
            {
                evenSteps.push_back((secondPPos - firstPPos));
            }
            oddEven = 1 - oddEven;

            firstPPos = pPelvVec[i + 3];
        }
    }

    if (oddSteps.size() < 50 || evenSteps.size() < 50)
    {
        std::cout << std::endl
                  << "!!!!!! Bad simulation !!!!!!" << std::endl
                  << "OddSteps coman " + std::to_string(comNum) + ": " << oddSteps.size() << std::endl
                  << "evenSteps coman " + std::to_string(comNum) + ": " << evenSteps.size() << std::endl;
        return baseScore * 1.1;
    }

    double meanOdd = std::accumulate(oddSteps.begin() + int(oddSteps.size() * 0.5), oddSteps.end() - 5, 0.0) / (oddSteps.size() - (5 + int(oddSteps.size() * 0.5)));
    double meanEven = std::accumulate(evenSteps.begin() + int(evenSteps.size() * 0.5), evenSteps.end() - 5, 0.0) / (evenSteps.size() - (5 + int(evenSteps.size() * 0.5)));

    double scoreSym = (1 - meanOdd / meanEven) * 100;
    double scoreOdd = (desiredSL - meanOdd) / desiredSL * 100;
    double scoreEven = (desiredSL - meanEven) / desiredSL * 100;

    double scoreTot = sqrt(pow((0.5 * scoreEven + 0.5 * scoreOdd), 2) + pow(scoreSym, 2));

    // std::cout << "meanOdd: " << meanOdd << std::endl
    //           << "meanEven: " << meanEven << std::endl
    //           << "scoreSym: " << scoreSym << std::endl
    //           << "scoreOdd: " << scoreOdd << std::endl
    //           << "scoreEven: " << scoreEven << std::endl;

    return scoreTot;
}

void saveResults(double param, double score)
{
    std::ofstream resOutput;
    resOutput.open("/home/biorob/Tuning/resultsRotDamp.txt", std::ios_base::out | std::ios_base::app);

    resOutput << param << " " << score << std::endl;
    resOutput.close();
}

void execSim(double param)
{
    modifyParam(param);
    system("/home/biorob/Tuning/rungaz.sh");
}

Eigen::MatrixXd readMatrix(std::string filename)
{
    int cols = 0, rows = 0;
    double buff[MAXBUFSIZE];

    // Read numbers from file into buffer.
    ifstream infile;
    infile.open(filename);
    while (!infile.eof())
    {
        string line;
        getline(infile, line);

        int temp_cols = 0;
        stringstream stream(line);
        while (!stream.eof())
            stream >> buff[cols * rows + temp_cols++];

        if (temp_cols == 0)
            continue;

        if (cols == 0)
            cols = temp_cols;

        rows++;
    }

    infile.close();

    rows--;

    // Populate matrix with numbers.
    MatrixXd result(rows, cols);
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < cols; j++)
            result(i, j) = buff[cols * i + j];

    return result;
}
