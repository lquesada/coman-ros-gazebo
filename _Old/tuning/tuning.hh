#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <vector>
#include <cmath>

#define MAXBUFSIZE  ((int) 1e6)

using namespace std;
using namespace Eigen;

int main();
void modifyPID(double pid[]);
double readScore(int comNum, double baseScore);
void saveResults(double pid[], double score, bool mainSim);
void execSim(double pid[]);
Eigen::MatrixXd readMatrix(std::string filename);
Eigen::VectorXd calcGradient(double pid[], double baseScore);