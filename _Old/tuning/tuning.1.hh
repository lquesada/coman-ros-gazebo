#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <vector>
#include <cmath>

#define MAXBUFSIZE  ((int) 1e6)

using namespace std;
using namespace Eigen;

int main();
void modifyPID(double pid1[], double pid2[]);
double readScore(int numComan, double baseScore);
void saveResults(double pid1[], double pid2[], double score, bool mainSim);
void execSim(double pid1[], double pid2[]);
Eigen::MatrixXd readMatrix(std::string filename);
Eigen::VectorXd calcGradient(double pid1[], double pid2[], double baseScore);