#include "StiffJoint.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/gazebo.hh"
#include "ignition/math4/ignition/math.hh"
#include <iostream>
#include <math.h>

StiffJoint::StiffJoint(gazebo::physics::LinkPtr link1_, gazebo::physics::LinkPtr link2_)
{
    // Get the pointers to the links that will be jointed

    this->link1 = link1_;
    this->link2 = link2_;
}

void StiffJoint::Initialize()
{
    // Save the initial offsets, the joints will want to keep these offsets constant

    initialPosOffset = link2->WorldPose().Pos() - link1->WorldPose().Pos();
    initialRotOffset = link1->WorldPose().Rot() * link2->WorldPose().Rot().Inverse();

    // Logging

    // std::cerr << "Stiffness: " << this->linearStiffness << std::endl;
    // std::cerr << "rotStiffness: " << this->rotationalStiffness << std::endl;
}

void StiffJoint::Apply()
{
    //--- Here the code is vectorized in order to optimize computation, see belowe to have a clearer depiction$
    // Basically we use a spring damper model
    // The spring has a stiffness coefficient k, the force is given by F = k*distance or C = k*angle
    // Likewise the damper works such as F = -c * v and C = -c * theta'
    // This is really simple, in fact most of the code only align the different frames

    // Rotationnal offset

    rotLink1 = link1->WorldPose().Rot();
    rotLink2 = link2->WorldPose().Rot();

    currentRotOffset = rotLink1 * rotLink2.Inverse();
    offsetRotOffset = initialRotOffset * currentRotOffset.Inverse();

    offsetRotOffset.Normalize();

    torqueStiff = stiffnessLawRotational(offsetRotOffset.Euler());

    // Linear offset

    forceStiff = stiffnessLawLinear(link2->WorldPose().Pos() - link1->WorldPose().Pos() - rotLink1.RotateVector(initialPosOffset));

    // Force and torques applications

    link1->AddLinkForce(rotLink1.RotateVectorReverse(forceStiff) + rotLink1.RotateVectorReverse(dampingLawLinear(link1->WorldLinearVel())));
    link2->AddLinkForce(-rotLink2.RotateVectorReverse(forceStiff) + rotLink2.RotateVectorReverse(dampingLawLinear(link2->WorldLinearVel())));

    link1->AddTorque(torqueStiff + dampingLawRotational(link1->WorldAngularVel()));
    link2->AddTorque(-torqueStiff + dampingLawRotational(link2->WorldAngularVel()));

    //--A more clear depiction

    // // Linear offset

    // currentPosOffset = link2->WorldPose().Pos() - link1->WorldPose().Pos();
    // offsetPosOffset = currentPosOffset - initialPosOffset;
    // forceStiff = offsetPosOffset;

    // linearVelocityLink1 = link1->WorldLinearVel();
    // linearVelocityLink2 = link2->WorldLinearVel();

    // forceVel1 = dampingLawLinear(linearVelocityLink1);
    // forceVel2 = dampingLawLinear(linearVelocityLink2);

    // forceStiff = stiffnessLawLinear(forceStiff);

    // // Rotationnal offset

    // rotLink1 = link1->WorldPose().Rot();
    // rotLink2 = link2->WorldPose().Rot();

    // currentRotOffset = rotLink1 * rotLink2.Inverse();
    // offsetRotOffset = initialRotOffset * currentRotOffset.Inverse();

    // offsetRotOffset.Normalize();

    // torqueStiff = offsetRotOffset.Euler();

    // angularVel1 = link1->WorldAngularVel();
    // angularVel2 = link2->WorldAngularVel();

    // torqueStiff = stiffnessLawRotational(torqueStiff);
    // torqueVelLink1 = dampingLawRotational(angularVel1);
    // torqueVelLink2 = dampingLawRotational(angularVel2);

    // // Frame alignments

    // alignedForceStiffLink1 = rotLink1.RotateVectorReverse(forceStiff);
    // alignedForceStiffLink2 = -rotLink2.RotateVectorReverse(forceStiff);
    // alignedForceVelLink1 = rotLink1.RotateVectorReverse(forceVel1);
    // alignedForceVelLink2 = rotLink2.RotateVectorReverse(forceVel2);

    // forceLink1 = alignedForceStiffLink1 + alignedForceVelLink1;
    // forceLink2 = alignedForceStiffLink2 + alignedForceVelLink2;

    // alignedTorqueVelLink1 = rotLink1.RotateVectorReverse(torqueVelLink1);
    // alignedTorqueVelLink2 = rotLink2.RotateVectorReverse(torqueVelLink2);

    // alignedTorqueStiffLink1 = torqueStiff;
    // alignedTorqueStiffLink2 = -torqueStiff;

    // torqueLink1 = alignedTorqueStiffLink1 + alignedTorqueVelLink1;
    // torqueLink2 = alignedTorqueStiffLink2 + alignedTorqueVelLink2;

    // // Force and torques applications

    // link1->AddLinkForce(forceLink1);
    // link2->AddLinkForce(forceLink2);

    // link1->AddTorque(torqueLink1);
    // link2->AddTorque(torqueLink2);

    //-- Logging

    // std::cerr << "---------- " << link1->GetName() << std::endl;

    // std::cerr << "Initial offset: " << initialPosOffset << std::endl;
    // std::cerr << "Pos link1: " << link1->WorldPose() << std::endl;
    // std::cerr << "Pos link2: " << link2->WorldPose() << std::endl;
    // std::cerr << "Offset offset: " << offsetPosOffset << std::endl;
    // std::cerr << "Force link 1: " << forceLink1 << std::endl;
    // std::cerr << "Force link 2: " << forceLink2 << std::endl;

    // //

    // std::cerr << "Initial rot offset: " << initialRotOffset << std::endl;
    // std::cerr << "Rot link1: " << rotLink1 << std::endl;
    // std::cerr << "Rot link2: " << rotLink2 << std::endl;
    // std::cerr << "Current offset: " << currentRotOffset << std::endl;
    // std::cerr << "Offset offset: " << offsetRotOffset.Euler() << std::endl;
    // std::cerr << "Ang vel link 1: " << angularVel1 << std::endl;
    // std::cerr << "Ang vel link 2: " << angularVel2 << std::endl;
    // std::cerr << "Torque: " << torqueStiff << std::endl;
    // std::cerr << "Torque link 2: " << torqueLink2 << std::endl;
}

// Stiffness and damping laws
// They can be modified to have different stiffness/damping depending on the direction
// Or using non linear laws (not coded yet)

ignition::math::Vector3d StiffJoint::stiffnessLawLinear(ignition::math::Vector3d offsetVec)
{
    ignition::math::Vector3d vec;

    double ks[] = {linearStiffness, linearStiffness, linearStiffness};
    for (int i = 0; i < 3; i++)
    {
        vec[i] = ks[i] * offsetVec[i];
    }
    return vec;
}

ignition::math::Vector3d StiffJoint::dampingLawLinear(ignition::math::Vector3d velocityVec)
{
    double c = -linearDamping;
    return c * velocityVec;
}

ignition::math::Vector3d StiffJoint::stiffnessLawRotational(ignition::math::Vector3d offsetVec)
{
    double ks[] = {rotationalStiffness, rotationalStiffness, rotationalStiffness};

    ignition::math::Vector3d torqueVec;

    for (int i = 0; i < 3; i++)
    {
        torqueVec[i] = ks[i] * offsetVec[i] / 1.57;
    }

    return torqueVec;
}

ignition::math::Vector3d StiffJoint::dampingLawRotational(ignition::math::Vector3d velocityVec)
{
    double c = -rotationalDamping;

    ignition::math::Vector3d torqueVec;

    for (int i = 0; i < 3; i++)
    {
        torqueVec[i] = c * velocityVec[i] / 1.57;
    }

    return torqueVec;
}