template <class S>
void MeanFilter(std::vector<S> &vec, S &avgOfVec, const S &el, const unsigned int &SIZE)
{
    vec.push_back(el);

    avgOfVec = std::accumulate(vec.begin(), vec.end(),0.0)/vec.size();

    if (vec.size() > SIZE){
        vec.erase(vec.begin());
    }
}
