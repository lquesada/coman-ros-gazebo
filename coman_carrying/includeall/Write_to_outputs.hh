void Write_to_outputs(double *h, double *dh, double *hD, double *dhD, Output output, int index)
{
    h[index] = output.h;
    dh[index] = output.dh;
    hD[index] = output.hD;
    dhD[index] = output.dhD;
}