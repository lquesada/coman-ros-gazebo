#ifndef WALKINGCONTROLLER3_H
#define WALKINGCONTROLLER3_H

#include <iostream>
#include <fstream>
#include <ignition/math/Vector3.hh>
#include <ignition/math/Pose3.hh>

class WalkingController3
{
public:
  WalkingController3();
  void EvalOutputs(double s, double f0, double Q_INIT[], double qSens[], double dqSens[], double kR, double kL, int indexSt[], int indexSw[], double thpF,
                   double dthpF, double thr, double dthrF, double thy, double dthyF, double x0, double deltaX, double deltaHipPitchvy, double qSensInit[], double pSw_init[],
                   double thpF_init, double thrF_init, double thyF_init, double px0, double P_SWFTinH[], double vSwFtInH[], double Or_SWFTinH[], double dorSwFt[],
                   double orSwFtInit[], double h[], double dh[], double hD[], double dhD[], double STEP_LENGTH, double QswKMid, double yaw_curr);

  void EvalTorques(double s, double tInStep, double f_d, double f0, double STEP_LENGTH, double px0, double Q_INIT[], double qSens[], double dqSens[], double kR, double kL, double orSwFt[],
                   double tauAnkTorque[], double forceRightAnkle[], double forceLeftAnkle[], double torqueRightAnkle[], double torqueLeftAnkle[], double pPelvis[], double vxAbsF, double h[], double dh[], double hD[], double dhD[], double tauDes[], double vals[]);

  unsigned int whichComan_ = 0;

  // Hips PID

  double parameters[8] = {};

  // Navigation variables

  std::ifstream inputParams;
  double tme = 0;

  ignition::math::Pose3d poseComan1;
  ignition::math::Pose3d poseComan2;
  ignition::math::Pose3d poseTable;

  ignition::math::Pose3d prevPoseComan1;
  ignition::math::Pose3d prevPoseComan2;
  ignition::math::Pose3d prevPoseTable;

  double kp = 1.1;
  double kd = 1.7;

  int activeNav = 0;
};

#endif // WALKINGCONTROLLER2_H
