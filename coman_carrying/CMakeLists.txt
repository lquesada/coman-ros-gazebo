cmake_minimum_required(VERSION 3.0)
project(coman_carrying_plugin)

list( APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR} )

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++14)

## Finding packages

find_package(catkin REQUIRED COMPONENTS
  gazebo_ros
  roscpp
)

find_package(gazebo REQUIRED)

FIND_PACKAGE (RBDL COMPONENTS URDFReader REQUIRED)
FIND_PACKAGE (Eigen3 REQUIRED)

find_package(iDynTree REQUIRED)

## Link directories

link_directories(${GAZEBO_LIBRARY_DIRS})

file(GLOB srcAll
    "srcall/*.cc"
    "srcall/*.cpp"
)

file(GLOB inclAll
    "includeall/*.hh"
    "includeall/*.hpp"
)

file(GLOB srcCarr
    "Carrying/src/*.cc"
    "Carrying/src/*.cpp"
)

file(GLOB inclCarr
    "Carrying/include/*.hh"
    "Carrying/include/*.hpp"
)


add_library(carrying_plugin Carrying/mainGaz/Carrying.cc 
    ${srcCarr}
    ${srcAll}
    ${srcCarr}
    ${inclCarr}
)

catkin_package(
  DEPENDS
    roscpp
    gazebo_ros
)


include_directories(
  Carrying/include
  Carrying/src
  includeall
  srcall
  ${catkin_INCLUDE_DIRS}
)

include_directories(${EIGEN3_INCLUDE_DIR})

include_directories(${RBDL_INCLUDE_DIR})

include_directories(${Boost_INCLUDE_DIR} ${catkin_INCLUDE_DIRS} ${GAZEBO_INCLUDE_DIRS})

target_link_libraries(carrying_plugin ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES} ${RBDL_LIBRARY} ${RBDL_URDFReader_LIBRARY} ${iDynTree_LIBRARIES})