#include "Output.hh"
#include "bezier.hh"
Output::Output(int n_degree, double h_in, double dh_in)
{
    n = n_degree;
    h = h_in;
    dh = dh_in;
}
Output::Output(int n_degree, double h_st_in, double dh_st_in, double h_sw_in, double dh_sw_in, double k)
{
    n = n_degree;
    h = Lin_comb(h_st_in, h_sw_in, k);
    dh = Lin_comb(dh_st_in, dh_sw_in, k);
}
double Output::Lin_comb(double xst, double xsw, double k)
{
    return k * xst + (1 - k) * xsw; 
}
void Output::Set_outputD(double coeff[], double s)
{
   bezier bezierPoly;
   bezierPoly.degree = n;
   hD = bezierPoly.get_bezier(coeff, s);
   dhD = bezierPoly.get_derv_bezier(coeff, s);
}
void Output::Set_outputD(double coeff_st[], double coeff_sw[], double k, double s)
{
   bezier bezierPoly;
   bezierPoly.degree = n;
   double hD_st = bezierPoly.get_bezier(coeff_st, s);
   double dhD_st = bezierPoly.get_derv_bezier(coeff_st, s);
   double hD_sw = bezierPoly.get_bezier(coeff_sw, s);
   double dhD_sw = bezierPoly.get_derv_bezier(coeff_sw, s);
   hD = Lin_comb(hD_st, hD_sw, k);
   dhD = Lin_comb(dhD_st, dhD_sw, k);
}
void Output::Set_outputD(double hD_st, double dhD_st, double hD_sw, double dhD_sw, double k)
{
   hD = Lin_comb(hD_st, hD_sw, k);
   dhD = Lin_comb(dhD_st, dhD_sw, k);
}