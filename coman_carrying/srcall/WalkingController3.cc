#include "WalkingController3.hh"
#include "bezier.hh"
#include <cmath>
#include <iostream>
#include "SwFtPosToHip.hh"
#include "SwFtXtoHipPitch.hh"
#include "SwFtOrToAnk.hh"
#include "saturate.hh"
#include "Output.hh"
#include "Write_to_outputs.hh"

#define N 31
#define INV_KIN_FTX
//#define INV_KIN_FTY
#define INV_KIN_FTOR

using namespace std;
WalkingController3::WalkingController3()
{
    // Opening PD parameters for navigation

    inputParams.open("/home/biorob/Parameters/yawNavGain.txt");
    inputParams >> kp;
    inputParams >> kd;
    inputParams.close();

    inputParams.open("/home/biorob/Parameters/activateNav.txt");
    inputParams >> activeNav;
    inputParams.close();
}

void WalkingController3::EvalOutputs(double s, double f0, double Q_INIT[], double qSens[], double dqSens[], double kR, double kL, int indexSt[], int indexSw[], double thpF,
                                     double dthpF, double thr, double dthrF, double thyF, double dthyF, double x0, double deltaX, double deltaHipPitchvy, double qSensInit[], double pSw_init[],
                                     double thpF_init, double thrF_init, double thyF_init, double px0, double pSwFtInH[], double vSwFtInH[], double orSwFt[], double dorSwFt[],
                                     double orSwFtInit[], double h[], double dh[], double hD[], double dhD[], double STEP_LENGTH, double QswKMid, double yaw_curr)
{

    double qSw[6] = {qSens[3], qSens[5], qSens[6], qSens[7], qSens[8], qSens[9]}, qHipSwDes[2], dqHipSwDes[2];
    double pSwFtInHDes[3], orSwFtDes[3], swFtPosDes[2] = {0}, dswFtPosDes[2] = {0};
    bool swSide = 0;
    for (int i = 0; i < 6; i++)
    {
        qSw[i] = qSens[indexSw[i]];
    }
    double calSwRoll = 0.0;

    //*** Turning and navigation control

    double yaw_desired = 0;
    double E = 0;
    double dE = 0;

    if (activeNav == 1)
    {
        // Error on front coman yaw

        // E = poseComan2.Rot().Yaw();
        // dE = (prevPoseComan2.Rot().Yaw() - poseComan2.Rot().Yaw()) / 0.001;

        // Error on table yaw

        E = poseTable.Rot().Yaw();
        dE = (prevPoseTable.Rot().Yaw() - poseTable.Rot().Yaw()) / 0.001;

        // The front coman turns, the back one follows

        if (whichComan_ == 1)
        {
            yaw_desired = kp * E + kd * dE; // PD controller
        }
        else
        {
            tme = tme + 0.001;

            if (tme > 14 && tme < 74)
            {
                yaw_desired = -1.5 * (tme - 14) / 60;
            }
            else
            {
                yaw_desired = 1.5 * ((tme - 74) / 60 - 1);
            }
        }
    }

    double yawAngle = saturate(0.4 * (yaw_desired - yaw_curr), 0.1, -0.1);

    prevPoseComan1 = poseComan1;
    prevPoseComan2 = poseComan2;
    prevPoseTable = poseTable;

    //***

    const double L_KNEE_BIAS = -0.0;

    const double TORSO_YAW_D = 0, TORSO_PITCH_D = Q_INIT[1], TORSO_ROLL_D = Q_INIT[2], THP_D = 0, THR_D = 0;
    const double R_LEG_YAW_D = 0, L_LEG_YAW_D = 0, RST_KNEE_FINAL_D = 0.15, LST_KNEE_FINAL_D = 0.15 + L_KNEE_BIAS, RSW_KNEE_FINAL_D = 0.17 + 0.6 * STEP_LENGTH,
                 LSW_KNEE_FINAL_D = 0.17 + 0.6 * STEP_LENGTH + L_KNEE_BIAS;
    double SW_FT_PITCH_D = 0;
    const double R_ST_ANKLE_ROLL_D = -Q_INIT[5], R_SW_FT_ROLL_D = 0;
    const double L_ST_ANKLE_ROLL_D = -Q_INIT[10] - calSwRoll, L_SW_FT_ROLL_D = 0;
    const double R_SW_FT_YAW_D = yawAngle, L_SW_FT_YAW_D = yawAngle, R_ST_ANKLE_YAW_D = 0, L_ST_HIP_YAW_D = 0;

    double stAnkCoef = 1;
    double RST_ANKLE_PITCH_D = -stAnkCoef * (x0 + 0.24 * qSens[7]) / (0.44);
    double LST_ANKLE_PITCH_D = -stAnkCoef * (x0 + 0.24 * qSens[12]) / (0.44);

    int legFlag = 1; // Right Leg is at stance
    if (indexSw[3] == 7)
    {
        legFlag = -1;
        swSide = 1;
    }
    const double Y0 = 0.1;

    // degree of the bezier polynomial
    const int N_B = 5;
    const int TORSO_YAW_INDEX = 0, TORSO_PITCH_INDEX = 1, TORSO_ROLL_INDEX = 2, R_HIP_PITCH_INDEX = 3, L_HIP_PITCH_INDEX = 4, R_HIP_ROLL_INDEX = 5,
              L_HIP_ROLL_INDEX = 10;

    bezier bezierPoly5;
    bezierPoly5.degree = 5;

    // Torso yaw outputs
    double hTorsoYawInit = qSensInit[TORSO_YAW_INDEX];
    double hTorsoYawAlphaD[6] = {hTorsoYawInit, 0.5 * (hTorsoYawInit + TORSO_YAW_D), TORSO_YAW_D, TORSO_YAW_D, TORSO_YAW_D, TORSO_YAW_D};
    Output torsoYaw(N_B, qSens[TORSO_YAW_INDEX], dqSens[TORSO_YAW_INDEX]);
    torsoYaw.Set_outputD(hTorsoYawAlphaD, s);

    // Torso pitch outputs
    double hTorsoPitchInit = qSensInit[TORSO_PITCH_INDEX];
    double hTorsoPitchAlphaD[6] = {hTorsoPitchInit, 0.5 * (hTorsoPitchInit + TORSO_PITCH_D), TORSO_PITCH_D, TORSO_PITCH_D, TORSO_PITCH_D, TORSO_PITCH_D};
    Output torsoPitch(N_B, qSens[TORSO_PITCH_INDEX], dqSens[TORSO_PITCH_INDEX]);
    torsoPitch.Set_outputD(hTorsoPitchAlphaD, s);

    // Torso roll outputs
    double hTorsoRollInit = qSensInit[TORSO_ROLL_INDEX];
    double hTorsoRollAlphaD[6] = {hTorsoRollInit, 0.5 * (hTorsoRollInit + TORSO_ROLL_D), TORSO_ROLL_D, TORSO_ROLL_D, TORSO_ROLL_D, TORSO_ROLL_D};
    Output torsoRoll(N_B, qSens[TORSO_ROLL_INDEX], dqSens[TORSO_ROLL_INDEX]);
    torsoRoll.Set_outputD(hTorsoRollAlphaD, s);

    // Right and left hip pitch outputs
    double hSwFtXD, dhSwFtXD, hSwFtX, dhSwFtX, hSwFtXInit;
    double swFtXD = x0 + deltaX;
    hSwFtX = pSwFtInH[0];
    dhSwFtX = vSwFtInH[0];
    hSwFtXInit = pSw_init[0];
    double hSwFtXAlphaD[6] = {hSwFtXInit, (1 / 2 * hSwFtXInit + 1 / 2 * swFtXD), (1 / 2 * hSwFtXInit + 1 / 2 * swFtXD), (1 / 3 * hSwFtXInit + 2 / 3 * swFtXD), swFtXD, swFtXD};
    hSwFtXD = bezierPoly5.get_bezier(hSwFtXAlphaD, s);
    dhSwFtXD = bezierPoly5.get_derv_bezier(hSwFtXAlphaD, s);

    double hPelvisPitchD, dhPelvisPitchD, hPelvisPitch, dhPelvisPitch, hPelvisPitchInit;
    hPelvisPitch = thpF;
    dhPelvisPitch = dthpF;
    hPelvisPitchInit = thpF_init;
    double hPelvisPitchAlphaD[6] = {hPelvisPitchInit, 0.5 * (hPelvisPitchInit + THP_D), THP_D, THP_D, THP_D, THP_D};
    hPelvisPitchD = bezierPoly5.get_bezier(hPelvisPitchAlphaD, s);
    dhPelvisPitchD = bezierPoly5.get_derv_bezier(hPelvisPitchAlphaD, s);

    double hSwFtYD, dhSwFtYD, hSwFtY, dhSwFtY, hSwFtYInit;
    const double swFtYD = legFlag * Y0 + deltaHipPitchvy; //TEMP
    hSwFtY = pSwFtInH[1];
    dhSwFtY = vSwFtInH[1]; //TEMP
    hSwFtYInit = pSw_init[1];
    double hSwFtYAlphaD[6] = {hSwFtYInit, 0.5 * (hSwFtYInit + 1.1 * swFtYD), 1.1 * swFtYD, swFtYD, swFtYD, swFtYD};
    hSwFtYD = bezierPoly5.get_bezier(hSwFtYAlphaD, s);
    dhSwFtYD = bezierPoly5.get_derv_bezier(hSwFtYAlphaD, s);

    double hPelvisRollD, dhPelvisRollD, hPelvisRoll, dhPelvisRoll, hPelvisRollInit;
    hPelvisRoll = thr;
    dhPelvisRoll = dthrF;
    hPelvisRollInit = thrF_init;
    double hPelvisRollAlphaD[6] = {hPelvisRollInit, 0.5 * (hPelvisRollInit + THR_D), THR_D, THR_D, THR_D, THR_D};
    hPelvisRollD = bezierPoly5.get_bezier(hPelvisRollAlphaD, s);
    dhPelvisRollD = bezierPoly5.get_derv_bezier(hPelvisRollAlphaD, s);

    swFtPosDes[0] = hSwFtXD;
    dswFtPosDes[0] = dhSwFtXD;
    swFtPosDes[1] = hSwFtYD;
    dswFtPosDes[1] = dhSwFtYD;

    double qRhipDes[2], dqRhipDes[2];
    double qR[6] = {qSens[3], qSens[5], qSens[6], qSens[7], qSens[8], qSens[9]};
    SwFtXtoHipPitch(swFtPosDes[0], dswFtPosDes[0], qR, 1, qRhipDes, dqRhipDes);
    Output rHipPitch(N_B, -thpF, -dthpF, qSens[R_HIP_PITCH_INDEX], dqSens[R_HIP_PITCH_INDEX], kR);
    rHipPitch.Set_outputD(-hPelvisPitchD, -dhPelvisPitchD, qRhipDes[0], dqRhipDes[0], kR);

    double qLhipDes[2], dqLhipDes[2];
    double qL[6] = {qSens[4], qSens[10], qSens[11], qSens[12], qSens[13], qSens[14]};
    SwFtXtoHipPitch(swFtPosDes[0], dswFtPosDes[0], qL, 0, qLhipDes, dqLhipDes);
    Output lHipPitch(N_B, -thpF, -dthpF, qSens[L_HIP_PITCH_INDEX], dqSens[L_HIP_PITCH_INDEX], kL);
    lHipPitch.Set_outputD(-hPelvisPitchD, -dhPelvisPitchD, qLhipDes[0], dqLhipDes[0], kL);

    // Right hip roll outputs
    Output rHipRoll(N_B, qSens[R_HIP_ROLL_INDEX] - hPelvisRoll, dqSens[R_HIP_ROLL_INDEX] - dhPelvisRoll, qSens[R_HIP_ROLL_INDEX] - hPelvisRoll, dqSens[R_HIP_ROLL_INDEX], kR);
    rHipRoll.Set_outputD(Q_INIT[R_HIP_ROLL_INDEX] - hPelvisRollD, -dhPelvisRollD, Q_INIT[R_HIP_ROLL_INDEX] + deltaHipPitchvy, 0, kR);

    // Left hip roll outputs
    Output lHipRoll(N_B, qSens[L_HIP_ROLL_INDEX] - hPelvisRoll, dqSens[L_HIP_ROLL_INDEX] - dhPelvisRoll, qSens[L_HIP_ROLL_INDEX] - hPelvisRoll, dqSens[L_HIP_ROLL_INDEX], kL);
    lHipRoll.Set_outputD(Q_INIT[L_HIP_ROLL_INDEX] - hPelvisRollD, -dhPelvisRollD, Q_INIT[L_HIP_ROLL_INDEX] + deltaHipPitchvy, 0, kL);

#ifdef INV_KIN_FTY
    h[5] = k * (qSens[5] - hPelvisRoll) + (1 - k) * qSens[5];
    dh[5] = k * (dqSens[5] - dhPelvisRoll) + (1 - k) * dqSens[5];
    hD[5] = k * (Q_INIT[5] - hPelvisRollD) + (1 - k) * hSwHipRollD;
    dhD[5] = k * (0 - dhPelvisRollD) + (1 - k) * dhSwHipRollD;

    h[10] = (1 - k) * (qSens[10] - hPelvisRoll) + k * qSens[10];
    dh[10] = (1 - k) * (dqSens[10] - dhPelvisRoll) + k * dqSens[10];
    hD[10] = (1 - k) * (Q_INIT[10] - hPelvisRollD) + k * hSwHipRollD;
    dhD[10] = (1 - k) * (0 - dhPelvisRollD) + k * dhSwHipRollD;
#endif
    double hRStKneeInit;
    hRStKneeInit = qSensInit[7];
    double hRStKneeAlphaD[6] = {hRStKneeInit, 0.5 * (hRStKneeInit + RST_KNEE_FINAL_D), RST_KNEE_FINAL_D, RST_KNEE_FINAL_D, RST_KNEE_FINAL_D, RST_KNEE_FINAL_D};

    double hLStKneeInit;
    hLStKneeInit = qSensInit[12];
    double hLStKneeAlphaD[6] = {hLStKneeInit, 0.5 * (hLStKneeInit + LST_KNEE_FINAL_D), LST_KNEE_FINAL_D, LST_KNEE_FINAL_D, LST_KNEE_FINAL_D, LST_KNEE_FINAL_D};

    double hRSwKneeInit;
    hRSwKneeInit = qSensInit[7];
    double hRSwKneeAlphaD[6] = {hRSwKneeInit, 0.75 * QswKMid, QswKMid, 0.5, RSW_KNEE_FINAL_D, RSW_KNEE_FINAL_D};

    double hLSwKneeInit;
    hLSwKneeInit = qSensInit[12];
    double hLSwKneeAlphaD[6] = {hLSwKneeInit, 0.75 * QswKMid, QswKMid, 0.5, LSW_KNEE_FINAL_D, LSW_KNEE_FINAL_D};

    Output rKnee(N_B, qSens[7], dqSens[7]);
    rKnee.Set_outputD(hRStKneeAlphaD, hRSwKneeAlphaD, kR, s);

    Output lKnee(N_B, qSens[12], dqSens[12]);
    lKnee.Set_outputD(hLStKneeAlphaD, hLSwKneeAlphaD, kL, s);

    // Right and left ankle pitch
    double hRStAnkPitchD, dhRStAnkPitchD, hRStAnkPitch, dhRStAnkPitch, hRStAnkPitchInit;
    hRStAnkPitchInit = qSensInit[8];
    double hRStAnkPitchAlphaD[6] = {hRStAnkPitchInit, 0.5 * (hRStAnkPitchInit + RST_ANKLE_PITCH_D), 1 / 4 * hRStAnkPitchInit + 3 / 4 * RST_ANKLE_PITCH_D, RST_ANKLE_PITCH_D, RST_ANKLE_PITCH_D, RST_ANKLE_PITCH_D};
    hRStAnkPitchD = bezierPoly5.get_bezier(hRStAnkPitchAlphaD, s);
    dhRStAnkPitchD = bezierPoly5.get_derv_bezier(hRStAnkPitchAlphaD, s);

    double hSwFtPitchD, dhSwFtPitchD, hSwFtPitch, dhSwFtPitch, hSwFtPitchInit;
    hSwFtPitch = orSwFt[1];
    dhSwFtPitch = dqSens[indexSw[4]]; // or dorSwFt[1]?
    hSwFtPitchInit = orSwFtInit[1];
    double hSwFtPitchAlphaD[6] = {hSwFtPitchInit, 0.5 * (hSwFtPitchInit + SW_FT_PITCH_D), SW_FT_PITCH_D, SW_FT_PITCH_D, SW_FT_PITCH_D, SW_FT_PITCH_D};
    hSwFtPitchD = bezierPoly5.get_bezier(hSwFtPitchAlphaD, s);
    dhSwFtPitchD = bezierPoly5.get_derv_bezier(hSwFtPitchAlphaD, s);

    double hLStAnkPitchD, dhLStAnkPitchD, hLStAnkPitch, dhLStAnkPitch, hLStAnkPitchInit;
    hLStAnkPitchInit = qSensInit[13];
    double hLStAnkPitchAlphaD[6] = {hLStAnkPitchInit, 0.5 * (hLStAnkPitchInit + LST_ANKLE_PITCH_D), 1 / 3 * hLStAnkPitchInit + 3 / 4 * LST_ANKLE_PITCH_D, LST_ANKLE_PITCH_D, LST_ANKLE_PITCH_D, LST_ANKLE_PITCH_D};
    hLStAnkPitchD = bezierPoly5.get_bezier(hLStAnkPitchAlphaD, s);
    dhLStAnkPitchD = bezierPoly5.get_derv_bezier(hLStAnkPitchAlphaD, s);

    // Right and left ankle roll
    double hRStAnkRollD, dhRStAnkRollD, hRStAnkRoll, dhRStAnkRoll, hRStAnkRollInit;
    hRStAnkRollInit = qSensInit[9];
    double hRStAnkRollAlphaD[6] = {hRStAnkRollInit, 0.5 * (hRStAnkRollInit + R_ST_ANKLE_ROLL_D), R_ST_ANKLE_ROLL_D, R_ST_ANKLE_ROLL_D, R_ST_ANKLE_ROLL_D, R_ST_ANKLE_ROLL_D};
    hRStAnkRollD = bezierPoly5.get_bezier(hRStAnkRollAlphaD, s);
    dhRStAnkRollD = bezierPoly5.get_derv_bezier(hRStAnkRollAlphaD, s);

    double hRSwFtRollD, dhRSwFtRollD, hRSwFtRoll, dhRSwFtRoll, hRSwFtRollInit;
    hRSwFtRoll = orSwFt[0];
    dhRSwFtRoll = dqSens[9]; // or dorSwFt[1]?
    hRSwFtRollInit = orSwFtInit[0];
    double hRSwFtRollAlphaD[6] = {hRSwFtRollInit, 0.5 * (hRSwFtRollInit + R_SW_FT_ROLL_D), R_SW_FT_ROLL_D, R_SW_FT_ROLL_D, R_SW_FT_ROLL_D, R_SW_FT_ROLL_D};
    hRSwFtRollD = bezierPoly5.get_bezier(hRSwFtRollAlphaD, s);
    dhRSwFtRollD = bezierPoly5.get_derv_bezier(hRSwFtRollAlphaD, s);

    double hLStAnkRollD, dhLStAnkRollD, hLStAnkRoll, dhLStAnkRoll, hLStAnkRollInit;
    hLStAnkRollInit = qSensInit[14];
    double hLStAnkRollAlphaD[6] = {hLStAnkRollInit, 0.5 * (hLStAnkRollInit + L_ST_ANKLE_ROLL_D), L_ST_ANKLE_ROLL_D, L_ST_ANKLE_ROLL_D, L_ST_ANKLE_ROLL_D, L_ST_ANKLE_ROLL_D};
    hLStAnkRollD = bezierPoly5.get_bezier(hLStAnkRollAlphaD, s);
    dhLStAnkRollD = bezierPoly5.get_derv_bezier(hLStAnkRollAlphaD, s);

    double hLSwFtRollD, dhLSwFtRollD, hLSwFtRoll, dhLSwFtRoll, hLSwFtRollInit;
    hLSwFtRoll = orSwFt[0];
    dhLSwFtRoll = dqSens[14]; // or dorSwFt[1]?
    hLSwFtRollInit = orSwFtInit[0];
    double hLSwFtRollAlphaD[6] = {hLSwFtRollInit, 0.5 * (hLSwFtRollInit + L_SW_FT_ROLL_D), L_SW_FT_ROLL_D, L_SW_FT_ROLL_D, L_SW_FT_ROLL_D, L_SW_FT_ROLL_D};
    hLSwFtRollD = bezierPoly5.get_bezier(hLSwFtRollAlphaD, s);
    dhLSwFtRollD = bezierPoly5.get_derv_bezier(hLSwFtRollAlphaD, s);

    // turning controller (hip yaw)
    double hRSwFtYawD, dhRSwFtYawD, hRSwFtYaw, dhRSwFtYaw, hRSwFtYawInit;
    hRSwFtYaw = orSwFt[2];
    dhRSwFtYaw = dqSens[6]; // or dorSwFt[1]?
    hRSwFtYawInit = orSwFtInit[2];
    double hRSwFtYawAlphaD[6] = {hRSwFtYawInit, 0.5 * (hRSwFtYawInit + R_SW_FT_YAW_D), R_SW_FT_YAW_D, R_SW_FT_YAW_D, R_SW_FT_YAW_D, R_SW_FT_YAW_D};
    hRSwFtYawD = bezierPoly5.get_bezier(hRSwFtYawAlphaD, s);
    dhRSwFtYawD = bezierPoly5.get_derv_bezier(hRSwFtYawAlphaD, s);

    double hRStHipYawD, dhRStHipYawD, hRStHipYawInit;
    hRStHipYawInit = qSensInit[6];
    double hRStHipYawAlphaD[6] = {hRStHipYawInit, 0.5 * (hRStHipYawInit + R_ST_ANKLE_YAW_D), 1 / 3 * hRStHipYawInit + 3 / 4 * R_ST_ANKLE_YAW_D, R_ST_ANKLE_YAW_D, R_ST_ANKLE_YAW_D, R_ST_ANKLE_YAW_D};
    hRStHipYawD = bezierPoly5.get_bezier(hRStHipYawAlphaD, s);
    dhRStHipYawD = bezierPoly5.get_derv_bezier(hRStAnkPitchAlphaD, s);

    double hLSwFtYawD, dhLSwFtYawD, hLSwFtYaw, dhLSwFtYaw, hLSwFtYawInit;
    hLSwFtYaw = orSwFt[2];
    dhLSwFtYaw = dqSens[11]; // or dorSwFt[1]?
    hLSwFtYawInit = orSwFtInit[2];
    double hLSwFtYawAlphaD[6] = {hLSwFtYawInit, 0.5 * (hLSwFtYawInit + L_SW_FT_YAW_D), L_SW_FT_YAW_D, L_SW_FT_YAW_D, L_SW_FT_YAW_D, L_SW_FT_YAW_D};
    hLSwFtYawD = bezierPoly5.get_bezier(hLSwFtYawAlphaD, s);
    dhLSwFtYawD = bezierPoly5.get_derv_bezier(hLSwFtYawAlphaD, s);

    double hLStHipYawD, dhLStHipYawD, hLStHipYaw, dhLStHipYaw, hLStHipYawInit;
    hLStHipYawInit = qSensInit[11];
    double hLStHipYawAlphaD[6] = {hLStHipYawInit, 0.5 * (hLStHipYawInit + L_ST_HIP_YAW_D), 1 / 3 * hLStHipYawInit + 3 / 4 * L_ST_HIP_YAW_D, L_ST_HIP_YAW_D, L_ST_HIP_YAW_D, L_ST_HIP_YAW_D};
    hLStHipYawD = bezierPoly5.get_bezier(hLStHipYawAlphaD, s);
    dhLStHipYawD = bezierPoly5.get_derv_bezier(hLStAnkPitchAlphaD, s);

#ifdef INV_KIN_FTOR
    double swRFtOrDes[3] = {0}, dswRFtOrDes[3] = {0};
    swRFtOrDes[0] = hSwFtPitchD;
    swRFtOrDes[1] = hRSwFtRollD;
    swRFtOrDes[2] = hRSwFtYawD; // Added yaw for inv kinematics
    dswRFtOrDes[0] = dhSwFtPitchD;
    dswRFtOrDes[1] = dhRSwFtRollD;
    dswRFtOrDes[2] = dhRSwFtYawD; // Added yaw for inv kinematics
    double qRSwAnkDes[3], dqRSwAnkDes[3];
    SwFtOrToAnk(swRFtOrDes, dswRFtOrDes, qR, 1, qRSwAnkDes, dqRSwAnkDes);

    double swLFtOrDes[3] = {0}, dswLFtOrDes[3] = {0};
    swLFtOrDes[0] = hSwFtPitchD;
    swLFtOrDes[1] = hLSwFtRollD;
    swLFtOrDes[2] = hLSwFtYawD; // Added yaw for inv kinematics
    dswLFtOrDes[0] = dhSwFtPitchD;
    dswLFtOrDes[1] = dhLSwFtRollD;
    dswLFtOrDes[2] = dhLSwFtYawD; // Added yaw for inv kinematics
    double qLSwAnkDes[3], dqLSwAnkDes[3];
    SwFtOrToAnk(swLFtOrDes, dswLFtOrDes, qL, 0, qLSwAnkDes, dqLSwAnkDes);

    Output rHipYaw(N_B, qSens[6], dqSens[6]);
    rHipYaw.Set_outputD(hRStHipYawD, dhRStHipYawD, qRSwAnkDes[0], dqRSwAnkDes[0], kR);

    Output lHipYaw(N_B, qSens[11], dqSens[11]);
    lHipYaw.Set_outputD(hLStHipYawD, dhLStHipYawD, qLSwAnkDes[0], dqLSwAnkDes[0], kL);

    Output rAnkPitch(N_B, qSens[8], dqSens[8]);
    rAnkPitch.Set_outputD(hRStAnkPitchD, dhRStAnkPitchD, qRSwAnkDes[1], dqRSwAnkDes[1], kR);

    Output lAnkPitch(N_B, qSens[13], dqSens[13]);
    lAnkPitch.Set_outputD(hLStAnkPitchD, dhLStAnkPitchD, qLSwAnkDes[1], dqLSwAnkDes[1], kL);

    Output rAnkRoll(N_B, qSens[9], dqSens[9]);
    rAnkRoll.Set_outputD(hRStAnkRollD, dhRStAnkRollD, qRSwAnkDes[2], dqRSwAnkDes[2], kR);

    Output lAnkRoll(N_B, qSens[14], dqSens[14]);
    lAnkRoll.Set_outputD(hLStAnkRollD, dhLStAnkRollD, qLSwAnkDes[2], dqLSwAnkDes[2], kL);

    Write_to_outputs(h, dh, hD, dhD, torsoRoll, TORSO_ROLL_INDEX);
    Write_to_outputs(h, dh, hD, dhD, lAnkRoll, 14);
    Write_to_outputs(h, dh, hD, dhD, rHipYaw, 6);
    Write_to_outputs(h, dh, hD, dhD, lHipYaw, 11);
    Write_to_outputs(h, dh, hD, dhD, rAnkPitch, 8);
    Write_to_outputs(h, dh, hD, dhD, rAnkRoll, 9);
    Write_to_outputs(h, dh, hD, dhD, rKnee, 7);
    Write_to_outputs(h, dh, hD, dhD, lKnee, 12);
    Write_to_outputs(h, dh, hD, dhD, lHipPitch, L_HIP_PITCH_INDEX);
    Write_to_outputs(h, dh, hD, dhD, lHipRoll, L_HIP_ROLL_INDEX);
    Write_to_outputs(h, dh, hD, dhD, rHipRoll, R_HIP_ROLL_INDEX);
    Write_to_outputs(h, dh, hD, dhD, rHipPitch, R_HIP_PITCH_INDEX);
    Write_to_outputs(h, dh, hD, dhD, torsoPitch, TORSO_PITCH_INDEX);
    Write_to_outputs(h, dh, hD, dhD, torsoYaw, TORSO_YAW_INDEX);
    Write_to_outputs(h, dh, hD, dhD, lAnkPitch, 13);
#endif
}

void WalkingController3::EvalTorques(double s, double tInStep, double f_d, double f0, double x0, double px0, double Q_INIT[], double qSens[], double dqSens[], double kR, double kL, double orSwFt[],
                                     double tauAnkTorque[], double forceRightAnkle[], double forceLeftAnkle[], double torqueRightAnkle[], double torqueLeftAnkle[],
                                     double pPelvis[], double vxAbsF, double h[], double dh[], double hD[], double dhD[], double tauDes[], double vals[])
{
    double y[N];  // desired output to be driven to zero
    double dy[N]; // derivative of the desired output to be driven to zero
    double Kp[N], Kd[N], I[N];

    // #ifndef REAL_ROBOT
    //     Kp[23] = 5;
    //     Kd[23] = 0;
    //     Kp[24] = 5;
    //     Kd[24] = 0;
    //     Kp[25] = 5;
    //     Kd[25] = 0;
    //     Kp[26] = 0;
    //     Kd[26] = 0;
    //     Kp[27] = 5;
    //     Kd[27] = 0;
    //     Kp[28] = 5;
    //     Kd[28] = 0;
    //     Kp[29] = 0;
    //     Kd[29] = 0;
    //     Kp[30] = 0;
    //     Kd[30] = 0;
    //     Kp[31] = 0;
    //     Kd[31] = 0;

    //     for (int i = 0; i < 23; i++)
    //     {
    //         Kp[i] = 300;
    //         Kd[i] = 0;
    //     }
    //     Kp[6] = 50 + 160 * kR;
    //     ;
    //     Kd[6] = 15 - 12.5 * pow(kR, 3);
    //     Kp[11] = 50 + 160 * kL;
    //     Kd[11] = 15 - 12.5 * pow(kL, 3);
    // #else
    //     for (int i = 0; i < N; i++)
    //     {
    //         Kp[i] = 500;
    //         Kd[i] = 15;
    //     }
    // #endif

    //     Kp[3] = 70 + 160 * pow(kR, 3);
    //     Kd[3] = 15 - 12.5 * pow(kR, 3);
    //     Kp[4] = 70 + 160 * pow(kL, 3);  //180+70*pow(1-k,3);
    //     Kd[4] = 15 - 12.5 * pow(kL, 3); //25-12.5*pow(1-k,3);
    // #ifdef INV_KIN_FTX
    //     Kp[3] = 200 + 130 * kR;
    //     Kd[3] = 7 + 0 * kR;
    //     if (whichComan_ == 1)
    //     {
    //         Kp[4] = 285 + 130 * kL; //180+70*pow(1-k,3);
    //         Kd[4] = 10 - 2 * kL;    //25-12.5*pow(1-k,3);
    //     }
    //     else
    //     {
    //         Kp[4] = 200 + 130 * kL; //180+70*pow(1-k,3);
    //         Kd[4] = 7 - 0 * kL;     //25-12.5*pow(1-k,3);
    //     }
    //     Kp[7] = 500;
    //     Kd[7] = 10;
    //     Kp[12] = 500;
    //     Kd[12] = 10;

    //     Kp[9] = 270 - 70 * kR;
    //     Kd[9] = 17.5 - 7.5 * kR;
    //     Kp[14] = 270 - 70 * kL; //180+70*pow(1-k,3);
    //     Kd[14] = 17.5 - 7.5 * kL;
    // #endif

    for (int i = 0; i < 15; i++)
    {
        y[i] = h[i] - hD[i];
        dy[i] = dh[i] - dhD[i];
    }

    for (int i = 15; i < N; i++)
    {
        y[i] = qSens[i] - Q_INIT[i];
        dy[i] = dqSens[i] - 0;
    }

    // #ifdef REAL_ROBOT
    //     for (int i = 23; i < N; i++)
    //     {
    //         Kd[i] = 0;
    //     }
    //     Kd[23] = 10;
    //     Kp[23] = 100;
    //     if (whichComan_ == 2)
    //     {
    //         Kp[24] = 50; //coman 1 -50
    //         Kp[29] = 50;
    //         Kp[30] = 100;
    //     }
    //     else
    //     {
    //         Kp[24] = -50;
    //         Kp[29] = -50;
    //         Kp[30] = -100;
    //     }
    //     if (whichComan_ == 0)
    //     {
    //         Kp[24] = 0;

    //         std::cout << "Attention!! WhichComan is not set!! Kp[24] was set to zero!" << std::endl;
    //     }
    //     Kp[25] = 50;
    //     Kp[26] = 100;
    //     Kd[26] = 10;
    //     Kp[27] = 50;
    //     Kp[28] = 50;

    // #else
    for (int i = 0; i < N; i++)
    {
        Kp[i] = 300;
        Kd[i] = 1.5;
    }

    Kp[3] = parameters[0] + parameters[1] * pow(kR, 3);
    Kd[3] = parameters[2] + parameters[3] * pow(kR, 3);
    Kp[4] = parameters[0] + parameters[1] * pow(kL, 3);
    Kd[4] = parameters[2] + parameters[3] * pow(kL, 3);

    // Kp[3] = parameters[0] + parameters[1] * pow(kR, 3);
    // Kd[3] = parameters[2] + parameters[3] * pow(kR, 3);
    // Kp[4] = parameters[4] + parameters[5] * pow(kL, 3);
    // Kd[4] = parameters[6] + parameters[7] * pow(kL, 3);

    // Kp[3] = 1300 - 400 * pow(kR, 3);
    // Kd[3] = 35 - 30 * pow(kR, 3);
    // Kp[4] = 1450 + 500 * pow(kL, 3);
    // Kd[4] = 30 - 20 * pow(kL, 3);

    // std::cerr << "PID: Kp3= " << Kp[3] << " Kd3= " << Kd[3] << " | Kp4= " << Kp[4] << " Kd4= " << Kd[4] << std::endl;

    // #endif

    double VelZero[N];
    for (int i = 0; i < N; i++)
    {
        VelZero[i] = 0;
    }
    VelZero[24] = 1;
    VelZero[25] = 1;
    VelZero[27] = 1;
    VelZero[28] = 1;
    VelZero[29] = 1;
    VelZero[30] = 1;

    double MAX_VOLTAGE[N];
    for (int i = 0; i < 15; i++)
    {
        if (whichComan_ == 1)
        {
            MAX_VOLTAGE[i] = 12; // for fd = 2.5 this was set to 15 volts
        }
        else
        {
            MAX_VOLTAGE[i] = 11; // for fd = 2.5 this was set to 15 volts
        }
    }
    for (int i = 15; i < 23; i++)
    {
        MAX_VOLTAGE[i] = 6;
    }
    for (int i = 23; i < N; i++)
    {
        MAX_VOLTAGE[i] = 6;
    }
    double MAX_CURRENT = 7;
    if (whichComan_ == 1)
    {
        MAX_CURRENT = 8;
    }
    else
    {
        MAX_CURRENT = 7;
    }
    if (whichComan_ == 2)
    {
        MAX_VOLTAGE[1] = 10;
    }

    const double H = 0.05;
    double xCopR = forceRightAnkle[2] < 50 ? -(torqueRightAnkle[1] + forceRightAnkle[0] * H) / 50 : -(torqueRightAnkle[1] + forceRightAnkle[0] * H) / forceRightAnkle[2];
    double xCopL = forceLeftAnkle[2] < 50 ? -(torqueLeftAnkle[1] + forceLeftAnkle[0] * H) / 50 : -(torqueLeftAnkle[1] + forceLeftAnkle[0] * H) / forceLeftAnkle[2];
    double p_d = f0 == 0 ? 0 : 0;
    double t = f0 == 0 ? 0 : s / f0;
    double pxx0 = f0 == 0 ? 0 : px0;
    double vdes = (-px0 + x0) * f_d;
    double kVdes = x0 == 0 ? 0 : 1;
    double deltaPR = (pPelvis[0] - 0 - kVdes * (pxx0 + vdes * t));
    double deltaPL = (pPelvis[0] - 0 - kVdes * (pxx0 + vdes * t));
    double deltaV = (vxAbsF - vdes);
    double KpAR = deltaPR > 0 ? 370 : 370;
    double KpAL = deltaPL > 0 ? 370 : 370;
    double trqFeedFwd = 100 * x0 - 11.5; // 1 * f0 worked nicely for T = 0.5714, but for T = 0.4, 0.5 * f0 seems to work better maybe a nonlinear function is needed?
    if (x0 == 0)
    {
        KpAR = deltaPR > 0 ? 370 : 370;
        KpAL = deltaPL > 0 ? 370 : 370;
    }
    double KpC = pPelvis[0] > 0 ? 30000000 : 0;
    if (x0 == 0)
    {
        KpC = KpC * 0;
    }
    double KpL = 150;
    double purePosTrq = KpC * pow(pPelvis[0], 5) > 12.5 ? 12.5 : KpC * pow(pPelvis[0], 5);
    double tau8torque_d = 1 * (pPelvis[2] * forceRightAnkle[0] - pPelvis[0] * forceRightAnkle[2]) + (KpAR * deltaPR + 30 * deltaV + purePosTrq) + trqFeedFwd;
    double tau13torque_d = 1 * (pPelvis[2] * forceLeftAnkle[0] - pPelvis[0] * forceLeftAnkle[2]) + (KpAL * deltaPL + 30 * deltaV + purePosTrq) + trqFeedFwd;
    double tau9torque_d = 1 * (pPelvis[1] * forceRightAnkle[2] - pPelvis[2] * forceRightAnkle[1]) - KpL * (pPelvis[1] - 0.1);
    double tau14torque_d = 1 * (pPelvis[1] * forceLeftAnkle[2] - pPelvis[2] * forceLeftAnkle[1]) - KpL * (pPelvis[1] + 0.1);

#ifndef REAL_ROBOT
    double swP = 1; // 0.3
    double swT = 0; // 1

    for (int i = 0; i < N; i++)
    {
        tauDes[i] = -Kp[i] * y[i] - Kd[i] * dy[i];
        if (s >= 0)
        {
            if (i == 8)
            {
                tauDes[8] = ((1 - kR) * tauDes[8] + swP * kR * tauDes[8]) + swT * kR * (tau8torque_d + 0 * (torqueRightAnkle[2] + tau8torque_d));
                //            tauDes[8] = kR * tau8torque;
            }
            if (i == 13)
            {
                tauDes[13] = ((1 - kL) * tauDes[13] + swP * kL * tauDes[13]) + swT * kL * (tau13torque_d + 0 * (torqueLeftAnkle[2] + tau13torque_d));
                //            tauDes[13] = kL * tau13torque;
            }
            //        if (i == 9){
            //            tauDes[9] = ((1 - kR) * tauDes[9] + 0.75 * kR * tauDes[9]) + 1 * kR * (tau9torque_d);
            //        }
            //        if (i == 14){
            //            tauDes[14] = ((1 - kL) * tauDes[14] + 0.75 * kL * tauDes[14]) + 1 * kL * (tau14torque_d);
            //        }
        }
        I[i] = (tauDes[i] + 0 * 0.25 + 0) / (0.055 * 100);
        I[i] = I[i] > 8 ? 8 : (I[i] < -8 ? -8 : I[i]);
        vals[i] = 0.055 * 100 * (1 - VelZero[i]) * dqSens[i] + 2.5 * I[i]; // 100 is the grear ratio
        vals[i] = vals[i] > MAX_VOLTAGE[i] ? MAX_VOLTAGE[i] : (vals[i] < -MAX_VOLTAGE[i] ? -MAX_VOLTAGE[i] : vals[i]);
    }
    // cout << swFtYD << " " << hSwFtY << " hswFtyD " << hSwFtYD << "TauDes5" << tauDes[5] << endl;
    tauAnkTorque[0] = kR * tau8torque_d;
    tauAnkTorque[1] = kL * tau13torque_d;

#else
    double swP = 0.3; // 0.3
    double swT = 1;   // 1

    for (int i = 0; i < N; i++)
    {
        tauDes[i] = -Kp[i] * y[i] - Kd[i] * dy[i];
        if (s >= 0)
        {
            if (i == 8)
            {
                tauDes[8] = ((1 - kR) * tauDes[8] + swP * kR * tauDes[8]) + swT * kR * (tau8torque_d + 0 * (torqueRightAnkle[2] + tau8torque_d));
                //            tauDes[8] = kR * tau8torque;
            }
            if (i == 13)
            {
                tauDes[13] = ((1 - kL) * tauDes[13] + swP * kL * tauDes[13]) + swT * kL * (tau13torque_d + 0 * (torqueLeftAnkle[2] + tau13torque_d));
                //            tauDes[13] = kL * tau13torque;
            }
            //        if (i == 9){
            //            tauDes[9] = ((1 - kR) * tauDes[9] + 0.75 * kR * tauDes[9]) + 1 * kR * (tau9torque_d);
            //        }
            //        if (i == 14){
            //            tauDes[14] = ((1 - kL) * tauDes[14] + 0.75 * kL * tauDes[14]) + 1 * kL * (tau14torque_d);
            //        }
        }
        I[i] = (tauDes[i] + 0 * 0.25 + 0) / (0.055 * 100);
        I[i] = I[i] > MAX_CURRENT ? MAX_CURRENT : (I[i] < -MAX_CURRENT ? -MAX_CURRENT : I[i]);
        vals[i] = 0.055 * 100 * (1 - VelZero[i]) * dqSens[i] + 2.5 * I[i]; // 100 is the grear ratio
        vals[i] = vals[i] > MAX_VOLTAGE[i] ? MAX_VOLTAGE[i] : (vals[i] < -MAX_VOLTAGE[i] ? -MAX_VOLTAGE[i] : vals[i]);
    }
    // cout << swFtYD << " " << hSwFtY << " hswFtyD " << hSwFtYD << "TauDes5" << tauDes[5] << endl;
    tauAnkTorque[0] = kR * tau8torque_d;
    tauAnkTorque[1] = kL * tau13torque_d;
#endif
}
