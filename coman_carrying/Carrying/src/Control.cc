
#include "Control.hh"
#include <numeric>
#include "WhichSide.hh"
#include "forward_kinematics_pelvis1.hh"
#include "forward_kinematics_swing_foot.hh"

#include "bezier.hh"
#include "saturate.hh"
//#include "init_pos.hh"
#include <math.h>
#include "StFtToPelvisFK.hh"
#include "SwapArrays.hh"
// #include "EraseVectors.hh"
#include "DesiredFtPos.hh"
//#include "ControlLowerBody.hh"
//#include "AvgFilter.hh" to add
#include "MedianFilter.h"
//#include "ControlUpperBody.hh"
#include "StackAsVector.hh"
// #include "state_estimation.h"
#include "R2Euler.hh"
#include <chrono>

#include <fstream>
// Eigen headers
#include </usr/include/eigen3/Eigen/Dense>
#include <Eigen/Eigen>
using namespace Eigen;
//! The class of Cmatrix abreviated from Eigen
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Cmatrix;
//! The class of Cvector abreviated from Eigen VectorXd
#define Cvector Eigen::VectorXd
//! The class of Cvector3 abreviated from Eigen Vector3d
#define Cvector3 Eigen::Vector3d
#define Cvector4 Eigen::Vector4d
#define zero_v3 Cvector3(0.0, 0.0, 0.0)
#define N_OF_ITERATIONS 10
#define EPSILON 0.001
#define ALPHA 1
#define N_C 3
#define N_J 23
#define ALPHA_ 1
/*Cmatrix jacM(N_C, N_J);
Cmatrix hM(N_C, 1);
Cmatrix jacInvMhM(N_C, 1), qM(N_J, 1);*/
#include <Eigen/QR>

void Control::SetInitPos(double qInit[N])
{
    for (int i = 0; i < N; i++)
    {
        Q_INIT[i] = qInit[i];
    }
}

void Control::ClassifyInit(int x, std::string d_LDAStop1, std::string d_FeatLabelStop1, std::string d_ClassesStop1)
{
    classifier = Classifier(x);
    //this->classifier=Classifier(2);
    this->d_ClassesStop = d_ClassesStop1;
    this->d_LDAStop = d_LDAStop1;
    this->d_FeatLabelStop = d_FeatLabelStop1;
    this->classifier.load(d_LDAStop1, d_FeatLabelStop1, d_ClassesStop1);
    this->staterob = "stop";
}

std::string Control::ClassifyStart(double forceDiff, double velRel, double posRel, double vel, double Time, bool &simpleThreshold)
{
    std::string OUTPUT;

    //    if (true){ // MFMV
    if (simpleThreshold == false)
    { // MFMV

        double featVect[nFeat] = {posRel, forceDiff};
        OUTPUT = this->classifier.Classify(featVect, Time);
    }
    else
    { //MFSV

        //        if(vel<0.09){
        if (vel < 0.162)
        {
            OUTPUT = "perturbation";
        }
        else
        {

            OUTPUT = "pullfastStart";
        }
    }

    return OUTPUT;
}

int Control::testing()
{
    classifier = Classifier(1);
    int n = classifier.test();
    return n;
}

void Control::Leveling(double time, const char *whichArm, double *qDes, double valsUpperBody[N], double trans[][3], double posSens[], double velSens[], Model *modelRBDL, EigenRobotState eigRobotState3, const iDynTree::Model model3, iDynTree::KinDynComputations &kinDynComp3, Model *modelRBDLleg)
{

    VectorNd Q = VectorNd::Zero(modelRBDL->dof_count);
    VectorNd QlegR = VectorNd::Zero(modelRBDLleg->dof_count);
    VectorNd QlegL = VectorNd::Zero(modelRBDLleg->dof_count);
    const char *setHand;
    const char *setFoot;
    Cmatrix qM2(2, 1);
    Math::Vector3d pointPosition, handPos, footPosL, footPosR;
    Eigen::VectorXd handPosWRTFootR, handPosWRTFootL;
    Math::MatrixNd G(6, modelRBDL->dof_count);
    Eigen::MatrixXd JacobRed;

    int it = 0;
    pointPosition.setZero();
    handPos.setZero();
    footPosL.setZero();
    footPosR.setZero();
    G.setZero();

    Q(0) = posSens[2];
    Q(1) = posSens[1];
    Q(2) = posSens[0];
    QlegR(0) = posSens[2];
    QlegR(1) = posSens[1];
    QlegR(2) = posSens[0];
    QlegL(0) = posSens[2];
    QlegL(1) = posSens[1];
    QlegL(2) = posSens[0];

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            rotImuGlobal(i, j) = trans[i][j]; //NB you are now assuming that trans is R_world_to_something; if it is not you should TRANSPOSE
        }
    }

    //Select Arm
    if (whichArm == "right")
    {
        setHand = "RSoftHand";
        Q(3) = posSens[15];
        Q(4) = posSens[16];
        Q(5) = posSens[17];
        Q(6) = posSens[18];
        Q(7) = posSens[23];
        Q(8) = posSens[24];
        Q(9) = posSens[25];
    }
    else
    {
        setHand = "LSoftHand";
        Q(3) = posSens[19];
        Q(4) = posSens[20];
        Q(5) = posSens[21];
        Q(6) = posSens[22];
        Q(7) = posSens[26];
        Q(8) = posSens[27];
        Q(9) = posSens[28];
    }
    //

    //Select foot
    setFoot = "RFoot";
    QlegR(3) = posSens[3];
    QlegR(4) = posSens[5];
    QlegR(5) = posSens[6];
    QlegR(6) = posSens[7];
    QlegR(7) = posSens[8];
    QlegR(8) = posSens[9];

    QlegL(3) = posSens[4];
    QlegL(4) = -posSens[10];
    QlegL(5) = -posSens[11];
    QlegL(6) = posSens[12];
    QlegL(7) = posSens[13];
    QlegL(8) = -posSens[14];
    //

    qM2(0, 0) = Q(5);
    qM2(1, 0) = Q(6);

    handPos = CalcBodyToBaseCoordinates(*modelRBDL, Q, modelRBDL->GetBodyId(setHand), pointPosition, true);

    /*if(whichArm == "left"){
        handPos(1)=-handPos(1);
    }*/
    footPosR = CalcBodyToBaseCoordinates(*modelRBDLleg, QlegR, modelRBDLleg->GetBodyId(setFoot), pointPosition, true);
    footPosL = CalcBodyToBaseCoordinates(*modelRBDLleg, QlegL, modelRBDLleg->GetBodyId(setFoot), pointPosition, true);
    footPosL(1) = -footPosL(1);
    /*std::cout<<footPosL(0)<<"  "<<footPosL(1)<<"  "<<footPosL(2)<<"  "<<std::endl;
    std::cout<<footPosR(0)<<"  "<<footPosR(1)<<"  "<<footPosR(2)<<"  "<<std::endl;
    std::cout<<"  ----------  "<<std::endl;*/

    //Assumption: the orientation of the right and the left foot wrt the base is the same; that is why you use rotMatrixFoot2Base for both
    handPosWRTFootR = rotImuGlobal * (handPos - footPosR); //rotMatrixFoot2Base*(handPos-footPosR);
    handPosWRTFootL = rotImuGlobal * (handPos - footPosL); //rotMatrixFoot2Base*(handPos-footPosL);

    /*if(whichArm=="left"){
    std::cout<<handPosWRTFootL(2)<<"                     "<<handPosWRTFootR(2)<<"     "<<std::endl;
    }*/

    varsOut.handPosWRTFootL_ = handPosWRTFootL(2);
    varsOut.handPosWRTFootR_ = handPosWRTFootR(2);
    varsOut.handPos_ = handPos(2);
    varsOut.footPosL_ = footPosL(2);
    varsOut.footPosR_ = footPosR(2);
    if (whichArm == "right")
    {
        if (firstTime)
        {
            firstTime = false;
            handPosDes = handPosWRTFootR(2);
        }
        handPosDesR = handPosDes;
        varsOut.handPosDesR_ = handPosDesR;
        varsOut.errLevelsR_ = (kF * (handPosWRTFootR(2)) + (1 - kF) * (handPosWRTFootL(2))) - handPosDesR;
        varsOut.handPosR_ = (kF * (handPosWRTFootR(2)) + (1 - kF) * (handPosWRTFootL(2)));
    }
    else
    {
        if (firstTime)
        {
            firstTime = false;
            handPosDes = handPosWRTFootL(2);
        }
        handPosDesL = handPosDes;
        varsOut.handPosDesL_ = handPosDesL;
        varsOut.errLevelsL_ = (kF * (handPosWRTFootR(2)) + (1 - kF) * (handPosWRTFootL(2))) - handPosDesL;
        varsOut.handPosL_ = (kF * (handPosWRTFootR(2)) + (1 - kF) * (handPosWRTFootL(2)));
    }

    eigRobotState.resize(model3.getNrOfDOFs());
    eigRobotState.random();
    varsOut.rightStance_ = 0; //rightStance;
    double hNorm2 = 100;

    //old while ( (it < 3) && (hNorm2 >0.01)){
    while ((it < 9) && (hNorm2 > 0.01))
    {

        it = it + 1;
        //Jacobian of the arm reduced wrt the arm
        CalcPointJacobian6D(*modelRBDL, Q, modelRBDL->GetBodyId(setHand), pointPosition, G, true);
        JacobRed = G.block<1, 2>(5, 5);
        handPos = CalcBodyToBaseCoordinates(*modelRBDL, Q, modelRBDL->GetBodyId(setHand), pointPosition, true);
        handPosWRTFootR = rotImuGlobal * (handPos - footPosR);
        handPosWRTFootL = rotImuGlobal * (handPos - footPosL);

        //Error
        Eigen::MatrixXd jacM2(1, 1);
        Eigen::MatrixXd hM2(1, 1);
        Eigen::MatrixXd jacInvMhM2(1, 1);
        jacM2 = JacobRed; // Jacob Reduced to the orientation +
        if (whichArm == "right")
        {
            hM2(0, 0) = (kF * (handPosWRTFootR(2)) + (1 - kF) * (handPosWRTFootL(2))) - handPosDesR;
        }
        else
        {

            hM2(0, 0) = (kF * (handPosWRTFootR(2)) + (1 - kF) * (handPosWRTFootL(2))) - handPosDesL; // error
        }

        if (whichArm == "left")
        {
            // std::cout<<hM2(0,0)<<"  "<<std::endl;
        }

        //SOLVE
        jacInvMhM2 = jacM2.bdcSvd(ComputeThinU | ComputeThinV).solve(hM2);
        qM2 -= 0.08 * jacInvMhM2;
        hNorm2 = hM2.norm();
        Q(5) = qM2(0, 0);
        Q(6) = qM2(1, 0);
    }

    qDes[0] = Q(5) * 1;
    qDes[1] = Q(6) * 1;
    if (whichArm == "left")
    {
        //std::cout<<"----------------  "<<std::endl;
    }

    for (int i = 0; i < 2; i++)
    {
        //MedianFilter(orientationVec[i], orientationMed[i], orientation[i], 5);
        if (whichArm == "right")
        {
            varsOut.errorConvergenceLevR_ = hNorm2;
            AvgFilter(qDesVec2R[i], qDesAvg2R[i], qDes[i], 5);
        }
        else
        {
            varsOut.errorConvergenceLevL_ = hNorm2;
            AvgFilter(qDesVec2L[i], qDesAvg2L[i], qDes[i], 5);
        }
    }

    if (whichArm == "right")
    {
        for (int i = 0; i < 2; i++)
        {
            qDes[i] = qDesAvg2R[i];
        }
        varsOut.qLevelDes1R = qDes[0];
        varsOut.qLevelDes2R = qDes[1];
    }
    else
    {
        for (int i = 0; i < 2; i++)
        {
            qDes[i] = qDesAvg2L[i];
        }
        varsOut.qDesLevL_ = qDes;
        varsOut.qLevelDes1L = qDes[0];
        varsOut.qLevelDes2L = qDes[1];
    }

    if (qDes[1] >= 0)
        qDes[1] = 0; //joint24
    if (qDes[1] <= -2)
        qDes[1] = -1.5;
    if (qDes[0] >= .5)
        qDes[0] = .45; //joint24
    if (qDes[0] <= -0.5)
        qDes[0] = -.45;
    varsOut.qDesLevR_ = qDes;
    //std::cout<<qDes[0]<<"   "<<qDes[1]<<"   "<<std::endl;
    /*  varsOut.qLevelDes1=qDes[0];
    varsOut.qLevelDes2=qDes[1];*/
    // std::cout<<"ok"<<std::endl;
}

/*void Control::SetOrientation (double time, std::string whichArm,  double * qDes, double valsUpperBody[N], double trans[][3], double posSens [], double velSens[], EigenRobotState eigRobotState3, const iDynTree::Model  model3, iDynTree::KinDynComputations &kinDynComp3, double * valsOrient, Model * modelRBDL){

    gettimeofday(&start, NULL);
       VectorNd Q = VectorNd::Zero (modelRBDL->dof_count);
       //Mapping from urdf
       Q(0)=posSens[2];
       Q(1)=posSens[1];
       Q(2)=posSens[0];
       double qDes_[N];
       Math::MatrixNd G(6, modelRBDL->dof_count);
       Eigen::MatrixXd JacobRed1, JacobRed2;
       Eigen::MatrixXd JacobRed(4,4);
       JacobRed.setZero()swe;
       std::string endEffector;
       G.setZero();
       Math::Vector3d point_position;
       point_position.setZero();
       Math::Matrix3d  rotHandImu, rotHandImuTr, rotImuGlobal, rotHandGlobal;
       Math::Vector3d  handPos;
       static double handPosDes;
       Cmatrix  qM2(4, 1);
       double transRotGlobalHand[3][3], orientationMed[3];
       double orientation[3], errorOrient[3], errLevels;
       static double orientationDes[3];
       int it;

       if (whichArm == "right"){
           endEffector = "RSoftHand";
           Q(3)=posSens[15];
           Q(4)=posSens[16];
           Q(5)=posSens[17];
           Q(6)=posSens[18];
           Q(7)=posSens[23];
           Q(8)=posSens[24];
           Q(9)=posSens[25];
       }
       else{
           endEffector = "LSoftHand";
           Q(3)=posSens[19];
           Q(4)=posSens[20];
           Q(5)=posSens[21];
           Q(6)=posSens[22];
           Q(7)=posSens[26];
           Q(8)=posSens[27];
           Q(9)=posSens[28];
       }




   double hNorm2  = 100;
   qM2(0, 0) = Q(6);
   qM2(1, 0) = Q(7);
   qM2(2, 0) = Q(8);
   qM2(3, 0) = Q(9);
   for (int i=0; i<3; i++){
       for (int j=0; j<3; j++){
           rotImuGlobal(i, j)= trans[i][j]; //NB you are now assuming that trans is R_world_to_something; if it is not you should TRANSPOSE
       }
   }


   while ( (it < 10) && (hNorm2 >0.01)){

       it=it+1;
       CalcPointJacobian6D ( *modelRBDL, Q, modelRBDL->GetBodyId("RSoftHand"), point_position, G, true);
       JacobRed1=G.block<3,4>(0,6);
       JacobRed2=G.block<1,4>(5,6);

       for (int i=0; i<4; i++){

           for (int j=0; j<4; j++){

             if (i<3){

                JacobRed(i,j)=JacobRed1(i,j);
             }
             else{

                JacobRed(i,j)=JacobRed2(i-3,j);
             }
           }
       }

       rotHandImuTr = CalcBodyWorldOrientation(*modelRBDL,Q,modelRBDL->GetBodyId("RSoftHand"),true ) ;
       rotHandImu=rotHandImuTr.transpose();
       rotHandGlobal=rotImuGlobal*rotHandImu;
       handPos = CalcBodyToBaseCoordinates(*modelRBDL, Q, modelRBDL->GetBodyId("RSoftHand"), point_position,true );

       for (int i=0; i<3; i++){
           for (int j=0; j<3; j++){
               transRotGlobalHand[i][j]=rotHandGlobal(j, i);//NB you are considering the TRANSPOSE of your matrix because in the imuData.get_Orientation he is computing the euler angles knowing a rotation matrix from the world to something else(in your case the hands)
               }
       }
       imuData.get_Orientation(transRotGlobalHand, cdPR, orientation);
   if (firstTime){
       firstTime=false;
       orientationDes[0]=orientation[0];
       orientationDes[1]=orientation[1];
       orientationDes[2]=orientation[2];
       handPosDes=handPos(2);//z components
   }
   //Error
   Eigen::MatrixXd jacM2(4, 4);
   Eigen::MatrixXd hM2(4, 1);
   Eigen::MatrixXd jacInvMhM2(4, 4);

   jacM2=JacobRed; // Jacob Reduced to the orientation +

   //std::cout<<"JAcob:   "<<JacobRed<<std::endl;
   hM2(0, 0) = -(orientation[0]-orientationDes[0]); // error
   hM2(1, 0) = -(orientation[1]-orientationDes[1]);
   hM2(2, 0) = -(orientation[2]-orientationDes[2]);
   hM2(3, 0) = (handPos(2)-handPosDes);
   std::cout<<handPos(2)-handPosDes<<std::endl;
   errorOrient[0] = -(orientation[0]-orientationDes[0]); // error
   errorOrient[1] = -(orientation[1]-orientationDes[1]);
   errorOrient[2] = -(orientation[2]-orientationDes[2]);

   //SOLVE
   jacInvMhM2 =jacM2.bdcSvd(ComputeThinU | ComputeThinV).solve(hM2);


   qM2 -= 0.15 * jacInvMhM2;
   hNorm2 = hM2.norm();
       Q(6) = qM2(0, 0);
       Q(7) = qM2(1, 0);
       Q(8) = qM2(2, 0);
       Q(9) = qM2(3, 0);

   }


   qDes[0]=Q(6)*1;
   qDes[1]=Q(7)*1;
   qDes[2]=Q(8)*1;
   qDes[3]=Q(9)*1;


   if (qDes[2]>=0.45) qDes[2]=0.45; //joint24
   if (qDes[2]<=-0.45) qDes[2]=-0.45;

   if (qDes[3]>=0.85) qDes[3]=0.85;
   if (qDes[3]<=-0.85) qDes[3]=-0.85;

   if (qDes[1]>=1.5) qDes[1]=1.5;
   if (qDes[1]<=-1.5) qDes[1]=-1.5;

   if (qDes[0]>=0) qDes[0]=0;
   if (qDes[0]<=-2) qDes[0]=-2;

   qDesTemp[0]=qDes[0];
   qDesTemp[1]=qDes[1];
   qDesTemp[2]=qDes[2];


  for (int i = 0; i < 4; i++){
       AvgFilter(qDesVec[i], qDesAvg[i],qDes[i], 10);
   }

   errLevels = (handPos(2)-handPosDes);
   varsOut.orientationDes_=orientationDes;
   varsOut.errLevels_=errLevels;
   varsOut.qDesLev_=qDesAvg;
   varsOut.orientationRoll_=orientation[0];
   varsOut.orientationPitch_=orientation[1];
   varsOut.orientationYaw_=orientation[2];
   varsOut.orientationError_=errorOrient;
   varsOut.errorConvergence_=hNorm2;



   for (int i=0;i<4; i++){
     qDes[i]=qDesAvg[i];
   }


    varsOut.tm_=time;
    varsOut.qSens_=posSens;
    varsOut.qSensAbs_=posSens;
    varsOut.dqSens_=velSens;

   gettimeofday(&end, NULL);
   double sampleTime=((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));

}*/

void Control::SetOrientation(double time, const char *whichArm, double *qDes, double valsUpperBody[N], double trans[][3], double posSens[], double velSens[], Model *modelRBDL)
{

    gettimeofday(&start, NULL);
    VectorNd Q = VectorNd::Zero(modelRBDL->dof_count);
    //Mapping from urdf
    Q(0) = posSens[2];
    Q(1) = posSens[1];
    Q(2) = posSens[0];
    double qDes_[N] = {};
    Math::MatrixNd G(6, modelRBDL->dof_count);
    Eigen::MatrixXd JacobRed;
    const char *setHand;
    G.setZero();
    Math::Vector3d point_position;
    point_position.setZero();
    Math::Matrix3d rotHandImu, rotHandImuTr, rotImuGlobal, rotHandGlobal;
    Cmatrix qM2(3, 1);
    double transRotGlobalHand[3][3] = {}, orientationMed[3] = {};
    double orientation[3] = {}, errorOrient[3] = {};
    int it = 0;

    if (whichArm == "right")
    {
        setHand = "RSoftHand";
        Q(3) = posSens[15];
        Q(4) = posSens[16];
        Q(5) = posSens[17];
        Q(6) = posSens[18];
        Q(7) = posSens[23];
        Q(8) = posSens[24];
        Q(9) = posSens[25];
    }
    else
    {
        setHand = "LSoftHand";
        Q(3) = posSens[19];
        Q(4) = posSens[20];
        Q(5) = posSens[21];
        Q(6) = posSens[22];
        Q(7) = posSens[26];
        Q(8) = posSens[27];
        Q(9) = posSens[28];
    }

    //Convergence Error Orientation

    double hNorm2 = 100;
    qM2(0, 0) = Q(7);
    qM2(1, 0) = Q(8);
    qM2(2, 0) = Q(9);
    //std::cout<<"qInit::       "<<qM(0, 0)<<"         "<<qM(1, 0)<<std::endl;

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            rotImuGlobal(i, j) = trans[i][j]; //NB you are now assuming that trans is R_world_to_something; if it is not you should TRANSPOSE
        }
    }

    //Compute Real Error
    rotHandImuTr = CalcBodyWorldOrientation(*modelRBDL, Q, modelRBDL->GetBodyId(setHand), true);
    rotHandImu = rotHandImuTr.transpose();
    rotHandGlobal = rotImuGlobal * rotHandImu;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            transRotGlobalHand[i][j] = rotHandGlobal(j, i); //NB you are considering the TRANSPOSE of your matrix because in the imuData.get_Orientation he is computing the euler angles knowing a rotation matrix from the world to something else(in your case the hands)
        }
    }
    //Get Roll Pitch from transRotGlobalHand
    imuData.get_OrientationUB(transRotGlobalHand, cdPR, orientation);

    if (whichArm == "right")
    {
        if (firstTimeR)
        {
            firstTimeR = false;
            orientationDesR[0] = orientation[0];
            orientationDesR[1] = orientation[1];
            orientationDesR[2] = orientation[2];
        }
        errorOrient[0] = -(orientation[0] - orientationDesR[0]); // error
        errorOrient[1] = -(orientation[1] - orientationDesR[1]);
        errorOrient[2] = -(orientation[2] - orientationDesR[2]);
        varsOut.orientationDesR_ = orientationDesR;
        varsOut.orientationRollR_ = orientation[0];
        varsOut.orientationPitchR_ = orientation[1];
        varsOut.orientationYawR_ = orientation[2];
        varsOut.orientationErrorR_ = errorOrient;
    }
    else
    {
        if (firstTimeL)
        {
            firstTimeL = false;
            orientationDesL[0] = orientation[0];
            orientationDesL[1] = orientation[1];
            orientationDesL[2] = orientation[2];
        }
        errorOrient[0] = -(orientation[0] - orientationDesL[0]); // error
        errorOrient[1] = -(orientation[1] - orientationDesL[1]);
        errorOrient[2] = 0; // -(orientation[2]-orientationDesL[2]);
        varsOut.orientationDesL_ = orientationDesL;
        varsOut.orientationRollL_ = orientation[0];
        varsOut.orientationPitchL_ = orientation[1];
        varsOut.orientationYawL_ = orientation[2];
        varsOut.orientationErrorL_ = errorOrient;
    }

    // while ( (it < 9) && (hNorm2 >0.01)){
    while ((it < 6) && (hNorm2 > 0.01))
    {
        it = it + 1;
        CalcPointJacobian6D(*modelRBDL, Q, modelRBDL->GetBodyId(setHand), point_position, G, true);
        //CalcBodySpatialJacobian ( *modelRBDL, Q, modelRBDL->GetBodyId("RSoftHand"), G, true);
        JacobRed = G.block<3, 3>(0, 7);

        rotHandImuTr = CalcBodyWorldOrientation(*modelRBDL, Q, modelRBDL->GetBodyId(setHand), true);
        rotHandImu = rotHandImuTr.transpose();
        rotHandGlobal = rotImuGlobal * rotHandImu;
        //ComputeRotMatrix(posSensCopy, velSensCopy, from, to , fromJac, toJac , eigRobotState3, model3, kinDynComp3, rotHandImu, relPosFromJacToJAc, outJacobian);

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                transRotGlobalHand[i][j] = rotHandGlobal(j, i); //NB you are considering the TRANSPOSE of your matrix because in the imuData.get_Orientation he is computing the euler angles knowing a rotation matrix from the world to something else(in your case the hands)
            }
        }
        //Get Roll Pitch from transRotGlobalHand
        imuData.get_OrientationUB(transRotGlobalHand, cdPR, orientation);
        //  std::cout<<orientation[0]<<"    "<<orientation[1]<<"    "<<orientation[2]<<"    "<<std::endl;
        /*  if (firstTime){
        firstTime=false;
        orientationDes[0]=orientation[0];
        orientationDes[1]=orientation[1];
        orientationDes[2]=orientation[2];
    }*/
        //Error
        Eigen::MatrixXd jacM2(3, 3);
        Eigen::MatrixXd hM2(3, 1);
        Eigen::MatrixXd jacInvMhM2(3, 3);
        jacM2 = JacobRed; // Jacob Reduced to the orientation +
        //std::cout<<"JAcob:   "<<JacobRed<<std::endl;
        if (whichArm == "right")
        {
            hM2(0, 0) = -(orientation[0] - orientationDesR[0]); // error
            hM2(1, 0) = -(orientation[1] - orientationDesR[1]);
            hM2(2, 0) = 0; // -(orientation[2]-orientationDesR[2]);
        }
        else
        {
            hM2(0, 0) = -(orientation[0] - orientationDesL[0]); // error
            hM2(1, 0) = -(orientation[1] - orientationDesL[1]);
            hM2(2, 0) = 0; //-(orientation[2]-orientationDesL[2]);
        }
        // errorOrient[0] = -(orientation[0]-orientationDes[0]); // error
        //  errorOrient[1] = -(orientation[1]-orientationDes[1]);
        // errorOrient[2] = -(orientation[2]-orientationDes[2]);

        //SOLVE
        jacInvMhM2 = jacM2.bdcSvd(ComputeThinU | ComputeThinV).solve(hM2);

        qM2 -= 0.2 * jacInvMhM2;
        //qM2 -= 0.2 * jacInvMhM2;
        hNorm2 = hM2.norm();
        Q(7) = qM2(0, 0);
        Q(8) = qM2(1, 0);
        Q(9) = qM2(2, 0);
    }

    qDes[0] = Q(7) * 1;
    qDes[1] = Q(8) * 1;
    qDes[2] = Q(9) * 1;

    // if (qDes[1]<=0.05 && qDes[1]>=-0.05) qDes[1]=0; //joint24

    //if (qDes[2]<=0.01 && qDes[2]>=-0.01) qDes[2]=0;

    // if (qDes[0]<=0.05 && qDes[0]>=-0.05) qDes[0]=0;

    if (qDes[1] >= 0.45)
        qDes[1] = 0.45; //joint24
    if (qDes[1] <= -0.45)
        qDes[1] = -0.45;

    if (qDes[2] >= 0.85)
        qDes[2] = 0.85;
    if (qDes[2] <= -0.85)
        qDes[2] = -0.85;

    if (qDes[0] >= 1.5)
        qDes[0] = 1.5;
    if (qDes[0] <= -1.5)
        qDes[0] = -1.5;

    qDesTemp[0] = qDes[0];
    qDesTemp[1] = qDes[1];
    qDesTemp[2] = qDes[2];

    //std::cout<<orientation[0]<<"          "<<orientation[1]<<"          "<<qDes[0]<<"          "<<qDes[1]<<"          "<<qDes[2]<<std::endl;

    for (int i = 0; i < 3; i++)
    {
        //MedianFilter(orientationVec[i], orientationMed[i], orientation[i], 5);
        if (whichArm == "right")
        {
            AvgFilter(qDesVecR[i], qDesAvgR[i], qDes[i], 5);
        }
        else
        {
            AvgFilter(qDesVecL[i], qDesAvgL[i], qDes[i], 5);
        }
    }

    if (whichArm == "right")
    {
        for (int i = 0; i < 3; i++)
        {
            qDes[i] = qDesAvgR[i];
        }
        varsOut.orientationRollDesR_ = qDes[0];
        varsOut.orientationPitchDesR_ = qDes[1];
        varsOut.orientationYawDesR_ = qDes[2];
        varsOut.errorConvergenceR_ = hNorm2;
        varsOut.qDesOrientR_ = qDesAvgR;
    }
    else
    {
        for (int i = 0; i < 3; i++)
        {
            qDes[i] = qDesAvgL[i];
        }
        varsOut.orientationRollDesL_ = qDes[0];
        varsOut.orientationPitchDesL_ = qDes[1];
        varsOut.orientationYawDesL_ = qDes[2];
        varsOut.errorConvergenceL_ = hNorm2;
        varsOut.qDesOrientL_ = qDesAvgL;
    }

    varsOut.tm_ = time;
    varsOut.qSens_ = posSens;
    varsOut.qSensAbs_ = posSens;
    varsOut.dqSens_ = velSens;

    gettimeofday(&end, NULL);
    double sampleTime = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));
}

void Control::SetOrientation2(double time, const char *whichArm, double *qDes, double valsUpperBody[N], double trans[][3], double posSens[], double velSens[], Model *modelRBDL)
{

    gettimeofday(&start, NULL);
    VectorNd Q = VectorNd::Zero(modelRBDL->dof_count);
    //Mapping from urdf
    Q(0) = posSens[2];
    Q(1) = posSens[1];
    Q(2) = posSens[0];
    double qDes_[N] = {};
    Math::MatrixNd G(6, modelRBDL->dof_count);
    Eigen::MatrixXd JacobRed;
    const char *setHand;
    G.setZero();
    Math::Vector3d point_position;
    point_position.setZero();
    Math::Matrix3d rotHandImu, rotHandImuTr, rotHandGlobal;
    //Math::Matrix3d rotImuGlobal;
    Cmatrix qM2(2, 1);
    double transRotGlobalHand[3][3] = {}, orientationMed[3] = {};
    double orientation[3] = {}, errorOrient[2] = {};
    int it = 0;

    if (whichArm == "right")
    {
        setHand = "RSoftHand";
        Q(3) = posSens[15];
        Q(4) = posSens[16];
        Q(5) = posSens[17];
        Q(6) = posSens[18];
        Q(7) = posSens[23];
        Q(8) = posSens[24];
        Q(9) = posSens[25];
    }
    else
    {
        setHand = "LSoftHand";
        Q(3) = posSens[19];
        Q(4) = posSens[20];
        Q(5) = posSens[21];
        Q(6) = posSens[22];
        Q(7) = posSens[26];
        Q(8) = posSens[27];
        Q(9) = posSens[28];
    }

    //Convergence Error Orientation

    double hNorm2 = 100;
    qM2(0, 0) = Q(7);
    qM2(1, 0) = Q(8);
    //qM2(2, 0) = Q(9);
    //std::cout<<"qInit::       "<<qM(0, 0)<<"         "<<qM(1, 0)<<std::endl;

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            rotImuGlobal(i, j) = trans[i][j]; //NB you are now assuming that trans is R_world_to_something; if it is not you should TRANSPOSE
        }
    }

    //Compute Real Error
    rotHandImuTr = CalcBodyWorldOrientation(*modelRBDL, Q, modelRBDL->GetBodyId(setHand), true);
    rotHandImu = rotHandImuTr.transpose();
    rotHandGlobal = rotImuGlobal * rotHandImu;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            transRotGlobalHand[i][j] = rotHandGlobal(j, i); //NB you are considering the TRANSPOSE of your matrix because in the imuData.get_Orientation he is computing the euler angles knowing a rotation matrix from the world to something else(in your case the hands)
        }
    }
    //Get Roll Pitch from transRotGlobalHand
    imuData.get_OrientationUB(transRotGlobalHand, cdPR, orientation);

    if (whichArm == "right")
    {
        if (firstTimeR)
        {
            firstTimeR = false;
            orientationDesR[0] = orientation[0];
            orientationDesR[1] = orientation[1];
            //   orientationDesR[2]=orientation[2];
        }
        errorOrient[0] = -(orientation[0] - orientationDesR[0]); // error
        errorOrient[1] = -(orientation[1] - orientationDesR[1]);

        //  errorOrient[2] = -(orientation[2]-orientationDesR[2]);
        varsOut.orientationDesR_ = orientationDesR;
        varsOut.orientationRollR_ = orientation[0];
        varsOut.orientationPitchR_ = orientation[1];
        varsOut.orientationYawR_ = orientation[2];
        varsOut.orientationErrorR_ = errorOrient;
    }
    else
    {
        if (firstTimeL)
        {
            firstTimeL = false;
            orientationDesL[0] = orientation[0];
            orientationDesL[1] = orientation[1];
            //      orientationDesL[2]=orientation[2];
        }
        errorOrient[0] = -(orientation[0] - orientationDesL[0]); // error
        errorOrient[1] = -(orientation[1] - orientationDesL[1]);
        //   errorOrient[2] = -(orientation[2]-orientationDesL[2]);
        varsOut.orientationDesL_ = orientationDesL;
        varsOut.orientationRollL_ = orientation[0];
        varsOut.orientationPitchL_ = orientation[1];
        varsOut.orientationYawL_ = orientation[2];
        varsOut.orientationErrorL_ = errorOrient;
        varsOut.orientationDesR_ = orientationDesL;
        varsOut.orientationRollR_ = orientation[0];
        varsOut.orientationPitchR_ = orientation[1];
        varsOut.orientationYawR_ = orientation[2];
        varsOut.orientationErrorR_ = errorOrient;
    }

    double itMax, gainMax;
    itMax = 0;
    gainMax = 0;
    if (whichComan_ == 1)
    {
        itMax = 5;
        gainMax = 0.1;
    }
    else
    {
        itMax = 9;
        gainMax = 0.1;
    }
    // while ( (it < 5) && (hNorm2 >0.01)){
    while ((it < itMax) && (hNorm2 > 0.01))
    { //20
        it = it + 1;
        CalcPointJacobian6D(*modelRBDL, Q, modelRBDL->GetBodyId(setHand), point_position, G, true);
        //CalcBodySpatialJacobian ( *modelRBDL, Q, modelRBDL->GetBodyId("RSoftHand"), G, true);
        JacobRed = G.block<2, 2>(0, 7);

        rotHandImuTr = CalcBodyWorldOrientation(*modelRBDL, Q, modelRBDL->GetBodyId(setHand), true);
        rotHandImu = rotHandImuTr.transpose();
        rotHandGlobal = rotImuGlobal * rotHandImu;
        //ComputeRotMatrix(posSensCopy, velSensCopy, from, to , fromJac, toJac , eigRobotState3, model3, kinDynComp3, rotHandImu, relPosFromJacToJAc, outJacobian);

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                transRotGlobalHand[i][j] = rotHandGlobal(j, i); //NB you are considering the TRANSPOSE of your matrix because in the imuData.get_Orientation he is computing the euler angles knowing a rotation matrix from the world to something else(in your case the hands)
            }
        }
        //Get Roll Pitch from transRotGlobalHand
        imuData.get_OrientationUB(transRotGlobalHand, cdPR, orientation);
        //std::cout<<orientation[0]<<"    "<<orientation[1]<<"    "<<orientationDesR[0]<<"    "<<orientationDesR[1]<<"    "<<std::endl;
        /*  if (firstTime){
        firstTime=false;
        orientationDes[0]=orientation[0];
        orientationDes[1]=orientation[1];
        orientationDes[2]=orientation[2];
    }*/
        //Error
        Eigen::MatrixXd jacM2(2, 2);
        Eigen::MatrixXd hM2(2, 1);
        Eigen::MatrixXd jacInvMhM2(2, 2);
        jacM2 = JacobRed; // Jacob Reduced to the orientation +
        //std::cout<<"JAcob:   "<<JacobRed<<std::endl;
        if (whichArm == "right")
        {
            hM2(0, 0) = -(orientation[0] - orientationDesR[0]); // error
            hM2(1, 0) = -(orientation[1] - orientationDesR[1]);
            // hM2(2, 0) = -(orientation[2]-orientationDesR[2]);
        }
        else
        {
            hM2(0, 0) = -(orientation[0] - orientationDesL[0]); // error
            hM2(1, 0) = -(orientation[1] - orientationDesL[1]);
            //  hM2(2, 0) = -(orientation[2]-orientationDesL[2]);
        }
        //std::cout<<hM2(0, 0) <<"   "<<hM2(1, 0)<<"     "<<orientation[0]<<"    "<<orientation[1]<<"    "<<orientationDesR[0]<<"    "<<orientationDesR[1]<<"    "<<std::endl;
        // errorOrient[0] = -(orientation[0]-orientationDes[0]); // error
        //  errorOrient[1] = -(orientation[1]-orientationDes[1]);
        // errorOrient[2] = -(orientation[2]-orientationDes[2]);

        //SOLVE
        jacInvMhM2 = jacM2.bdcSvd(ComputeThinU | ComputeThinV).solve(hM2);

        qM2 -= gainMax * jacInvMhM2;
        //qM2 -= 0.1 * jacInvMhM2;
        hNorm2 = hM2.norm();
        Q(7) = qM2(0, 0);
        Q(8) = qM2(1, 0);
        //Q(9) = qM2(2, 0);
    }

    qDes[0] = Q(7) * 1;
    qDes[1] = Q(8) * 1;
    //qDes[2]=Q(9)*1;
    //std::cout<<"  ----------  "<<std::endl;

    // if (qDes[1]<=0.05 && qDes[1]>=-0.05) qDes[1]=0; //joint24

    //if (qDes[2]<=0.01 && qDes[2]>=-0.01) qDes[2]=0;

    // if (qDes[0]<=0.05 && qDes[0]>=-0.05) qDes[0]=0;

    if (qDes[1] >= 0.45)
        qDes[1] = 0.45; //joint24
    if (qDes[1] <= -0.45)
        qDes[1] = -0.45;

    /*if (qDes[2]>=0.85) qDes[2]=0.85;
    if (qDes[2]<=-0.85) qDes[2]=-0.85;*/

    if (qDes[0] >= 1.5)
        qDes[0] = 1.5;
    if (qDes[0] <= -1.5)
        qDes[0] = -1.5;

    qDesTemp[0] = qDes[0];
    qDesTemp[1] = qDes[1];
    //qDesTemp[2]=qDes[2];

    //std::cout<<orientation[0]<<"          "<<orientation[1]<<"          "<<qDes[0]<<"          "<<qDes[1]<<"          "<<qDes[2]<<std::endl;

    for (int i = 0; i < 2; i++)
    {
        //MedianFilter(orientationVec[i], orientationMed[i], orientation[i], 5);
        if (whichArm == "right")
        {
            AvgFilter(qDesVecR[i], qDesAvgR[i], qDes[i], 5);
        }
        else
        {
            AvgFilter(qDesVecL[i], qDesAvgL[i], qDes[i], 5);
        }
    }

    if (whichArm == "right")
    {
        for (int i = 0; i < 2; i++)
        {
            qDes[i] = qDesAvgR[i];
        }
        varsOut.orientationRollDesR_ = qDes[0];
        varsOut.orientationPitchDesR_ = qDes[1];
        varsOut.orientationYawDesR_ = 0; //qDes[2];
        varsOut.errorConvergenceR_ = hNorm2;
        varsOut.qDesOrientR_ = qDesAvgR;
    }
    else
    {
        for (int i = 0; i < 3; i++)
        {
            qDes[i] = qDesAvgL[i];
        }
        varsOut.orientationRollDesL_ = qDes[0];
        varsOut.orientationPitchDesL_ = qDes[1];
        varsOut.orientationYawDesL_ = 0; //qDes[2];
        varsOut.errorConvergenceL_ = hNorm2;
        varsOut.qDesOrientL_ = qDesAvgL;

        varsOut.orientationRollDesR_ = qDes[0];
        varsOut.orientationPitchDesR_ = qDes[1];
        varsOut.orientationYawDesR_ = 0; //qDes[2];
        varsOut.errorConvergenceR_ = hNorm2;
        varsOut.qDesOrientR_ = qDesAvgL;
    }

    varsOut.tm_ = time;
    varsOut.qSens_ = posSens;
    varsOut.qSensAbs_ = posSens;
    varsOut.dqSens_ = velSens;

    gettimeofday(&end, NULL);
    double sampleTime = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));
}

void Control::SetOrientation3(double time, std::string whichArm, double *qDes, double valsUpperBody[N], double trans[][3], double posSens[], double velSens[], Model *modelRBDL)
{

    gettimeofday(&start, NULL);
    VectorNd Q = VectorNd::Zero(modelRBDL->dof_count);
    //Mapping from urdf
    Q(0) = posSens[2];
    Q(1) = posSens[1];
    Q(2) = posSens[0];
    double qDes_[N] = {};
    Math::MatrixNd G(6, modelRBDL->dof_count);
    Eigen::MatrixXd JacobRed;
    const char *setHand;
    G.setZero();
    Math::Vector3d point_position;
    point_position.setZero();
    Math::Matrix3d rotHandImu, rotHandImuTr, rotHandGlobal;
    //Math::Matrix3d rotImuGlobal;
    Cmatrix qM2(3, 1);
    double transRotGlobalHand[3][3] = {}, orientationMed[3] = {};
    double orientation[3] = {}, errorOrient[2] = {};
    int it = 0;

    if (whichArm == "right")
    {
        setHand = "RSoftHand";
    }
    else
    {
        setHand = "LSoftHand";
    }

    //

    if (whichArm == "right")
    {
        setHand = "RSoftHand";
        Q(3) = posSens[15]; //to check
        Q(4) = posSens[16];
        Q(5) = posSens[17];
        Q(6) = posSens[18];
        Q(7) = posSens[23];
        Q(8) = posSens[24];
        Q(9) = posSens[25];
    }
    else
    {
        setHand = "LSoftHand";
        Q(3) = posSens[19];
        Q(4) = posSens[20];
        Q(5) = posSens[21];
        Q(6) = posSens[22];
        Q(7) = posSens[26];
        Q(8) = posSens[27];
        Q(9) = posSens[28];
    }

    // std::cerr << "Check: " << whichArm << "-" << setHand << std::endl;
    //Convergence Error Orientation

    double hNorm2 = 100;
    qM2(0, 0) = Q(7);
    qM2(1, 0) = Q(8);
    qM2(2, 0) = Q(9);
    //std::cout<<"qInit::       "<<qM(0, 0)<<"         "<<qM(1, 0)<<std::endl;

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            rotImuGlobal(i, j) = trans[i][j]; //NB you are now assuming that trans is R_world_to_something; if it is not you should TRANSPOSE
        }
    }

    //Compute Real Error
    rotHandImuTr = CalcBodyWorldOrientation(*modelRBDL, Q, modelRBDL->GetBodyId(setHand), true);
    rotHandImu = rotHandImuTr.transpose();
    rotHandGlobal = rotImuGlobal * rotHandImu;

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            transRotGlobalHand[i][j] = rotHandGlobal(j, i); //NB you are considering the TRANSPOSE of your matrix because in the imuData.get_Orientation he is computing the euler angles knowing a rotation matrix from the world to something else(in your case the hands)
            rotHand[i][j] = transRotGlobalHand[i][j];
        }
    }
    //Get Roll Pitch from transRotGlobalHand
    imuData.get_OrientationUB(transRotGlobalHand, cdPR, orientation);

    if (whichArm == "right")
    {
        if (firstTimeR)
        {
            firstTimeR = false;
            orientationDesR[0] = orientation[0];
            orientationDesR[1] = orientation[1];
            orientationDesR[2] = orientation[2]; //
        }
        errorOrient[0] = -(orientation[0] - orientationDesR[0]); // error
        errorOrient[1] = -(orientation[1] - orientationDesR[1]);

        errorOrient[2] = -(orientation[2] - orientationDesR[2]); //
        orientationHandsRRoll = orientationDesR[0];
        orientationHandsRPitch = orientationDesR[1];
        orientationHandsRYaw = orientationDesR[2];
        varsOut.orientationDesR_ = orientationDesR;
        varsOut.orientationRollR_ = orientation[0];
        varsOut.orientationPitchR_ = orientation[1];
        varsOut.orientationYawR_ = orientation[2];
        varsOut.orientationErrorR_ = errorOrient;
    }
    else
    {
        if (firstTimeL)
        {
            firstTimeL = false;
            orientationDesL[0] = orientation[0];
            orientationDesL[1] = orientation[1];
            orientationDesL[2] = orientation[2]; //
        }
        errorOrient[0] = -(orientation[0] - orientationDesL[0]); // error
        errorOrient[1] = -(orientation[1] - orientationDesL[1]);
        errorOrient[2] = -(orientation[2] - orientationDesL[2]); //
        orientationHandsLRoll = orientationDesL[0];
        orientationHandsLPitch = orientationDesL[1];
        orientationHandsLYaw = orientationDesL[2];
        varsOut.orientationDesL_ = orientationDesL;
        varsOut.orientationRollL_ = orientation[0];
        varsOut.orientationPitchL_ = orientation[1];
        varsOut.orientationYawL_ = orientation[2];
        varsOut.orientationErrorL_ = errorOrient;
    }

    double itMax, gainMax;
    itMax = 0;
    gainMax = 0;
    if (whichComan_ == 1)
    {
        itMax = 30;    //to check
        gainMax = 0.8; // CHANGE GAIN HERE (=0.1)
    }
    else
    {
        itMax = 9;
        gainMax = 0.1;
    }
    // while ( (it < 5) && (hNorm2 >0.01)){

    // std::cerr << "Set orientation eror: ";
    while ((it < itMax) && (hNorm2 > 0.0001)) //(it < itMax) &&
    {                                         //20
        it = it + 1;
        CalcPointJacobian6D(*modelRBDL, Q, modelRBDL->GetBodyId(setHand), point_position, G, true);
        //CalcBodySpatialJacobian ( *modelRBDL, Q, modelRBDL->GetBodyId("RSoftHand"), G, true);
        JacobRed = G.block<3, 3>(0, 7); //RBDL has first the orientations and then the positions // 2,3

        rotHandImuTr = CalcBodyWorldOrientation(*modelRBDL, Q, modelRBDL->GetBodyId(setHand), true);
        rotHandImu = rotHandImuTr.transpose();
        rotHandGlobal = rotImuGlobal * rotHandImu;
        //ComputeRotMatrix(posSensCopy, velSensCopy, from, to , fromJac, toJac , eigRobotState3, model3, kinDynComp3, rotHandImu, relPosFromJacToJAc, outJacobian);

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                transRotGlobalHand[i][j] = rotHandGlobal(j, i); //NB you are considering the TRANSPOSE of your matrix because in the imuData.get_Orientation he is computing the euler angles knowing a rotation matrix from the world to something else(in your case the hands)
            }
        }
        //Get Roll Pitch from transRotGlobalHand
        imuData.get_OrientationUB(transRotGlobalHand, cdPR, orientation);
        //std::cout<<orientation[0]<<"    "<<orientation[1]<<"    "<<orientationDesR[0]<<"    "<<orientationDesR[1]<<"    "<<std::endl;
        /*  if (firstTime){
        firstTime=false;
        orientationDes[0]=orientation[0];
        orientationDes[1]=orientation[1];
        orientationDes[2]=orientation[2];
    }*/
        //Error
        Eigen::MatrixXd jacM2(3, 3); // 2,3
        Eigen::MatrixXd hM2(3, 1);   // 2,1
        Eigen::MatrixXd jacInvMhM2(3, 1);
        jacM2 = JacobRed; // Jacob Reduced to the orientation +
        //std::cout<<"JAcob:   "<<JacobRed<<std::endl;
        if (whichArm == "right")
        {
            hM2(0, 0) = -(orientation[0] - orientationDesR[0]); // error
            hM2(1, 0) = -(orientation[1] - orientationDesR[1]);
            hM2(2, 0) = -(orientation[2] - orientationDesR[2]); //
        }
        else
        {
            hM2(0, 0) = -(orientation[0] - orientationDesL[0]); // error
            hM2(1, 0) = -(orientation[1] - orientationDesL[1]);
            hM2(2, 0) = -(orientation[2] - orientationDesL[2]); //
        }
        // std::cout << hM2(0, 0) << "   " << hM2(1, 0) << "     " << orientation[0] << "    " << orientation[1] << "    " << orientationDesR[0] << "    " << orientationDesR[1] << "    " << std::endl;
        // errorOrient[0] = -(orientation[0] - orientationDes[0]); // error
        // errorOrient[1] = -(orientation[1] - orientationDes[1]);
        // errorOrient[2] = -(orientation[2] - orientationDes[2]);

        //SOLVE

        jacInvMhM2 = jacM2.bdcSvd(ComputeThinU | ComputeThinV).solve(hM2);

        qM2 -= gainMax * jacInvMhM2;
        hNorm2 = hM2.norm();
        Q(7) = qM2(0, 0);
        Q(8) = qM2(1, 0);
        Q(9) = qM2(2, 0);
        // std::cerr << hNorm2 << " | ";
    }

    // std::cerr << std::endl;
    // std::cerr << "OrientationDes right: " << orientationDesR[0] << " | " << orientationDesR[1] << " | " << orientationDesR[2] << std::endl;
    // std::cerr << "OrientationDes Left: " << orientationDesL[0] << " | " << orientationDesL[1] << " | " << orientationDesL[2] << std::endl;

    qDes[0] = Q(7) * 1;
    qDes[1] = Q(8) * 1;
    qDes[2] = Q(9) * 1;

    //std::cout<<"  ----------  "<<std::endl;

    // if (qDes[1]<=0.05 && qDes[1]>=-0.05) qDes[1]=0; //joint24

    //if (qDes[2]<=0.01 && qDes[2]>=-0.01) qDes[2]=0;

    // if (qDes[0]<=0.05 && qDes[0]>=-0.05) qDes[0]=0;

    if (qDes[1] >= 0.45)
        qDes[1] = 0.45; //joint24
    if (qDes[1] <= -0.45)
        qDes[1] = -0.45;

    if (qDes[2] >= 0.85)
        qDes[2] = 0.85;
    if (qDes[2] <= -0.85)
        qDes[2] = -0.85;

    if (qDes[0] >= 1.5)
        qDes[0] = 1.5;
    if (qDes[0] <= -1.5)
        qDes[0] = -1.5;

    qDesTemp[0] = qDes[0];
    qDesTemp[1] = qDes[1];
    qDesTemp[2] = qDes[2];

    //std::cout<<orientation[0]<<"          "<<orientation[1]<<"          "<<qDes[0]<<"          "<<qDes[1]<<"          "<<qDes[2]<<std::endl;

    for (int i = 0; i < 3; i++)
    {
        //MedianFilter(orientationVec[i], orientationMed[i], orientation[i], 5);
        if (whichArm == "right")
        {
            AvgFilter(qDesVecR[i], qDesAvgR[i], qDes[i], 5);
        }
        else
        {
            AvgFilter(qDesVecL[i], qDesAvgL[i], qDes[i], 5);
        }
    }

    if (whichArm == "right")
    {
        for (int i = 0; i < 3; i++)
        {
            // qDes[i] = qDesAvgR[i];
        }
        varsOut.orientationRollDesR_ = qDes[0];
        varsOut.orientationPitchDesR_ = qDes[1];
        varsOut.orientationYawDesR_ = qDes[2];
        varsOut.errorConvergenceR_ = hNorm2;
        varsOut.qDesOrientR_ = qDesAvgR;
    }
    else
    {
        for (int i = 0; i < 3; i++)
        {
            // qDes[i] = qDesAvgL[i];
        }
        varsOut.orientationRollDesL_ = qDes[0];
        varsOut.orientationPitchDesL_ = qDes[1];
        varsOut.orientationYawDesL_ = qDes[2];
        varsOut.errorConvergenceL_ = hNorm2;
        varsOut.qDesOrientL_ = qDesAvgL;
    }

    varsOut.tm_ = time;
    varsOut.qSens_ = posSens;
    varsOut.qSensAbs_ = posSens;
    varsOut.dqSens_ = velSens;

    gettimeofday(&end, NULL);
    double sampleTime = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));
}

void Control::UpperBody(double vals[N], double time, double ForceRightHand0[3], double ForceLeftHand0[3], double Pos_sens[], double Vel_sens[], double forceRightHand[3], double forceLeftHand[3], double torqueRightHand[3], double torqueLeftHand[3], double Q0[], double Q1[], bool LoadModelFlag, EigenRobotState eigRobotStateMainARM, const iDynTree::Model modelMainARM, iDynTree::KinDynComputations &kinDynCompMainARM, bool onlyUpperBody)
{

    //    static double handRightPosFilt=0;
    Eigen::VectorXf f_des_right(6);
    Eigen::VectorXf f_real_right(6);
    Eigen::VectorXf f_des_left(6);
    Eigen::VectorXf f_real_left(6);
    Eigen::VectorXf torqueDesRight1(13), torqueToT(13);
    Eigen::VectorXf torqueRealRight1(13);
    Eigen::VectorXf torqueDesLeft1(13);
    Eigen::VectorXf torqueRealLeft1(13);
    Eigen::VectorXf qd_rightFloatingBase(13);
    Eigen::VectorXf handRightPos(6);
    Eigen::VectorXf qd_leftFloatingBase(13);
    Eigen::VectorXf handLeftVel(6);
    //Internal Variables
    //        static bool LoadModelFlag = true;
    //        double FR[3];
    //        double FL[3];
    std::vector<double> FRxVec, FRyVec, FRzVec, FLxVec, FLyVec, FLzVec;
    double ForceRightHand[3] = {};
    double ForceLeftHand[3] = {};

    //Filtering
    AvgFilter(FRxVec, ForceRightHand[0], forceRightHand[0], M2);
    AvgFilter(FRyVec, ForceRightHand[1], forceRightHand[1], M2);
    AvgFilter(FRzVec, ForceRightHand[2], forceRightHand[2], M2);
    AvgFilter(FLxVec, ForceLeftHand[0], forceLeftHand[0], M2);
    AvgFilter(FLyVec, ForceLeftHand[1], forceLeftHand[1], M2);
    AvgFilter(FLzVec, ForceLeftHand[2], forceLeftHand[2], M2);

    double alpha = 0.01;

    for (int i = 0; i < N; i++)
    {
        pos_des = Q1[i]; //+(Q0[i]-Q1[i])*exp(-alpha*time);
        if (i == 29)
        {
            pos_des = 550;
        }
        vel_des = 0; //-alpha*(Q0[i]-Q1[i])*exp(-alpha*time);
        y_des[i] = Pos_sens[i] - pos_des;
        dy_des[i] = Vel_sens[i] - vel_des;
    }

    // here filter in case

    // Right
    double q_right[N_JOINT_ARM] = {};
    double qd_right[N_JOINT_ARM] = {};
    //double qd_rightFloatingBase[N_JOINT_ARM+6];
    for (int i = 0; i < N_JOINT_ARM; i++) // shoulder+elbow pitch
    {
        q_right[i] = 0;
        qd_right[i] = 0;
    }

    for (int i = 15; i < 19; i++) // shoulder+elbow pitch
    {
        q_right[i - 15] = Pos_sens[i];
        qd_right[i - 15] = Vel_sens[i];
    }

    for (int i = 23; i < 26; i++) //forearm+wrist
    {
        q_right[i - 19] = Pos_sens[i];
        qd_right[i - 19] = Vel_sens[i];
    }

    /*for(int i=0; i<13; i++) // shoulder+elbow pitch
        {
            if (i<6)
                qd_rightFloatingBase(i) =0;
            else
                qd_rightFloatingBase(i) =qd_right[i];
        }*/

    this->computeDynKin(q_right, qd_right, ForceRightHand, "RightArm", eigRobotStateMainARM, modelMainARM, kinDynCompMainARM); // compute dynamics for right arm

    for (int i = 0; i < 3; i++)
    {
        handPositionR_Abs[i] = 0;     //handPositionTemp_Abs[i];
        handRightPos[i] = 0;          //handPositionR_Abs[i];
        handRightPos[i] = 0;          //handPositionR_Abs[i];
        shoulderPositionR_Abs[i] = 0; //shoulderPositionTemp_Abs[i];
        elbowPositionR_Abs[i] = 0;    // elbowPositionTemp_Abs[i];
        basePositionR_Abs[i] = 0;     // basePositionTemp_Abs[i];
    }

    //std::cout<<ForceLeftHand[0]<<"     "<<ForceLeftHand[1]<<"       "<<ForceLeftHand[2]<<"   "<<ForceRightHand[0]<<"     "<<ForceRightHand[1]<<"       "<<ForceRightHand[2]<<std::endl;
    for (int i = 0; i < 6; i++)
    {
        f_des_right(i) = 0.0;
    }

    if (whichComan_ == 1)
    {
        f_des_right(1) = -ForceRightHand0[1]; //-(ForceRightHand0[1]+ForceLeftHand0[1])/2;
        f_des_right(2) = -(ForceRightHand0[2] + ForceLeftHand0[2]) / 2;
        f_des_right(0) = (ForceRightHand0[0] + ForceLeftHand0[0]) / 2; //sin(time);
    }
    else
    {
        f_des_right(1) = (ForceRightHand0[1] - ForceLeftHand0[1]) / 2; // positive for COMAN2, negative for COMA1
        f_des_right(2) = (ForceRightHand0[2] + ForceLeftHand0[2]) / 2;
        f_des_right(0) = (ForceRightHand0[0] - ForceLeftHand0[0]) / 2; //sin(time);
    }

    for (int i = 0; i < 6; i++)
    {
        f_real_right(i) = 0.0;
    }

    if (whichComan_ == 1)
    {
        f_real_right(1) = -ForceRightHand[1]; //-(ForceRightHand[1]-ForceLeftHand[1])/2;
        f_real_right(2) = -(ForceRightHand[2] + ForceLeftHand[2]) / 2;
        f_real_right(0) = (ForceRightHand[0] + ForceLeftHand[0]) / 2; //sin(time);
    }
    else
    {
        f_real_right(1) = (ForceRightHand[1] - ForceLeftHand[1]) / 2; // positive for COMAN2, negative for COMA1
        f_real_right(2) = (ForceRightHand[2] + ForceLeftHand[2]) / 2;
        f_real_right(0) = (ForceRightHand[0] - ForceLeftHand[0]) / 2; //sin(time);
    }

    torqueDesRight1 = JacobTrasp * f_des_right;
    torqueRealRight1 = JacobTrasp * f_real_right;
    torqueToT = JacobTrasp * (f_des_right - f_real_right);
    for (int i = 0; i < 7; i++)
    {
        torqueDesRight[i] = torqueDesRight1(i + 6);
        torqueRealRight[i] = torqueRealRight1(i + 6);
        torqueALLRight[i] = torqueToT(i);
        //torqueALLRight[i]=jntTorques(i);
        torqueGravityRight[i] = gGF.jointTorques().getVal(i);
    }

    // Left arm
    /*  double q_left[N_JOINT_ARM];
        double qd_left[N_JOINT_ARM];
        for(int i=19; i<23; i++) // shoulder+elbow pitch
        {
            q_left[i-19] =Pos_sens[i];   // CHANGEEEEEEEEEEEEEEEEEEE
            qd_left[i-19] =Vel_sens[i];
        }
        for(int i=26; i<29; i++)//forearm+wrist
        {
            q_left[i-22] =Pos_sens[i];
            qd_left[i-22] =Vel_sens[i];
        }

       //toAdd this-> computeDynKin(q_left,qd_left,ForceLeftHand, "LeftArm",eigRobotStateMainARM, modelMainARM,kinDynCompMainARM); // compute dynamics for right arm
        for (int i=0; i<3; i++){
            handPositionL_Abs[i]= 0;//handPositionTemp_Abs[i];
        }

        for (int i=0; i<6; i++){
            f_des_left(i)=0.0;
        }
        if (whichComan_==1){
            f_des_left(0)=ForceLeftHand0[0];//sin(time); negative for COMAN2 and positive for COMAN1
        }
        else{
            f_des_left(0)=-ForceLeftHand0[0];//sin(time); negative for COMAN2 and positive for COMAN1
        }

        f_des_left(1)=-ForceLeftHand0[1];
        f_des_left(2)=ForceLeftHand0[2];
        for (int i=0; i<6; i++){
            f_real_left(i)=0.0;
        }
        if (whichComan_==1){
        f_real_left(0)=ForceLeftHand[0];//sin(time);//sin(time); negative for COMAN2 and positive for COMAN1
        }
        else{

           f_real_left(0)=-ForceLeftHand[0];
        }
        f_real_left(1)=-ForceLeftHand[1];
        f_real_left(2)=ForceLeftHand[2];
        torqueDesLeft1=JacobTrasp*f_des_left;
        torqueRealLeft1=JacobTrasp*f_real_left;
        torqueToT= JacobTrasp*(f_des_left- f_real_left);*/
    for (int i = 0; i < 7; i++)
    {
        torqueDesLeft[i] = 0;  //torqueDesLeft1(i+6);
        torqueRealLeft[i] = 0; //torqueRealLeft1(i+6);
        //torqueALLLeft[i]=jntTorques(i);
        torqueALLLeft[i] = 0;     //torqueToT(i);
        torqueGravityLeft[i] = 0; //gGF.jointTorques().getVal(i);
        //std::cout<<(torqueDesLeft[i]-torqueRealLeft[i])<<std::endl;
    }

    double tauDES[N] = {};
    double tauREAL[N] = {};
    double tauG[N] = {};
    for (int i = 0; i < N; i++)
    {
        tauDES[i] = 0;
        tauG[i] = 0;
        if (i >= 15 && i < 19)
        {
            tauDES[i] = torqueDesRight[i - 15];
            tauREAL[i] = torqueRealRight[i - 15];
            tauG[i] = torqueGravityRight[i - 15];
        }
        if (i >= 23 && i < 26)
        {
            tauDES[i] = torqueDesRight[i - 19];
            tauREAL[i] = torqueRealRight[i - 19];
            tauG[i] = torqueGravityRight[i - 19];
        }
        if (i >= 19 && i < 23)
        {
            tauDES[i] = torqueDesLeft[i - 19];
            tauREAL[i] = torqueRealLeft[i - 19];
            tauG[i] = torqueGravityLeft[i - 19];
        }
        if (i >= 26 && i < 29)
        {
            tauDES[i] = torqueDesLeft[i - 22];
            tauREAL[i] = torqueRealLeft[i - 22];
            tauG[i] = torqueGravityLeft[i - 22];
        }
        // std::cout<<(tauDES[i]-tauREAL[i])<<std::endl;
    }

    for (int i = 0; i < N; i++)
    {
        Kp[i] = 100 / 3; //100/50;
        Kd[i] = 15;      //15;
    }

    for (int i = 23; i < N; i++)
        Kd[i] = 0;

    Kp[23] = 100;
    Kd[23] = 10;
    if (whichComan_ == 1)
    {

        Kp[24] = -50; //coman1 -50 while coman2 +50
    }
    else
    {

        Kp[24] = 50; //coman1 -50 while coman2 +50
    }

    Kp[25] = 50;
    Kp[26] = 50; //changed from 100 to 50
    Kd[26] = 10;
    Kp[27] = 50;
    Kp[28] = 50;
    Kp[29] = -500;
    Kp[30] = -100;

    Kp[15] = 100 / 3;
    Kp[15] = 15;
    Kp[19] = 100 / 3;
    Kp[19] = 15;

    /*     // Gains

    for (int i = 0; i < 23; i++)
    {
        Kp[i] = 300;
        Kd[i] = 3;
    }

    double gainKP = 5; // Gain for the arms and wrists, initialy 5
    double gainKD = 0;
    double specialGain = 5;

    Kp[23] = gainKP;
    Kd[23] = gainKD;
    Kp[24] = gainKP;
    Kd[24] = gainKD;
    Kp[25] = gainKP;
    Kd[25] = gainKD;
    Kp[26] = gainKP;
    Kd[26] = gainKD;
    Kp[27] = gainKP;
    Kd[27] = gainKD;
    Kp[28] = gainKP;
    Kd[28] = gainKD;
    Kp[29] = 0;
    Kd[29] = 0;
    Kp[30] = 0;
    Kd[30] = 0; */

    //

    double VelZero[N] = {};
    VelZero[24] = 1;
    VelZero[25] = 1;
    VelZero[27] = 1;
    VelZero[28] = 1;
    VelZero[29] = 1;
    VelZero[30] = 1;

    double vmax = 9;

    // Kp[15] = 0;
    // Kd[15] = 0;

    // Kp[19] = 0;
    // Kd[19] = 0;

    for (int i = 0; i < N; i++)
    {
        // tau_des[i] =-Kp[i]*(y_des[i])-Kd[i]*(dy_des[i])+tauG[i]*0.5+(tauDES[i]-tauREAL[i])*5;

        /*tau_des[i] =-Kp[i]*(y_des[i])-Kd[i]*(dy_des[i])+(tauDES[i]-tauREAL[i])*3;*/ //+tauG[i]*0.5;//*4; last update 28July with Kp[i] = 100/50;

        tau_des[i] = -Kp[i] * (y_des[i]) - Kd[i] * (dy_des[i]) + (tauDES[i] - tauREAL[i]) * 4;
        if (i == 15 || i == 16 || i == 19 || i == 20)
        {
            // tau_des[i] = -(Kp[i] / 10) * (y_des[i]) - (Kd[i] / 10) * (dy_des[i]) + (tauDES[i] - tauREAL[i]) * 2.5 + tauG[i] * 0.05;
            tau_des[i] = -(Kp[i] / 10) * (y_des[i]) - (Kd[i] / 10) * (dy_des[i]) + (tauDES[i] - tauREAL[i]) * 2.5 + tauG[i] * 0.05;
            // tau_des[i] = -Kp[i] * (y_des[i]) - 25 * (dy_des[i]) + (tauDES[i] - tauREAL[i]) * 3; //+tauG[i]*0.5;//*4;//for COMAN2 you can remove - for COMAN1 you can add
        }

        // added in order to let the shoulde be more compliant 25 July 2017
        if (i == 16)
        {
            tau_des[i] = -(Kp[i] / 4) * (y_des[i]) - (Kd[i] / 4) * (dy_des[i]) + (tauDES[i] - tauREAL[i]) * 1 + tauG[i] * 0.5;
            /* tau_des[i] =-100/50*(y_des[i])-0.2*(dy_des[i])+(tauDES[i]-tauREAL[i])+tauG[i]*0.5;*/ //*4; last update 28 July
            // tau_des[i] =-2*(y_des[i])-0.2*(dy_des[i])+(tauDES[i]-tauREAL[i])+tauG[i]*0.5;

            // tau_des[i] =-2*(y_des[i])-0.2*(dy_des[i])+(tauDES[i]-tauREAL[i])+tauG[i]*0.5;
        }
        //

        vals[i] = tau_des[i];
    }

    varsOut.torqueRightHand_ = torqueRightHand;
    varsOut.torqueLeftHand_ = torqueLeftHand;
    if (onlyUpperBody)
    {

        // Var for Output
        /*s varsOut.tm_=time;
            varsOut.qSens_=Pos_sens;
            varsOut.qSensAbs_=Pos_sens;
            varsOut.dqSens_=Vel_sens;
            varsOut.tauSens_= tauREAL;
            varsOut.forceRightHand_=forceRightHand;
            varsOut.forceLeftHand_=forceLeftHand;*/
    }
}

void Control::ForceEndEffector(double *fMapping, std::string whichArm, double posSens[], double velSens[], double forceRightHandGen[6], double forceLeftHandGen[6], EigenRobotState eigRobotState2, EigenRobotState eigRobotState3, const iDynTree::Model model2, const iDynTree::Model model3, iDynTree::KinDynComputations &kinDynComp2, iDynTree::KinDynComputations &kinDynComp3, double forceSensors[3])
{

    //std::cout<<forceLeftHandGen [3]<<"   "<<forceLeftHandGen [4]<<"   "<<forceLeftHandGen [5]<<std::endl;
    iDynTree::MatrixDynSize jacobFootHand;
    iDynTree::MatrixDynSize jacobElbowHand;
    Eigen::MatrixXd jacobFootHandTransp;
    Eigen::MatrixXd jacobFootElbowTransp;
    iDynTree::MatrixDynSize jacobFootElbow;
    Eigen::MatrixXd jacobFootHandEig;
    Eigen::MatrixXd jacobFootElbowEig;
    std::string whichEndEffectorElbow;
    std::string whichEndEffectorHand;
    std::string whichFoot;
    iDynTree::FreeFloatingGeneralizedTorques gFootHand;
    iDynTree::FreeFloatingGeneralizedTorques gFootElbow;
    //Eigen::VectorXd fExt(6);
    Eigen::VectorXd fExtWRTFootGen(6), fExtWRTFoot(3), tExtWRTFoot(3), fIntWRTFoot(3), fgWRTFoot(3), tIntWRTFoot2(3), tFgWRTFoot2(3), relPosElbowHand(3), relPosCoMHand(3), fIntOnlyNoTransf(3), tIntWRTFoot(3), tIntOnlyNoTransf(3), relPosHandFoot(3), relPosElbowFoot(3);
    Eigen::VectorXd fExtOnly(3), tExtOnly(3), fElbowHand(3), fExtSensor(3);
    Eigen::VectorXd jTFint(6);
    Eigen::Matrix<double, 3, 3> rotMatrixEigFootHand, rotMatrixEigHandFoot;
    Eigen::Matrix<double, 3, 3> rotMatrixEigElbowHand;
    Eigen::Matrix<double, 3, 3> rotMatrixEigElbowFoot;
    Eigen::Matrix<double, 3, 3> rotMatrixEigForceSensElbow;

    //    int kkk=0;
    int kkk = 500;

    AvgFilter(ForceVecLx, forceLeftHandGenFilt[0], forceLeftHandGen[0], kkk);
    AvgFilter(ForceVecLy, forceLeftHandGenFilt[1], forceLeftHandGen[1], kkk);
    AvgFilter(ForceVecLz, forceLeftHandGenFilt[2], forceLeftHandGen[2], kkk);
    AvgFilter(TorqueVecLx, forceLeftHandGenFilt[3], forceLeftHandGen[3], kkk);
    AvgFilter(TorqueVecLy, forceLeftHandGenFilt[4], forceLeftHandGen[4], kkk);
    AvgFilter(TorqueVecLz, forceLeftHandGenFilt[5], forceLeftHandGen[5], kkk);

    AvgFilter(ForceVecRx, forceRightHandGenFilt[0], forceRightHandGen[0], kkk);
    AvgFilter(ForceVecRy, forceRightHandGenFilt[1], forceRightHandGen[1], kkk);
    AvgFilter(ForceVecRz, forceRightHandGenFilt[2], forceRightHandGen[2], kkk);
    AvgFilter(TorqueVecRx, forceRightHandGenFilt[3], forceRightHandGen[3], kkk);
    AvgFilter(TorqueVecRy, forceRightHandGenFilt[4], forceRightHandGen[4], kkk);
    AvgFilter(TorqueVecRz, forceRightHandGenFilt[5], forceRightHandGen[5], kkk);

    double n = 0, p = 0, fact = 0;

    //Select the arm (right or left??)
    if (whichArm == "right")
    {

        //the following transformation if valid ONLY for COMAN2
        if (whichComan_ == 2)
        {
            rotMatrixEigForceSensElbow << -1, 0, 0,
                0, -1, 0,
                0, 0, 1;
        }
        else
        {

            rotMatrixEigForceSensElbow << -1, 0, 0,
                0, -1, 0,
                0, 0, 1;
        }
        whichEndEffectorElbow = "RElb";
        whichEndEffectorHand = "RSoftHand";
        whichFoot = "RFoot";
        for (int i = 0; i < 6; i++)
        {
            //fInt(i)=forceRightHand[i];
            if (i < 3)
            {
                fIntOnlyNoTransf(i) = forceRightHandGenFilt[i];
            }
            else
            {
                tIntOnlyNoTransf(i - 3) = forceRightHandGenFilt[i];
            }
        }
    }
    else
    {
        if (whichArm == "left")
        {
            //the following transformation if valid ONLY for COMAN2
            if (whichComan_ == 2)
            {
                fact = 6;
                rotMatrixEigForceSensElbow << 1, 0, 0,
                    0, 1, 0,
                    0, 0, 1;
            }
            else
            { //coman 1

                rotMatrixEigForceSensElbow << -1, 0, 0,
                    0, -1, 0,
                    0, 0, 1;
            }

            whichEndEffectorElbow = "LElb";
            whichFoot = "LFoot";
            whichEndEffectorHand = "LSoftHand";
            for (int i = 0; i < 6; i++)
            {

                if (i < 3)
                {
                    fIntOnlyNoTransf(i) = forceLeftHandGenFilt[i];
                }
                else
                {
                    tIntOnlyNoTransf(i - 3) = forceLeftHandGenFilt[i];
                }
            }
        }
        else
        {
            throw std::invalid_argument("whichArm is not set correctly: it can be ONLY 'right' or 'left'");
        }
    }

    //Select the base frame
    whichFoot = "LFoot";
    /*   if (kL>=0.5){
//       whichFoot = "LFoot";
       whichFoot = "RFoot";
    }
    else{
        if (kR>=0.5){
           whichFoot = "RFoot";
        }
        else{
            whichFoot = "RFoot";
            std::cout<<"WARNING!!!!! -->> ROBOT ON AIR!!!!!!!! COMPUTATION OF FORCES NOT RELIABLE"<<std::endl;
        }
    }*/
    //Convert Forces at the Elbow in the base frame

    //Compute Relative Jacobian (m=6 x n=change according to the selected end effector)
    // void Control::ComputeRotMatrix(double  qq[], double  qqd[],std::string from, std::string to, EigenRobotState eigRobotState, const iDynTree::Model  model, iDynTree::KinDynComputations &kinDynComp, Eigen::Matrix<double,3,3> &rotMatrixEig, Eigen::VectorXd & relPos){
    iDynTree::MatrixDynSize outJacobian;
    //TO BE ADDED    ComputeRotMatrix(posSens, velSens,  whichEndEffectorHand , whichFoot, whichEndEffectorHand , whichFoot,  eigRobotState3, model3, kinDynComp3, rotMatrixEigHandFoot, relPosHandFoot, outJacobian);
    // here the homogMatrixEigFootHand is the hom transformation from the hand to the foot
    // std::cout<<rotMatrixEigElbowFoot<<std::endl;
    //TO BE ADDED     ComputeRotMatrix(posSens, velSens, whichEndEffectorElbow, whichFoot, whichEndEffectorHand , whichFoot,  eigRobotState2, model2, kinDynComp2, rotMatrixEigElbowFoot, relPosElbowFoot, outJacobian);
    //here the homogMatrixEigElbowFoot is the hom transfomation from the elbow to the foot

    // Computation of the force at the hand wrt foot frame
    fgWRTFoot(0) = 0;
    fgWRTFoot(1) = 0;
    fgWRTFoot(2) = -9.81 * 4; //4 Kg is the approximated weight of the elbow and the hand
                              //    fIntWRTFoot = rotMatrixEigElbowFoot*fIntOnlyNoTransf;
    fIntWRTFoot = rotMatrixEigElbowFoot * rotMatrixEigForceSensElbow * fIntOnlyNoTransf;
    fExtWRTFoot = (fIntWRTFoot - fgWRTFoot); //check signs

    // Computation of the torque at the hand wrt foot frame
    //TO BE ADDED     ComputeRotMatrix(posSens, velSens, whichEndEffectorElbow, whichEndEffectorHand, whichEndEffectorElbow, whichEndEffectorHand,  eigRobotState3, model3, kinDynComp3, rotMatrixEigElbowHand, relPosElbowHand, outJacobian);
    relPosCoMHand = relPosElbowHand / 2;

    tIntWRTFoot = rotMatrixEigElbowFoot * rotMatrixEigForceSensElbow * tIntOnlyNoTransf; // not sure if the torque can be rototranslated in this way
    //cross product dxF
    tIntWRTFoot2(0) = relPosElbowHand(1) * fIntWRTFoot(2) - relPosElbowHand(2) * fIntWRTFoot(1);
    tIntWRTFoot2(1) = -relPosElbowHand(0) * fIntWRTFoot(2) + relPosElbowHand(2) * fIntWRTFoot(0);
    tIntWRTFoot2(2) = relPosElbowHand(0) * fIntWRTFoot(1) - relPosElbowHand(1) * fIntWRTFoot(0);

    tFgWRTFoot2(0) = relPosCoMHand(1) * fgWRTFoot(2) - relPosCoMHand(2) * fgWRTFoot(1);
    tFgWRTFoot2(1) = -relPosCoMHand(0) * fgWRTFoot(2) + relPosCoMHand(2) * fgWRTFoot(0);
    tFgWRTFoot2(2) = relPosCoMHand(0) * fgWRTFoot(1) - relPosCoMHand(1) * fgWRTFoot(0);

    tExtWRTFoot = (tIntWRTFoot - tIntWRTFoot2 - tFgWRTFoot2); //check signs
    fExtWRTFootGen << fExtWRTFoot, tIntWRTFoot;
    varsOut.fExt_ = fExtWRTFootGen;
    fMapping[0] = fExtWRTFootGen(2) * 6; //7(molto bene), 8(meglio),9, 10
    fMapping[1] = fExtWRTFootGen(1) * 6;
    fMapping[2] = fExtWRTFootGen(0) * 6;
}

void Control::ComputeRotMatrix(double qq[], double qqd[], std::string from, std::string to, EigenRobotState eigRobotState, const iDynTree::Model model, iDynTree::KinDynComputations &kinDynComp, Eigen::Matrix<double, 3, 3> &rotMatrixEig, Eigen::VectorXd &relPos)
{

    for (unsigned int i = 0; i < model.getNrOfDOFs(); i++)
    {
        eigRobotState.jointPos(i) = qq[i];
        eigRobotState.jointVel(i) = qqd[i];
    }

    // Kinematics

    idynRobotState.resize(model.getNrOfDOFs());
    iDynTree::fromEigen(idynRobotState.world_H_base, eigRobotState.world_H_base);
    iDynTree::toEigen(idynRobotState.jointPos) = eigRobotState.jointPos;
    iDynTree::fromEigen(idynRobotState.baseVel, eigRobotState.baseVel);
    toEigen(idynRobotState.jointVel) = eigRobotState.jointVel;
    toEigen(idynRobotState.gravity) = eigRobotState.gravity;

    kinDynComp.setRobotState(idynRobotState.world_H_base, idynRobotState.jointPos,
                             idynRobotState.baseVel, idynRobotState.jointVel, idynRobotState.gravity);

    iDynTree::FrameIndex refFrameIndex = model.getFrameIndex(to);
    iDynTree::FrameIndex frameIndex = model.getFrameIndex(from);
    //iDynTree::FrameIndex refFrameIndexJac = model.getFrameIndex(toJac);
    //iDynTree::FrameIndex frameIndexJac = model.getFrameIndex(fromJac);
    //iDynTree::MatrixDynSize  outJacobian;

    iDynTree::Transform Prova;
    //iDynTree::Transform ProvaJac;
    iDynTree::Matrix4x4 homogMatrix;
    // iDynTree::Matrix4x4 homogMatrixJac;
    Eigen::Matrix<double, 4, 4> rotMatrix;
    // Eigen::Matrix<double,4,4> rotMatrixJac;
    //Eigen::Matrix<double,3,3> rotMatrixEigJac;

    //Computation of the relative Jacobian between two frame "fromJac" and "toJac"
    // This is done in case you want to compute the orientation (rotMatrixEig) from a frameX to a frameY while the relative position (relPos) from the frameX to the frameZ
    //kinDynComp.getRelativeJacobian(refFrameIndexJac, frameIndexJac, outJacobian);
    //  ProvaJac = kinDynComp.getRelativeTransform(refFrameIndexJac, frameIndexJac);
    //  homogMatrixJac = ProvaJac.asHomogeneousTransform();
    //  rotMatrixJac = iDynTree::toEigen(homogMatrixJac);
    //  rotMatrixEigJac=rotMatrixJac.block<3,3>(0,0);
    // relPos= rotMatrixJac.block<3,1>(0,3);

    //Computation of the rotation Homogeneous matrix between the frames "from" and "to"
    // gettimeofday(&start, NULL);
    //kinDynComp.getRelativeJacobian(refFrameIndex, frameIndex, outJacobian);
    //gettimeofday(&end, NULL);
    // double sampleTime=((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));
    //std::cout<<"timeINNNN:   "<<sampleTime/1000<<"    "<<end.tv_usec<<"     "<<start.tv_usec<<std::endl;

    //gettimeofday(&start, NULL);
    Prova = kinDynComp.getRelativeTransform(refFrameIndex, frameIndex);
    homogMatrix = Prova.asHomogeneousTransform();
    rotMatrix = iDynTree::toEigen(homogMatrix);
    rotMatrixEig = rotMatrix.block<3, 3>(0, 0);
    relPos = rotMatrix.block<3, 1>(0, 3); // if you want the pos before comment it

    //gettimeofday(&end, NULL);
    //double sampleTime=((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec));
    //std::cout<<"timeINNNN:   "<<sampleTime/1000<<"    "<<end.tv_usec<<"     "<<start.tv_usec<<std::endl;

    //gGF.resize(model);
    //kinDynComp.generalizedGravityForces(gGF);

    /*iDynTree::RotationSemantics::RotationSemantics 	( 	int  	_orientationFrame,
            int  	_body,
            int  	_refOrientationFrame,
            int  	_refBody
        ) 	*/

    // forse pero' prima di questa matrce di rototraslazione ti serve una matrice di rotazione per convertire la reale posizione del force sensor wrt la terna posizionata all elbow
}

iDynTree::FreeFloatingGeneralizedTorques Control::ComputeGravity(double qq[], double qqd[], EigenRobotState eigRobotState, const iDynTree::Model model, iDynTree::KinDynComputations &kinDynComp)
{

    eigRobotState.resize(model.getNrOfDOFs());
    //    std::cout<<"model size: "<<model.getNrOfDOFs()<<std::endl;
    eigRobotState.random();
    for (unsigned int i = 0; i < model.getNrOfDOFs(); i++)
    {
        eigRobotState.jointPos(i) = qq[i];
        eigRobotState.jointVel(i) = qqd[i];
    }

    // Kinematics
    idynRobotState.resize(model.getNrOfDOFs());
    iDynTree::fromEigen(idynRobotState.world_H_base, eigRobotState.world_H_base);
    iDynTree::toEigen(idynRobotState.jointPos) = eigRobotState.jointPos;
    iDynTree::fromEigen(idynRobotState.baseVel, eigRobotState.baseVel);
    toEigen(idynRobotState.jointVel) = eigRobotState.jointVel;
    toEigen(idynRobotState.gravity) = eigRobotState.gravity;
    kinDynComp.setRobotState(idynRobotState.world_H_base, idynRobotState.jointPos,
                             idynRobotState.baseVel, idynRobotState.jointVel, idynRobotState.gravity);
    //    std::cout<<idynRobotState.world_H_base.asHomogeneousTransform().toString() <<std::endl;
    iDynTree::FreeFloatingGeneralizedTorques gF;
    gF.resize(model);
    kinDynComp.generalizedGravityForces(gF);
    //    std::cout<<"g.size-->"<<gF.getNrOfDOFs()<<std::endl;
    //    for (int i=0; i< g.getNrOfDOFs(); i++){

    //        std::cout<<g.jointTorques().getVal(i)<<std::endl;
    //    }

    return gF;
}

void Control::LowerBody(double tm, double *Q0, double *qSens, double *qSensAbs, double *dqSens, double *tauSens, double *forceRightAnkle, double *forceLeftAnkle, double *torqueRightAnkle, double *torqueLeftAnkle, double *forceRightHand, double *forceLeftHand, double trans[][3], double *imuAngRates, double *aPelvis, double *h, double *dh, double *hD, double *dhD, double *tauDes, double *vals, double euler[3])
{
    StackAsVector(tmVec, tm, M);

    DTm = 0.001;

    // Which Coman

    walkingController3.whichComan_ = comNum;

    // Navigation

    walkingController3.poseComan1 = this->poseComan1;
    walkingController3.poseComan2 = this->poseComan2;
    walkingController3.poseTable = this->poseTable;

    // IMU orientation

    thr = euler[0]; // 0
    thp = euler[1]; // 1
    thy = euler[2];
    double seOrientation[3] = {0.0};

    thPelvis[0] = thp;
    thPelvis[1] = thr;
    thPelvis[2] = thy;

    // Filtering

    for (int i = 0; i < N; i++)
    {
        MedianFilter(qSensAbsVec[i], qSensAbsMed[i], qSensAbs[i], M2);
    }

    forward_kinematics_pelvis1(qSt, pPelvis, trans, side);
    forward_kinematics_pelvis1(qSt, pPelvisTest, trans, side);
    forward_kinematics_swing_foot(qSw, pSwFtInH, orSwFt, trans, 1 - side);
    forward_kinematics_pelvis1(qStAbs, pPelvisAbs, trans, side);
    forward_kinematics_pelvis1(qStAbsMed, pPelvisAbsMed, trans, side);
    StFtToPelvisFK(qStAbs, dqSt, side, pPelvisFK, vPelvisFk);

    // const double IMU_TO_PELVIS = 0.072;
    double pPelvisAbsTmpX = pPelvisAbs[0];
    //    if (side == 1){
    //        pPelvisAbs[0] = p_pelvis_right[0] + IMU_TO_PELVIS;
    //        pPelvisAbsMed[0] = p_pelvis_right[0] + IMU_TO_PELVIS;
    //        pxAbs = p_pelvis_right[0] + IMU_TO_PELVIS;
    //    }
    //    else{
    //        pPelvisAbs[0] = p_pelvis_left[0] + IMU_TO_PELVIS;
    //        pPelvisAbsMed[0] = p_pelvis_left[0] + IMU_TO_PELVIS;
    //        pxAbs = p_pelvis_left[0] + IMU_TO_PELVIS;
    //    }

    lM = tmVec.size() == 1 ? 1 : tmVec.back() - tmVec[0];
    // lM = vecPxAbs.size() == 0 ? 1 : (DTm * vecPxAbs.size());
    AvgFilter(pxVec, px, pPelvis[0], M);
    AvgFilter(pxAbsVec, pxAbs, pPelvisAbs[0], M);
    AvgFilter(pxAbsMedVec, pxAbsMed, pPelvisAbsMed[0], M);
    pxAbs = pxAbsMed;
    AvgFilter(pyVec, py, pPelvis[1], M);
    AvgFilter(pyAbsVec, pyAbs, pPelvisAbs[1], M);
    AvgFilter(pxswfVec, pxswf, pSwFtInH[0], M4);
    AvgFilter(pyswfVec, pyswf, pSwFtInH[1], M4);
    AvgFilter(vxVec, vx, (px - px_old) / (DTm * M), M2);
    AvgFilter(vxAbsVec, vxAbs, (pxAbs - pxAbs_old) / lM, M2);
    AvgFilter(vxAbsVecF, vxAbsF, (pxAbs - pxAbs_old) / lM, M);
    AvgFilter(vxFkVec, vxFK, vPelvisFk[0], M4);
    AvgFilter(vyVec, vy, (py - py_old) / (DTm * M), M2);
    AvgFilter(vyAbsVec, vyAbs, (pyAbs - pyAbs_old) / (DTm * M), M2);
    AvgFilter(vyswfVec, vyswf, (pyswf - pyswf_old) / (DTm * M5), M5);
    AvgFilter(vxswfVec, vxswf, (pxswf - pxswf_old) / (DTm * M5), M5);
    AvgFilter(axVec, ax, aPelvis[0], M4);
    AvgFilter(thpFVec, thpF, thp, M5);
    AvgFilter(dthpFVec, dthpF, angRates[1], M5);
    AvgFilter(thrFVec, thrF, thr, M5);
    AvgFilter(dthrFVec, dthrF, angRates[0], M5);
    AvgFilter(thyFVec, thyF, thy, M5);
    AvgFilter(dthyFVec, dthyF, angRates[2], M5);
    AvgFilter(swFtPitchVec, swFtPitch, orSwFt[1], M4);
    AvgFilter(dswFtPitchVec, dswFtPitch, (swFtPitch - swFtPitch_old) / (DTm * M3), M2);
    AvgFilter(swFtRollVec, swFtRoll, orSwFt[0], M4);
    AvgFilter(dswFtRollVec, dswFtRoll, (swFtRoll - swFtRoll_old) / (DTm * M3), M2);

    for (int i(0); i < 3; i++)
    {
        AvgFilter(frcRVec[i], forceRightAnkleF[i], forceRightAnkle[i], M5);
        AvgFilter(frcLVec[i], forceLeftAnkleF[i], forceLeftAnkle[i], M5);
        AvgFilter(trqRVec[i], torqueRightAnkleF[i], torqueRightAnkle[i], M5);
        AvgFilter(trqLVec[i], torqueLeftAnkleF[i], torqueLeftAnkle[i], M5);
    }

    for (int i(0); i < 3; i++)
    {
        AvgFilter(pPelvisAbsVec[i], pPelvisAbsF[i], pPelvisAbsMed[i], M);
        StackAsVector(pPelvisAbsFVec[i], pPelvisAbsF[i], M);
    }

    unsigned int M_FORCE_FILT = 0;
    double K_FXY = 0;

    M_FORCE_FILT = 10;
    K_FXY = 1;

    double forceLeftAnkleZ = forceLeftAnkleF[2];
    double forceRightAnkleZ = forceRightAnkleF[2];

    MedianFilter(forceLeftAnkleZVec, forceLeftAnkleZMed, forceLeftAnkleZ, M_FORCE_FILT);                                        // coman 2
    MedianFilter(forceRightAnkleZVec, forceRightAnkleZMed, forceRightAnkleZ, M_FORCE_FILT);                                     // coman 2
    double f1 = pow((pow(K_FXY * forceRightAnkle[0], 2) + pow(K_FXY * forceRightAnkle[1], 2) + pow(forceRightAnkleZ, 2)), 0.5); // coman 2
    double f2 = pow((pow(K_FXY * forceLeftAnkle[0], 2) + pow(K_FXY * forceLeftAnkle[1], 2) + pow(forceLeftAnkleZ, 2)), 0.5);    // coman 2
    kRaw = f1 / (f1 + f2 + EPSILON);

    AvgFilter(kVec, kF, kRaw, M3);
    // kF = 0.5 * cos(2 * M_PI * (tm + 0.25)) + 0.5;
    kR = kF;
    kL = 1 - kF;
    /* //updated kR, kL toCheck
    if (kR < 0.5){
        kR = 2.5 * kR - 0.25;
    }
    else
    {
        kR = 1;
    }
    if (kR < 0.1){
        kR = 0;
    }


    if (kL < 0.5){
        kL = 2.5 * kL - 0.25;
    }
    else
    {
        kL = 1;
    }
    if (kL < 0.1){
        kL = 0;
    }
    */
    if (f_d == 0)
    {
        kR = 1;
        kL = 1;
    }

    dorSwFt[1] = tInStep > DTm * M4 ? dswFtPitch : 0;
    thp = thpF;
    thr = thrF;
    thy = thyF;
    orSwFt[1] = swFtPitch;
    thPelvis[0] = thp;
    thPelvis[1] = thr;
    thPelvis[2] = thy;
    pSwFtInH[0] = pxswf;
    pSwFtInH[1] = pyswf;

    count1 = (count1 + 1) % M;
    count2 = (count2 + 1) % M2;
    count3 = (count3 + 1) % M3;
    count4 = (count4 + 1) % M4;
    count5 = (count5 + 1) % M5;
    px_old = vec_px[count1];
    vec_px[count1] = px;
    pxAbs_old = vec_pxAbs[count1];
    if (vecPxAbs.size() < M)
    {
        vecPxAbs.push_back(pxAbs);
        pxAbs_old = vecPxAbs[0];
    }
    else
    {
        pxAbs_old = vecPxAbs[count1];
        vecPxAbs[count1] = pxAbs;
    }
    vec_pxAbs[count1] = pxAbs;
    py_old = vec_py[count1];
    vec_py[count1] = py;
    pyAbs_old = vec_pyAbs[count1];
    vec_pyAbs[count1] = pyAbs;
    pxswf_old = vec_pxswf[count5];
    vec_pxswf[count5] = pxswf;
    pyswf_old = vec_pyswf[count5];
    vec_pyswf[count5] = pyswf;
    swFtPitch_old = vec_swFtPitch[count3];
    vec_swFtPitch[count3] = swFtPitch;
    swFtRoll_old = vec_swFtRoll[count3];
    vec_swFtRoll[count3] = swFtRoll;

    pxAbs_old = pPelvisAbsFVec[0][0];
    pyAbs_old = pPelvisAbsFVec[1][0];
    //  pxswf_old = pxswfVec[0];
    //  pyswf_old = pyswfVec[0];
    //  swFtPitch_old = swFtPitchVec[0];
    //  swFtRoll_old = swFtRollVec[0];

    pPelvis[0] = px;
    pPelvis[1] = py;
    vPelvis[0] = vx;
    vPelvis[1] = vy;
    pPelvisAbs[0] = pxAbs;
    pPelvisAbsMed[0] = pxAbsMed;
    pPelvisAbs[1] = pyAbs;
    vPelvisAbs[0] = vxAbs;
    vPelvisAbs[1] = vyAbs;

    const double H = 0.05;
    double xCopR = forceRightAnkleF[2] < 50 ? -(torqueRightAnkleF[1] + forceRightAnkleF[0] * H) / 50 : -(torqueRightAnkleF[1] + forceRightAnkleF[0] * H) / forceRightAnkleF[2];
    double xCopL = forceLeftAnkleF[2] < 50 ? -(torqueLeftAnkleF[1] + forceLeftAnkleF[0] * H) / 50 : -(torqueLeftAnkleF[1] + forceLeftAnkleF[0] * H) / forceLeftAnkleF[2];
    t = tm - TIME_WALK;
    double tw = 0;
    if (t >= TIME_WALK && t <= TIME_WALK + 0.1)
    {
        f_d = 1 / T;
        tw = t - TIME_WALK;
        f0 = tw <= 0.1 ? 10 * tw * f_d : f_d; // fast but continuously go to f_d from f0  = 0;
    }
    // if tm > 2 * TIME_WALK start walking
    if (t >= TIME_WALK && startWalkingFlag == 0)
    {
        startWalkingFlag = 1;
        for (int i = 0; i < N; i++)
        {
            qSensInit[i] = qSens[i];
        }
        for (int i = 0; i < 3; i++)
        {
            thPelvisInit[i] = thPelvis[i];
            swFtPosInit[i] = pSwFtInH[i];
            orSwFtInit[i] = orSwFt[i];
        }
        thpF_init = thpF;
        thrF_init = thrF;
        thyF_init = thyF;
        px0 = pxAbs;
    }

    if (flagMidStep == false && vyAbs > 0)
    {
        pyMidVec.push_back(pyAbs);
        if (pyMidVec.size() > 2)
        {
            pyMidVec.erase(pyMidVec.begin());
            frontalBias = (pyMidVec[0] + pyMidVec[1]) / 2;
        }
        flagMidStep = true;
    }

    if (side != oldSide)
    {
        begsteptime = tm;
        ++stepNumber;
        SwapArrays(indexSt, indexSw, 6);
        EraseVectors();
        flagMidStep = false;
        QswKMid = QswKMid_INIT;
    }

    // if (tm == 24) // S modif
    // {
    //     begsteptime = tm;
    // }
    tInStep = tm - begsteptime;

    for (int i = 0; i < 6; i++)
    {
        qSt[i] = qSens[indexSt[i]];
        dqSt[i] = dqSens[indexSt[i]];
        qStAbs[i] = qSensAbs[indexSt[i]];
        qStAbsMed[i] = qSensAbsMed[indexSt[i]];
        qSw[i] = qSens[indexSw[i]];
    }
    // Find initial values at the beginning of the step
    if (tInStep < ZERO_S)
    {
        for (int i = 0; i < N; i++)
        {
            qSensInit[i] = qSens[i];
        }
        for (int i = 0; i < 3; i++)
        {
            thPelvisInit[i] = thPelvis[i];
            swFtPosInit[i] = pSwFtInH[i];
            orSwFtInit[i] = orSwFt[i];
        }
        thpF_init = thpF;
        thrF_init = thrF;
        thyF_init = thyF;
        px0 = pxAbs;
    }

    // Determine the stance side and sign (sg)
    oldSide = side;
    WhichSide(kF, s, side, sg);
    if (side != oldSide)
    {
        //            if (stepNumber >= 15 && stepNumber < 30){
        //                STEP_LENGTH += 0.002;
        //                f_d += .02;
        //                if (stepNumber == 15){
        //                    freqVec[0] = f_d;
        //                    freqVec[1] = f_d;
        //                    QswKMid += 0.1;
        //                }
        //                f0 += 0.02;
        //                Q_INIT[5] = -0.04;
        //                Q_INIT[10] = 0.04;
        //            }
        //            if (stepNumber >= 30 && stepNumber < 35){
        //                STEP_LENGTH += DTm;
        //                f_d +=.02;
        //                if (stepNumber == 30){
        //                    freqVec[0] = f_d;
        //                    freqVec[1] = f_d;
        //                    QswKMid += 0.1;
        //                }
        //                f0 += 0.02;
        //                Q_INIT[5] = -0.03;
        //                Q_INIT[10] = 0.03;
        //            }
        if (stepNumber == firstStopN)
        {
            STEP_LENGTH = 2 * STEP_LENGTH / 3;
        }
        if (stepNumber == firstStopN + 1)
        {
            STEP_LENGTH = STEP_LENGTH / 3;
        }
        if (stepNumber == firstStopN + 2)
        {
            STEP_LENGTH = 0;
        }
        x0 = (stepNumber > 2 && t > 1) ? STEP_LENGTH : 0;
        vxDes = STEP_LENGTH == 0 ? 0 : 2 * x0 * f_d;
        pyEnd = pyAbs;
        if (stepNumber > 1 && tInStep > 0.15)
        {
            freqVec.push_back(1 / tInStep);
            freqVec.erase(freqVec.begin());
            avgFreq = (freqVec[0] + freqVec[1]) / 2;
            last_step_freq = freqVec[1];
            f0 = f0 - 0.1 * (avgFreq - f_d); // for fd = 2.5 the coeff was set to 0.05
            f0 = saturate(f0, 3, 1.35);      // added check ****
        }
        if (stepNumber >= firstStopN + stepsToStop + 2 && abs(pSwFtInH[0] + pPelvisAbs[0]) < 0.03 && abs(vxAbsF) < 0.1 && s > 0.25 && flagStopCommand == false)
        {
            flagStopCommand = true;
            f0AtStop = f0;
            f_dAtStop = f_d;
            timeAtStop = tm;
        }
    }

    if (flagStopCommand == true)
    {
        f_d = saturatel(f_dAtStop - 10 * (tm - timeAtStop), 0);
        // drives the stance outputs to the end of the step (s = 1)
        if (f_d == 0 && tm - timeAtStop > 2 * T)
        {
            f0 = f_d;
        }
    }

    unsigned short int N_REST = 4;

    if (f0 == 0 && tm > timeAtStop + N_REST * T)
    {
        if (pPelvisAbs[0] > 0.1 || pPelvisAbs[0] < -0.06 || kF < 0.1 || kF > 0.9)
        {
            f_d = 1 / T;
            f0 = f_d;
            STEP_LENGTH = 0;
            stepNumber = 1;
            firstStopN = 2;
            side = kF < 0.5 ? 0 : 1;
            if (pPelvisAbs[0] < -0.06)
            {
                QswKMid = 1.3;
            }
            else
            {
                QswKMid = 1.1;
            }
            flagStopCommand = false;
        }
    }

    s = saturate((tInStep)*f0, 1, 0);
    //        kR = pow(kF, pow(f0 / 3, 0.3));
    //        kL = pow(1 - kF, pow(f0 / 3, 0.3));
    if (tInStep > DTm * (M4 + M2 + M5))
    {
        //            pSwFtInH[0] = pxswf;
        //            pSwFtInH[1] = pyswf;
        vSwFtInH[0] = vxswf;
        vSwFtInH[1] = vyswf;
    }
    else if (tInStep > DTm * (M5 + M4))
    {
        vSwFtInH[0] = (pxswf - pxswf_old) / (DTm * M5);
        dswFtPitch = (swFtPitch - swFtPitch_old) / (DTm * M5);
        dswFtRoll = (swFtRoll - swFtRoll_old) / (DTm * M5);
    }
    for (int i = 0; i < 3; i++)
    {
        swFtPos[i] = pSwFtInH[i];
        dswFtPos[i] = vSwFtInH[i];
    }

    // Calculate average yaw in the last two steps
    if (side != oldSide)
    {
        AvgFilter(yawVec, yaw_curr, thyF, 2);
    }

    // High-level foot adjustment
    DesiredFtPos(pxAbs, pyAbs, tInStep, px0, vxDes, vxAbs, vxAbsF, vyAbs, sg, deltaX, deltaY, kv, frontalBias, s, T);
#ifndef REAL_ROBOT
    deltaY = 0;
#endif
    // std::cerr << "Air tresh: " << forceRightAnkleZMed << " | " << forceLeftAnkleZMed << std::endl;

    if (forceRightAnkleZMed < AirTresh && forceLeftAnkleZMed < AirTresh && tm > 24.2)
    {
        stepNumber = 1;
        side = SIDE_INITIAL;
        oldSide = side;
        inAir = 1;
        if (timeInAir == 0)
        {
            timeInAir = tm;
            for (int i = 0; i < N; i++)
            {
                Q0[i] = qSens[i];
            }
        }
        init_pos(tm - timeInAir, Q0, Q_INIT, qSens, dqSens, tauDes, vals, whichComan_);
        //   cout << tm - timeInAir << endl;
    }
    else
    {
        if (inAir == 1)
        {
            t0 = tm;
            inAir = 0;
            timeInAir = 0;
        }
        if (tm - t0 < TIME_REWALK)
        {
            init_pos(tm - t0, Q_INIT, Q_INIT, qSens, dqSens, tauDes, vals, whichComan_);
            // cout << tm - t0 << endl;
        }
        else
        {
            // s = 0;
            // f0 = 0;
            // f_d = 0;
            walkingController3.EvalOutputs(s, f0, Q_INIT, qSens, dqSens, kR, kL, indexSt, indexSw, thp, dthpF, thr, dthrF, thy, dthyF, x0, deltaX, deltaY, qSensInit,
                                           swFtPosInit, thpF_init, thrF_init, thyF_init, px0, pSwFtInH, vSwFtInH, orSwFt, dorSwFt, orSwFtInit, h, dh, hD, dhD, STEP_LENGTH, QswKMid, yaw_curr);
            walkingController3.EvalTorques(s, tInStep, f_d, f0, x0, px0, Q_INIT, qSens, dqSens, kR, kL, orSwFt, tauAnkTorque, forceRightAnkleF, forceLeftAnkleF, torqueRightAnkleF, torqueLeftAnkleF,
                                           pPelvisAbs, vxAbsF, h, dh, hD, dhD, tauDes, vals);
        }
    }

    // std::cerr << f0 << " | " << f_d << std::endl;
    // std::cerr << inAir << std::endl;
    // std::cerr << std::isnan(f0) << std::endl;
    // std::cerr << std::isnan(f_d) << std::endl;
    // std::cerr << std::isinf(f0) << std::endl;
    // std::cerr << std::isinf(f_d) << std::endl;

    if (tm > 15 && (inAir == 1 || (f0 == 0 && f_d == 0) || (std::isnan(f0) || std::isnan(f_d)) || (std::isinf(f0) || std::isinf(f_d))))
    {
        stopSimulation = true;
    }

    // Var for Output
    varsOut.tm_ = tm;
    varsOut.n_ = N;
    varsOut.qSens_ = qSens;
    varsOut.qSensAbs_ = qSensAbs;
    varsOut.dqSens_ = dqSens;
    varsOut.pPelvis_ = pPelvis;
    varsOut.pPelvisAbs_ = pPelvisAbs;
    varsOut.vPelvis_ = vPelvis;
    varsOut.vPelvisAbs_ = vPelvisAbs;
    varsOut.aPelvis_ = aPelvis;
    varsOut.forceRightAnkle_ = forceRightAnkle;
    varsOut.forceLeftAnkle_ = forceLeftAnkle;
    varsOut.torqueRightAnkle_ = torqueRightAnkle;
    varsOut.torqueLeftAnkle_ = torqueLeftAnkle;
    varsOut.forceRightHand_ = forceRightHand;
    varsOut.forceLeftHand_ = forceLeftHand;
    varsOut.orSwFt_ = orSwFt;
    varsOut.dorSwFt_ = dorSwFt;
    varsOut.pSwFtInH_ = pSwFtInH;
    varsOut.angRates_ = angRates;
    varsOut.h_ = h;
    varsOut.hD_ = hD;
    varsOut.pPelvisFK_ = pPelvisFK;
    varsOut.pPelvisTest_ = pPelvisTest;
    varsOut.vPelvisFk_ = vPelvisFk;
    varsOut.tauSens_ = tauSens;
    varsOut.tauDes_ = tauDes;
    varsOut.tauAnkTorque_ = tauAnkTorque;
    varsOut.k_ = kF;
    varsOut.s_ = s;
    varsOut.tInStep_ = tInStep;
    varsOut.deltaX_ = deltaX;
    varsOut.deltaY_ = deltaY;
    varsOut.thp_ = thp;
    varsOut.thpF_init_ = thpF_init;
    varsOut.thr_ = thr;
    varsOut.side_ = side;
    varsOut.vxFK_ = vxFK;
    varsOut.kv_ = kv;
    varsOut.px0_ = px0;
    varsOut.kOrg_ = kRaw;
    varsOut.vxDes_ = vxDes;
    varsOut.x0_ = x0;
    varsOut.T_ = T;
    memcpy(varsOut.trans_, trans, sizeof(varsOut.trans_)); // check values
    //varsOut.trans_=trans;  //???? error with types???
    varsOut.imuAngRates_ = imuAngRates;
    varsOut.imuAccelerations_ = aPelvis;
    varsOut.kR_ = kR;
    varsOut.kL_ = kL;
    varsOut.avgFreq_ = avgFreq;
    varsOut.f0_ = f0;
    varsOut.last_step_freq_ = last_step_freq;
    varsOut.frontalBias_ = frontalBias;
    varsOut.vxAbsF_ = vxAbsF;
    varsOut.qSensAbsMed_ = qSensAbsMed;
    varsOut.pPelvisAbsMed_ = pPelvisAbsMed;
    varsOut.qSt_ = qSt;
    varsOut.indexSt_ = indexSt;
    varsOut.pxAbsOld_ = pxAbs_old;
    varsOut.pPelvisAbsF_ = pPelvisAbsF;
    varsOut.position_ = position;
    varsOut.p_left_w_ = p_left_w;
    varsOut.p_right_w_ = p_right_w;
    varsOut.p_pelvis_right_ = p_pelvis_right;
    varsOut.p_pelvis_left_ = p_pelvis_left;
    varsOut.pPelvisAbsTmpX_ = pPelvisAbsTmpX;
    varsOut.seOrientation_ = seOrientation;
    varsOut.velocity_ = velocity;
    varsOut.velocity_straight_ = velocity_straight;
    varsOut.thyF_ = thyF;
    varsOut.yaw_curr_ = yaw_curr;
    varsOut.yawangle_ = yawangle;
    varsOut.forceRAnkleZF_ = forceRightAnkleF[2];
    varsOut.forceLAnkleZF_ = forceLeftAnkleF[2];
}

// void Control::SaveCustVars(std::ofstream &outputFile)
// {
//     // start_id = 1
//     outputFile << varsOut.tm_;
//     outputFile << " " << this->poseComan1
//                << " " << this->poseComan2 << std::endl;
// }

void Control::SaveCustVars(std::ofstream &outputFile)
{
    // start_id = 1
    outputFile << varsOut.tm_;
    outputFile << " " << varsOut.side_
               << " " << varsOut.kR_
               << " " << varsOut.energyR_
               << " " << varsOut.energyL_;

    outputFile << std::endl;
}

// void Control::SaveCustVars(std::ofstream &outputFile)
// {
//     // start_id = 1
//     outputFile << varsOut.tm_;
//     // start_id = 2
//     for (int i = 0; i < N; i++)
//     {
//         outputFile << " " << varsOut.qSens_[i];
//     }
//     // start_id = 2 + 3N
//     for (int i = 0; i < 3; i++)
//     {
//         outputFile << " " << varsOut.pPelvis_[i];
//     }
//     // start_id = 17 + 3N
//     for (int i = 0; i < 3; i++)
//     {
//         outputFile << " " << varsOut.forceRightAnkle_[i];
//     }
//     // start_id = 20 + 3Ntrue){//
//     for (int i = 0; i < 3; i++)
//     {
//         outputFile << " " << varsOut.forceLeftAnkle_[i];
//     }
//     // start_id = 29 + 3N
//     for (int i = 0; i < 3; i++)
//     {
//         outputFile << " " << varsOut.forceRightHand_[i];
//     }
//     // start_id = 32 + 3N
//     for (int i = 0; i < 3; i++)
//     {
//         outputFile << " " << varsOut.forceLeftHand_[i];
//     }
//     for (int i = 0; i < 3; i++)
//     {                                                          //z-y-x
//         outputFile << " " << varsOut.comDesMomentumWeight_(i); //70-71-72
//     }
//     outputFile << " " << varsOut.k_ // 57                           // 49
//                << " " << varsOut.side_
//                << " " << varsOut.kR_                //40
//                << " " << varsOut.kL_                //39                            //29+1
//                << "   " << varsOut.qPelvis_[0]      //28+1
//                << "   " << varsOut.qPelvis_[1]      //27+1                                 //24+1
//                << "   " << varsOut.comDesMomentum0_ //23+1
//                << "   " << varsOut.comDesMomentum1_ //22+1
//                << "   " << varsOut.comDesMomentum2_ //21+1
//                << std::endl;

//     //std::cout << varsOut.tm_ << std::endl;
// }

void Control::SaveVars(std::ofstream &outputFile, bool onlyUpperBody)
{

    if (onlyUpperBody)
    {

        // start_id = 1
        outputFile << varsOut.tm_
                   // <<"   "<<varsOut.comUB_
                   //<<"   "<<varsOut.comUB_[1]
                   //<<"   "<<varsOut.comUB_[2]
                   << "   " << varsOut.qPelvis_[0]
                   << "   " << varsOut.qPelvis_[1]
                   << "   " << varsOut.hNormPostureRBDL_
                   << "   " << varsOut.itRBDL_
                   << "   " << varsOut.hNormPostureIdyntree_
                   << " " << varsOut.itIdyntree_
                   << std::endl;
    }
    else
    {

        // start_id = 1
        outputFile << varsOut.tm_;
        // start_id = 2
        for (int i = 0; i < N; i++)
        {
            outputFile << " " << varsOut.qSens_[i];
        }
        //  start_id = 2 + n
        for (int i = 0; i < N; i++)
        {
            outputFile << " " << varsOut.qSensAbs_[i];
        }
        // start_id = 2 + 2N
        for (int i = 0; i < N; i++)
        {
            outputFile << " " << varsOut.dqSens_[i];
        }
        // start_id = 2 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.pPelvis_[i];
        }
        // start_id = 5 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.pPelvisAbs_[i];
        }
        // start_id = 8 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.vPelvis_[i];
        }
        // start_id = 11 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.vPelvisAbs_[i];
        }
        // start_id = 14 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.aPelvis_[i];
        }
        // start_id = 17 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.forceRightAnkle_[i];
        }
        // start_id = 20 + 3Ntrue){//
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.forceLeftAnkle_[i];
        }
        // start_id = 23 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.torqueRightAnkle_[i];
        }
        // start_id = 26 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.torqueLeftAnkle_[i];
        }
        // start_id = 29 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.forceRightHand_[i];
        }
        // start_id = 32 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.forceLeftHand_[i];
        }

        /* // start_id = 35 + 3N
        for (int i = 0; i < 3; i++)
        {

            outputFile << " " << varsOut.torqueRightHand_[i];
        }
       // start_id = 38 + 3N
        for (int i = 0; i < 3; i++)
        {

            outputFile << " " << varsOut.torqueLeftHand_[i];
        }*/

        // start_id = 41 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.orSwFt_[i];
        }
        // start_id = 44 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.dorSwFt_[i];
        }
        // start_id = 47 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.pSwFtInH_[i];
        }
        // start_id = 50 + 3N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.angRates_[i];
        }
        // start_id = 53 + 3N
        for (int i = 0; i < N; i++)
        {
            outputFile << " " << varsOut.h_[i];
        }
        // start_id = 53 + 4N
        for (int i = 0; i < N; i++)
        {
            outputFile << " " << varsOut.hD_[i];
        }
        // start_id = 53 + 5N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.pPelvisFK_[i];
        }
        // start_id = 56 + 5N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.pPelvisTest_[i];
        }
        // start_id = 59 + 5N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.vPelvisFk_[i];
        }
        // start_id = 62 + 5N
        for (int i = 0; i < N; i++)
        {
            outputFile << " " << varsOut.tauSens_[i];
        }
        // start_id = 62 + 6N
        for (int i = 0; i < N; i++)
        {
            outputFile << " " << varsOut.tauDes_[i];
        }
        // start_id = 62 + 7N
        for (int i = 0; i < 2; i++)
        {
            outputFile << " " << varsOut.tauAnkTorque_[i];
        }
        // start_id = 64 + 7N
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                outputFile << " " << varsOut.trans_[i][j];
            }
        }
        // start_id = 73 + 7N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.imuAngRates_[i];
        }
        // start_id = 76 + 7N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.imuAccelerations_[i];
        }
        // start_id = 79 + 7N
        for (int i = 0; i < N; i++)
        {
            outputFile << " " << varsOut.qSensAbsMed_[i];
        }
        // start_id = 79 + 8N
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.pPelvisAbsMed_[i];
        }
        //82+8N
        for (int i = 0; i < 6; i++)
        {
            outputFile << " " << varsOut.qSt_[i];
        }
        //88+8N
        for (int i = 0; i < 6; i++)
        {
            outputFile << " " << varsOut.indexSt_[i];
        }
        for (int i = 0; i < 3; i++)
        {
            outputFile << " " << varsOut.seOrientation_[i];
        }

        for (int i = 0; i < 3; i++)
        {                                                          //z-y-x
            outputFile << " " << varsOut.comDesMomentumWeight_(i); //70-71-72
        }
        for (int i = 0; i < 3; i++)
        {                                                              //z-y-x
            outputFile << " " << varsOut.torqueRForearm2BaseFrame_(i); //61-65-69
            outputFile << " " << varsOut.torqueLForearm2BaseFrame_(i); //60-64-68
            outputFile << " " << varsOut.comDesMomentumR_(i);          //59-63-67
            outputFile << " " << varsOut.comDesMomentumL_(i);          //58-62-66
        }

        outputFile << " " << varsOut.k_                                       // 57
                   << "   " << varsOut.comUBz_                                //56
                   << " " << varsOut.s_                                       // 55
                   << " " << varsOut.tInStep_                                 // 54
                   << " " << varsOut.deltaX_                                  // 53
                   << " " << varsOut.deltaY_                                  // 52
                   << " " << varsOut.thp_                                     // 51
                   << " " << varsOut.thpF_init_                               // 50
                   << " " << varsOut.thr_                                     // 49
                   << " " << varsOut.side_                                    // 48
                   << " " << varsOut.vxFK_                                    // 47
                   << " " << varsOut.kv_                                      // 46
                   << " " << varsOut.px0_                                     // 45
                   << " " << varsOut.kOrg_                                    // 44
                   << " " << varsOut.vxDes_ * varsOut.tInStep_ + varsOut.px0_ //43
                   << " " << varsOut.x0_                                      //42
                   << " " << varsOut.T_                                       //41
                   << " " << varsOut.kR_                                      //40
                   << " " << varsOut.kL_                                      //39
                   << " " << varsOut.avgFreq_                                 //38
                   << " " << varsOut.f0_                                      //37
                   << " " << varsOut.last_step_freq_                          //36
                   << " " << varsOut.frontalBias_                             //35
                   << " " << varsOut.vxAbsF_                                  //34
                   << "   " << varsOut.rightStance_                           //33+1
                   << "   " << varsOut.comUBx_                                //32+1
                   << "   " << varsOut.comUBxDes_                             //31+1
                   << "   " << varsOut.comUBy_                                //30+1
                   << "   " << varsOut.comUByDes_                             //29+1
                   << "   " << varsOut.qPelvis_[0]                            //28+1
                   << "   " << varsOut.qPelvis_[1]                            //27+1
                   << "   " << varsOut.hNormPostureRBDL_                      //26+1
                   << "   " << varsOut.hM0_                                   //25+1
                   << "   " << varsOut.hM1_                                   //24+1
                   << "   " << varsOut.comDesMomentum0_                       //23+1
                   << "   " << varsOut.comDesMomentum1_                       //22+1
                   << "   " << varsOut.comDesMomentum2_                       //21+1
                   << "   " << varsOut.torqueHandRBaseFrame0_                 //20+1
                   << "   " << varsOut.torqueHandRBaseFrame1_                 //19+1
                   << "   " << varsOut.torqueHandRBaseFrame2_                 //18+1
                   << "   " << varsOut.torqueHandLBaseFrame0_                 //17+1
                   << "   " << varsOut.torqueHandLBaseFrame1_                 //16+1
                   << "   " << varsOut.torqueHandLBaseFrame2_                 //15+1
                   << "   " << varsOut.forceHandR0_                           //14+1
                   << "   " << varsOut.forceHandR1_                           //13+1
                   << "   " << varsOut.forceHandR2_                           //12+1
                   << "   " << varsOut.forceHandL0_                           //11+1
                   << "   " << varsOut.forceHandL1_                           //10+1
                   << "   " << varsOut.forceHandL2_                           //9+1
                   << "   " << varsOut.forceHandRBaseFrame0_                  //8+1
                   << "   " << varsOut.forceHandRBaseFrame1_                  //7+1
                   << "   " << varsOut.forceHandRBaseFrame2_                  //6+1
                   << "   " << varsOut.forceHandLBaseFrame0_                  //5+1
                   << "   " << varsOut.forceHandLBaseFrame1_                  //4+1
                   << "   " << varsOut.forceHandLBaseFrame2_                  //3+1
                   << "   " << varsOut.itRBDL_                                //2+1
                   << "   " << varsOut.forceRAnkleZF_
                   << "   " << varsOut.forceLAnkleZF_
                   << "   " << varsOut.energyR_
                   << "   " << varsOut.energyL_
                   << "   " << varsOut.xPos_
                   << "   " << varsOut.yPos_
                   << "   " << varsOut.zPos_
                   << std::endl;
    }

    //std::cout << varsOut.tm_ << std::endl;
}

// void Control::SaveVars(std::ofstream &outputFile, bool onlyUpperBody)
// {

//     if (onlyUpperBody)
//     {

//         // start_id = 1
//         outputFile << varsOut.tm_
//                    << std::endl;
//     }
//     else
//     {

//         // start_id = 1
//         outputFile << varsOut.tm_;
//         // start_id = 2
//         for (int i = 0; i < N; i++)
//         {
//             outputFile << " " << varsOut.qSens_[i];
//         }
//         //  start_id = 2 + n
//         for (int i = 0; i < N; i++)
//         {
//             outputFile << " " << varsOut.qSensAbs_[i];
//         }
//         // start_id = 2 + 2N
//         for (int i = 0; i < N; i++)
//         {
//             outputFile << " " << varsOut.dqSens_[i];
//         }
//         // start_id = 2 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.pPelvis_[i];
//         }
//         // start_id = 5 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.pPelvisAbs_[i];
//         }
//         // start_id = 8 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.vPelvis_[i];
//         }
//         // start_id = 11 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.vPelvisAbs_[i];
//         }
//         // start_id = 14 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.aPelvis_[i];
//         }
//         // start_id = 17 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.forceRightAnkle_[i];
//         }
//         // start_id = 20 + 3Ntrue){//
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.forceLeftAnkle_[i];
//         }
//         // start_id = 23 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.torqueRightAnkle_[i];
//         }
//         // start_id = 26 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.torqueLeftAnkle_[i];
//         }
//         // start_id = 29 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.forceRightHand_[i];
//         }
//         // start_id = 32 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.forceLeftHand_[i];
//         }

//         /* // start_id = 35 + 3N
//         for (int i = 0; i < 3; i++)
//         {

//             outputFile << " " << varsOut.torqueRightHand_[i];
//         }
//        // start_id = 38 + 3N
//         for (int i = 0; i < 3; i++)
//         {

//             outputFile << " " << varsOut.torqueLeftHand_[i];
//         }*/

//         // start_id = 41 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.orSwFt_[i];
//         }
//         // start_id = 44 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.dorSwFt_[i];
//         }
//         // start_id = 47 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.pSwFtInH_[i];
//         }
//         // start_id = 50 + 3N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.angRates_[i];
//         }
//         // start_id = 53 + 3N
//         for (int i = 0; i < N; i++)
//         {
//             outputFile << " " << varsOut.h_[i];
//         }
//         // start_id = 53 + 4N
//         for (int i = 0; i < N; i++)
//         {
//             outputFile << " " << varsOut.hD_[i];
//         }
//         // start_id = 53 + 5N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.pPelvisFK_[i];
//         }
//         // start_id = 56 + 5N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.pPelvisTest_[i];
//         }
//         // start_id = 59 + 5N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.vPelvisFk_[i];
//         }
//         // start_id = 62 + 5N
//         for (int i = 0; i < N; i++)
//         {
//             outputFile << " " << varsOut.tauSens_[i];
//         }
//         // start_id = 62 + 6N
//         for (int i = 0; i < N; i++)
//         {
//             outputFile << " " << varsOut.tauDes_[i];
//         }
//         // start_id = 62 + 7N
//         for (int i = 0; i < 2; i++)
//         {
//             outputFile << " " << varsOut.tauAnkTorque_[i];
//         }
//         // start_id = 64 + 7N
//         for (int i = 0; i < 3; i++)
//         {
//             for (int j = 0; j < 3; j++)
//             {
//                 outputFile << " " << varsOut.trans_[i][j];
//             }
//         }
//         // start_id = 73 + 7N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.imuAngRates_[i];
//         }
//         // start_id = 76 + 7N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.imuAccelerations_[i];
//         }
//         // start_id = 79 + 7N
//         for (int i = 0; i < N; i++)
//         {
//             outputFile << " " << varsOut.qSensAbsMed_[i];
//         }
//         // start_id = 79 + 8N
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.pPelvisAbsMed_[i];
//         }
//         //82+8N
//         for (int i = 0; i < 6; i++)
//         {
//             outputFile << " " << varsOut.qSt_[i];
//         }
//         //88+8N
//         for (int i = 0; i < 6; i++)
//         {
//             outputFile << " " << varsOut.indexSt_[i];
//         }
//         for (int i = 0; i < 3; i++)
//         {
//             outputFile << " " << varsOut.seOrientation_[i];
//         }

//         /* for (int i = 0; i < 3; i++)
//         {
//             if (i<2){
//             outputFile << " " << varsOut.orientationDesR_[i];
//             }
//             else{
//                 outputFile << " " << 0;
//             }
//         }*/
//         //94+8N

//         /*   for (int i = 0; i < 6; i++){
//             outputFile << " " << varsOut.fExt_(i);
//         }*/
//         //        //100+8N
//         //        for (int i = 0; i < 6; i++){
//         //            outputFile << " " << varsOut.fInt_(i);
//         //        }

//         //        //106+8N
//         //        for (int i = 0; i < 3; i++){
//         //            outputFile << " " << varsOut.fElbowHand_(i);
//         //        }
//         //        for (int i = 0; i < 800; i++){
//         //            outputFile << " " << varsOut.velTimeWind_[i];
//         //        }
//         /*outputFile <<"   "<< varsOut.handRightVelFilt_//100+8N
//                   << " " << varsOut.inCommands_//101
//                   << " " << 0.0//varsOut.handRightPos_//102
//                   << " " << varsOut.forceSensors_//103
//                   << " " << varsOut.forceDiff_//113
//                   << " " << varsOut.velRel_
//                   << " " << varsOut.velSimpleThreshold_
//                   << " " << varsOut.simpleThreshold_//101
//                   << " " << varsOut.posRel_*/
//         // start_id = 76 + 8N

//         outputFile << " " << varsOut.k_                                       // 76 + 8N
//                    << " " << varsOut.s_                                       // 77 + 8N
//                    << " " << varsOut.tInStep_                                 // 78 + 8N
//                    << " " << varsOut.deltaX_                                  // 79 + 8N
//                    << " " << varsOut.deltaY_                                  // 80 + 8N
//                    << " " << varsOut.thp_                                     // 81 + 8N
//                    << " " << varsOut.thpF_init_                               // 82 + 8N
//                    << " " << varsOut.thr_                                     // 83 + 8N
//                    << " " << varsOut.side_                                    // 84 + 8N
//                    << " " << varsOut.vxFK_                                    // 85 + 8N
//                    << " " << varsOut.kv_                                      // 119 + 8N
//                    << " " << varsOut.px0_                                     // 87 + 8N
//                    << " " << varsOut.kOrg_                                    // 88 + 8N
//                    << " " << varsOut.vxDes_ * varsOut.tInStep_ + varsOut.px0_ // 89 + 8N
//                    << " " << varsOut.x0_                                      // 87 + 7N
//                    << " " << varsOut.T_
//                    << " " << varsOut.kR_ //125
//                    << " " << varsOut.kL_ //126
//                    << " " << varsOut.avgFreq_
//                    << " " << varsOut.f0_
//                    << " " << varsOut.last_step_freq_
//                    << " " << varsOut.frontalBias_
//                    << " " << varsOut.vxAbsF_
//                    //<< " " << varsOut.handPosDesL_
//                    // << " " << varsOut.handPosDesR_
//                    // << " " << varsOut.errorConvergenceLevR_
//                    // << " " << varsOut.errorConvergenceLevL_

//                    //<< " " << varsOut.qLevelDes1L//43+3N
//                    // << " " << varsOut.qLevelDes2L//43+3N

//                    // << "   "<<varsOut.handPosWRTFootL_
//                    // << "   "<<varsOut.handPosWRTFootR_
//                    // << "   "<<   varsOut.handPos_
//                    // << "   "<<   varsOut.footPosL_
//                    // << "   "<<   varsOut.footPosR_
//                    << "   " << varsOut.rightStance_
//                    // << " " << varsOut.qLevelDes1R//43+3N
//                    // << " " << varsOut.qLevelDes2R//43+3N
//                    //<< " " <<varsOut.handPosDesR_       //36+3N
//                    //<< " " <<varsOut.handPosR_          //37+3N
//                    << "   " << varsOut.orientationRollR_                               //18+3N
//                    << " " << varsOut.orientationPitchR_                                //19+3N
//                    << "   " << varsOut.orientationRollR_ - varsOut.orientationDesR_[0] //21+3N
//                    << " " << varsOut.orientationPitchR_ - varsOut.orientationDesR_[1]  //22+3N
//                    << " " << varsOut.errorConvergenceR_                                //30+3N
//                    << " " << varsOut.qDesOrientR_[0]                                   //38+3N
//                    << " " << varsOut.qDesOrientR_[1]                                   //39+3N
//                    << " " << varsOut.qDesOrientR_[2]                                   //39+3N
//                    //<< " " << varsOut.speedCommandAvg_
//                    << std::endl;
//     }

//     //std::cout << varsOut.tm_ << std::endl;
// }

void Control::IntentionDetection(double tme, double &handRightVelFilt, Eigen::VectorXf &handRightPos, double forceSensors[3], bool &stop, double &speedCommandAvg)
{

    // Internal Variables
    double handRightPosTemp[3];
    int timeWindWS = 100;  //prima era 500
    int timeWindWS2 = 200; //prima era 500
    int timeWindSS = 200;
    double Ff, Fi, Pf, Pi;
    double velRel, posRel;
    double forceDiff;
    int walk = 0;
    //    static double inCommands;
    double sumVelTimeWind = 0;
    double sumPosTimeWind = 0;
    Eigen::Vector3f Ha(handPositionR_Abs[0], handPositionR_Abs[1], handPositionR_Abs[2]);
    Eigen::Vector3f Sh(shoulderPositionR_Abs[0], shoulderPositionR_Abs[1], shoulderPositionR_Abs[2]);
    Eigen::Vector3f Elb(elbowPositionR_Abs[0], elbowPositionR_Abs[1], elbowPositionR_Abs[2]);
    // Calculate the Arm_length in order to normalize.
    Eigen::Vector3f Forearm = Elb - Ha;
    Eigen::Vector3f Backarm = Sh - Elb;
    double arm_length = Forearm.norm() + Backarm.norm();

    // Relative Velocity Computation
    handRightPosTemp[0] = handRightPos[0];
    AvgFilter(pHand, handRightPosFilt, handRightPosTemp[0], M);
    pHandVectTemp.push_back(handRightPosFilt);
    AvgFilter(vHandVec, handRightVelFilt, (pHandVectTemp.back() - pHandVectTemp.front()) / (0.001 * pHandVectTemp.size()), M2);
    if (pHandVectTemp.size() >= M)
    {
        pHandVectTemp.erase(pHandVectTemp.begin());
    }

    //Position Vector for WS
    relPosVec.push_back(handRightPosFilt / arm_length); //normalized
    if (relPosVec.size() >= timeWindWS2)
    {
        relPosVec.erase(relPosVec.begin());
    }
    for (int i = 0; i < relPosVec.size(); i++)
    {
        sumPosTimeWind = sumPosTimeWind + relPosVec[i];
    }

    //         // Velocity vector for SS (In the case of SS the relative velocity is equal to the absolute one)
    //             velTimeWind.push_back(handRightVelFilt/arm_length); //Normalized
    //             if (velTimeWind.size()>=timeWindSS){
    //                 velTimeWind.erase(velTimeWind.begin());
    //                 for (int i=0; i<timeWindSS; i++){
    //                      sumVelTimeWind=sumVelTimeWind+velTimeWind[i];
    //                 }
    //             }

    // Velocity vector for SS (In the case of SS the relative velocity is equal to the absolute one)
    velTimeWind.push_back(handRightPosFilt / arm_length); //Normalized
    if (velTimeWind.size() >= timeWindSS)
    {
        velTimeWind.erase(velTimeWind.begin());
        for (int i = 0; i < timeWindSS; i++)
        {
            sumVelTimeWind = sumVelTimeWind + velTimeWind[i];
        }
    }

    //Force Sensor filtered + creation of the force vector
    AvgFilter(sensorExtForceVec, forceSensorsFilt, forceSensors[2], 500);
    intForceVec.push_back(forceSensorsFilt);
    if (intForceVec.size() >= timeWindWS)
    {
        intForceVec.erase(intForceVec.begin());
    }

    //Features' computation
    double vDes = 0.2;
    Ff = intForceVec.back(); //
    Fi = intForceVec[0];
    Pf = relPosVec.back(); //
    Pi = relPosVec[0];
    forceDiff = ((Ff - Fi));
    // AvgFilter(forceDiffvec, forceDiffFilt,forceDiff, 500);
    velRel = handRightVelFilt / arm_length;
    posRel = (Pf - Pi) / (0.8); // divide for the time window dimension in sec
    //posRel=posRel/vDes;
    //velRel=velRel/vDes; // /2;
    posRel = ((sumPosTimeWind / relPosVec.size()) - initPosArm); //*0.65;//(handRightPosFilt/arm_length);
    //std::cout<<"initPosArm"<<initPosArm<<std::endl;
    velRel = velRel / vDes; // /2;
                            // forceDiff= -forceSensorsFilt/10;

    // Classification:
    cc = this->ClassifyStart(forceDiff, velRel, posRel, sumVelTimeWind / velTimeWind.size(), tme, simpleThreshold);
    //    std::cout<<cc<<"   -INIT POS ARM-"<<initPosArm<< std::endl;
    if (true)
    { //aggiungi true
        if (simpleThreshold == true)
        {
            //               std::cout<<"stai fermo - "<<vSupport.size()<< "initiPos" <<initPosArm<< std::endl;
            if (primaVolta)
            {
                vSupport.clear();
                primaVolta = false;
            }
            vSupport.push_back(handRightPosFilt / arm_length);
            initPosArm = 0;
        }
        else
        {
            //               std::cout<<"cammina - "<<vSupport.size()<< "initiPos" <<initPosArm<<std::endl;
            primaVolta = true;
            initPosArm = std::accumulate(vSupport.begin(), vSupport.end(), 0.0) / vSupport.size();
        }
    }

    if (cc == "stop")
    {
        //std::cout<<tme-T1<<std::endl;
    }
    //State Machine
    if (tme - T0 > 4 && cc == "pullfastStart")
    { //Start Walking -
        //I am walking
        T0 = tme;
        simpleThreshold = false;
        CC = "pullfastStart";
    }
    else if (tme - T0 > 3 && cc == "stop" && tme - T1 > 0.004)
    {
        T0 = tme;
        CC = "stop";
        primaVolta = true;
        simpleThreshold = true; //to add!!!!
    }

    stop = simpleThreshold;

    if (CC == "stop")
    {
        //    firstLoop=true;
        walk = 0;
        varsOut.inCommands_ = 100;
        if (cc == "perturbation")
        {
            varsOut.inCommands_ = 0;
        }
        //       if (cc == "pushfastStart"){ varsOut.inCommands_=2;}
        if (cc == "pullfastStart")
        {
            varsOut.inCommands_ = 2;
        }
        if (cc == "Uncertainty")
        {
            varsOut.inCommands_ = 0;
        }
        if (cc == "stop")
        {
            varsOut.inCommands_ = -2;
        }
        if (varsOut.inCommands_ == 100)
        {
            //           std::cout<<"value c in walk0="<<cc<<std::endl;
        }
    }
    else
    {
        walk = 1;
        varsOut.inCommands_ = 100;
        if (cc == "keep")
        {
            varsOut.inCommands_ = 0.5;
        }
        if (cc == "increase")
        {
            varsOut.inCommands_ = 1;
        }
        if (cc == "decrease")
        {
            varsOut.inCommands_ = -1;
        }
        if (cc == "Uncertainty")
        {
            varsOut.inCommands_ = 0;
        }
        if (cc == "stop")
        {
            varsOut.inCommands_ = -2;
        }
        if (cc == "pullfastStart")
        {
            varsOut.inCommands_ = 2;
        }
        if (varsOut.inCommands_ == 100)
        {
            std::cout << "value c in walk1=" << cc << std::endl;
        }
    }

    // Var for Out

    varsOut.handRightPos_ = handRightPos[0];
    // varsOut.cc_=cc;
    varsOut.forceSensors_ = forceSensors[2];
    //    varsOut.inCommands_=inCommands;
    varsOut.handRightVelFilt_ = handRightVelFilt; //Normalized
    varsOut.forceDiff_ = forceDiff;
    varsOut.velRel_ = velRel;
    varsOut.velSimpleThreshold_ = sumVelTimeWind / velTimeWind.size();
    varsOut.velTimeWind_ = velTimeWind;
    varsOut.simpleThreshold_ = simpleThreshold;
    varsOut.posRel_ = posRel;

    /* // Internal Variables
             double handRightPosTemp[3];
             static double handRightPosFilt=0;
             int timeWindWS=100; //prima era 500
             int timeWindWS2=200; //prima era 500
             int timeWindSS=200;
             double Ff, Fi, Pf, Pi;
             double velRel, posRel;
             double forceDiff;
             static std::string cc;
             static bool simpleThreshold= true;
             static std::string CC = "stop";
             int walk;
         //    static double inCommands;
             static double T0=0;
             static double T1=0;
             double sumVelTimeWind=0;
             double sumPosTimeWind=0;
             Eigen::Vector3f Ha(handPositionR_Abs[0],handPositionR_Abs[1],handPositionR_Abs[2]);
             Eigen::Vector3f Sh(shoulderPositionR_Abs[0],shoulderPositionR_Abs[1], shoulderPositionR_Abs[2]);
             Eigen::Vector3f Elb(elbowPositionR_Abs[0],elbowPositionR_Abs[1], elbowPositionR_Abs[2]);
             // Calculate the Arm_length in order to normalize.
             Eigen::Vector3f Forearm = Elb - Ha;
             Eigen::Vector3f Backarm = Sh - Elb;
             double arm_length = Forearm.norm() + Backarm.norm();
             static std::vector<double> vSupport;
             static double initPosArm=0;
             static bool primaVolta=true;

             // Relative Velocity Computation
             handRightPosTemp[0]=handRightPos[0];
             AvgFilter(pHand, handRightPosFilt, handRightPosTemp[0], M, count1);
             pHandVectTemp.push_back(handRightPosFilt);
             AvgFilter(vHandVec, handRightVelFilt, (pHandVectTemp.back()-pHandVectTemp.front())/(0.001*pHandVectTemp.size()), M2);
             if (pHandVectTemp.size() >= M){
                pHandVectTemp.erase(pHandVectTemp.begin());
              }

             //Position Vector for WS
             relPosVec.push_back(handRightPosFilt/arm_length); //normalized
             if (relPosVec.size()>=timeWindWS2){
                 relPosVec.erase(relPosVec.begin());
             }
             for (int i=0; i<relPosVec.size(); i++){
                  sumPosTimeWind=sumPosTimeWind+relPosVec[i];
             }

             // Velocity vector for SS (In the case of SS the relative velocity is equal to the absolute one)
                 velTimeWind.push_back(handRightPosFilt/arm_length); //Normalized
                 if (velTimeWind.size()>=timeWindSS){
                     velTimeWind.erase(velTimeWind.begin());
                     for (int i=0; i<timeWindSS; i++){
                          sumVelTimeWind=sumVelTimeWind+velTimeWind[i];
                     }
                 }

             //Force Sensor filtered + creation of the force vector
             AvgFilter(sensorExtForceVec, forceSensorsFilt,forceSensors[2], 500);


             double forceToConsider= forceSensors[2]; // because in the past you saved the filtered
             intForceVec.push_back(forceToConsider);


             if (intForceVec.size()>=timeWindWS){
                 intForceVec.erase(intForceVec.begin());
             }


           //Features' computation
             double vDes=0.2;
              Ff = intForceVec.back();//
              Fi = intForceVec[0];
              Pf = relPosVec.back();//
              Pi = relPosVec[0];
              forceDiff= ((Ff-Fi));
              velRel= handRightVelFilt/arm_length;
              posRel= (Pf-Pi)/(0.8); // divide for the time window dimension in sec
              //posRel=posRel/vDes;
              //velRel=velRel/vDes; // /2;
              posRel=((sumPosTimeWind/relPosVec.size())-initPosArm);//(handRightPosFilt/arm_length);
              //std::cout<<"initPosArm"<<initPosArm<<std::endl;
              velRel=velRel/vDes; // /2;
             // forceDiff= -forceSensorsFilt/10;

         // Classification:
               cc= this->ClassifyStart(forceDiff, velRel,posRel,sumVelTimeWind/velTimeWind.size(), tme, simpleThreshold);
    //    std::cout<<cc<<"   -INIT POS ARM-"<<initPosArm<< std::endl;
           if(tme>5){
               if (simpleThreshold==true){
    //               std::cout<<"stai fermo - "<<vSupport.size()<< "initiPos" <<initPosArm<< std::endl;
                if(primaVolta){
                    vSupport.clear();
                    primaVolta=false;
                }
                vSupport.push_back(handRightPosFilt/arm_length);
                initPosArm=0;
               }
               else{
    //               std::cout<<"cammina - "<<vSupport.size()<< "initiPos" <<initPosArm<<std::endl;
                   primaVolta=true;
                  initPosArm =std::accumulate(vSupport.begin(),vSupport.end(), 0.0)/vSupport.size();
               }
             }

               if (cc=="stop"){
                std::cout<<tme-T1<<std::endl;
               }
          //State Machine
             if(tme-T0 >4  && cc=="pullfastStart"){ //Start Walking -
                    //I am walking
                    T0=tme;
                    simpleThreshold=false;
                    CC="pullfastStart";}
             else if (tme-T0 >1.5 && cc=="stop" && tme-T1>0.004){
                    T0=tme;
                    CC="stop";
                    primaVolta=true;
                    simpleThreshold=true; //to add!!!!
             }

         stop = simpleThreshold;

         if (CC == "stop"){
         //    firstLoop=true;
                walk=0;
                varsOut.inCommands_=100;
                if (cc == "perturbation"){ varsOut.inCommands_=0;}
         //       if (cc == "pushfastStart"){ varsOut.inCommands_=2;}
                if (cc == "pullfastStart"){ varsOut.inCommands_=2;}
                if (cc == "Uncertainty"){ varsOut.inCommands_=0;}
                if (cc == "stop"){ varsOut.inCommands_=-2;}
                if(varsOut.inCommands_==100){
         //           std::cout<<"value c in walk0="<<cc<<std::endl;
                }


            }
            else
            { walk=1;
             varsOut.inCommands_=100;
                if (cc == "keep"){ varsOut.inCommands_=0.5;}
                if (cc == "increase"){ varsOut.inCommands_=1;}
                if (cc == "decrease"){ varsOut.inCommands_=-1;}
                if (cc == "Uncertainty"){ varsOut.inCommands_=0;}
                if (cc == "stop"){ varsOut.inCommands_=-2;}
                       if (cc == "pullfastStart"){ varsOut.inCommands_=2;}
                if(varsOut.inCommands_==100){
                    std::cout<<"value c in walk1="<<cc<<std::endl;
                }
              }



         // Var for Out

             varsOut.handRightPos_=handRightPos[0];
            // varsOut.cc_=cc;
            varsOut.forceSensors_=forceSensors[2];
         //    varsOut.inCommands_=inCommands;
             varsOut.handRightVelFilt_=handRightVelFilt;//Normalized
             varsOut.forceDiff_=forceDiff;
             varsOut.velRel_=velRel;
             varsOut.velSimpleThreshold_=sumVelTimeWind/velTimeWind.size();
             varsOut.velTimeWind_=velTimeWind;
             varsOut.simpleThreshold_=simpleThreshold;
             varsOut.posRel_=posRel;

*/
}

/*void CalcJacCom(Model *model, VectorNd qUB_, Math::MatrixNd &Gcom) {

  const unsigned int N_UB = 17;
  Math::VectorNd q(N_UB, 1), qdot(N_UB, 1);
  Math::Vector3d comUb, colGcom;
  Math::MatrixNd GcomTmp(3, 17);
  GcomTmp.setZero();
  double ubMass = 17.206;
  for (int i(0); i < N_UB; i++){
    q(i) = qUB_(i);
  }
  qdot.setZero();

  // Calculate Gcom
  for (int k(0); k < N_UB; k++) {
    qdot(k) = 1;
    RigidBodyDynamics::Utils::CalcCenterOfMass(*model, q, qdot, NULL, ubMass, comUb, &colGcom, NULL, NULL, NULL, true);
    qdot(k) = 0;
    for (int i(0); i < 3; i++) Gcom(i, k) = colGcom(i);
  }
  /*for (int i=0; i<3;i++){
           Gcom(i,0)=GcomTmp(i,0);
           Gcom(i,1)=GcomTmp(i,1);
           Gcom(i,2)=GcomTmp(i,2);
           Gcom(i,3)=GcomTmp(i,10);
           Gcom(i,4)=GcomTmp(i,11);
           Gcom(i,5)=GcomTmp(i,12);
           Gcom(i,6)=GcomTmp(i,13);
           Gcom(i,7)=GcomTmp(i,3);
           Gcom(i,8)=GcomTmp(i,4);
           Gcom(i,9)=GcomTmp(i,5);
           Gcom(i,10)=GcomTmp(i,6);
           Gcom(i,11)=GcomTmp(i,7);
           Gcom(i,12)=GcomTmp(i,8);
           Gcom(i,13)=GcomTmp(i,9);
           Gcom(i,14)=GcomTmp(i,14);
           Gcom(i,15)=GcomTmp(i,15);
           Gcom(i,16)=GcomTmp(i,16);
  }
}*/

void CalcJacCom(Model model, VectorNd qSens, Math::Vector3d &com, Eigen::MatrixXd &jacCoM)
{
    int N_UB = 17;
    Math::VectorNd q(N_UB, 1), qdot(N_UB, 1);
    Math::Vector3d comUb, colGcom;
    double ubMass = 0;
    for (int i(0); i < N_UB; i++)
        q(i) = qSens(i);

    // Calculate Gcom
    for (int k(0); k < 2; k++)
    {
        qdot(k) = 1;
        // CalcCenterOfMass(model, q, qdot, NULL, ubMass, comUb, &colGcom, NULL, NULL, NULL, true);
        UpdateSingleColumnOfJacCoM(model, q, ubMass, comUb, &colGcom, k);
        qdot(k) = 0;
        for (int i(0); i < 2; i++)
            jacCoM(i, k) = colGcom(i);
    }
    for (int i(0); i < 3; i++)
        com(i, 0) = comUb(i);
}

void CalcJacCom2(Model *model, double qSens[], Math::MatrixNd &Gcom)
{

    // mapping
    double qUB_[model->dof_count] = {};
    qUB_[0] = qSens[2]; // lateral
    qUB_[1] = qSens[1]; //pitch saggittal
    qUB_[2] = qSens[0]; //yaw

    qUB_[3] = qSens[19];
    qUB_[4] = qSens[20];
    qUB_[5] = qSens[21];
    qUB_[6] = qSens[22];
    qUB_[7] = qSens[26];
    qUB_[8] = qSens[27];
    qUB_[9] = qSens[28];

    qUB_[10] = qSens[15];
    qUB_[11] = qSens[16];
    qUB_[12] = qSens[17];
    qUB_[13] = qSens[18];
    qUB_[14] = qSens[23];
    qUB_[15] = qSens[24];
    qUB_[16] = qSens[25];

    const unsigned int N_UB = 17;
    Math::VectorNd q(N_UB, 1), qdot(N_UB, 1);
    Math::Vector3d comUb, colGcom;
    std::vector<Math::Vector3d> JacCom;
    Math::MatrixNd GcomTmp(3, 17);
    GcomTmp.setZero();
    double ubMass = 17.206; //?? to check
    std::cout << "q2" << std::endl;
    for (int i(0); i < N_UB; i++)
    {
        q(i) = qUB_[i];
        std::cout << q(i) << "  ";
    }
    std::cout << "   " << std::endl;

    // Calculate Gcom
    qdot.setZero();
    for (int k(0); k < N_UB; k++)
    {
        qdot(k) = 1;
        std::vector<int> jIndex({k});
        RigidBodyDynamics::Utils::UpdateSingleColumnOfJacCoM(*model, q, ubMass, comUb, &colGcom, k);
        qdot(k) = 0;
        for (int i(0); i < 3; i++)
            GcomTmp(i, k) = colGcom(i);
    }

    for (int i = 0; i < 3; i++)
    {
        Gcom(i, 0) = GcomTmp(i, 0);
        Gcom(i, 1) = GcomTmp(i, 1);
        Gcom(i, 2) = GcomTmp(i, 2);
        Gcom(i, 3) = GcomTmp(i, 10);
        Gcom(i, 4) = GcomTmp(i, 11);
        Gcom(i, 5) = GcomTmp(i, 12);
        Gcom(i, 6) = GcomTmp(i, 13);
        Gcom(i, 7) = GcomTmp(i, 3);
        Gcom(i, 8) = GcomTmp(i, 4);
        Gcom(i, 9) = GcomTmp(i, 5);
        Gcom(i, 10) = GcomTmp(i, 6);
        Gcom(i, 11) = GcomTmp(i, 7);
        Gcom(i, 12) = GcomTmp(i, 8);
        Gcom(i, 13) = GcomTmp(i, 9);
        Gcom(i, 14) = GcomTmp(i, 14);
        Gcom(i, 15) = GcomTmp(i, 15);
        Gcom(i, 16) = GcomTmp(i, 16);
    }
}

void Control::Posture(double prova[], double Pos_sens[], double Vel_sens[], double qPelvis[], double comUB[], EigenRobotState eigRobotStateMain, const iDynTree::Model &modelMain, iDynTree::KinDynComputations &kinDynCompMain, double accuracy, double filt)
{

    //inverseJacobian
    /* for (int i=0; i<modelMain.getNrOfDOFs(); i++){
        std::cout<<modelMain.getJointName(i)<<std::endl;
    }

    std::cout<<"-----"<<std::endl;*/
    qPelvis[0] = 0;
    qPelvis[1] = 0;
    // qPelvis[2]=0;
    int it = 0;
    //double qUB[N_JOINT_ARM*2+3];
    double qdUB[N_JOINT_ARM * 2 + 3];
    double qUBtemp[N_JOINT_ARM * 2 + 3];
    double hNorm = 100;
    // mapping
    /*WaistLat
    WaistSag
    WaistYaw
    RShSag
    RShLat
    RShYaw
    RElbj
    LShSag
    LShLat
    LShYaw
    LElbj
    LForearmPlate
    LWrj1
    LWrj2
    RForearmPlate
    RWrj1
    RWrj2*/

    qUB[0] = Pos_sens[2];   // lateral
    qUB[1] = Pos_sens[1];   //pitch saggittal
    qUB[2] = Pos_sens[0];   //yaw
    qUB[3] = Pos_sens[15];  //10
    qUB[4] = Pos_sens[16];  //11
    qUB[5] = Pos_sens[17];  //12
    qUB[6] = Pos_sens[18];  //13
    qUB[7] = Pos_sens[19];  //3
    qUB[8] = Pos_sens[20];  //4
    qUB[9] = Pos_sens[21];  //5
    qUB[10] = Pos_sens[22]; //6
    qUB[11] = Pos_sens[26]; //7
    qUB[12] = Pos_sens[27]; //8
    qUB[13] = Pos_sens[28]; //9
    qUB[14] = Pos_sens[23]; //14
    qUB[15] = Pos_sens[24]; //15
    qUB[16] = Pos_sens[25]; //16

    qdUB[0] = Vel_sens[2];
    qdUB[1] = Vel_sens[1];
    qdUB[2] = Vel_sens[0];
    qdUB[3] = Vel_sens[15];
    qdUB[4] = Vel_sens[16];
    qdUB[5] = Vel_sens[17];
    qdUB[6] = Vel_sens[18];
    qdUB[7] = Vel_sens[19];
    qdUB[8] = Vel_sens[20];
    qdUB[9] = Vel_sens[21];
    qdUB[10] = Vel_sens[22];
    qdUB[11] = Vel_sens[26];
    qdUB[12] = Vel_sens[27];
    qdUB[13] = Vel_sens[28];
    qdUB[14] = Vel_sens[23];
    qdUB[15] = Vel_sens[24];
    qdUB[16] = Vel_sens[25];

    double ERR[3] = {100, 100, 100};
    double ACC[3] = {0.1, 0.1, 0.1};
    Eigen::MatrixXd qM(23, 1);

    for (int i = 0; i < 23; i++)
    {
        if (i < 6)
        {
            qM(i, 0) = 0;
        }
        else
        {
            qM(i, 0) = qUB[i - 6];
        }
    }

    for (int i = 0; i < 17; i++)
    {
        if (i < 3)
        {
            qUBtemp[i] = 0;
        }
        else
        {

            qUBtemp[i] = qUB[i];
        }
    }

    for (unsigned int i = 0; i < modelMain.getNrOfDOFs(); i++)
    {
        eigRobotStateMain.jointPos(i) = qUB[i];
        eigRobotStateMain.jointVel(i) = qdUB[i];
    }

    idynRobotState.resize(modelMain.getNrOfDOFs());
    iDynTree::fromEigen(idynRobotState.world_H_base, eigRobotStateMain.world_H_base);
    iDynTree::toEigen(idynRobotState.jointPos) = eigRobotStateMain.jointPos;
    iDynTree::fromEigen(idynRobotState.baseVel, eigRobotStateMain.baseVel);
    toEigen(idynRobotState.jointVel) = eigRobotStateMain.jointVel;
    toEigen(idynRobotState.gravity) = eigRobotStateMain.gravity;
    kinDynCompMain.setRobotState(idynRobotState.world_H_base, idynRobotState.jointPos,
                                 idynRobotState.baseVel, idynRobotState.jointVel, idynRobotState.gravity);
    com = iDynTree::toEigen(kinDynCompMain.getCenterOfMassPosition());

    double comInit[3] = {};
    if (firstLoop)
    {
        for (int i = 0; i < 3; i++)
        {
            comInit[i] = com[i];
        }
        firstLoop = false;
    }
    //   std::cout<<"des  "<<comInit[0]<<"   "<<comInit[1]<<"   "<<comInit[2]<<"   "<<"real  "<<com[0]<<"   "<<com[1]<<"   "<<com[2]<<"   "<<std::endl;

    comUB[0] = com[0];
    comUB[1] = com[1];
    comUB[2] = com[2];

    //std::cout<<"-----------"<<std::endl;
    while ((it < 5) && (hNorm > accuracy))
    {
        //for (int i=0;i<5;i++){

        for (unsigned int i = 0; i < modelMain.getNrOfDOFs(); i++)
        {
            eigRobotStateMain.jointPos(i) = qUB[i];
            eigRobotStateMain.jointVel(i) = qdUB[i];
        }

        idynRobotState.resize(modelMain.getNrOfDOFs());
        iDynTree::fromEigen(idynRobotState.world_H_base, eigRobotStateMain.world_H_base);
        iDynTree::toEigen(idynRobotState.jointPos) = eigRobotStateMain.jointPos;
        iDynTree::fromEigen(idynRobotState.baseVel, eigRobotStateMain.baseVel);
        toEigen(idynRobotState.jointVel) = eigRobotStateMain.jointVel;
        toEigen(idynRobotState.gravity) = eigRobotStateMain.gravity;
        kinDynCompMain.setRobotState(idynRobotState.world_H_base, idynRobotState.jointPos,
                                     idynRobotState.baseVel, idynRobotState.jointVel, idynRobotState.gravity);
        com = iDynTree::toEigen(kinDynCompMain.getCenterOfMassPosition());

        iDynTree::MatrixDynSize idynCOMJacobian(3, modelMain.getNrOfDOFs() + 6);
        kinDynCompMain.getCenterOfMassJacobian(idynCOMJacobian);
        eigCOMJacobian = iDynTree::toEigen(idynCOMJacobian);

        it = it + 1;
        Eigen::MatrixXd hM(2, 1);
        Eigen::MatrixXd jacM(2, 23);
        Eigen::MatrixXd jacInvMhM(23, 2);
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 23; j++)
            {
                jacM(i, j) = eigCOMJacobian(i, j);
                comVel[i] = eigCOMJacobian(i, j) * qdUB[i];
            }
        }
        comInit[0] = 0.016;
        comInit[1] = 0;
        // comInit[2]=0;
        hM(0, 0) = (com[0] - comInit[0]); // error
        hM(1, 0) = (com[1] - comInit[1]);
        //     hM(2, 0) =((com[2]-sqrt(com[0]*com[0]+com[1]*com[1]+com[2]*com[2]))-(comInit[2]-sqrt(comInit[0]*comInit[0]+comInit[1]*comInit[1]+comInit[2]*comInit[2]))); //the total length of the initial zcom component is removed

        /*comUB[0]=com[0];
        comUB[1]=com[1];
        comUB[2]=com[2]-sqrt(com[0]*com[0]+com[1]*com[1]+com[2]*com[2]);*/

        jacInvMhM = jacM.bdcSvd(ComputeThinU | ComputeThinV).solve(hM);

        qM -= 30 * ALPHA_ * jacInvMhM; // we are interested in qM[6:22] because first 6 are related to the fixed frame since it is a floating system
        for (int i = 6; i < 8; i++)
        {                          // you start taking elements from 6 because you have computed everything with respect to the floating frame that has 6 joints
            qUB[i - 6] = qM(i, 0); //you take only the first three joint (torso) as variables... the others are kept fixed
            qPelvis[i - 6] = qUB[i - 6];
        }

        hNorm = hM.norm();
        // std::cout<<"idy: "<<hNorm<<std::endl;
    }
    // std::cout<<"-----------"<<std::endl;

    if (qPelvis[0] > 0.3)
    {
        qPelvis[0] = 0.3;
    }
    if (qPelvis[1] > 0.3)
    {
        qPelvis[1] = 0.3;
    }
    // if (qPelvis[2] > 0.3){qPelvis[2]=0.3;}
    if (qPelvis[0] < -0.3)
    {
        qPelvis[0] = -0.3;
    }
    if (qPelvis[1] < -0.3)
    {
        qPelvis[1] = -0.3;
    }
    // if (qPelvis[2] < -0.3){qPelvis[2]=-0.3;}
    /* comUB[0]=com[0];
    comUB[1]=com[1];
    comUB[2]=com[2];*/
    if (filt)
    {
        for (int i = 0; i < 2; i++)
        {
            AvgFilter(qPelvisVec[i], qPelvisFilt[i], qPelvis[i], 10); //100
        }
        prova[0] = qPelvis[0];
        prova[1] = qPelvis[1];
        qPelvis[0] = qPelvisFilt[0];
        qPelvis[1] = qPelvisFilt[1];
    }
    else
    {

        prova[0] = qPelvis[0];
        prova[1] = qPelvis[1];
    }

    varsOut.qPelvis_ = qPelvis;
    varsOut.hNormPostureIdyntree_ = hNorm;
    varsOut.itIdyntree_ = it;
}

void Control::ComputeMomentum(VectorNd Q, double forceRightHand[3], double forceLeftHand[3], double torqueRightHand[3], double torqueLeftHand[3], Model *modelRBDL, Math::Vector3d &comDesMomentum, Math::Vector3d &torqueRForearm2BaseFrame, Math::Vector3d &torqueLForearm2BaseFrame, Math::Vector3d &comDesMomentumR, Math::Vector3d &comDesMomentumL)
{
    Math::Matrix3d rotRForearm2Base, rotLForearm2Base, rotRForceSens2Base, rotLForceSens2Base, rotBase2RForceSens0, rotBase2LForceSens0;
    Math::Vector3d pointPosition, forceHandL, forceHandR, forceHand0L, forceHand0R, torqueHandL, torqueHandR, fStaticBaseFrame, fStaticBaseFrame0, comElbow, mStaticBaseFrameR, mStaticBaseFrameL, mStaticBaseFrameR0, mStaticBaseFrameL0;
    Math::Vector3d handsMeanPos, RForearmBaseFrame, LForearmBaseFrame, forceRForearm2BaseFrame, forceLForearm2BaseFrame, forceHand;
    Math::Vector3d comElbowBaseFrameR, comElbowBaseFrameL, fStaticBaseFrameR0, fStaticBaseFrameL0;
    int kkk = 500;
    double massUBtot = 17.206;
    pointPosition.setZero();
    rotRForceSens2Base.setZero();
    rotLForceSens2Base.setZero();
    rotRForearm2Base.setZero();
    rotLForearm2Base.setZero();
    forceHandL.setZero();
    forceHandR.setZero();
    torqueHandL.setZero();
    torqueHandR.setZero();
    comDesMomentumR.setZero();
    comDesMomentumL.setZero();
    fStaticBaseFrame.setZero();
    comElbow.setZero();
    mStaticBaseFrameR.setZero();
    mStaticBaseFrameL.setZero();
    RForearmBaseFrame.setZero();
    LForearmBaseFrame.setZero();
    forceRForearm2BaseFrame.setZero();
    forceLForearm2BaseFrame.setZero();
    torqueRForearm2BaseFrame.setZero();
    torqueLForearm2BaseFrame.setZero();
    forceHand.setZero();
    comElbowBaseFrameR.setZero();
    comElbowBaseFrameL.setZero();

    //Filter
    AvgFilter(ForceVecLx, forceLeftHandFilt[0], forceLeftHand[0], kkk);
    AvgFilter(ForceVecLy, forceLeftHandFilt[1], forceLeftHand[1], kkk);
    AvgFilter(ForceVecLz, forceLeftHandFilt[2], forceLeftHand[2], kkk);
    AvgFilter(ForceVecRx, forceRightHandFilt[0], forceRightHand[0], kkk);
    AvgFilter(ForceVecRy, forceRightHandFilt[1], forceRightHand[1], kkk);
    AvgFilter(ForceVecRz, forceRightHandFilt[2], forceRightHand[2], kkk);
    AvgFilter(TorqueVecLx, torqueLeftHandFilt[0], torqueLeftHand[0], kkk);
    AvgFilter(TorqueVecLy, torqueLeftHandFilt[1], torqueLeftHand[1], kkk);
    AvgFilter(TorqueVecLz, torqueLeftHandFilt[2], torqueLeftHand[2], kkk);
    AvgFilter(TorqueVecRx, torqueRightHandFilt[0], torqueRightHand[0], kkk);
    AvgFilter(TorqueVecRy, torqueRightHandFilt[1], torqueRightHand[1], kkk);
    AvgFilter(TorqueVecRz, torqueRightHandFilt[2], torqueRightHand[2], kkk);
    forceHandR(0) = forceRightHandFilt[0];
    forceHandR(1) = forceRightHandFilt[1];
    forceHandR(2) = forceRightHandFilt[2];
    forceHandL(0) = forceLeftHandFilt[0];
    forceHandL(1) = forceLeftHandFilt[1];
    forceHandL(2) = forceLeftHandFilt[2];
    torqueHandR(0) = torqueRightHandFilt[0];
    torqueHandR(1) = torqueRightHandFilt[1];
    torqueHandR(2) = torqueRightHandFilt[2];
    torqueHandL(0) = torqueLeftHandFilt[0];
    torqueHandL(1) = torqueLeftHandFilt[1];
    torqueHandL(2) = torqueLeftHandFilt[2];

    Eigen::Matrix<double, 3, 3> rotMatrixEigForceSens2Elbow;
    rotMatrixEigForceSens2Elbow << -1, 0, 0,
        0, 1, 0,
        0, 0, -1;

    rotRForearm2Base = CalcBodyWorldOrientation(*modelRBDL, Q, modelRBDL->GetBodyId("RForearm"), true);
    rotLForearm2Base = CalcBodyWorldOrientation(*modelRBDL, Q, modelRBDL->GetBodyId("LForearm"), true);
    RForearmBaseFrame = CalcBodyToBaseCoordinates(*modelRBDL, Q, modelRBDL->GetBodyId("RForearm"), pointPosition, true);
    LForearmBaseFrame = CalcBodyToBaseCoordinates(*modelRBDL, Q, modelRBDL->GetBodyId("LForearm"), pointPosition, true);

    //static force and momentum in the waist frame (base frame)
    fStaticBaseFrame << 0, 0, -1.5 * 9.81;
    comElbow << 0, -0.01, 0.075;
    rotRForceSens2Base = rotRForearm2Base * rotMatrixEigForceSens2Elbow;
    rotLForceSens2Base = rotLForearm2Base * rotMatrixEigForceSens2Elbow;

    comElbowBaseFrameR = rotRForceSens2Base * comElbow + RForearmBaseFrame;
    comElbowBaseFrameL = rotLForceSens2Base * comElbow + LForearmBaseFrame;
    mStaticBaseFrameR = comElbowBaseFrameR.cross(fStaticBaseFrame);
    mStaticBaseFrameL = comElbowBaseFrameL.cross(fStaticBaseFrame); //r constant
    //if (firstTime){
    //    rotBase2RForceSens0=rotRForceSens2Base.transpose();
    //    rotBase2LForceSens0=rotLForceSens2Base.transpose();
    //    mStaticBaseFrameR0 = mStaticBaseFrameR;
    //    mStaticBaseFrameL0 = mStaticBaseFrameL;
    //    firstTime = false;
    //}
    //fStaticBaseFrameR0=rotRForceSens2Base*(rotBase2RForceSens0*fStaticBaseFrame);
    //fStaticBaseFrameL0=rotLForceSens2Base*(rotBase2LForceSens0*fStaticBaseFrame);

    //forceRForearm2BaseFrame = rotRForceSens2Base*(forceHandR)+fStaticBaseFrameR0-fStaticBaseFrame;//should be : -fStaticBaseFrame+fStaticBaseFrame0;
    //forceLForearm2BaseFrame = rotLForceSens2Base*(forceHandL)+fStaticBaseFrameL0-fStaticBaseFrame; //-fStaticBaseFrame+fStaticBaseFrame0;
    //torqueRForearm2BaseFrame = rotRForceSens2Base*torqueHandR-mStaticBaseFrameR+mStaticBaseFrameR0;
    //torqueLForearm2BaseFrame = rotLForceSens2Base*torqueHandL-mStaticBaseFrameL+mStaticBaseFrameL0;

    forceRForearm2BaseFrame = rotRForceSens2Base * (forceHandR)-fStaticBaseFrame; //should be : -fStaticBaseFrame+fStaticBaseFrame0;
    forceLForearm2BaseFrame = rotLForceSens2Base * (forceHandL)-fStaticBaseFrame; //-fStaticBaseFrame+fStaticBaseFrame0;
    torqueRForearm2BaseFrame = rotRForceSens2Base * torqueHandR - mStaticBaseFrameR;
    torqueLForearm2BaseFrame = rotLForceSens2Base * torqueHandL - mStaticBaseFrameL;
    //Pay attention, maybe you should have left fStaticBaseFrameL0 instead of mStaticBaseFrameL

    Math::Vector3d l1, l2, f1, f1BaseFrame;
    l1 << 0.17, -0.175, -0.12; // 0.1979+0.21;
    l2 << 0.17, 0.22, 0.1979 + 0.21;
    f1 << 0, 0, -5;

    //cross-product
    comDesMomentumR = RForearmBaseFrame.cross(forceRForearm2BaseFrame);
    comDesMomentumL = LForearmBaseFrame.cross(forceLForearm2BaseFrame);

    //result
    //comDesMomentum = ((comDesMomentumL + comDesMomentumR) +torqueRForearm2BaseFrame+ torqueLForearm2BaseFrame)/(massUBtot*9.81);

    if (firstTime2)
    {
        std::cout << "ENTRATO" << std::endl;
        comDesMomentum0 = ((comDesMomentumL + comDesMomentumR) + torqueRForearm2BaseFrame + torqueLForearm2BaseFrame) / (massUBtot * 9.81);
        firstTime2 = false;
    }

    comDesMomentum = ((comDesMomentumL + comDesMomentumR) + torqueRForearm2BaseFrame + torqueLForearm2BaseFrame) / (massUBtot * 9.81);
    comDesMomentum = comDesMomentum - comDesMomentum0;
}

void Control::PostureRBDL_FT(double posSens[], double velSens[], Model *modelRBDL, Model *modelRBDLJac, double accuracy, double filt, double qPelvis[], double forceRightHand[3], double forceLeftHand[3], double forceRightHand0[3], double forceLeftHand0[3], double torqueRightHand[3], double torqueLeftHand[3], double trans[][3])
{
    double qMap[modelRBDL->dof_count] = {};
    Math::Vector3d point_position;
    point_position.setZero();
    Math::Vector3d comUB, comDesMomentum, comDesMomentum0, comDesMomentumWeight;
    Math::Vector3d comUBVel, fStaticBaseFrame;
    Math::Vector3d torqueRForearm2BaseFrame, torqueLForearm2BaseFrame, comDesMomentumR, comDesMomentumL;
    torqueRForearm2BaseFrame.setZero();
    torqueLForearm2BaseFrame.setZero();
    comDesMomentumR.setZero();
    comDesMomentumL.setZero();
    comUBVel.setZero();
    comDesMomentum.setZero();
    fStaticBaseFrame.setZero();
    comDesMomentumWeight.setZero();
    Eigen::MatrixXd hM(2, 1);
    Eigen::MatrixXd jacM(2, 2);
    Eigen::MatrixXd jacInvMhM(2, 2);
    int it = 0;
    double hNorm = 100;
    VectorNd Q = VectorNd::Zero(modelRBDL->dof_count);
    Eigen::MatrixXd qM(2, 1);
    double comInit[2] = {};
    double comUBout[3] = {};
    double massUBtot = 17.206;

    if (filt)
    {
        ComputeMomentum(Q, forceRightHand, forceLeftHand, torqueRightHand, torqueLeftHand, modelRBDL, comDesMomentum, torqueRForearm2BaseFrame, torqueLForearm2BaseFrame, comDesMomentumR, comDesMomentumL);
        comDesMomentum = comDesMomentum / 3;
    }

    //Mapping
    qMap[0] = -posSens[2];  // lateral
    qMap[1] = posSens[1];   //pitch saggittal
    qMap[2] = posSens[0];   //yaw
    qMap[3] = posSens[19];  //7
    qMap[4] = posSens[20];  //8
    qMap[5] = posSens[21];  //9
    qMap[6] = posSens[22];  //10
    qMap[7] = posSens[26];  //11
    qMap[8] = posSens[27];  //12
    qMap[9] = posSens[28];  //13
    qMap[10] = posSens[15]; //3
    qMap[11] = posSens[16]; //4
    qMap[12] = posSens[17]; //5
    qMap[13] = posSens[18]; //6
    qMap[14] = posSens[23];
    qMap[15] = posSens[24];
    qMap[16] = posSens[25];

    for (int i = 0; i < modelRBDL->dof_count; i++)
    {
        Q(i) = qMap[i];
    }
    qM(0, 0) = qMap[0];
    qM(1, 0) = qMap[1];

    //CalcJacCom(*modelRBDL, Q, comUB, jacM);
    double ubMass = 17.206;
    Math::Vector3d colGcom;
    UpdateSingleColumnOfJacCoM(*modelRBDL, Q, ubMass, comUB, &colGcom, 0);

    comUBout[0] = comUB(0);
    comUBout[1] = comUB(1);
    comUBout[2] = comUB(2);
    fStaticBaseFrame << 0, 0, -massUBtot * 9.81;
    comDesMomentumWeight = comUB.cross(fStaticBaseFrame);

    if (comNum == 1) // Try without --------------------------------------
    {
        comInit[0] = 0.005 - comDesMomentum(1);
    }
    else
    {
        comInit[0] = 0.025 - comDesMomentum(1);
    }

    hM(0) = comUB(0) - comInit[0];
    hM(1) = comUB(1) - comInit[1];

    double prevError = hM.norm();
    // std::cerr << "Errors: " << prevError;

    while (hNorm > accuracy && it < 20) //Modifying the number of iterations --------------------
    {                                   // && (hNorm >accuracy)){
        // for (int i=0;i<5;i++){
        it = it + 1;
        CalcJacCom(*modelRBDL, Q, comUB, jacM);
        hM(0) = comUB(0) - comInit[0];
        hM(1) = comUB(1) - comInit[1];
        jacInvMhM = jacM.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(hM);
        qM -= 0.5 * jacInvMhM;
        Q(0) = qM(0);
        Q(1) = qM(1);

        hNorm = hM.norm();
        // std::cerr << " ; " << hNorm;

        if (hNorm > prevError)
        {
            overNum += 1;
        }

        prevError = hNorm;
    }

    // std::cerr << std::endl;

    // std::cerr << "Number of overshoots: " << overNum << std::endl;
    // std::cerr << "Pitch: " << Q(1) << " - Yaw: " << Q(0) << std::endl;

    qPelvis[0] = Q(0);
    qPelvis[1] = Q(1);

    if (qPelvis[0] > 0.5)
    {
        qPelvis[0] = 0.5;
    }
    if (qPelvis[1] > 0.5)
    {
        qPelvis[1] = 0.5;
    }
    if (qPelvis[0] < -0.5)
    {
        qPelvis[0] = -0.5;
    }
    if (qPelvis[1] < -0.5)
    {
        qPelvis[1] = -0.5;
    }

    varsOut.qPelvis_ = qPelvis;
    varsOut.itRBDL_ = it;
    varsOut.hM0_ = hM(0);
    varsOut.hM1_ = hM(1);
    varsOut.hNormPostureRBDL_ = hNorm;
    varsOut.comUBx_ = comUBout[0];
    varsOut.comUBxDes_ = comInit[0];
    varsOut.comUBy_ = comUBout[1];
    varsOut.comUBz_ = comUBout[2];
    varsOut.comUByDes_ = comInit[1];
    varsOut.comDesMomentum0_ = comDesMomentum(0) * (massUBtot * 9.81);
    varsOut.comDesMomentum1_ = comDesMomentum(1) * (massUBtot * 9.81);
    varsOut.comDesMomentum2_ = comDesMomentum(2) * (massUBtot * 9.81);
    varsOut.torqueRForearm2BaseFrame_ = torqueRForearm2BaseFrame;
    varsOut.torqueLForearm2BaseFrame_ = torqueLForearm2BaseFrame;
    varsOut.comDesMomentumR_ = comDesMomentumR;
    varsOut.comDesMomentumL_ = comDesMomentumL;
    varsOut.comDesMomentumWeight_ = comDesMomentumWeight;
    // varsOut.posRArm_ = RForearmBaseFrame;
    // varsOut.posLArm_ = LForearmBaseFrame;
    // varsOut.forceRArm_ = forceRForearm2BaseFrame;
    // varsOut.forceLArm_ = forceLForearm2BaseFrame;
}

void Control::PostureRBDL(double posSens[], double velSens[], Model *modelRBDL, Model *modelRBDLJac, double accuracy, double filt, double qPelvis[])
{

    double qMap[modelRBDL->dof_count];
    Math::Vector3d point_position;
    point_position.setZero();
    Math::Vector3d comUB;
    Math::Vector3d comUBVel;
    comUBVel.setZero();
    Eigen::MatrixXd hM(2, 1);
    Eigen::MatrixXd jacM(2, 2);
    Eigen::MatrixXd jacInvMhM(2, 2);
    int it = 0;
    double hNorm = 100;
    VectorNd Q = VectorNd::Zero(modelRBDL->dof_count);
    Eigen::MatrixXd qM(2, 1);
    double comInit[2];
    double comUBout[3];

    //Mapping
    qMap[0] = posSens[2];   // lateral
    qMap[1] = posSens[1];   //pitch saggittal
    qMap[2] = posSens[0];   //yaw
    qMap[3] = posSens[19];  //7
    qMap[4] = posSens[20];  //8
    qMap[5] = posSens[21];  //9
    qMap[6] = posSens[22];  //10
    qMap[7] = posSens[26];  //11
    qMap[8] = posSens[27];  //12
    qMap[9] = posSens[28];  //13
    qMap[10] = posSens[15]; //3
    qMap[11] = posSens[16]; //4
    qMap[12] = posSens[17]; //5
    qMap[13] = posSens[18]; //6
    qMap[14] = posSens[23];
    qMap[15] = posSens[24];
    qMap[16] = posSens[25];

    for (int i = 0; i < modelRBDL->dof_count; i++)
    {
        Q(i) = qMap[i];
    }
    qM(0, 0) = qMap[0];
    qM(1, 0) = qMap[1];

    //CalcJacCom(*modelRBDL, Q, comUB, jacM);
    double ubMass = 17.206;
    Math::Vector3d colGcom;
    UpdateSingleColumnOfJacCoM(*modelRBDL, Q, ubMass, comUB, &colGcom, 0);

    comUBout[0] = comUB(0);
    comUBout[1] = comUB(1);
    comUBout[2] = comUB(2);

    if (whichComan_ == 1)
    {
        comInit[0] = 0.013;
    }
    else
    {
        comInit[0] = 0.025;
    }
    comInit[1] = 0;
    hM(0) = comUB(0) - comInit[0];
    hM(1) = comUB(1) - comInit[1];

    while ((it < 2))
    { // && (hNorm >accuracy)){
        // for (int i=0;i<5;i++){
        it = it + 1;
        CalcJacCom(*modelRBDL, Q, comUB, jacM);
        hM(0) = comUB(0) - comInit[0];
        hM(1) = comUB(1) - comInit[1];
        jacInvMhM = jacM.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(hM);
        qM -= 1 * jacInvMhM;
        Q(0) = qM(0);
        Q(1) = qM(1);

        hNorm = hM.norm();
    }
    qPelvis[0] = Q(0);
    qPelvis[1] = Q(1);

    if (qPelvis[0] > 0.5)
    {
        qPelvis[0] = 0.5;
    }
    if (qPelvis[1] > 0.5)
    {
        qPelvis[1] = 0.5;
    }
    if (qPelvis[0] < -0.5)
    {
        qPelvis[0] = -0.5;
    }
    if (qPelvis[1] < -0.5)
    {
        qPelvis[1] = -0.5;
    }
    if (filt)
    {
        for (int i = 0; i < 2; i++)
        {
            AvgFilter(qPelvisVec[i], qPelvisFilt[i], qPelvis[i], 10);
        }

        qPelvis[0] = qPelvisFilt[0];
        qPelvis[1] = qPelvisFilt[1];
    }

    varsOut.qPelvis_ = qPelvis;
    varsOut.itRBDL_ = it;
    varsOut.hNormPostureRBDL_ = hNorm;
    //varsOut.comUB_=comUBout[0];
}

void Control::computeDynKin(double qq[], double qqd[], double ForceHand[], std::string whichPart, EigenRobotState eigRobotStateMainARM, const iDynTree::Model modelMainARM, iDynTree::KinDynComputations &kinDynCompMainARM)
{

    // import model
    // bool ok = kinDynComp.loadRobotModel(mdlLoader.model());

    // if( !ok )
    // {
    //     std::cerr << "KinDynComputationsWithEigen: impossible to load the following model in a KinDynComputations class:" << std::endl
    //               << mdlLoader.model().toString() << std::endl;
    //     //return EXIT_FAILURE;
    // }
    // const iDynTree::Model & model = kinDynComp.model();
    eigRobotStateMainARM.resize(modelMainARM.getNrOfDOFs());
    //std::cout<<mdlLoader.model().toString()<<std::endl; //check the model characteristics

    eigRobotStateMainARM.random(); //first I write the random state and then I overwrite the variables that I know. The others I should ask.
    for (unsigned int i = 0; i < modelMainARM.getNrOfDOFs(); i++)
    {
        eigRobotStateMainARM.jointPos(i) = qq[i];
        eigRobotStateMainARM.jointVel(i) = qqd[i];
    }

    // Kinematics
    idynRobotStateMainARM.resize(modelMainARM.getNrOfDOFs());
    iDynTree::fromEigen(idynRobotStateMainARM.world_H_base, eigRobotStateMainARM.world_H_base);
    iDynTree::toEigen(idynRobotStateMainARM.jointPos) = eigRobotStateMainARM.jointPos;
    iDynTree::fromEigen(idynRobotStateMainARM.baseVel, eigRobotStateMainARM.baseVel);
    toEigen(idynRobotStateMainARM.jointVel) = eigRobotStateMainARM.jointVel;
    toEigen(idynRobotStateMainARM.gravity) = eigRobotStateMainARM.gravity;
    kinDynCompMainARM.setRobotState(idynRobotStateMainARM.world_H_base, idynRobotStateMainARM.jointPos,
                                    idynRobotStateMainARM.baseVel, idynRobotStateMainARM.jointVel, idynRobotStateMainARM.gravity);
    // Once we called the setRobotState, we can call all the methods of KinDynComputations
    // For methods returning fixed size vector/matrices, the conversion to eigen types is trivial
    /*  com = iDynTree::toEigen(kinDynCompMainARM.getCenterOfMassPosition());


    //here we need to distinguish which part of the robot we are given as model
    if (whichPart =="RightArm"){

        handPositionTemp_Abs =  this-> get_absolute_position("RSoftHand", modelMainARM, kinDynCompMainARM);
        shoulderPositionTemp_Abs =  this-> get_absolute_position("RShr", modelMainARM, kinDynCompMainARM);
        elbowPositionTemp_Abs =  this-> get_absolute_position("RElb", modelMainARM, kinDynCompMainARM);
    }
    else
        if(whichPart =="LeftArm"){
            handPositionTemp_Abs =  this-> get_absolute_position("RSoftHand", modelMainARM, kinDynCompMainARM);
            shoulderPositionTemp_Abs =  this-> get_absolute_position("RShr", modelMainARM, kinDynCompMainARM);
            elbowPositionTemp_Abs =  this-> get_absolute_position("RElb", modelMainARM, kinDynCompMainARM);
        }
        else
            if(whichPart =="UpperBody"){

                handPositionTemp_Abs =  this-> get_absolute_position("LSoftHand", modelMainARM, kinDynCompMainARM);
                shoulderPositionTemp_Abs =  this-> get_absolute_position("LShr", modelMainARM, kinDynCompMainARM);
                elbowPositionTemp_Abs =  this-> get_absolute_position("LElb", modelMainARM, kinDynCompMainARM);

            }
    basePositionTemp_Abs =  this-> get_absolute_position("torso", modelMainARM, kinDynCompMainARM);
    torsoPositionTemp_Abs =  this-> get_absolute_position("torso", modelMainARM,kinDynCompMainARM);*/

    iDynTree::FrameIndex arbitraryFrameIndexHAND = modelMainARM.getFrameIndex("r_hand_lower_right_link");
    //iDynTree::FreeFloatingMassMatrix idynMassMatrix(modelMainARM);
    //kinDynCompMainARM.getFreeFloatingMassMatrix(idynMassMatrix);
    //eigMassMatrix = iDynTree::toEigen(idynMassMatrix);// eigRobotState.jointPos=robotstatus.angles; // NOTA!!!!->// eigRobotState.jointPos=robotstatus.angles; // NOTA!!!!->da rimuovere ed aggiornare con il nuovo stato del robotda rimuover// eigRobotState.jointPos=robotstatus.angles; // NOTA!!!!->da rimuovere ed aggiornare con il nuovo stato del robote ed aggiornare con il nuovo stato del robot
    //iDynTree::MatrixDynSize idynCOMJacobian(3,modelMainARM.getNrOfDOFs()+6);
    //kinDynCompMainARM.getCenterOfMassJacobian(idynCOMJacobian);
    //eigCOMJacobian = iDynTree::toEigen(idynCOMJacobian);
    //Jacob2 = eigCOMJacobian.cast <float> ();
    // JacobTrasp2= Jacob2.transpose();
    const iDynTree::FrameIndex FI = arbitraryFrameIndexHAND;
    iDynTree::FrameFreeFloatingJacobian idynJacobian(modelMainARM);
    bool j = kinDynCompMainARM.getFrameFreeFloatingJacobian(FI, idynJacobian);
    gGF.resize(modelMainARM);
    kinDynCompMainARM.generalizedGravityForces(gGF);
    eigJacobian = iDynTree::toEigen(idynJacobian);
    Jacob = eigJacobian.cast<float>();
    JacobTrasp = Jacob.transpose();

    /* eigRobotAcc.resize(modelMainARM.getNrOfDOFs());
    eigRobotAcc.random();
    //Acceleration to 0 both the joint and the base- Maybe after you can find another solution
    for(unsigned int i =0; i < 7; i++ ) {
        eigRobotAcc.jointAcc(i)= 0;
    }
    idynRobotAcc.resize(modelMainARM.getNrOfDOFs());
    iDynTree::toEigen(idynRobotAcc.baseAcc) = eigRobotAcc.baseAcc;
    iDynTree::toEigen(idynRobotAcc.jointAcc) = eigRobotAcc.jointAcc;*/
}

std::vector<double> Control::filter(double S1[], double S2[], double S3[], std::vector<double> result, int MM)

{
    double sum_x = 0;
    double sum_y = 0;
    double sum_z = 0;
    double sum[3] = {};

    for (int i = 0; i < MM; i++)
    {

        sum_x = sum_x + S1[i];
        sum_y = sum_y + S2[i];
        sum_z = sum_z + S3[i];
    }
    sum[0] = sum_x;
    sum[1] = sum_y;
    sum[2] = sum_z;

    for (int i = 0; i < 3; i++)
    {

        result[i] = sum[i] / MM;
    }

    return result;
}

Eigen::Vector3d Control::get_absolute_position(std::string link, const iDynTree::Model &modelMainARM, iDynTree::KinDynComputations &kinDynCompMainARM)
{

    //Computation of the position and the orientation of the endEffector (hand) respect to "torso" or "RShp"<- da controllare
    //iDynTree::FrameIndex arbitraryFrameIndex = modelMainARM.getFrameIndex("RForearm");
    iDynTree::FrameIndex arbitraryFrameIndex = modelMainARM.getFrameIndex(link);
    // std::cout<<model.getNrOfDOFs()<<std::endl;
    Eigen::Matrix4d world_H_arbitraryFrame = iDynTree::toEigen(kinDynCompMainARM.getWorldTransform(arbitraryFrameIndex).asHomogeneousTransform());
    // Eigen::Matrix4d world_H_arbitraryFrame = iDynTree::toEigen(kinDynCompMainARM.getRelativeTransform(arbitraryFrameIndex, arbitraryFrameIndex2).asHomogeneousTransform());

    Eigen::Vector3d link_pos;
    for (unsigned int i = 0; i < 3; i++)
    {
        link_pos[i] = world_H_arbitraryFrame(i, 3);
    }
    return link_pos;
}

void Control::EraseVectors()
{
    pxVec.clear();
    pxVec.clear();
    pyVec.clear();
    pxswfVec.clear();
    pyswfVec.clear();
    //    vxVec.clear();
    //    vyVec.clear();
    vxswfVec.clear();
    vyswfVec.clear();
    axVec.clear();
    thpFVec.clear();
    thrFVec.clear();
    thyFVec.clear();
    dthpFVec.clear();
    dthrFVec.clear();
    dthyFVec.clear();
    kVec.clear();
    pxAbsVec.clear();
    pxAbsMedVec.clear();
    pyAbsVec.clear();
    //    vxFkVec.clear();
    //    vxAbsVec.clear();
    //    vyAbsVec.clear();
    swFtPitchVec.clear();
    dswFtPitchVec.clear();
    swFtRollVec.clear();
    dswFtRollVec.clear();
    vecPxAbs.clear();
    for (int i(0); i < 3; i++)
    {
        pPelvisAbsVec[i].clear();
        pPelvisAbsFVec[i].clear();
    }
}

/*  if(begin_time == -1)
    {
        begin_time = time;
        //iface.SetStiffness(Joints::Values(index), Units::Newton(200));
        //iface.SetDamping(Joints::Values(index), Units::Newton(0));
        p0 = S.MotorSensors[index].Position.Convert(Units::Radian).Value;
        p02 = S.MotorSensors[index2].Position.Convert(Units::Radian).Value;
        p03 = S.MotorSensors[index3].Position.Convert(Units::Radian).Value;
        p04 = S.MotorSensors[index4].Position.Convert(Units::Radian).Value;
    }
    // Right Elbow Pitch Sinusoidal Motion with frequency = freq and amplitude = amp
    double freq = 1;
    double amp = 0.15;
    double alpha = 0.2; // How fast exp approaches zero.
    time -= begin_time;
    double time_1 = 100;
    double ang_final = -0;
    double ang_final2 = -0;
    double ang_final3 = 0;
    double ang_final4 = 0;
    if (time <= time_1)
    {
        // Slowly go to ..
        iface.SetReferencePosition(Joints::Values(index), Units::Radian(ang_final+(p0-ang_final)*exp(-alpha*time)));
        iface.SetReferencePosition(Joints::Values(index2), Units::Radian(ang_final2+(p02-ang_final2)*exp(-alpha*time)));
        iface.SetReferencePosition(Joints::Values(index3), Units::Radian(ang_final3+(p03-ang_final4)*exp(-alpha*time)));
        iface.SetReferencePosition(Joints::Values(index4), Units::Radian(ang_final4+(p04-ang_final4)*exp(-alpha*time)));

        iface.SetReferencePosition(Joints::Values(index_torque), Units::Radian(0));
        iface.SetStiffness(Joints::Values(index_torque), Units::Newton(0));
        iface.SetDamping(Joints::Values(index_torque), Units::Newton(0));
        double Kp = 10;
        double Kd = 1;
        double p_des = 0;
        double v_des = 0;
        double p_index_torque = S.MotorSensors[index_torque].Position.Convert(Units::Radian).Value;
        double v_index_torque = S.MotorSensors[index_torque].Velocity.Convert(Units::Radian).Value;
        double tau_des = 0-Kp*(p_index_torque-p_des)-Kd*(v_index_torque-v_des);
        iface.SetReferenceTorque(Joints::Values(index_torque), Units::Newton(tau_des));
        iface.Submit();
    }
    else
    {
    double t = time - time_1;
        // Start oscillating about ..
        iface.SetReferencePosition(Joints::Values(index), Units::Radian(ang_final+amp*sin(freq*2*M_PI*t)));
        iface.SetReferencePosition(Joints::Values(index2), Units::Radian(ang_final2+amp*sin(freq*2*M_PI*t)));
        iface.SetReferencePosition(Joints::Values(index2), Units::Radian(ang_final3+amp*sin(freq*2*M_PI*t)));
        iface.SetReferencePosition(Joints::Values(index2), Units::Radian(ang_final4+amp*sin(freq*2*M_PI*t)));
    }*/
