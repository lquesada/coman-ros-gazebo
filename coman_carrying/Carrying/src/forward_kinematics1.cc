#include <math.h>
#include <stdlib.h>
#include <stdio.h>
//#include "forward_kinematics.hh"

#define LEFT 0
#define RIGHT 1
void forward_kinematics(double q_joints[], double P_cartesian[], int side)
{
  double C12 = 0;
  double S12 = 0;
  double C13 = 0;
  double S13 = 0;
  double C14 = 0;
  double S14 = 0;
  double C15 = 0;
  double S15 = 0;
  double C16 = 0;
  double S16 = 0;
  double C17 = 0;
  double S17 = 0;

  double RO_0_414 = 0;
  double RO_0_614 = 0;
  double RO_0_714 = 0;
  double RO_0_914 = 0;
  double RO_0_115 = 0;
  double RO_0_215 = 0;
  double RO_0_315 = 0;
  double RO_0_415 = 0;
  double RO_0_515 = 0;
  double RO_0_615 = 0;
  double RO_0_116 = 0;
  double RO_0_216 = 0;
  double RO_0_316 = 0;
  double RO_0_716 = 0;
  double RO_0_816 = 0;
  double RO_0_916 = 0;
  double RO_0_417 = 0;
  double RO_0_517 = 0;
  double RO_0_617 = 0;
  double RO_0_717 = 0;
  double RO_0_817 = 0;
  double RO_0_917 = 0;
  double RO_0_718 = 0;
  double RO_0_818 = 0;
  double RO_0_918 = 0;
  double RL_0_115 = 0;
  double RL_0_215 = 0;
  double RL_0_315 = 0;
  double RL_0_116 = 0;
  double RL_0_216 = 0;
  double RL_0_316 = 0;
  double RL_0_117 = 0;
  double RL_0_217 = 0;
  double RL_0_317 = 0;
  double RL_0_119 = 0;
  double RL_0_219 = 0;
  double RL_0_319 = 0;

  double q[18] = {0};
  double dpt[6][6] = {0};

  // coman joints
  q[12] = q_joints[0];
  q[13] = q_joints[1];
  q[14] = q_joints[2];
  q[15] = q_joints[3];
  q[16] = q_joints[4];
  q[17] = q_joints[5];

  if (side == LEFT)
  {
    dpt[2][1] = 0.023;
    dpt[2][2] = 0.0496;
    dpt[3][3] = -0.1024;
    dpt[3][4] = -0.1234;
    dpt[3][5] = -0.201;
    dpt[3][7] = -0.1;
  }

  if (side == RIGHT)
  {
    dpt[2][1] = -0.023;
    dpt[2][2] = -0.0496;
    dpt[3][3] = -0.1024;
    dpt[3][4] = -0.1234;
    dpt[3][5] = -0.201;
    dpt[3][7] = -0.1;
  }

  // Trigonometric Variables

  C12 = cos(q[12]);
  S12 = sin(q[12]);
  C13 = cos(q[13]);
  S13 = sin(q[13]);
  C14 = cos(q[14]);
  S14 = sin(q[14]);
  C15 = cos(q[15]);
  S15 = sin(q[15]);
  C16 = cos(q[16]);
  S16 = sin(q[16]);
  C17 = cos(q[17]);
  S17 = sin(q[17]);

  RO_0_414 = S12 * S13;
  RO_0_614 = C12 * S13;
  RO_0_714 = S12 * C13;
  RO_0_914 = C12 * C13;
  RO_0_115 = RO_0_414 * S14 + C12 * C14;
  RO_0_215 = C13 * S14;
  RO_0_315 = RO_0_614 * S14 - S12 * C14;
  RO_0_415 = RO_0_414 * C14 - C12 * S14;
  RO_0_515 = C13 * C14;
  RO_0_615 = RO_0_614 * C14 + S12 * S14;
  RO_0_116 = RO_0_115 * C15 - RO_0_714 * S15;
  RO_0_216 = RO_0_215 * C15 + S13 * S15;
  RO_0_316 = RO_0_315 * C15 - RO_0_914 * S15;
  RO_0_716 = RO_0_115 * S15 + RO_0_714 * C15;
  RO_0_816 = RO_0_215 * S15 - S13 * C15;
  RO_0_916 = RO_0_315 * S15 + RO_0_914 * C15;
  RO_0_417 = RO_0_415 * C16 + RO_0_716 * S16;
  RO_0_517 = RO_0_515 * C16 + RO_0_816 * S16;
  RO_0_617 = RO_0_615 * C16 + RO_0_916 * S16;
  RO_0_717 = -(RO_0_415 * S16 - RO_0_716 * C16);
  RO_0_817 = -(RO_0_515 * S16 - RO_0_816 * C16);
  RO_0_917 = -(RO_0_615 * S16 - RO_0_916 * C16);
  RO_0_718 = RO_0_116 * S17 + RO_0_717 * C17;
  RO_0_818 = RO_0_216 * S17 + RO_0_817 * C17;
  RO_0_918 = RO_0_316 * S17 + RO_0_917 * C17;
  RL_0_115 = RO_0_714 * dpt[3][3];
  RL_0_215 = -dpt[3][3] * S13;
  RL_0_315 = RO_0_914 * dpt[3][3];
  RL_0_116 = RO_0_714 * dpt[3][4];
  RL_0_216 = -dpt[3][4] * S13;
  RL_0_316 = RO_0_914 * dpt[3][4];
  RL_0_117 = RO_0_716 * dpt[3][5];
  RL_0_217 = RO_0_816 * dpt[3][5];
  RL_0_317 = RO_0_916 * dpt[3][5];
  RL_0_119 = RO_0_718 * dpt[3][7];
  RL_0_219 = RO_0_818 * dpt[3][7];
  RL_0_319 = RO_0_918 * dpt[3][7];

  // Symbolic Outputs

  P_cartesian[0] = RL_0_115 + RL_0_116 + RL_0_117 + RL_0_119;
  P_cartesian[1] = RL_0_215 + RL_0_216 + RL_0_217 + RL_0_219 + dpt[2][1] + dpt[2][2];
  P_cartesian[2] = RL_0_315 + RL_0_316 + RL_0_317 + RL_0_319;
  P_cartesian[3] = RO_0_617;
  P_cartesian[4] = RO_0_718;
  P_cartesian[5] = (RO_0_216 * C17) - (RO_0_817 * S17);

  // Added by Hamed: This is to make the forward kinematics consistent with IMU Data (Nov 17, 2016)
  P_cartesian[1] = -P_cartesian[1]; // y component of the CoM of the Pelvis
  P_cartesian[2] = -P_cartesian[2]; //  y component of the CoM of the Pelvis
  P_cartesian[4] = -P_cartesian[4]; // Pitch Angle of the Pelvis
  P_cartesian[5] = -P_cartesian[5]; // Yaw Angle of the Pelvis
}
