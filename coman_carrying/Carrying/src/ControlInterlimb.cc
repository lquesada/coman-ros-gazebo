#include "ControlInterlimb.hh"
#include <math.h>
# define N 31

void ControlInterlimb::controlPosture(double  qq[], double  qqd[], iDynTree::ModelLoader mdlLoader1, double qPelvis[], Eigen::Vector3d com)
{

    // import model
    bool ok = kinDynComp1.loadRobotModel(mdlLoader1.model());

    if( !ok )
    {
        std::cerr << "kinDynComp1utationsWithEigen: impossible to load the following model in a kinDynComp1utations class:" << std::endl
                  << mdlLoader1.model().toString() << std::endl;
        //return EXIT_FAILURE;
    }
    const iDynTree::Model & model = kinDynComp1.model();
    eigRobotState1.resize(model.getNrOfDOFs());

    eigRobotState1.random(); //first I write the random state and then I overwrite the variables that I know. The others I should ask.
    for(unsigned int i =0; i < 7; i++ ) {
        eigRobotState1.jointPos(i)=qq[i];
        eigRobotState1.jointVel(i)=qqd[i];
       // eigRobotState1.baseVel (i)=0;
    }

   // Kinematics
    idynRobotState1.resize(model.getNrOfDOFs());
    iDynTree::fromEigen(idynRobotState1.world_H_base,eigRobotState1.world_H_base);
    iDynTree::toEigen(idynRobotState1.jointPos) = eigRobotState1.jointPos;
    iDynTree::fromEigen(idynRobotState1.baseVel,eigRobotState1.baseVel);
    toEigen(idynRobotState1.jointVel) = eigRobotState1.jointVel;
    toEigen(idynRobotState1.gravity)  = eigRobotState1.gravity;
    kinDynComp1.setRobotState(idynRobotState1.world_H_base,idynRobotState1.jointPos,
                             idynRobotState1.baseVel,idynRobotState1.jointVel,idynRobotState1.gravity);
    iDynTree::FrameIndex arbitraryFrameIndex = model.getFrameIndex("r_hand_lower_right_link");
    Eigen::Matrix4d world_H_arbitraryFrame = iDynTree::toEigen(this->kinDynComp1.getWorldTransform(arbitraryFrameIndex).asHomogeneousTransform());
    Eigen::Vector3d link_pos;
    for(unsigned int i =0; i < 3; i++ ){
    com[i]=world_H_arbitraryFrame(i,3);
    }
    std::cout<<com[0]<<std::endl;
   // com = iDynTree::toEigen(kinDynComp1.getCenterOfMassPosition());
    comPosDes[0]=0;
    comPosDes[1]=0;
    comPosDes[2]=sqrt(com(0)*com(0)+com(1)*com(1)+com(2)*com(2));

    //std::cout<<"COM ---->"<<com(0)<<"  "<<com(1)<<"  "<<com(2)<<std::endl;


    for(int i=0; i<3; i++){

        qPelvis[i] = asin((com(i)-comPosDes(i))/com(i));

    }




}
