#include "bezier.hh"
#include <cmath>
#define LOWER_BODY_N 31
#define N 31

void ControlLowerBody(double s, double Q_Init_UB[], double Pos_sens[], double Vel_sens[], double k, double kr, int indexSt[], int indexSw[],double thpF,
                     double dthpF, double thr, double deltaHipPitch, double deltaHipPitchvy, double Pos_sens_init[], double pSw_init[],
                     double thpF_init, double px0, double P_SWFTinH[], double Vx_SWF, double Or_SWFTinH[], double valsLowerBody[])
{
    //double Q_Init_UB[15] = {0,0.125,0,-0.15,-0.1,-0.065,0,0.35,-0.13,0.065,0.05,0,0.2,-0.13,-0.05};
    const double LEFT_ANKLE_PITCH_TUNE = 0;
    const double LEFT_ANKLE_ROLL_TUNE = 0;
    double deltaSwingAnkleRoll = 0;

    int sgn = -1;
    double y[LOWER_BODY_N];  // desired output to be driven to zero
    double dy[LOWER_BODY_N]; // derivative of the desired output to be driven to zero
    double tau_des[LOWER_BODY_N], Kp[LOWER_BODY_N], Kd[LOWER_BODY_N], I[LOWER_BODY_N];

    double thp_d = 0; // thp IMU desired
    double thytorso_d = 0;
    double thptorso_d = Q_Init_UB[1];
    double thrtorso_d = 0;

    double amp = 0.15;
    double deltaStanceFootPitch = 0*(thpF-thp_d);
    double deltaStanceHipPitch = 1*(thpF-thp_d);
    double deltaStanceHipRoll = 1*(thr);

    bezier bezierSwingHip;
    bezierSwingHip.degree = 5;
    double bSwingHipAlpha[6] = {Pos_sens_init[indexSw[0]],-0.15+amp/2,-0.15-amp/2+3*P_SWFTinH[0],-0.15-amp/2+3*P_SWFTinH[0],-0.15+amp/2,-0.15+amp/2};
    double bSwPxAlpha_d[6] = {pSw_init[0],(pSw_init[0]-deltaHipPitch)/2,-deltaHipPitch,-deltaHipPitch,-deltaHipPitch,-deltaHipPitch}; //Pelvis Pitch desired

    bezier bezierStanceHip;
    bezierStanceHip.degree = 5;
    double bStanceHipAlpha[6] = {Pos_sens_init[indexSt[0]],-0.15+amp/2,-0.15+amp/2,-0.15+amp/2,-0.15+amp/2,-0.15+amp/2};
    double bPelThpAlpha_d[6] = {thpF_init, (thp_d+thpF_init)/2, thp_d, thp_d, thp_d, thp_d};

    for (int i =1; i < 6; i++)
        bStanceHipAlpha[i] = bStanceHipAlpha[i]+deltaStanceHipPitch;

    bezier bezierSwingKnee;
    bezierSwingKnee.degree = 5;
//    double bSwingKneeAlpha[6]= {Pos_sens_init[indexSw[3]],0.30-amp,0.30+amp,0.30+amp+.075,0.30-amp+0.075,0.30-amp+0.075};
    double bSwingKneeAlpha[6]= {Pos_sens_init[indexSw[3]],0.3,0.7,0.5,0.25,0.25};
    if (indexSw[3] == 7)
    {
        for (int i = 1; i < 6; i++)
        {
            bSwingKneeAlpha[i] = 1*bSwingKneeAlpha[i];
        }

    }

    bezier bezierStanceKnee;
    bezierStanceKnee.degree = 5;
    double bStanceKneeAlpha[6] = {Pos_sens_init[indexSt[3]],(Pos_sens_init[indexSt[3]]+0.15)/2,0.30-amp,0.30-amp,0.30-amp,0.30-amp};
 if (indexSt[3] == 7)
    {
        for (int i = 1; i < 6; i++)
        {
            bStanceKneeAlpha[i] = 1*bStanceKneeAlpha[i];
        }

    }
    bezier bezierSwingAnklePitch;
    bezierSwingAnklePitch.degree = 5;
    double bSwingAnklePitchAlpha[6];
    for (int i = 0; i< 6; i++)
        bSwingAnklePitchAlpha[i] = -(bSwingHipAlpha[i]+bSwingKneeAlpha[i]);
    bSwingAnklePitchAlpha[0] = Pos_sens_init[indexSw[4]];

    bezier bezierStanceAnklePitch;
    bezierStanceAnklePitch.degree = 5;
    double bStanceAnklePitchAlpha[6];
    for (int i = 0; i< 6; i++)
        bStanceAnklePitchAlpha[i] = -(bStanceHipAlpha[i]+bStanceKneeAlpha[i]);
    for (int i = 1; i < 6; i++)
        bStanceAnklePitchAlpha[i] = bStanceAnklePitchAlpha[i]+deltaStanceHipPitch;
    bStanceAnklePitchAlpha[0] = Pos_sens_init[indexSt[4]];

//    if (indexSw[4] == 13)
//    {
//        for (int i = 1; i < 6; i++)
//        {
//            bSwingAnklePitchAlpha[i] += LEFT_ANKLE_PITCH_TUNE;
//            deltaSwingAnkleRoll += LEFT_ANKLE_ROLL_TUNE;
//        }
//    }
//    else
//    {
//        for (int i = 1; i < 6; i++)
//        {
//            bStanceAnklePitchAlpha[i] += LEFT_ANKLE_PITCH_TUNE;
//            deltaSwingAnkleRoll += LEFT_ANKLE_ROLL_TUNE;

//        }
//    }

    double StAnkPitch_init = Pos_sens_init[indexSt[4]];
    double desStAnkPitch = -0.0; // more positive--> falls back -.11
    double bStAnkPitchAlpha_d[6] = {StAnkPitch_init,(StAnkPitch_init+desStAnkPitch)/2,desStAnkPitch,desStAnkPitch,desStAnkPitch,desStAnkPitch};

    if (indexSw[3] == 7)
        sgn = -1;
    double a = 1;
    double bTorsoYawAlpha[6] = {Pos_sens_init[0], (sgn*a*P_SWFTinH[0]+Pos_sens_init[0])/2, sgn*a*P_SWFTinH[0],sgn*a*P_SWFTinH[0],\
                                  sgn*a*P_SWFTinH[0], sgn*a*P_SWFTinH[0]};
    double hSwPx = P_SWFTinH[0];
    double hSwPx_d = bezierSwingHip.get_bezier(bSwPxAlpha_d,s);
    double bStanceHip = bezierStanceHip.get_bezier(bStanceHipAlpha,s);
    double hPelThp = thpF;
    double hPelThp_d = bezierStanceHip.get_bezier(bPelThpAlpha_d,s);
    double bSwingKnee = bezierSwingKnee.get_bezier(bSwingKneeAlpha,s);
    double bStanceKnee = bezierStanceKnee.get_bezier(bStanceKneeAlpha,s);
    double bSwingAnklePitch = bezierSwingAnklePitch.get_bezier(bSwingAnklePitchAlpha,s);
    double bStanceAnklePitch = bezierStanceAnklePitch.get_bezier(bStanceAnklePitchAlpha,s);
    double bTorsoYaw = bezierStanceAnklePitch.get_bezier(bTorsoYawAlpha,s);

    double dhSwPx = Vx_SWF;
    double dhSwPx_d = bezierSwingHip.get_derv_bezier(bSwPxAlpha_d,s);
    double bdStanceHip = bezierStanceHip.get_derv_bezier(bStanceHipAlpha,s);
    double dhPelThp = dthpF;
    double dhPelThp_d = bezierStanceHip.get_derv_bezier(bPelThpAlpha_d,s);
    if (s < 0.03)
        dhPelThp_d = dthpF;
    double hStAnkPitch_d = bezierSwingAnklePitch.get_bezier(bStAnkPitchAlpha_d,s);
    double dhStAnkPitch_d = bezierSwingAnklePitch.get_derv_bezier(bStAnkPitchAlpha_d,s);
    double bdSwingKnee = bezierSwingKnee.get_derv_bezier(bSwingKneeAlpha,s);
    double bdStanceKnee = bezierStanceKnee.get_derv_bezier(bStanceKneeAlpha,s);
    double bdSwingAnklePitch = bezierSwingAnklePitch.get_derv_bezier(bSwingAnklePitchAlpha,s);
    double bdStanceAnklePitch = bezierStanceAnklePitch.get_derv_bezier(bStanceAnklePitchAlpha,s);
    double bdTorsoYaw = bezierStanceAnklePitch.get_derv_bezier(bTorsoYawAlpha,s);

//    if (s > 0.05)
//        bStanceAnklePitch = -(Pos_sens[indexSt[0]]+Pos_sens[indexSt[3]]);

    double desftpitch;
    if (P_SWFTinH[0] <= 0)
        desftpitch = 0.0*P_SWFTinH[0];
    else
        desftpitch = -0.0*P_SWFTinH[0];

    if (s > 0.1)
    {
        bSwingAnklePitch = bSwingAnklePitch-2*(Or_SWFTinH[1]-desftpitch);
        deltaSwingAnkleRoll = -2.5*Or_SWFTinH[0];
    }

    //outputs
    y[0] = Pos_sens[0]-bTorsoYaw; // torso yaw
    dy[0] = Vel_sens[0]-bdTorsoYaw;
    y[1] = Pos_sens[1]-thptorso_d;  // torso pitch
    dy[1] = Vel_sens[1]-0;
    y[2] = Pos_sens[2]-thrtorso_d;  // torso roll
    dy[2] = Vel_sens[2]-0;

    // Right Leg
    y[3] = k*(hPelThp_d-hPelThp)+(1-k)*(hSwPx_d-hSwPx);  // Right hip pitch
    dy[3] = k*(dhPelThp_d-dhPelThp)+(1-k)*(dhSwPx_d-(dhSwPx));
    y[5] = k*(Pos_sens[5]-(Q_Init_UB[5]+deltaStanceHipRoll))+(1-k)*(Pos_sens[5]-(Q_Init_UB[5]+deltaStanceHipRoll+deltaHipPitchvy));  // Right hip roll
    dy[5] = k*(Vel_sens[5]-0)+(1-k)*(Vel_sens[5]-0);
    y[6] = Pos_sens[6]-0;  // Right hip yaw
    dy[6] = Vel_sens[6]-0;
    y[7] = k*(Pos_sens[7]-bStanceKnee)+(1-k)*(Pos_sens[7]-bSwingKnee);  // Right Knee
    dy[7] = k*(Vel_sens[7]-bdStanceKnee)+(1-k)*(Vel_sens[7]-bdSwingKnee);
    y[8] = k*(Pos_sens[8]-hStAnkPitch_d)+(1-k)*(Pos_sens[8]-bSwingAnklePitch);  // Foot Pitch to be driven to
    dy[8] = k*(Vel_sens[8]-dhStAnkPitch_d)+(1-k)*(Vel_sens[8]-bdSwingAnklePitch);
    y[9] = k*(Pos_sens[9]+Q_Init_UB[5])+(1-k)*(Pos_sens[9]+Q_Init_UB[5]-0*deltaSwingAnkleRoll);  //  Foot Roll to be driven to
    dy[9] = k*(Vel_sens[9]-0)+(1-k)*(Vel_sens[9]-0);

    // Left Leg
    y[4] = (1-k)*(hPelThp_d-hPelThp)+k*(hSwPx_d-hSwPx);  // Left hip pitch
    dy[4] = (1-k)*(dhPelThp_d-dhPelThp)+k*(dhSwPx_d-(dhSwPx));
    y[10] = (1-k)*(Pos_sens[10]-(Q_Init_UB[10]+deltaStanceHipRoll))+k*(Pos_sens[10]-(Q_Init_UB[10]+deltaStanceHipRoll+deltaHipPitchvy));  // Left hip roll
    dy[10] = (1-k)*(Vel_sens[10]-0)+k*(Vel_sens[10]-0);
    y[11] = Pos_sens[11]-0;  // Left hip yaw
    dy[11] = Vel_sens[11]-0;
    y[12] = (1-k)*(Pos_sens[12]-bStanceKnee)+k*(Pos_sens[12]-bSwingKnee);  // Left Knee
    dy[12] = (1-k)*(Vel_sens[12]-bdStanceKnee)+k*(Vel_sens[12]-bdSwingKnee);
    y[13] = (1-k)*(Pos_sens[13]-hStAnkPitch_d-LEFT_ANKLE_PITCH_TUNE)+k*(Pos_sens[13]-bSwingAnklePitch-LEFT_ANKLE_PITCH_TUNE);  // Foot Pitch to be driven to
    dy[13] = (1-k)*(Vel_sens[13]-dhStAnkPitch_d)+k*(Vel_sens[13]-bdSwingAnklePitch);
    y[14] = (1-k)*(Pos_sens[14]+Q_Init_UB[10])+k*(Pos_sens[14]+Q_Init_UB[10]-deltaSwingAnkleRoll-LEFT_ANKLE_ROLL_TUNE);  //  Foot Roll to be driven to
    dy[14] = (1-k)*(Vel_sens[14]-0)+k*(Vel_sens[14]-0);

    for (int i = 0; i < LOWER_BODY_N; i++)
    {
        Kp[i] = 500;
        Kd[i] = 15;
    }

    Kp[3] = 70+160*pow(kr,3);
    Kd[3] = 15-12.5*pow(kr,3);
    Kp[4] = 70+160*pow(1-kr,3);//180+70*pow(1-kr,3);
    Kd[4] = 15-12.5*pow(1-kr,3);//25-12.5*pow(1-kr,3);


    for (int i = 15; i < N; i++)
    {
        y[i] = Pos_sens[i]-Q_Init_UB[i];
        dy[i] = Vel_sens[i]-0;
    }

//    Kp[9] = 500*pow(1-kr,3);
//    Kd[9] = 15*pow(1-kr,3);
//    Kp[14] = 500*pow(kr,3);//180+70*pow(1-kr,3);
//    Kd[14] = 15*pow(kr,3);//25-12.5*pow(1-kr,3);


//double MAX_VOLTAGE[LOWER_BODY_N];
//    for (int i = 0; i < LOWER_BODY_N; i++)
//    {
//        MAX_VOLTAGE[i] = 12;
//    }

//    for (int i = 0; i < LOWER_BODY_N; i++)
//    {
//        tau_des[i] = -Kp[i]*(y[i])-Kd[i]*(dy[i]);
//        I[i] = (tau_des[i]+0*0.25+0)/(0.055*100);
//        I[i] = I[i]>8 ? 8: (I[i]<-8 ? -8 : I[i]);
//        valsLowerBody[i] = 0.055*100*Vel_sens[i]+2.5*I[i]; // 100 is the grear ratio
//        valsLowerBody[i] = valsLowerBody[i] > MAX_VOLTAGE[i] ? MAX_VOLTAGE[i]: (valsLowerBody[i] < -MAX_VOLTAGE[i] ? -MAX_VOLTAGE[i] : valsLowerBody[i]);
//    }


    //*************
    for (int i = 23; i < N; i++)
        Kd[i] = 0;
    Kd[23] = 10;
    Kp[23] = 100;
    Kp[24] = -50;
    Kp[25] = 50;
    Kp[26] = 100;
    Kd[26] = 10;
    Kp[27] = 50;
    Kp[28] = 50;
    Kp[29] = -50;
    Kp[30] = -100;


    double VelZero[N];
    for (int i = 0; i < N; i++)
    {
        VelZero[i] = 0;
    }
    VelZero[24] = 1;
    VelZero[25] = 1;
    VelZero[27] = 1;
    VelZero[28] = 1;
    VelZero[29] = 1;
    VelZero[30] = 1;

    double MAX_VOLTAGE[N];
    for (int i = 0; i < 15; i++)
    {
        MAX_VOLTAGE[i] = 12;
    }
    for (int i = 15; i < 23; i++)
    {
        MAX_VOLTAGE[i] = 6;
    }
    for (int i = 23; i < N; i++)
    {
        MAX_VOLTAGE[i] = 6;
    }

    double MAX_CURRENT[N];
    for (int i = 0; i < 15; i++)
    {
        MAX_CURRENT[i] = 8;
    }
    for (int i = 15; i < 23; i++)
    {
        MAX_CURRENT[i] = 8;
    }
    for (int i = 23; i < N; i++)
    {
        MAX_CURRENT[i] = 8;
    }

    for (int i = 0; i < N; i++)
    {
        tau_des[i] = -Kp[i]*(y[i])-Kd[i]*(dy[i]);
        I[i] = (tau_des[i]+0*0.25+0)/(0.055*100);
        I[i] = I[i]>MAX_CURRENT[i] ? MAX_CURRENT[i]: (I[i]<-MAX_CURRENT[i] ? -MAX_CURRENT[i] : I[i]);
        valsLowerBody[i] = 0.055*100*(1-VelZero[i])*Vel_sens[i]+2.5*I[i]; // 100 is the grear ratio
        valsLowerBody[i] = valsLowerBody[i]>MAX_VOLTAGE[i] ? MAX_VOLTAGE[i]: (valsLowerBody[i]<-MAX_VOLTAGE[i] ? -MAX_VOLTAGE[i] : valsLowerBody[i]);
    }


}


