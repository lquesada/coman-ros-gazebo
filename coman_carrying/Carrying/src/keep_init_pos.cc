#include <math.h>
#define N 31
#include <iostream>

using namespace std;

void keep_init_pos(double time, double Pos_sens0[], double Qfinal[], double Pos_sens[], double Vel_sens[], double tauDes[], double vals[], int whichComan)
{
    double y[N] = {};  // desired output to be driven to zero
    double dy[N] = {}; // derivative of the desired output to be driven to zero
    double Kp[N] = {}, Kd[N] = {}, I[N] = {};
    double pos_des = 0;
    double vel_des = 0;

    // Gains

    double gainKP = 5; // Gain for the arms and wrists, initialy 5

    Kp[1] = 300;
    Kp[2] = 300;

    Kp[23] = gainKP;
    Kp[24] = gainKP;
    Kp[25] = gainKP;
    Kp[26] = gainKP;
    Kp[27] = gainKP;
    Kp[28] = gainKP;

    for (int i = 0; i < 31; i++)
    {
        Kp[i] = 300;
        Kd[i] = 2;
    }

    // Gains

    for (int i = 0; i < 15; i++)
    {
        Kp[i] = 300;
        Kd[i] = 2;
    }

    for (int i = 15; i < 31; i++)
    {
        Kp[i] = 10;
        Kd[i] = 0;
    }

    // PID

    for (int i = 0; i < N; i++)
    {
        pos_des = Qfinal[i];
        vel_des = 0;
        y[i] = Pos_sens[i] - pos_des;
        dy[i] = Vel_sens[i] - vel_des;
        tauDes[i] = -Kp[i] * (y[i]) - Kd[i] * (dy[i]);
    }
}
