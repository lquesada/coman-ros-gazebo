//#include "HeaderFiles.hh"
#include <sys/time.h>
#include "Control.hh"
#include "Carrying.hh"
#include "SensoryCalc.hh"
//#include "MinimalOpto.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
//#include <ctime>
#include <math.h>
#include <fstream>
#include "oscillate.hh"
#include <iDynTree/Model/FreeFloatingState.h>
// Eigen headers
#include </usr/include/eigen3/Eigen/Dense>
#include "keep_init_pos.hh"
#include <sys/time.h>
#include <chrono>
#define LOWER_BODY_N 15
#define UPPER_BODY_N 16
#ifndef RBDL_BUILD_ADDON_URDFREADER
#error "Error: RBDL addon URDFReader not enabled."
#endif

using namespace Eigen;
using namespace RigidBodyDynamics;
using namespace RigidBodyDynamics::Math;

//! The class of Cmatrix abreviated from Eigen
Eigen::Vector3d com;
using namespace std;
using namespace coman;
EigenRobotState eigRobotStateMain;
EigenRobotState eigRobotStateMainARM;
EigenRobotState eigRobotState2;
EigenRobotState eigRobotState3;
Eigen::VectorXf handRightPos(3);
double fExtMap[3];
double handRightVelFilt;
bool notInitialized;
double tmeOld;
double forceRightHand0[3] = {0};
double forceLeftHand0[3] = {0};
std::string d_LDA = "/home/biorob/Jessica/gitFolder/data/LDA13.txt";
std::string d_FeatLabel = "/home/biorob/Jessica/gitFolder/data/Features9.csv";
std::string d_Classes = "/home/biorob/Jessica/gitFolder/data/Classes.csv";
bool ReadFromForceSensors = true; // it should be false if you don't want to read from force sensors
double vals[N], valsPosture[N];
static double begin_time = -1;
bool onlyUpperBody = false;
double tme;
static double Q0[N];
static std::ofstream outputfile;
const double TIME1 = 1, TIME2 = 2;
const double DISABLED_MOTORS[3] = {29, 30, 39};
double qSens[N], dqSens[N], tauSens[N], qSensAbs[N];
double trans[3][3];
double imuAngRates[3], imuAccelerations[3];
double forceRightAnkle[3], torqueRightAnkle[3], forceLeftAnkle[3], torqueLeftAnkle[3], forceRightHand[3], forceLeftHand[3], torqueRightHand[3], torqueLeftHand[3];
// double kRaw;
const double TORQUE_CORR = -1; // Torque correction from real to webots
double tauDes[N];
double forceSensors[3];
double forceSensorsIntentionDetection[3];
double forceSensors0[3];
double valsLowerBody[LOWER_BODY_N];
static double valsUpperBody[N], valsLevel[N], valsUpperBodyOscillate[N];
double ignoreVar;
double h[N], dh[N], hD[N], dhD[N];
double fExtRight[3];
double fExtLeft[3];
const double TIME2WALK = 10; //10;
static timeval t_global;
static bool startWalkingFlag_0 = false;
static double qknee0 = 0.2, qP0 = -0.1, qR0 = 0.055 * 1;
double qDes[4];
double valsOrient[3];
timeval start2, end2;
Model *modelRBDLright;
Model *modelRBDLleft;
Model *modelRBDLleg;
Model *modelRBDLUB;
Model *modelRBDLUB2;
timeval t_sys_start;
double prova[2];
static double qInit[N];
static double qInitCOMAN1[N] = {0, 0.0, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1, qR0, qR0 * 1, 0, qknee0, qP0 * 1, -qR0 * 1, 0.01, -0.23, 0.015, -0.3, 0.01, 0.17, -0.015, -0.3, 0.06, -0.001, 0.06481, 0.06, -0.001, -0.06481, 0, 0};
static double qInitCOMAN2[N] = {0, 0.175, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1, qR0, qR0 * 1, 0, qknee0, qP0 * 1, -qR0 * 1, 0.2, -0.16, -0.06398, -0.46973, 0.2, 0.2, -0.06398, -0.46973, -0.01725, 0.2, 0.05, 0.01725, 0.2, -0.05, 0, 0};
static double speedCommandAvg = 0;
Control control;
static bool LoadModelFlag = true;
bool stop = false;

#ifdef REAL_ROBOT
DefaultController::DefaultController(coman::Robot &robot)
    : d_robot(robot)
{
}
#endif

UnitValue DefaultController::Period(coman::RobotIface &iface)
{
    return coman::Units::Seconds(0.001);
}

void DefaultController::Initialize(coman::RobotIface &iface)
{
    control.whichComan_ = 1;
    if (control.whichComan_ == 1)
    {
        for (int i = 0; i < N; i++)
        {
            qInit[i] = qInitCOMAN1[i];
        }
    }
    else
    {
        for (int i = 0; i < N; i++)
        {
            qInit[i] = qInitCOMAN2[i];
        }
    }
    control.SetInitPos(qInit);
    tmeOld = 0;
    iface.SetSensorDataRate(Units::MilliSeconds(1));
    iface.SetMaximumVelocityProfile(Joints::ALL, PiecewiseLinear());

    //Upper Body Controller
    notInitialized = true;
    for (int i = 0; i < 3; i++)
    {
        forceRightHand0[i] = 0;
        forceLeftHand0[i] = 0;
    }

    bool ok = mdlLoader.loadModelFromFile(modelFile);    // right arm
    bool ok1 = mdlLoader1.loadModelFromFile(modelFile1); // upper body
    bool ok2 = mdlLoader2.loadModelFromFile(modelFile2); // all robot without forearm
    bool ok3 = mdlLoader3.loadModelFromFile(modelFile3); // all robot
    bool ok4 = kinDynComp.loadRobotModel(mdlLoader1.model());
    bool ok5 = kinDynCompARM.loadRobotModel(mdlLoader.model());
    bool ok6 = kinDynComp2.loadRobotModel(mdlLoader2.model()); // all robot without forearm
    bool ok7 = kinDynComp3.loadRobotModel(mdlLoader3.model()); // all robot
    model = kinDynComp.model();
    modelARM = kinDynCompARM.model();
    model2 = kinDynComp2.model(); // all robot without forearm
    model3 = kinDynComp3.model(); // all robot

    eigRobotStateMain.resize(model.getNrOfDOFs());
    eigRobotStateMain.random();
    eigRobotStateMainARM.resize(modelARM.getNrOfDOFs());
    eigRobotStateMainARM.random();
    eigRobotState2.resize(model2.getNrOfDOFs());
    eigRobotState2.random();
    eigRobotState3.resize(model3.getNrOfDOFs());
    eigRobotState3.random();
    rbdl_check_api_version(RBDL_API_VERSION);
    modelRBDLright = new Model();
    modelRBDLleft = new Model();
    modelRBDLleg = new Model();
    modelRBDLUB = new Model();
    modelRBDLUB2 = new Model();
    if (!Addons::URDFReadFromFile("/home/biorob/Jessica/ModelComanURDF/coman_right_arm_form_IMU.urdf", modelRBDLright, false))
    {
        std::cerr << "Error loading model ./coman_right_arm_form_IMU.urdf" << std::endl;
        abort();
    }

    if (!Addons::URDFReadFromFile("/home/biorob/Jessica/ModelComanURDF/coman_left_arm_form_IMU.urdf", modelRBDLleft, false))
    {
        std::cerr << "Error loading model ./coman.urdf" << std::endl;
        abort();
    }

    if (!Addons::URDFReadFromFile("/home/biorob/Jessica/ModelComanURDF/coman_right_leg.urdf", modelRBDLleg, false))
    {
        std::cerr << "Error loading model ./coman_right_leg.urdf" << std::endl;
        abort();
    }
    if (!Addons::URDFReadFromFile("/home/biorob/Jessica/ModelComanURDF/coman_upper_body.urdf", modelRBDLUB, false))
    {
        std::cerr << "Error loading model ./coman_upper_body.urdf" << std::endl;
        abort();
    }
    if (!Addons::URDFReadFromFile("/home/biorob/Jessica/ModelComanURDF/coman_upper_body.urdf", modelRBDLUB2, false))
    {
        std::cerr << "Error loading model ./coman_upper_body.urdf" << std::endl;
        abort();
    }

    stop = true;

#ifndef REAL_ROBOT
    for (int i = 0; i < N; i++)
    {
        iface.SetReferenceTorque(Joints::Values(i), Units::Newton(0));
    }
    iface.Submit();
#endif
    for (int i = 0; i < coman::Joints::NUM; i++)
    {
        vals[i] = 0;
    }
}

void DefaultController::Loop(coman::RobotIface &iface)
{
    coman::SensorData S = iface.Sensors();
    SensoryCalc sensorCalc(S);
    tme = sensorCalc.GetTime();

    if (begin_time == -1)
    {
        begin_time = tme;
        sensorCalc.Get_q(Q0);
        sensorCalc.ApplyBias_q(Q0);
        gettimeofday(&t_sys_start, NULL);
#ifdef REAL_ROBOT
        outputfile.open("/home/biorob/Jessica/gitFolder/data/LevelingExp/Test2Coman.txt");
#else
        outputfile.open("");
#endif
    }
    tme -= begin_time;
#ifdef REAL_ROBOT
    if (tme < TIME1)
    {
        iface.SetStiffness(Joints::ALL, Units::Newton(0));
        iface.SetDamping(Joints::ALL, Units::Newton(0));
        return;
    }
    if (tme < TIME2)
    {
        for (int i = 0; i < N; i++)
        {
            if (i != DISABLED_MOTORS[0] && i != DISABLED_MOTORS[1] && i != DISABLED_MOTORS[2])
            {

                iface.EnableMotors(Joints::Values(i), true);
            }
        }
        iface.EnableMotors(Joints::Values(29), false);
        iface.EnableMotors(Joints::Values(30), false);
        if (control.whichComan_ == 2)
        {
            iface.EnableMotors(Joints::Values(25), false);
        }
        return;
    }
    tme -= TIME2;
#endif
    //Getting the sensor data for angles and torques
    sensorCalc.Get_all(qSens, dqSens, qSensAbs, tauSens, trans, imuAngRates, imuAccelerations);
    //Calculation of k
    sensorCalc.Get_ankForces(forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand, torqueRightHand, torqueLeftHand);

    if (tme > TIME2WALK && notInitialized)
    {
        for (int i = 0; i < 3; i++)
        {
            forceRightHand0[i] = S.ForceTorqueSensors[Sensors::RIGHT_HAND_FT_SENSOR].Force[i].Convert(Units::Newton).Value;
            forceLeftHand0[i] = S.ForceTorqueSensors[Sensors::LEFT_HAND_FT_SENSOR].Force[i].Convert(Units::Newton).Value;
        }
        notInitialized = false;
    }

    control.UpperBody(valsUpperBody, tme, forceRightHand0, forceLeftHand0, qSens, dqSens, forceRightHand, forceLeftHand, torqueRightHand, torqueLeftHand, Q0, qInit,
                      LoadModelFlag, eigRobotStateMainARM, modelARM, kinDynCompARM, handRightPos, onlyUpperBody);
    double qPelvis[2];
    double qPelvisRBDL[2];
    double comUB[3];
    if (tme < TIME2WALK)
    {
        init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);
        if (tme > TIME2WALK - 2)
        {
            static bool enterFirst = true;
            double tmeRead;
            if (enterFirst)
            {
                //std::cout<<"old: "<<qInit[1]<<"   "<<qInit[2]<<std::endl;
                //std::chrono::_V2::high_resolution_clock::time_point t1Rbdl = std::chrono::_V2::high_resolution_clock::now();
                control.PostureRBDL(qSens, dqSens, modelRBDLUB, modelRBDLUB2, 0.001, 0, qPelvis);

                /*std::chrono::_V2::high_resolution_clock::time_point t1 = std::chrono::_V2::high_resolution_clock::now();
                control.Posture(prova, qSens,dqSens,qPelvis, comUB, eigRobotStateMain, model, kinDynComp, 0.005, 0);
                std::chrono::_V2::high_resolution_clock::time_point t2 = std::chrono::_V2::high_resolution_clock::now();
                auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
                static std::vector<std::chrono::microseconds::rep> avgTimeVec;
                avgTimeVec.push_back(duration);
                double avgTime = std::accumulate(avgTimeVec.begin(), avgTimeVec.end(), 0) / avgTimeVec.size();
                std::cout << "iDyntree time " << avgTime << std::endl;*/
                qInit[2] = -qPelvis[0];
                qInit[1] = qPelvis[1];
                //std::cout<<"new: "<<qPelvis[0]<<"   "<<qPelvis[1]<<std::endl;
                //std::cout<<"newRBDL: "<<qPelvisRBDL[0]<<"   "<<qPelvisRBDL[1]<<std::endl;
                enterFirst = false;
                tmeRead = tme;
            }
            init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);
            init_pos(tme - tmeRead, Q0, qInit, qSens, dqSens, tauDes, valsPosture, control.whichComan_);
            vals[1] = valsPosture[1];
            vals[2] = valsPosture[2];
        }
    }
    else
    {
        //Lower Body Controller
        if (control.whichComan_ == 1)
        {
            control.SetOrientation3(tme, "right", qDes, valsLevel, trans, qSens, dqSens, modelRBDLright);
            qInit[26] = -qDes[0];
            qInit[27] = qDes[1];
            qInit[28] = qDes[2] - 0.06481 * 2;
            qInit[23] = qDes[0];
            qInit[24] = qDes[1];
            qInit[25] = qDes[2];
        }
        else
        {
            control.SetOrientation3(tme, "left", qDes, valsLevel, trans, qSens, dqSens, modelRBDLleft);
            qInit[26] = qDes[0];
            qInit[27] = qDes[1];
            qInit[28] = qDes[2];
            qInit[23] = qDes[0];
            qInit[24] = qDes[1];
            qInit[25] = qDes[2];
        }
        init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);
        if (startWalkingFlag_0 == false)
        {
            gettimeofday(&t_global, NULL);
        }

        static double tme_0 = 0;
        if (t_global.tv_sec % 10 == 0 && startWalkingFlag_0 == false)
        {
            startWalkingFlag_0 = true;
            std::cout << t_global.tv_usec << "   " << t_global.tv_sec << std::endl;
            tme_0 = tme - TIME2WALK;
        }
        tme -= tme_0;

        if (tme > TIME2WALK + 2 && startWalkingFlag_0)
        {
            control.LowerBody(tme, Q0, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,
                              trans, imuAngRates, imuAccelerations, h, dh, hD, dhD, tauDes, vals);
            //init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);
            control.PostureRBDL(qSens, dqSens, modelRBDLUB, modelRBDLUB2, 0.005, 0, qPelvis);

            qInit[2] = -qPelvis[0];
            qInit[1] = qPelvis[1]; //prova a farlo entrare solo una volta*/
            //std::cout<<"new: "<<qInit[1]<<"   "<<qInit[2]<<std::endl;
            vals[19] = valsUpperBody[15];
            vals[15] = valsUpperBody[15];
            control.SaveVars(outputfile, onlyUpperBody);
            ///std::cout<<"pay attention PUT BACK SAVING OF ORIENTATION DATA!!!!!"<<std::endl;
            outputfile << " " << t_sys_start.tv_sec << " " << t_sys_start.tv_usec << std::endl;
        }

        keep_init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, valsLevel, control.whichComan_);
        vals[1] = valsLevel[1];
        vals[2] = valsLevel[2];
        vals[26] = valsLevel[26];
        vals[27] = valsLevel[27];
        vals[28] = valsLevel[28];
        vals[23] = valsLevel[23];
        vals[24] = valsLevel[24];
        vals[25] = valsLevel[25];
    }

    d_robot.SetPidOffset(vals);
    iface.Submit();
}
