
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>

//#include <Eigen/Core>

// iDynTree headers
#include <iDynTree/Model/FreeFloatingState.h>
#include <iDynTree/KinDynComputations.h>
#include <iDynTree/ModelIO/ModelLoader.h>
#include <iDynTree/Model/FreeFloatingState.h>
#include <iDynTree/Model/FreeFloatingMatrices.h>

// Helpers function to convert between
// iDynTree datastructures
#include <iDynTree/Core/EigenHelpers.h>

#include <math.h>












class Classifier
{

public:
    Classifier(int x);
    int test();
    void load(std::string d_LDA, std::string d_FeatLabel , std::string d_Classes);
    void comb(int N, int K);
    std::string Classify(double featVect[], double Time);

    int valorX;
    int type_of_node;


private:
   
    void slidingWindow(double Time);
    Eigen::ArrayXXf getFeatures(double Time);
    Eigen::VectorXf norm_pc(Eigen::ArrayXXf Features);
    void LinearDiscriminant(Eigen::VectorXf Feat_vec_pc);
    bool add_element(int i, int j, double Time);
    Eigen::ArrayXXf zStandarization(Eigen::ArrayXXf Features2);
    int factorial(int n);
    Eigen::VectorXf extra_features();
    void get_means();
    
    //useful variables
    std::vector<std::string> Class;
    std::vector<std::string> v;
    std::vector<double> n_feats;
    std::vector<double> ST;
    Eigen::ArrayXXf LDA;//(3,7);
    Eigen::ArrayXXf pc;//(21,21);
    Eigen::ArrayXXf zStand;//(70,2);   


    // Variables Para el Classifier

    int rowsize;
    int colssize;
    Eigen::ArrayXXf OldData; 
    Eigen::ArrayXXf NewData;
    Eigen::VectorXf Feat; 

    bool length_bool;
    double arm_length;

    int n_classes;
    int n_postPCA_feat;
    int n_features;
    int n_total_features;
    std::vector<std::string> real_feat_label;
    int size_RFL;

    double slidingWinSize;

    Eigen::ArrayXXf Features;

    Eigen::VectorXf extra_feats;
    Eigen::VectorXf mean_values;
    std::string outval;



};
