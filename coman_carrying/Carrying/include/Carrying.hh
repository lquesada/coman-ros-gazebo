#pragma once
#ifdef REAL_ROBOT
#include <comanepfl/robot.hh>
#else
//#include <coman/webots/robotwebots.hh>
#endif

#include <cmath>
#include <iostream>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <iDynTree/Model/FreeFloatingState.h>
#include <iDynTree/KinDynComputations.h>
#include <iDynTree/ModelIO/ModelLoader.h>
#include <iDynTree/Model/FreeFloatingState.h>
#include <iDynTree/Model/FreeFloatingMatrices.h>
#include <iDynTree/Core/EigenHelpers.h>
//Optoforce
#include "MinimalOpto.hpp"

#include <gazebo/gazebo.hh>
#include <gazebo/sensors/sensors.hh>
#include <gazebo/sensors/ImuSensor.hh>
#include <gazebo/sensors/Noise.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ignition/math/Vector3.hh>
#include <MeanFilter.hh>

using namespace std;
using namespace gazebo;
using namespace Eigen;
using namespace RigidBodyDynamics;
using namespace RigidBodyDynamics::Math;

class CarryingPlugin : public gazebo::ModelPlugin
{
public:
  void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/);
  void OnUpdate();
  CarryingPlugin();
  void ReadEnergy();
  void ReadPos();
  void ReadArms();

private:
  //** Loops

  void CarryingLoop();   // Usual carrying scenario in straigth line
  void NavigationLoop(); // Carrying scenario with turns

  //**

  void Initialize();

  //** State reading and writting functions

  void GetJointsInOrder();
  void ReadImu();
  void ReadFT();
  void ReadJS();
  void WriteJC();
  void WriteJP(const double q[31]);
  void initTorques();

  //** Kinematics

  EigenRobotState eigRobotStateMain;
  EigenRobotState eigRobotStateMainARM;
  EigenRobotState eigRobotState2;
  EigenRobotState eigRobotState3;
  Eigen::Vector3d handRightPos;

  RigidBodyDynamics::Model *modelRBDLright;
  RigidBodyDynamics::Model *modelRBDLleft;
  RigidBodyDynamics::Model *modelRBDLleg;
  RigidBodyDynamics::Model *modelRBDLUB;
  RigidBodyDynamics::Model *modelRBDLUB2;

  bool LoadModelFlag = true;

  //* iDynTree Variables

  iDynTree::ModelLoader mdlLoader;
  iDynTree::ModelLoader mdlLoader1;
  iDynTree::ModelLoader mdlLoader2;
  iDynTree::ModelLoader mdlLoader3;
  iDynTree::KinDynComputations kinDynComp;
  iDynTree::KinDynComputations kinDynCompARM;
  iDynTree::KinDynComputations kinDynComp2;
  iDynTree::KinDynComputations kinDynComp3;

  //NB PAY ATTENTION TO THE URDF CONVENTION: CHECK EVERYTIME WHETHER THE SEQUENCE OF JOINTS IN THE URDF IS THE SAME AS IT IS IN YOU REAL ROBOT!!!!
  std::string modelFile = "/home/biorob/ModelComanURDF/coman_right_arm.urdf";   // to be replaced
  std::string modelFile1 = "/home/biorob/ModelComanURDF/coman_upper_body.urdf"; // to be replaced
  std::string modelFile2 = "/home/biorob/ModelComanURDF/iit-coman-no-forearms-CUT/model.urdf";
  std::string modelFile3 = "/home/biorob/ModelComanURDF/coman.urdf";

  iDynTree::Model model;
  iDynTree::Model modelARM;
  iDynTree::Model model2; // all robot without forearm
  iDynTree::Model model3; // all robot

  //**

  bool notInitialized = true;
  double forceRightHand0[3] = {0};
  double forceLeftHand0[3] = {0};

  double vals[N] = {}, valsPosture[N] = {}, valsOscillate[N] = {}, valsUpperBody[N] = {}, valsLevel[N] = {}, valsUpperBodyOscillate[N] = {};
  bool onlyUpperBody = false;

  double Q0[N] = {};
  double qSens[N] = {}, dqSens[N] = {}, tauSens[N] = {}, qSensAbs[N] = {};
  double trans[3][3] = {};
  double imuAngRates[3] = {}, imuAccelerations[3] = {};
  double forceRightAnkle[3] = {}, torqueRightAnkle[3] = {}, forceLeftAnkle[3] = {}, torqueLeftAnkle[3] = {}, forceRightHand[3] = {}, forceLeftHand[3] = {}, torqueRightHand[3] = {}, torqueLeftHand[3] = {};

  double h[N] = {}, dh[N] = {}, hD[N] = {}, dhD[N] = {};
  const double TIME2WALK = 10; //10;
  double qDes[4] = {};

  double cdPR[3][3] = {}, euler[3] = {};

  double tauDes[N] = {}, tauDesUB[N] = {}, tauDesLevel[N] = {}, tauDesPost[N] = {}, tauDesOscillate[N] = {}, tauDesLB[N] = {};

  //** Vector filters

  std::vector<double> vecForceRightHandX;
  std::vector<double> vecForceRightHandY;
  std::vector<double> vecForceRightHandZ;
  std::vector<double> vecForceLeftHandX;
  std::vector<double> vecForceLeftHandY;
  std::vector<double> vecForceLeftHandZ;

  Control control;

  //** Gazebo objects
  //* Pointers to gazebo objects

  gazebo::physics::ModelPtr gazModel;  // The robot himself
  gazebo::physics::ModelPtr gazModel2; // The other robot
  gazebo::physics::WorldPtr gazWorld;  // The world
  gazebo::physics::ModelPtr gazTable;  // The table

  //* Pointer vector to the joints

  std::vector<gazebo::physics::JointPtr> joints;

  //* Sensors

  gazebo::sensors::ImuSensorPtr imuSensor;
  gazebo::sensors::ForceTorqueSensorPtr rightArmSensor;
  gazebo::sensors::ForceTorqueSensorPtr leftArmSensor;

  ignition::math::Vector3d vecForceRightHand;
  ignition::math::Vector3d vecForceLeftHand;
  ignition::math::Vector3d vecTorqueRightHand;
  ignition::math::Vector3d vecTorqueLeftHand;

  gazebo::physics::JointWrench RightAnkleWrench;
  gazebo::physics::JointWrench LeftAnkleWrench;

  ignition::math::Vector3d vecForceRightAnkle;
  ignition::math::Vector3d vecForceLeftAnkle;
  ignition::math::Vector3d vecTorqueRightAnkle;
  ignition::math::Vector3d vecTorqueLeftAnkle;

  imu_data imuData;

  //* Pointer to the update event connection

  gazebo::event::ConnectionPtr updateConnection;

  //** Time

  double prevTime = 0;
  double tme = 0;
  double begin_time = -1;
  double tmeOld = 0;
  double tme_0 = 0;
  double tmeLoop = 0;
  double tmeRead = 0;

  //**

  int comNum = 0;
  bool enterFirst = true;
  bool firstTime = true;

  //** Initial configuration of the robots

  double qknee0 = 0.2, qP0 = -0.1, qR0 = 0.055 * 1;
  const double RIGHT_ELBOW_YAW = 0, LEFT_ELBOW_YAW = 0, HIP_YAW = 0;

  double qInit[N] = {};
  double qInitOld[N] = {};

  double bWaistYaw = 0;
  double bWaistSag = 0;
  double bWaistLat = 0;
  double bhipSag = qP0;
  double bhipLat = -qR0;
  double bhipYaw = 0;
  double bkneeSag = qknee0;
  double bankSag = qP0 * 1;
  double bankLat = qR0;
  double bshSag = 0.01;
  double bshLat = -0.23;
  double bshYaw = 0.015;
  double belbj = -0.3;
  double bforearmPlate = 0.06;
  double bWrj1 = -0.001;
  double bWrj2 = -0.06481;

  double fWaistYaw = 0;
  double fWaistSag = 0.175;
  double fWaistLat = 0;
  double fhipSag = qP0;
  double fhipLat = -qR0;
  double fhipYaw = 0;
  double fkneeSag = qknee0;
  double fankSag = qP0 * 1;
  double fankLat = qR0;
  double fshSag = 0.2;
  double fshLat = -0.2;
  double fshYaw = -0.06398;
  double felbj = -0.46973;
  double fforearmPlate = 0.1725;
  double fWrj1 = 0.2;
  double fWrj2 = 0.05;

  double qInitCOMAN1[N] = {bWaistYaw, bWaistSag, bWaistLat, bhipSag, bhipSag, bhipLat, bhipYaw, bkneeSag, bankSag, bankLat, -bhipLat, bhipYaw, bkneeSag, bankSag, -bankLat, bshSag, bshLat, bshYaw, belbj, bshSag, -bshLat, -bshYaw, belbj, bforearmPlate, bWrj1, bWrj2, -bforearmPlate, bWrj1, -bWrj2, 0, 0};
  double qInitCOMAN2[N] = {fWaistYaw, fWaistSag, fWaistLat, fhipSag, fhipSag, fhipLat, fhipYaw, fkneeSag, fankSag, fankLat, -fhipLat, fhipYaw, fkneeSag, fankSag, -fankLat, fshSag, fshLat, fshYaw, felbj, fshSag, -fshLat, -fshYaw, felbj, fforearmPlate, fWrj1, fWrj2, -fforearmPlate, fWrj1, -fWrj2, 0, 0};

  //** Input parameters

  //* File used to read parameters

  std::ifstream inputParams; // For opening parameter files

  //* Gait parameters

  double dT = 0;
  double dSL = 0;

  //* Hips PID parameters

  double pid_params[8] = {};

  //* Multi threading

  int threadNb = 0;

  //* Navigation

  int activateNav = 0;

  //** Saving output data

  std::ofstream controlOutput;

  //** Energy and arms position computation

  gazebo::physics::LinkPtr rForearmLink;
  gazebo::physics::LinkPtr lForearmLink;
  gazebo::physics::LinkPtr torsoLink;

  ignition::math::Vector3d precRPosition;
  ignition::math::Vector3d precLPosition;

  ignition::math::Vector3d rForearmPos;
  ignition::math::Vector3d lForearmPos;
  ignition::math::Quaterniond rForearmRot;
  ignition::math::Quaterniond lForearmRot;

  ignition::math::Vector3d torsoPos;
  ignition::math::Quaterniond torsoRot;

  double energyR;
  double energyL;

  Math::Vector3d forceRForearm2BaseFrame, forceHandL, forceHandR, forceLForearm2BaseFrame, RForearmBaseFrame, LForearmBaseFrame;
  Math::Matrix3d rotRForearm2Base, rotRForceSens2Base, rotLForceSens2Base, rotLForearm2Base;

  Math::Vector3d fStaticBaseFrame;

  Math::VectorNd Q;

  double qMap[N] = {};

  Eigen::Matrix<double, 3, 3> rotMatrixEigForceSens2Elbow;

  //** Shift

  double xPos;
  double yPos;
  double zPos;

};

// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(CarryingPlugin)
