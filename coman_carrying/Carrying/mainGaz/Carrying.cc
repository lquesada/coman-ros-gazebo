#include <sys/time.h>
#include "Control.hh"
#include "Carrying.hh"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <math.h>
#include <fstream>
#include <iDynTree/Model/FreeFloatingState.h>

// Eigen headers
#include </usr/include/eigen3/Eigen/Dense>
#include "keep_init_pos.hh"
#include "oscillate.hh"
#include <sys/time.h>
#include <chrono>

#include <rbdl/rbdl.h>

#ifndef RBDL_BUILD_ADDON_URDFREADER
#error "Error: RBDL addon URDFReader not enabled."
#endif

#include <rbdl/addons/urdfreader/urdfreader.h>
// #include <fenv.h>

using namespace gazebo;

CarryingPlugin::CarryingPlugin()
{
}

// Load function, called at the spawn of the robot

void CarryingPlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
{
    // Store the pointer to the model

    gazModel = _parent;
    std::string modelName = gazModel->GetName();
    comNum = (modelName == "coman") ? 1 : 2; // Which coman is the plugin running on ? 1 : Back, 2: Front

    // Get simulation time

    gazWorld = gazModel->GetWorld();
    tme = gazWorld->SimTime().Double();

    // Get simulation parameters

    inputParams.open("/home/biorob/Parameters/pid" + std::to_string(comNum) + ".txt", std::ios_base::in);

    for (int i = 0; i < 4; i++)
    {
        inputParams >> pid_params[i];
    }

    inputParams.close();

    inputParams.open("/home/biorob/Parameters/gait" + std::to_string(comNum) + ".txt", std::ios_base::in);
    inputParams >> dT;
    inputParams >> dSL;
    inputParams.close();

    inputParams.open("/home/biorob/Parameters/threadNb.txt", std::ios_base::in);
    inputParams >> threadNb;
    inputParams.close();

    inputParams.open("/home/biorob/Parameters/activateNav.txt", std::ios_base::in);
    inputParams >> activateNav;
    inputParams.close();

    std::cerr << "Params: " << dT << " | " << dSL << std::endl;

    // Initialize ouput data file

    controlOutput.open("/home/biorob/SimulationData/Fresh/Data/COMAN" + std::to_string(comNum) + "/carrying" + std::to_string(threadNb) + ".txt", std::ios_base::out | std::ios_base::trunc);

    // Navigation
    // Gather the other models position in case we want to navigate and turn

    if (activateNav == 1)
    {
        std::string otherModelName = (string)((modelName == "coman") ? "coman1" : "coman");

        std::cerr << "Current model: " << modelName << std::endl;
        std::cerr << "Seeking model: " << otherModelName << std::endl;
        gazModel2 = gazWorld->ModelByName(otherModelName.c_str()); // Getting the second coman model

        gazTable = gazWorld->ModelByName("table");
    }

    // Store the pointers of the joints

    GetJointsInOrder();

    // Sensors

    sensors::Sensor_V sensorVec = sensors::SensorManager::Instance()->GetSensors();

    for (int i = 0; i < sensorVec.size(); i++)
    {
        std::cerr << sensorVec[i]->Name() << std::endl;
    }

    imuSensor = std::dynamic_pointer_cast<gazebo::sensors::ImuSensor>(sensors::SensorManager::Instance()->GetSensor("imu_sensor_" + modelName));
    rightArmSensor = std::dynamic_pointer_cast<gazebo::sensors::ForceTorqueSensor>(sensors::SensorManager::Instance()->GetSensor("right_arm_" + modelName));
    leftArmSensor = std::dynamic_pointer_cast<gazebo::sensors::ForceTorqueSensor>(sensors::SensorManager::Instance()->GetSensor("left_arm_" + modelName));

    // Initialize joints states and torque

    this->Initialize();

    // Energy computation

    rForearmLink = gazModel->GetLink("RForearm");
    lForearmLink = gazModel->GetLink("LForearm");
    torsoLink = gazModel->GetLink("DWYTorso");

    // Listen to the update event. This event is broadcasted every
    // simulation iteration.

    this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&CarryingPlugin::OnUpdate, this));

    //

    std::cerr << "Loading plugin ended" << std::endl;
}

// Called by the world update start event, it is the main loop and is called at every timestep

void CarryingPlugin::OnUpdate()
{
    // Get simulation time

    tme = this->gazWorld->SimTime().Double();

    prevTime = tme;
    control.simuTime = tme;

    // For init_pos

    if (begin_time == -1)
    {
        std::cerr << "FIRST LOOP" << std::endl;

        // Initialize torques to 0

        initTorques();

        // set begin_time

        begin_time = tme;

        // Set QInit to qSens for init_pos

        for (int i = 0; i < N; i++)
        {
            Q0[i] = qInit[i];
            std::cerr << Q0[i] << " ";
        }
        std::cerr << std::endl;
    }

    tme -= begin_time;

    // Gather IMU data

    ReadImu();

    // Gather Force/Torque data

    ReadFT();

    // Gather joints state data

    ReadJS();

    // Energy exchange and arms position/orientation (only informative, we don't use them in computation)

    ReadEnergy();
    ReadArms();
    control.varsOut.energyR_ = this->energyR;
    control.varsOut.energyL_ = this->energyL;

    // Position of the robot in the world (only informative, we don't use them in computation)

    ReadPos();
    control.varsOut.xPos_ = this->xPos;
    control.varsOut.yPos_ = this->yPos;
    control.varsOut.zPos_ = this->zPos;

    // Compute controller, each coman can have a different update loop

    if (comNum == 1)
    {
        if (activateNav == 1)
        {
            NavigationLoop();
        }
        else
        {
            CarryingLoop();
        }
    }
    else
    {
        if (activateNav == 1)
        {
            NavigationLoop();
        }
        else
        {
            CarryingLoop();
        }
    }

    // Send torque commands to joints

    WriteJC();

    // This part stop the simulation when one of the goes back to balance mode
    // Useful when carrying bach of simulations automatically

    if ((control.stopSimulation || tme > 200) && true)
    {
        std::cerr << "Stopping simulation normally" << std::endl;
        gazWorld->Stop();
    }
}

// Controller computation

void CarryingPlugin::CarryingLoop()
{
    // Getting the forces in the initial position

    if (tme >= TIME2WALK && notInitialized)
    {
        for (int i = 0; i < 3; i++)
        {
            forceRightHand0[i] = forceRightHand[i];
            forceLeftHand0[i] = forceLeftHand[i];
        }
        notInitialized = false;
    }

    // Computing the upper body joints (compliance of the shoulders)

    control.UpperBody(tauDesUB, tme, forceRightHand0, forceLeftHand0, qSens, dqSens, forceRightHand, forceLeftHand, torqueRightHand, torqueLeftHand, Q0, qInit,
                      LoadModelFlag, eigRobotStateMainARM, modelARM, kinDynCompARM, onlyUpperBody);

    //

    double qPelvis[2] = {};
    double qPelvisRBDL[2] = {};
    double comUB[3] = {};

    //

    if (tme < TIME2WALK) // When it is not yet the time to walk
    {
        // Putting the robot in its initial pose

        init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);

        //

        if (tme > TIME2WALK - 1) // Just before entering in balance mode
        {
            // Compute the posture of the coman (waist joints)

            control.PostureRBDL_FT(qSens, dqSens, modelRBDLUB, modelRBDLUB2, 0.001, 0, qPelvis, forceRightHand, forceLeftHand, forceLeftHand, forceRightHand0, torqueRightHand, torqueLeftHand, trans);

            // Saving joints angles command from PostureRBDL_FT

            qInit[2] = -qPelvis[0];
            qInit[1] = qPelvis[1];

            // Save time of entering for PID timing

            if (enterFirst)
            {
                enterFirst = false;
                tmeRead = tme;
            }

            // Computing the PID for initial pose and waist posture

            init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);
            init_pos(tme - tmeRead, qInitOld, qInit, qSens, dqSens, tauDesPost, valsPosture, control.whichComan_);

            // Keeping only waist torques for the posture

            tauDes[1] = tauDesPost[1];
            tauDes[2] = tauDesPost[2];
        }
    }
    else // Now using the LowerBody controller, for balance (during 12s) then walking. The walking starts at t = 24s
    {
        // Orientation controller, keeps the hands in the same orientation relative to the IMU

        control.SetOrientation3(tme, "right", qDes, valsLevel, trans, qSens, dqSens, modelRBDLright);
        qInit[23] = qDes[0];
        qInit[24] = qDes[1];
        qInit[25] = qDes[2];

        control.SetOrientation3(tme, "left", qDes, valsLevel, trans, qSens, dqSens, modelRBDLleft);
        qInit[26] = qDes[0];
        qInit[27] = qDes[1];
        qInit[28] = qDes[2];

        // Keeping some of the joints in the initial pose

        init_pos(tme, qInitOld, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);

        if (tme > TIME2WALK + 2)
        {
            // Compute the lower body controller

            control.LowerBody(tme, Q0, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,
                              trans, imuAngRates, imuAccelerations, h, dh, hD, dhD, tauDes, vals, euler);

            // Compute the posture controller

            control.PostureRBDL_FT(qSens, dqSens, modelRBDLUB, modelRBDLUB2, 0.0001, 1, qPelvis, forceRightHand, forceLeftHand, forceRightHand0, forceLeftHand0, torqueRightHand, torqueLeftHand, trans);

            qInit[2] = -qPelvis[0];
            qInit[1] = qPelvis[1];

            tauDes[19] = tauDesUB[15];
            tauDes[15] = tauDesUB[15];

            // Saving data (choose on of the lines)

            // control.SaveVars(controlOutput, onlyUpperBody); // Saving all the data
            control.SaveCustVars(controlOutput); // Saving only one part of the data, see the SaveCustVar function in Control.cc
        }

        // Keeps some of the joints in their initial angle

        keep_init_pos(tme, qInitOld, qInit, qSens, dqSens, tauDesLevel, valsLevel, control.whichComan_);

        tauDes[1] = tauDesLevel[1];
        tauDes[2] = tauDesLevel[2];

        tauDes[26] = tauDesLevel[26];
        tauDes[27] = tauDesLevel[27];
        tauDes[28] = tauDesLevel[28];
        tauDes[23] = tauDesLevel[23];
        tauDes[24] = tauDesLevel[24];
        tauDes[25] = tauDesLevel[25];
    }
}

// Controller computation with navigation variables

void CarryingPlugin::NavigationLoop()
{
    // Getting navigation variables

    if (activateNav == 1)
    {
        if (comNum == 1)
        {
            control.poseComan1 = gazModel->WorldPose();
            control.poseComan2 = gazModel2->WorldPose();
        }
        else
        {
            control.poseComan2 = gazModel->WorldPose();
            control.poseComan1 = gazModel2->WorldPose();
        }

        control.poseTable = gazTable->WorldPose();
    }

    // Getting the forces in the initial position

    if (tme >= TIME2WALK && notInitialized)
    {
        for (int i = 0; i < 3; i++)
        {
            forceRightHand0[i] = forceRightHand[i];
            forceLeftHand0[i] = forceLeftHand[i];
        }
        notInitialized = false;
    }

    // Computing the upper body joints (compliance of the shoulders)

    control.UpperBody(tauDesUB, tme, forceRightHand0, forceLeftHand0, qSens, dqSens, forceRightHand, forceLeftHand, torqueRightHand, torqueLeftHand, Q0, qInit,
                      LoadModelFlag, eigRobotStateMainARM, modelARM, kinDynCompARM, onlyUpperBody);

    //

    double qPelvis[2] = {};
    double qPelvisRBDL[2] = {};
    double comUB[3] = {};

    //

    if (tme < TIME2WALK) // When it is not yet the time to walk
    {
        // Putting the robot in its initial pose

        init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);

        //

        if (tme > TIME2WALK - 1) // Just before entering in balance mode
        {
            // Compute the posture of the coman (waist joints)

            control.PostureRBDL_FT(qSens, dqSens, modelRBDLUB, modelRBDLUB2, 0.001, 0, qPelvis, forceRightHand, forceLeftHand, forceLeftHand, forceRightHand0, torqueRightHand, torqueLeftHand, trans);

            // Saving joints angles command from PostureRBDL_FT

            qInit[2] = -qPelvis[0];
            qInit[1] = qPelvis[1];

            // Save time of entering for PID timing

            if (enterFirst)
            {
                enterFirst = false;
                tmeRead = tme;
            }

            // Computing the PID for initial pose and waist posture

            init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);
            init_pos(tme - tmeRead, qInitOld, qInit, qSens, dqSens, tauDesPost, valsPosture, control.whichComan_);

            // Keeping only waist torques for the posture

            tauDes[1] = tauDesPost[1];
            tauDes[2] = tauDesPost[2];
        }
    }
    else // Now using the LowerBody controller, for balance (during 12s) then walking. The walking starts at t = 24s
    {
        // Orientation controller, keeps the hands in the same orientation relative to the IMU

        control.SetOrientation3(tme, "right", qDes, valsLevel, trans, qSens, dqSens, modelRBDLright);
        // qInit[23] = qDes[0];
        qInit[24] = qDes[1];
        qInit[25] = qDes[2];

        control.SetOrientation3(tme, "left", qDes, valsLevel, trans, qSens, dqSens, modelRBDLleft);
        // qInit[26] = qDes[0];
        qInit[27] = qDes[1];
        qInit[28] = qDes[2];

        // Keeping some of the joints in the initial pose

        init_pos(tme, qInitOld, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);

        if (tme > TIME2WALK + 2)
        {
            // Compute the lower body controller

            control.LowerBody(tme, Q0, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,
                              trans, imuAngRates, imuAccelerations, h, dh, hD, dhD, tauDes, vals, euler);

            // Compute the posture controller

            control.PostureRBDL_FT(qSens, dqSens, modelRBDLUB, modelRBDLUB2, 0.0001, 1, qPelvis, forceRightHand, forceLeftHand, forceRightHand0, forceLeftHand0, torqueRightHand, torqueLeftHand, trans);

            qInit[2] = -qPelvis[0];
            qInit[1] = qPelvis[1];

            tauDes[19] = tauDesUB[15];
            tauDes[15] = tauDesUB[15];

            // Saving data (choose on of the lines)

            // control.SaveVars(controlOutput, onlyUpperBody); // Saving all the data
            control.SaveCustVars(controlOutput); // Saving only one part of the data, see the SaveCustVar function in Control.cc
        }

        // Keeps some of the joints in their initial angle

        keep_init_pos(tme, qInitOld, qInit, qSens, dqSens, tauDesLevel, valsLevel, control.whichComan_);

        tauDes[1] = tauDesLevel[1];
        tauDes[2] = tauDesLevel[2];

        tauDes[26] = tauDesLevel[26];
        tauDes[27] = tauDesLevel[27];
        tauDes[28] = tauDesLevel[28];
        tauDes[23] = tauDesLevel[23];
        tauDes[24] = tauDesLevel[24];
        tauDes[25] = tauDesLevel[25];
    }
}

// Initialization

void CarryingPlugin::Initialize()
{
    std::cerr << "- Start initialization for coman " << comNum << std::endl;

    // Setting the initial pose depending on which coman it is

    control.whichComan_ = 1;

    if (comNum == 1)
    {
        for (int i = 0; i < N; i++)
        {
            qInit[i] = qInitCOMAN1[i];
            qInitOld[i] = qInitCOMAN1[i];
        }
        control.comNum = 1;
    }
    else
    {
        for (int i = 0; i < N; i++)
        {
            qInit[i] = qInitCOMAN2[i];
            qInitOld[i] = qInitCOMAN2[i];
        }
        control.comNum = 2;
    }

    // Sait the gait

    control.T = dT;            // 0.5
    control.STEP_LENGTH = dSL; // 0.025

    // Set the PID for the ankles in the walking controller (WalingController3.cc)

    std::cerr << "PID parameters: ";
    for (int i = 0; i < 4; i++)
    {
        std::cerr << pid_params[i] << " | ";
        control.walkingController3.parameters[i] = pid_params[i];
    }
    std::cerr << std::endl;

    // Give the initial pose to the controller

    tmeOld = 0;
    control.SetInitPos(qInit);

    // Initialize to 0 forces for UpperBody controller (not sure if needed)

    notInitialized = true;
    for (int i = 0; i < 3; i++)
    {
        forceRightHand0[i] = 0;
        forceLeftHand0[i] = 0;
    }

    // Load URDF models used in RBDL

    std::cerr << "-- Loading models" << std::endl;

    bool ok = mdlLoader.loadModelFromFile(modelFile);    // right arm
    bool ok1 = mdlLoader1.loadModelFromFile(modelFile1); // upper body
    bool ok2 = mdlLoader2.loadModelFromFile(modelFile2); // all robot without forearm
    bool ok3 = mdlLoader3.loadModelFromFile(modelFile3); // all robot
    bool ok4 = kinDynComp.loadRobotModel(mdlLoader1.model());
    bool ok5 = kinDynCompARM.loadRobotModel(mdlLoader.model());
    bool ok6 = kinDynComp2.loadRobotModel(mdlLoader2.model()); // all robot without forearm
    bool ok7 = kinDynComp3.loadRobotModel(mdlLoader3.model()); // all robot
    model = kinDynComp.model();
    modelARM = kinDynCompARM.model();
    model2 = kinDynComp2.model(); // all robot without forearm
    model3 = kinDynComp3.model(); // all robot

    eigRobotStateMain.resize(model.getNrOfDOFs());
    eigRobotStateMain.random();
    eigRobotStateMainARM.resize(modelARM.getNrOfDOFs());
    eigRobotStateMainARM.random();
    eigRobotState2.resize(model2.getNrOfDOFs());
    eigRobotState2.random();
    eigRobotState3.resize(model3.getNrOfDOFs());
    eigRobotState3.random();

    rbdl_check_api_version(RBDL_API_VERSION);

    modelRBDLright = new Model();
    modelRBDLleft = new Model();
    modelRBDLleg = new Model();
    modelRBDLUB = new Model();
    modelRBDLUB2 = new Model();

    std::cerr << "-- Reading URDFs" << std::endl;

    std::cerr << "--- File 1 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_right_arm_form_IMU.urdf", modelRBDLright, false))
    {
        std::cerr << "Error loading model ./coman_right_arm_form_IMU.urdf" << std::endl;
        abort();
    }

    std::cerr << "--- File 2 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_left_arm_form_IMU.urdf", modelRBDLleft, false))
    {
        std::cerr << "Error loading model ./coman.urdf" << std::endl;
        abort();
    }

    std::cerr << "--- File 3 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_right_leg.urdf", modelRBDLleg, false))
    {
        std::cerr << "Error loading model ./coman_right_leg.urdf" << std::endl;
        abort();
    }

    std::cerr << "--- File 4 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_upper_body.urdf", modelRBDLUB, false))
    {
        std::cerr << "Error loading model ./coman_upper_body.urdf" << std::endl;
        abort();
    }

    std::cerr << "--- File 5 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_upper_body.urdf", modelRBDLUB2, false))
    {
        std::cerr << "Error loading model ./coman_upper_body.urdf" << std::endl;
        abort();
    }

    // Set initial torques to 0

    initTorques();

    // Initialize varibles used in the arms position/orientation computation with RBDL

    Q = VectorNd::Zero(modelRBDLUB->dof_count);

    fStaticBaseFrame << 0, 0, -1.5 * 9.81;

    rotMatrixEigForceSens2Elbow << -1, 0, 0,
        0, 1, 0,
        0, 0, -1;

    std::cerr << "- End of initialization" << std::endl;
}

// Get the joints in the same order as in the controller
// The function model::GetJoints() doesn't return joints in the same order as in the URDF so the joints have to be sorted again

void CarryingPlugin::GetJointsInOrder()
{
    std::vector<std::string> jointsNames =
        {
            "WaistYaw", "WaistSag", "WaistLat", "RHipSag", "LHipSag", "RHipLat", "RHipYaw", "RKneeSag", "RAnkSag", "RAnkLat", "LHipLat", "LHipYaw", "LKneeSag", "LAnkSag", "LAnkLat", "RShSag", "RShLat", "RShYaw", "RElbj", "LShSag", "LShLat", "LShYaw", "LElbj", "RForearmPlate", "RWrj1", "RWrj2", "LForearmPlate", "LWrj1", "LWrj2", "r_handj", "l_handj"};

    for (int i = 0; i < jointsNames.size(); i++)
    {
        joints.push_back(gazModel->GetJoint(jointsNames[i]));
    }
}

// Read imu's data

void CarryingPlugin::ReadImu()
{
    // Reading from the imu sensor

    ignition::math::Vector3d imuAngRate = imuSensor->AngularVelocity();
    ignition::math::Vector3d imuLinAcc = imuSensor->LinearAcceleration();
    ignition::math::Quaterniond imuOrient = imuSensor->Orientation();

    // Quaternions to euler's angles and trans matrix

    double QW = imuOrient.W();
    double QX = imuOrient.Z();
    double QY = imuOrient.X();
    double QZ = imuOrient.Y();

    trans[0][0] = 2 * ((QX * QX) + (QW * QW)) - 1;
    trans[0][1] = 2 * ((QX * QY) - (QW * QZ));
    trans[0][2] = 2 * ((QX * QZ) + (QW * QY));
    trans[1][0] = 2 * ((QX * QY) + (QW * QZ));
    trans[1][1] = 2 * ((QW * QW) + (QY * QY)) - 1;
    trans[1][2] = 2 * ((QY * QZ) - (QW * QX));
    trans[2][0] = 2 * ((QX * QZ) - (QW * QY));
    trans[2][1] = 2 * ((QY * QZ) + (QW * QX));
    trans[2][2] = 2 * ((QW * QW) + (QZ * QZ)) - 1;

    imuData.get_Orientation(trans, cdPR, euler);

    // Angular acceleration

    imuAngRates[0] = imuAngRate.Z();
    imuAngRates[1] = imuAngRate.X();
    imuAngRates[2] = imuAngRate.Y();

    imuData.get_AngRates(imuAngRates, cdPR, imuAngRates);

    // Linear acceleration
    // This block is most likely wrong
    // The linear acceleration is not used yet, if it were to be you should check wether this computation is good or not

    imuAccelerations[0] = imuLinAcc.Z();
    imuAccelerations[1] = imuLinAcc.X();
    imuAccelerations[2] = imuLinAcc.Y();

    double gravArray[3] = {0, 0, -9.81};

    imuData.get_Accelerations(gravArray, cdPR, gravArray);

    imuData.get_Accelerations(imuAccelerations, cdPR, imuAccelerations);

    imuAccelerations[0] += gravArray[0];
    imuAccelerations[1] += gravArray[1];
    imuAccelerations[2] += gravArray[2];
}

// Function that read and store Force/Torque sensors
// Note: When talking about arms, hands or forearms, it is referring to the same thing

void CarryingPlugin::ReadFT()
{
    //** Ankles

    RightAnkleWrench = gazModel->GetJoint("RAnkSag")->GetForceTorque(0);
    LeftAnkleWrench = gazModel->GetJoint("LAnkSag")->GetForceTorque(0);

    vecForceRightAnkle = RightAnkleWrench.body2Force;
    vecForceLeftAnkle = LeftAnkleWrench.body2Force;

    vecTorqueRightAnkle = RightAnkleWrench.body2Torque;
    vecTorqueLeftAnkle = LeftAnkleWrench.body2Torque;

    //** Arms

    bool useURDFSensors = true; // Use integrated URDF sensors ? (Instead of gazebo's reading of joints efforts)

    if (useURDFSensors)
    {
        vecForceRightHand = rightArmSensor->Force();
        vecForceLeftHand = leftArmSensor->Force();
        vecTorqueRightHand = rightArmSensor->Torque();
        vecTorqueLeftHand = leftArmSensor->Torque();
    }
    else
    {
        gazebo::physics::JointWrench RightForearmWrench = gazModel->GetJoint("RForearmPlate")->GetForceTorque(0);
        gazebo::physics::JointWrench LeftForearmWrench = gazModel->GetJoint("LForearmPlate")->GetForceTorque(0);
        vecForceRightHand = RightForearmWrench.body2Force;
        vecForceLeftHand = LeftForearmWrench.body2Force;
        vecTorqueRightHand = RightForearmWrench.body2Torque;
        vecTorqueLeftHand = LeftForearmWrench.body2Torque;
    }

    //** Store the force/torque informations

    forceRightAnkle[0] = vecForceRightAnkle.X();
    forceRightAnkle[1] = vecForceRightAnkle.Y();
    forceRightAnkle[2] = vecForceRightAnkle.Z();

    forceLeftAnkle[0] = vecForceLeftAnkle.X();
    forceLeftAnkle[1] = vecForceLeftAnkle.Y();
    forceLeftAnkle[2] = vecForceLeftAnkle.Z();

    forceRightHand[0] = vecForceRightHand.X();
    forceRightHand[1] = vecForceRightHand.Y();
    forceRightHand[2] = vecForceRightHand.Z();

    forceLeftHand[0] = vecForceLeftHand.X();
    forceLeftHand[1] = vecForceLeftHand.Y();
    forceLeftHand[2] = vecForceLeftHand.Z();

    torqueRightAnkle[0] = vecTorqueRightAnkle.X();
    torqueRightAnkle[1] = vecTorqueRightAnkle.Y();
    torqueRightAnkle[2] = vecTorqueRightAnkle.Z();

    torqueLeftAnkle[0] = vecTorqueLeftAnkle.X();
    torqueLeftAnkle[1] = vecTorqueLeftAnkle.Y();
    torqueLeftAnkle[2] = vecTorqueLeftAnkle.Z();

    torqueRightHand[0] = vecTorqueRightHand.X();
    torqueRightHand[1] = vecTorqueRightHand.Y();
    torqueRightHand[2] = vecTorqueRightHand.Z();

    torqueLeftHand[0] = vecTorqueLeftHand.X();
    torqueLeftHand[1] = vecTorqueLeftHand.Y();
    torqueLeftHand[2] = vecTorqueLeftHand.Z();

    // Filtering the hand forces

    int filterWindow = 10;

    MeanFilter(vecForceRightHandX, forceRightHand[0], forceRightHand[0], filterWindow);
    MeanFilter(vecForceRightHandY, forceRightHand[1], forceRightHand[1], filterWindow);
    MeanFilter(vecForceRightHandZ, forceRightHand[2], forceRightHand[2], filterWindow);
    MeanFilter(vecForceLeftHandX, forceLeftHand[0], forceLeftHand[0], filterWindow);
    MeanFilter(vecForceLeftHandY, forceLeftHand[1], forceLeftHand[1], filterWindow);
    MeanFilter(vecForceLeftHandZ, forceLeftHand[2], forceLeftHand[2], filterWindow);
}

// Read and store joints states

void CarryingPlugin::ReadJS()
{
    for (int i = 0; i < joints.size(); i++)
    {
        qSens[i] = joints[i]->Position();
        dqSens[i] = joints[i]->GetVelocity(0);
    }
}

// Write joints commands

void CarryingPlugin::WriteJC()
{
    for (int i = 0; i < joints.size() - 2; i++)
    {
        joints[i]->SetForce(0, tauDes[i]);
    }
}

// Initialise torques to zero

void CarryingPlugin::initTorques()
{
    for (int i = 0; i < N; i++)
    {
        tauDes[i] = 0.0;
    }
}

// Set to a joint position
// This is a simulation only feature that instantly set a joint angle to a value
// Not actually used

void CarryingPlugin::WriteJP(const double q[31])
{
    for (int i = 0; i < joints.size(); i++)
    {
        joints[i]->SetPosition(0, q[i]);
    }
}

// Read/Compute the energy exchange of the hands/forearm during their interaction
// This function uses gazebo API in order to get positions and rotations

void CarryingPlugin::ReadEnergy()
{
    // Read the torsoLink position and rotation

    torsoPos = torsoLink->WorldInertialPose().Pos();
    torsoRot = torsoLink->WorldInertialPose().Rot();

    // Rotates vectors to have them all in the same frame

    rForearmPos = torsoRot.RotateVectorReverse(torsoPos - rForearmLink->WorldInertialPose().Pos());
    rForearmRot = rForearmLink->WorldInertialPose().Rot();

    lForearmPos = torsoRot.RotateVectorReverse(torsoPos - lForearmLink->WorldInertialPose().Pos());
    lForearmRot = lForearmLink->WorldInertialPose().Rot();

    // Compute the enery

    energyR = (precRPosition - rForearmPos).Dot((rForearmRot - torsoRot).RotateVector(vecForceRightHand));
    energyL = (precLPosition - lForearmPos).Dot((lForearmRot - torsoRot).RotateVector(vecForceLeftHand));

    // Record the current arms positions

    precRPosition = rForearmPos;
    precLPosition = lForearmPos;
}

// Read arms position and rotations
// This function uses RBDL to deduce hands positions for the joints angle values

void CarryingPlugin::ReadArms()
{
    // Initialise the angle map

    qMap[0] = -qSens[2];  // lateral
    qMap[1] = qSens[1];   //pitch saggittal
    qMap[2] = qSens[0];   //yaw
    qMap[3] = qSens[19];  //7
    qMap[4] = qSens[20];  //8
    qMap[5] = qSens[21];  //9
    qMap[6] = qSens[22];  //10
    qMap[7] = qSens[26];  //11
    qMap[8] = qSens[27];  //12
    qMap[9] = qSens[28];  //13
    qMap[10] = qSens[15]; //3
    qMap[11] = qSens[16]; //4
    qMap[12] = qSens[17]; //5
    qMap[13] = qSens[18]; //6
    qMap[14] = qSens[23];
    qMap[15] = qSens[24];
    qMap[16] = qSens[25];

    for (int i = 0; i < modelRBDLUB->dof_count; i++)
    {
        Q(i) = qMap[i];
    }

    for (int i = 0; i < 3; i++)
    {
        forceHandR(i) = vecForceRightHand[i];
        forceHandL(i) = vecForceLeftHand[i];
    }

    // Rotating vectors to have them in the same frame

    rotRForearm2Base = CalcBodyWorldOrientation(*modelRBDLUB, Q, modelRBDLUB->GetBodyId("RForearm"), true);
    rotLForearm2Base = CalcBodyWorldOrientation(*modelRBDLUB, Q, modelRBDLUB->GetBodyId("LForearm"), true);
    RForearmBaseFrame = CalcBodyToBaseCoordinates(*modelRBDLUB, Q, modelRBDLUB->GetBodyId("RForearm"), Math::Vector3d(0, 0, 0), true);
    LForearmBaseFrame = CalcBodyToBaseCoordinates(*modelRBDLUB, Q, modelRBDLUB->GetBodyId("LForearm"), Math::Vector3d(0, 0, 0), true);

    rotRForceSens2Base = rotRForearm2Base; // * rotMatrixEigForceSens2Elbow;
    rotLForceSens2Base = rotLForearm2Base; // * rotMatrixEigForceSens2Elbow;

    forceRForearm2BaseFrame = rotRForceSens2Base * (forceHandR)-fStaticBaseFrame;
    forceLForearm2BaseFrame = rotLForceSens2Base * (forceHandL)-fStaticBaseFrame;

    // Saving the arms positions

    control.varsOut.posRArm_ = RForearmBaseFrame;
    control.varsOut.posLArm_ = LForearmBaseFrame;
    control.varsOut.forceRArm_ = forceRForearm2BaseFrame;
    control.varsOut.forceLArm_ = forceLForearm2BaseFrame;
}

// Reads the position of the robot in the world frame

void CarryingPlugin::ReadPos()
{
    auto modelPose = gazModel->WorldPose().Pos();
    xPos = modelPose.X();
    yPos = modelPose.Y();
    zPos = modelPose.Z();
}