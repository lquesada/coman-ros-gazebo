The program have been developped under Ubuntu 16.04 LTS to run on Gazebo 9

# Installing dependencies for Ubuntu 16.04 LTS
(Installation is possible on Ubuntu 18, see below)

## Installing ROS kinetic and Gazebo 9

```bash

sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'

sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

wget http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -

sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116

sudo apt-get update

sudo apt-get install ros-kinetic-gazebo9-ros-pkgs

```
Sourcing ROS apps:

```
echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
source ~/.bashrc
```

## Installing the rigidbody dynamics library (RBDL)

- Take the zip [rbdl.zip](ressources/library/rbdl.zip) that is located in the *ressources/library* folder
- Unzip it wherever you want and open a terminal in the *rbdl-source* folder
- Type the following:
```bash
mkdir -p build && cd build
cmake -DCMAKE_BUILD_TYPE=Release -DRBDL_BUILD_ADDON_URDFREADER=ON -DRBDL_USE_ROS_URDF_LIBRARY=ON .. && make && sudo make install
```
***Note:*** *it is important to type the last command in its entirety, otherwise RBDL could experience some bugs*

## Installing the iDynTree library

This library needs depandancies that you may not have yet:

```bash
sudo apt-get install libeigen3-dev libxml2-dev coinor-libipopt-dev qtbase5-dev qtdeclarative5-dev qtmultimedia5-dev qml-module-qtquick2 qml-module-qtquick-window2 qml-module-qtmultimedia qml-module-qtquick-dialogs qml-module-qtquick-controls qml-module-qt-labs-folderlistmodel qml-module-qt-labs-settings
```

Then install the library:

```bash
cd ~/
git clone https://github.com/robotology/idyntree
cd idyntree
mkdir build && cd build
cmake ..
make
sudo make install
```

# Addendum for installation on Ubuntu 18
(Thanks Alessandro for the heads up!)

## ROS/Gazebo

The package name is not **ros-kinetic-gazebo9-ros-pkgs** but **ros-melodic-gazebo-ros-pkgs**

## RBDL/IDynTree

- It is necessary to install **libeigen3-dev** before trying to build those libraries.
- It is needed to patch *rbdl-source/addons/urdfreader/urdfreader.cc* by replacing **boost::shared_ptr** by **std::shared_ptr**

# Copying the models files in the right location

## Model files

You have to make sure that your models files are in the location expected by the program.

You can take the each model folders found into *ressources/model* and copy them into *~/.gazebo/models/*

Make sure that each path is correct in the **spawning.cc** (pathComan and pathTable) file when it is used.

## URDF for RBDL

You also have to indicated the location of some *.urdf* files in *Carrying.cc*  
These files can be found under *ressources/ModelComanURDF*  
You can put them wherever you want but make sure to put the correct path inside the *Carrying.cc* file.  
(You should also make sure that all the permissions are granted for the programm to acces them)

# Installing the program

During this step, it will ask for gitlab credentials in order to clone the repository

```bash
mkdir -p ~/catkin_ws/src
cd catkin_ws/src/
git https://gitlab.com/lquesada/coman-ros-gazebo.git
cd ~/catkin_ws/
catkin_make
```

You need to register your computer's ssh key on gitlab and join the project before being able to clone the repository or push to it.
If you just want to test the program, download the repository from the GitLab website instead of cloning it.

# Launching the program

In the *ressources/world* folder you can see several .xml files. Each file correspond to a program:

- The scenario file spawn the scenario contained into the scenario_spawning package, you can edit the spawning.cc file in order to make changes.
- The tool file allow you to visually tweak joints positions in order to obtain a wanted pose, then save the positions.

To launch a scenario, use the following on a new terminal:
- First source the workspace (needed only once per terminal)
```bash
source ~/catkin_ws/devel/setup.bash
```
- Then launch the world file with gazebo
```bash
gazebo ~/catkin_ws/src/coman-ros-gazebo/ressources/world/automation_carrying.xml -u --verbose --seed 1
```
You can also do it with two terminals, one for the server, one for the GUI:
```bash
gzserver ~/catkin_ws/src/coman-ros-gazebo/ressources/world/file.xml -u --verbose
```
(Open new terminal)
```bash
gzclient
```

# Creating a world file

When launching a simulation with gazebo, you have to provide it with a world file. It is a xml file that contains the description of the models and environment.  
  
In order to create such a file, simply launch gazebo, insert the models you want to be in and place them correctly in the environment.  
Then save the world file to a know location (File -> Save world as) as a .xml
If you want to add control to the models and the world, another manipulation has to be done.  
  
First open the world file with your favorite code editor.  
Then, under the openning world balise `<world name"...">`, paste the following code:
```
<plugin name='spawning' filename='libspawning_auto.so'/>
```
Likewise, search for the models you want to control in the file, and under the openning model balise `<model name="...">`, paste the following code:
```
<plugin name='carrying_plugin_coman' filename='libcarrying_plugin.so'/>
```
This will load the world and models plugin inside the simulation when you run it.

# Description of differents packages

There are a few packages in the repository, here is a little description for each one of them:

- auto_rep: Package that allows to replicate several time the actual setup in order to repeat experiments
- auto_grid: Package that allows to do several simulation on a grid of parameters (Caution: When doing a lot of simulation, take care of what data your are recording, you may fill up your hard drive)
- auto_scenario_spawning: Same as scenario_spawning but relies on a pre existing world file with models in it. Used to automatise experiements
- scenario_spawning: Package that create a World Plugin for gazebo, and that is used to spawn models according to a wanted scenario.
- coman_carrying: Package that contain the controller and the main file for the Control Plugin needed in the carrying experiment
- coman_walking: Package that contains the controller and the main file for the Contorl Plugin needed on the single coman walking experiment.
- coman_description: Package that only contain a model files and material/meshes and allows to have a ROS linkage to the files in the case one library needs it (it seems that it is the case with RBDL).
- tuning: Package that has been used to tune different gains with online Gauss-Newton methode

# Description of scenarios

The world files are located in ressources/world  
Look also in the decription of parameters while reading this section

## Usual COMAN-COMAN carrying
**World file:** automation_carrying.xml  
It is the common scenario, two coman, one table, straight walk, nothing fancy

## Perturbation with an object
**World file:** automation_carrying_object.xml  
Scenario where an object is added on top of the table  
To enable this scenario, edit the typeMass parameter file:
- **0**: Disable
- **1**: The mass increases linearly from initMass to newMass, starting at t=45s
- **2**: The mass starts at initMass, then jump to newMass at t=45s   
- **3**: The mass stays constant with a value of initMass

## Perturbation with a force
**World file:** automation_carrying.xml  
Scenario where a force is applied on the table at t=45s  
In order to enable this scenario you shoudl set a forceSpan to above 0s  
Then don't forget to set the force amplitude in the parameter file

# Description of parameters

Each simulation parameter is stored in a different .txt file. In the git, these files are located in ressources/Parameters. It should be kept in mind that when a file specifies several parameters (like force.txt), they should be written on different lines.  
Here is a description of what the parameters are about:  

- **activateNav**: 0 (false) or 1 (true)  
    Will enable or disable navigation system in the simulation. The navigation system orders comans to follow a given trajectory by changing their yaw angle.  
    In the current state, the front coman is imposed a trajectory and the back coman tries to follow it with a PD set on the table yaw.  
    The navigation code is located in the beginning of `WalkingController3::EvalOutputs`, see WalkingController3.cc

- **yawNavGain**: (Kp, Kd) Any value  
    Set the PD values inside the navigation code that makes the back coman follow the table yaw

- **coupled**: 0 (false) or 1 (true)
    Information to the spawning file if the coman are coupled or not.  
    This will enable or disable the joint between the hands and the handles.  

- **damping | stiffness | rotDamping | rotStiffness**: Any positive value  
    Value of the parameters used in the custom spring-damper joint between the hands and the handles.  
    damping and stiffness is for translationnal  
    rotDamping and rotStiffness is for rotationnal  
    Changing these will allow you to alter the coupling between robots.

- **force**: (Fx, Fy, Fz) Any value in newtons
    Used in the perturbation scenario where a force is applied on the table
    Correspond to the force vector that will be applied on the table durin the scenario

- **forceSpan**: Any positive value in seconds  
    Duration of the application of the force applied in the perturbation scenario  

- **gait1 | gait2**: (Walking period in seconds | Step length in meters), all positive
    Gait parameters for each coman (1 being the back one, and 2 the front one) during the simulation.  
    Changing theses will allow you to perform asynchronisation trials

- **phaseLag**: Double inside [0, 1]  
    Set the initial phaseLag of comans, 0 is close to trot, 1 is close to pace

- **pid1 | pid2**: Any positive value
    Gains of the hips PID inside `WalkingController3::EvalTorques`  
    Look at the line where Kp3 and Kp4 are computed inside this method, ex: `Kp[3] = parameters[0] + parameters[1] * pow(kR, 3);`  
    On this example, `parameters[i]` will be ith parameters set in the pid file.  
    Each coman have its own set of parameters!  

- **initMass**: Any positive non-zero value in kg  
    During perturbation trials with an object on the table, set the initial mass of the object

- **newMass**: Any positive non-zero value in kg  
    During perturbation trials with an object on the table, set the the final mass of the object

- **typeMass**: Integer inside [0, 3]  
    During perturbation scenario, choose what type of mass scenario is used (see Description of scenarios)

- **iters**: Positive non-zero integer
    Set the number of maximum iterations used in the physics engine solver

- **sor**: Double inside [0, 2]  
    Set the relaxation coefficient of the succesive over-relaxation solver for the physics engine  
    Can have an effect on repeatablity

- **threadNb**: Positive integer
    Thread number of the simulation. It is used for the parallel computation.  
    This isn't a parameter you will have to tweak, it is autmatic.

# Executing parallel simulations



# Acknowledgement

This branch of the project is under progress by the collaborating team working at the BioRobotics Lab at the Swiss Federal Institute of Technology, Lausanne (Ecole Polytechnique Fédérale de Lausanne).