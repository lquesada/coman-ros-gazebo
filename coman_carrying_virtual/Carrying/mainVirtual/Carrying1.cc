#include <sys/time.h>
#include "Control.hh"
#include "Carrying.hh"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <math.h>
#include <fstream>
#include <iDynTree/Model/FreeFloatingState.h>

// Eigen headers
#include </usr/include/eigen3/Eigen/Dense>
#include "keep_init_pos.hh"
#include "oscillate.hh"
#include "virtualSensoryCal.h"
#include <sys/time.h>
#include <chrono>

#include <rbdl/rbdl.h>

#ifndef RBDL_BUILD_ADDON_URDFREADER
#error "Error: RBDL addon URDFReader not enabled."
#endif

#include <rbdl/addons/urdfreader/urdfreader.h>

VirtualController::VirtualController()
{
}

// Loading simulation parameters

void VirtualController::Load()
{
    // ###### Getting simulation parameters

    inputPID.open("/home/biorob/Parameters/pid" + std::to_string(comNum) + ".txt", std::ios_base::in);

    for (int i = 0; i < 4; i++)
    {
        inputPID >> pid_params[i];
        std::cout << pid_params[i];;
    }

    inputPID.close();

    inputParams.open("/home/biorob/Parameters/gait" + std::to_string(comNum) + ".txt", std::ios_base::in);
    inputParams >> dT;
    inputParams >> dSL;
    inputParams.close();

    inputParams.open("/home/biorob/Parameters/threadNb.txt", std::ios_base::in);
    inputParams >> threadNb;
    inputParams.close();

    std::cout << "Params: " << dT << " | " << dSL << std::endl;

}

// Controller computation

void VirtualController::Controller()
{
    virtualSensoryCal(inputfile, tme, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand, torqueRightHand, torqueLeftHand, trans, imuAngRates, imuAccelerations, ignoreVar);
    
    if (tme >= TIME2WALK && notInitialized)
    {
        for (int i = 0; i < 3; i++)
        {
            forceRightHand0[i] = forceRightHand[i];
            forceLeftHand0[i] = forceLeftHand[i];
        }
        notInitialized = false;
    }
    std::cout << "Force 0 right hands: " << forceRightHand0[0] << " | " << forceRightHand0[1] << " | " << forceRightHand0[2] << std::endl;
    std::cout << "Force 0 left hands: " << forceLeftHand0[0] << " | " << forceLeftHand0[1] << " | " << forceLeftHand0[2] << std::endl;

    control.UpperBody(tauDesUB, tme, forceRightHand0, forceLeftHand0, qSens, dqSens, forceRightHand, forceLeftHand, torqueRightHand, torqueLeftHand, Q0, qInit,
                      LoadModelFlag, eigRobotStateMainARM, modelARM, kinDynCompARM, onlyUpperBody);

    double qPelvis[2] = {};
    double qPelvisRBDL[2] = {};
    double comUB[3] = {};

    std::cout << "SECOND CONDITON" << std::endl;
    control.SetOrientation3(tme, "right", qDes, valsLevel, trans, qSens, dqSens, modelRBDLright);
    qInit[23] = qDes[0];
    qInit[24] = qDes[1];
    qInit[25] = qDes[2];

    control.SetOrientation3(tme, "left", qDes, valsLevel, trans, qSens, dqSens, modelRBDLleft);
    qInit[26] = qDes[0];
    qInit[27] = qDes[1];
    qInit[28] = qDes[2];

    std::cout << "Initpos + setorientation: " << comNum << std::endl;

    init_pos(tme, qInitOld, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);

    if (tme > TIME2WALK + 2)
    {
        std::cout << "Lowerbody: " << comNum << std::endl;
        control.LowerBody(tme, Q0, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,
                            trans, imuAngRates, imuAccelerations, h, dh, hD, dhD, tauDes, vals, euler);

        control.PostureRBDL_FT(qSens, dqSens, modelRBDLUB, modelRBDLUB2, 0.0001, 1, qPelvis, forceRightHand, forceLeftHand, forceRightHand0, forceLeftHand0, torqueRightHand, torqueLeftHand, trans);

        qInit[2] = -qPelvis[0];
        qInit[1] = qPelvis[1];

        tauDes[19] = tauDesUB[15];
        tauDes[15] = tauDesUB[15];

        control.SaveVars(controlOutput, onlyUpperBody);
    }

    keep_init_pos(tme, qInitOld, qInit, qSens, dqSens, tauDesLevel, valsLevel, control.whichComan_);

    tauDes[1] = tauDesLevel[1];
    tauDes[2] = tauDesLevel[2];

    tauDes[26] = tauDesLevel[26];
    tauDes[27] = tauDesLevel[27];
    tauDes[28] = tauDesLevel[28];
    tauDes[23] = tauDesLevel[23];
    tauDes[24] = tauDesLevel[24];
    tauDes[25] = tauDesLevel[25];    
}

// Initialization

void VirtualController::Initialize()
{
    //

    std::cout << "- Initializing" << std::endl;

    // Setting the initial pose depending on whichcoman

    control.whichComan_ = 2;

    if (comNum == 1)
    {
        for (int i = 0; i < N; i++)
        {
            qInit[i] = qInitCOMAN1[i];
            qInitOld[i] = qInitCOMAN1[i];
        }
        control.comNum = 1;
    }
    else
    {
        for (int i = 0; i < N; i++)
        {
            qInit[i] = qInitCOMAN2[i];
            qInitOld[i] = qInitCOMAN2[i];
        }
        control.comNum = 2;
    }

    // Gait

    control.T = dT;            // 0.5
    control.STEP_LENGTH = dSL; // 0.025

    // PID

    std::cout << "PID parameters: ";
    for (int i = 0; i < 4; i++)
    {
        std::cout << pid_params[i] << " | ";
        control.walkingController3.parameters[i] = pid_params[i];
    }
    std::cout << std::endl;

    //

    tmeOld = 0;
    control.SetInitPos(qInit);

    // Initialize forces for UpperBody controller

    notInitialized = true;
    for (int i = 0; i < 3; i++)
    {
        forceRightHand0[i] = 0;
        forceLeftHand0[i] = 0;
    }

    // Load models

    std::cout << "-- Loading models" << std::endl;

    bool ok = mdlLoader.loadModelFromFile(modelFile);    // right arm
    bool ok1 = mdlLoader1.loadModelFromFile(modelFile1); // upper body
    bool ok2 = mdlLoader2.loadModelFromFile(modelFile2); // all robot without forearm
    bool ok3 = mdlLoader3.loadModelFromFile(modelFile3); // all robot
    bool ok4 = kinDynComp.loadRobotModel(mdlLoader1.model());
    bool ok5 = kinDynCompARM.loadRobotModel(mdlLoader.model());
    bool ok6 = kinDynComp2.loadRobotModel(mdlLoader2.model()); // all robot without forearm
    bool ok7 = kinDynComp3.loadRobotModel(mdlLoader3.model()); // all robot
    model = kinDynComp.model();
    modelARM = kinDynCompARM.model();
    model2 = kinDynComp2.model(); // all robot without forearm
    model3 = kinDynComp3.model(); // all robot

    eigRobotStateMain.resize(model.getNrOfDOFs());
    eigRobotStateMain.random();
    eigRobotStateMainARM.resize(modelARM.getNrOfDOFs());
    eigRobotStateMainARM.random();
    eigRobotState2.resize(model2.getNrOfDOFs());
    eigRobotState2.random();
    eigRobotState3.resize(model3.getNrOfDOFs());
    eigRobotState3.random();

    rbdl_check_api_version(RBDL_API_VERSION);

    modelRBDLright = new Model();
    modelRBDLleft = new Model();
    modelRBDLleg = new Model();
    modelRBDLUB = new Model();
    modelRBDLUB2 = new Model();

    std::cout << "-- Reading URDFs" << std::endl;

    std::cout << "--- File 1 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_right_arm_form_IMU.urdf", modelRBDLright, false))
    {
        std::cout << "Error loading model ./coman_right_arm_form_IMU.urdf" << std::endl;
        abort();
    }

    std::cout << "--- File 2 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_left_arm_form_IMU.urdf", modelRBDLleft, false))
    {
        std::cout << "Error loading model ./coman.urdf" << std::endl;
        abort();
    }

    std::cout << "--- File 3 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_right_leg.urdf", modelRBDLleg, false))
    {
        std::cout << "Error loading model ./coman_right_leg.urdf" << std::endl;
        abort();
    }

    std::cout << "--- File 4 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_upper_body.urdf", modelRBDLUB, false))
    {
        std::cout << "Error loading model ./coman_upper_body.urdf" << std::endl;
        abort();
    }

    std::cout << "--- File 5 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_upper_body.urdf", modelRBDLUB2, false))
    {
        std::cout << "Error loading model ./coman_upper_body.urdf" << std::endl;
        abort();
    }
    
    virtualSensoryCal(inputfile, tme, qSens, qSensAbs, dqSens, 
    tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, 
    torqueLeftAnkle, forceRightHand, forceLeftHand, torqueRightHand, 
    torqueLeftHand, trans, imuAngRates, imuAccelerations, ignoreVar);
    std::cout << "- Initializing ended" << std::endl;
}

// Main

int main(int argc, char *argv[])
{
    double nSample = 0;
    int fileNumber = 3;
    int outFileNumber = 0;

    if(argc > 1)
    {
        outFileNumber = strtol(argv[1], NULL, 10);
        std::cout << "Argument: " << outFileNumber << std::endl;
    }

    // Creating a virtual controller

    VirtualController *virt;
    virt = new VirtualController();

    virt->Load();

    // Counting number of steps

    virt->inputfile.open("/home/biorob/SimulationData/VirtualInput/DATA/COMAN2/carrying" + std::to_string(fileNumber) + ".txt");
    string line;

    while (getline(virt->inputfile, line))
    {
        ++nSample;
    }
    virt->inputfile.close();

    std::cout << "nSample "<<nSample << std::endl;

    // Input processing

    virt->inputfile.open("/home/biorob/SimulationData/VirtualInput/DATA/COMAN2/carrying" + std::to_string(0) + ".txt"); // Inputs
    virt->controlOutput.open("/home/biorob/SimulationData/VirtualOutput/DATA/COMAN2/carrying" + std::to_string(fileNumber) + ".txt", std::ios_base::out | std::ios_base::trunc); //Outputs

    // Start of the virtual loop

    for (int j = 0; j < nSample; j++){
        if (j == 0)
        {
            virt->Initialize();
        }
        else
        {            
            virt->Controller();
        }
    }

    virt->inputfile.close();
    virt->controlOutput.close();

    std::cout << "####### DONE #########" << std::endl;
}