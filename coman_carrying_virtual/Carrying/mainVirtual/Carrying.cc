#include <sys/time.h>
#include "Control.hh"
#include "Carrying.hh"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <math.h>
#include <fstream>
#include <iDynTree/Model/FreeFloatingState.h>

// Eigen headers
#include </usr/include/eigen3/Eigen/Dense>
#include "keep_init_pos.hh"
#include "oscillate.hh"
#include "virtualSensoryCal.h"
#include <sys/time.h>
#include <chrono>

#include <rbdl/rbdl.h>

#ifndef RBDL_BUILD_ADDON_URDFREADER
#error "Error: RBDL addon URDFReader not enabled."
#endif

#include <rbdl/addons/urdfreader/urdfreader.h>
// #include <fenv.h>

using namespace gazebo;

CarryingPlugin::CarryingPlugin()
{
}

// Load function, called at the spawn of the robot

void CarryingPlugin::Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
{
    //
    // feenableexcept(FE_INVALID | FE_DIVBYZERO);
    std::cerr << "Loading Carrying plugin 1" << std::endl;

    // Store the pointer to the model

    gazModel = _parent;
    std::string modelName = gazModel->GetName();
    comNum = (modelName == "coman") ? 1 : 2; // 1 : 2

    std::cerr << "Name: " << gazModel->GetName() << std::endl;

    // Get simulation time

    gazWorld = gazModel->GetWorld();

    tme = gazWorld->SimTime().Double();

    // ##### Get simulation parameters

    outputData.open("/home/biorob/SimulationData/tuningData" + std::to_string(comNum) + ".txt", std::ios_base::out | std::ios_base::trunc);
    inputPID.open("/home/biorob/Parameters/pid" + std::to_string(comNum) + ".txt", std::ios_base::in);

    for (int i = 0; i < 4; i++)
    {
        inputPID >> pid_params[i];
    }

    inputPID.close();

    inputParams.open("/home/biorob/Parameters/gait" + std::to_string(comNum) + ".txt", std::ios_base::in);
    inputParams >> dT;
    inputParams >> dSL;
    inputParams.close();

    inputParams.open("/home/biorob/Parameters/threadNb.txt", std::ios_base::in);
    inputParams >> threadNb;
    inputParams.close();

    std::cerr << "Params: " << dT << " | " << dSL << std::endl;

    int inputNb = 3;

    controlOutput.open("/home/biorob/SimulationData/Fresh/COMAN" + std::to_string(comNum) + "/carrying" + std::to_string(threadNb) + ".txt", std::ios_base::out | std::ios_base::trunc);
    inputfile.open("/home/biorob/SimulationData/VirtualInput/DATA/COMAN2/carrying" + std::to_string(inputNb) + ".txt");

    // Store the pointers of the joints

    GetJointsInOrder();

    // Sensors

    sensors::Sensor_V sensorVec = sensors::SensorManager::Instance()->GetSensors();

    for (int i = 0; i < sensorVec.size(); i++)
    {
        std::cerr << sensorVec[i]->Name() << std::endl;
    }

    imuSensor = std::dynamic_pointer_cast<gazebo::sensors::ImuSensor>(sensors::SensorManager::Instance()->GetSensor("imu_sensor_" + modelName));
    rightArmSensor = std::dynamic_pointer_cast<gazebo::sensors::ForceTorqueSensor>(sensors::SensorManager::Instance()->GetSensor("right_arm_" + modelName));
    leftArmSensor = std::dynamic_pointer_cast<gazebo::sensors::ForceTorqueSensor>(sensors::SensorManager::Instance()->GetSensor("left_arm_" + modelName));

    // Initialize joints states and torque

    this->Initialize();

    // Energy computation

    rForearmLink = gazModel->GetLink("RForearm");
    lForearmLink = gazModel->GetLink("LForearm");
    torsoLink = gazModel->GetLink("DWYTorso");

    // Listen to the update event. This event is broadcasted every
    // simulation iteration.

    this->updateConnection = event::Events::ConnectWorldUpdateBegin(std::bind(&CarryingPlugin::OnUpdate, this)); // Time update

    std::cerr << "Loading plugin ended" << std::endl;
}

// Called by the world update start event, it is the main loop

void CarryingPlugin::OnUpdate()
{

    // Get simulation time and compute timestep

    // std::cerr << "---------------- Begin Loop ------------------" << std::endl;

    prevTime = tme;
    control.simuTime = tme;

    // For init_pos

    if (begin_time == -1)
    {
        std::cerr << "FIRST LOOP" << std::endl;

        // Initialize tauDes to 0
        initTorques();

        // set begin_time
        begin_time = tme;

        // Set QInit to qSens for init_pos
        for (int i = 0; i < N; i++)
        {
            Q0[i] = qInit[i]; // TODO: Change qSens to vector
            std::cerr << Q0[i] << " ";
        }
        std::cerr << std::endl;
    }

    tme -= begin_time;
    std::cerr << "Time: " << tme << std::endl;

    // Compute controller

    if (comNum == 1)
    {
        Loop3();
    }
    else
    {
        Loop3();
    }

    controlOutput << tme << " ";
    for (int i = 0; i < N; i++)
        {
            controlOutput << " " << tauDes[i];
        }
    controlOutput << std::endl;

    // Record

    std::cerr << "Output" << std::endl;

    std::cerr << "Stopping? " << control.stopSimulation << std::endl;

    
    if (tme > 120 && true)
    {
        gazWorld->Stop();
    }
}

// Controller computation

void CarryingPlugin::Loop1()
{
    if (tme >= TIME2WALK && notInitialized)
    {
        for (int i = 0; i < 3; i++)
        {
            forceRightHand0[i] = forceRightHand[i];
            forceLeftHand0[i] = forceLeftHand[i];
        }
        notInitialized = false;
    }
    std::cerr << "Force 0 right hands: " << forceRightHand0[0] << " | " << forceRightHand0[1] << " | " << forceRightHand0[2] << std::endl;
    std::cerr << "Force 0 left hands: " << forceLeftHand0[0] << " | " << forceLeftHand0[1] << " | " << forceLeftHand0[2] << std::endl;

    control.UpperBody(tauDesUB, tme, forceRightHand0, forceLeftHand0, qSens, dqSens, forceRightHand, forceLeftHand, torqueRightHand, torqueLeftHand, Q0, qInit,
                      LoadModelFlag, eigRobotStateMainARM, modelARM, kinDynCompARM, onlyUpperBody);

    double qPelvis[2] = {};
    double qPelvisRBDL[2] = {};
    double comUB[3] = {};
    if (tme < TIME2WALK)
    {
        std::cerr << "FIRST CONDITON" << std::endl;
        std::cerr << "Initpos: " << comNum << std::endl;
        init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);
        if (tme > TIME2WALK - 1)
        {
            std::cerr << "Initpos + posture: " << comNum << std::endl;
            control.PostureRBDL_FT(qSens, dqSens, modelRBDLUB, modelRBDLUB2, 0.001, 0, qPelvis, forceRightHand, forceLeftHand, forceLeftHand, forceRightHand0, torqueRightHand, torqueLeftHand, trans);

            qInit[2] = -qPelvis[0];
            qInit[1] = qPelvis[1];

            if (enterFirst)
            {
                enterFirst = false;
                tmeRead = tme;
            }

            init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);
            init_pos(tme - tmeRead, qInitOld, qInit, qSens, dqSens, tauDesPost, valsPosture, control.whichComan_);

            tauDes[1] = tauDesPost[1];
            tauDes[2] = tauDesPost[2];
        }
    }
    else
    {
        std::cerr << "SECOND CONDITON" << std::endl;
        control.SetOrientation3(tme, "right", qDes, valsLevel, trans, qSens, dqSens, modelRBDLright);
        qInit[23] = qDes[0];
        qInit[24] = qDes[1];
        qInit[25] = qDes[2];

        control.SetOrientation3(tme, "left", qDes, valsLevel, trans, qSens, dqSens, modelRBDLleft);
        qInit[26] = qDes[0];
        qInit[27] = qDes[1];
        qInit[28] = qDes[2];

        std::cerr << "Initpos + setorientation: " << comNum << std::endl;

        init_pos(tme, qInitOld, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);

        if (tme > TIME2WALK + 2)
        {
            std::cerr << "Lowerbody: " << comNum << std::endl;
            control.LowerBody(tme, Q0, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,
                              trans, imuAngRates, imuAccelerations, h, dh, hD, dhD, tauDes, vals, euler);

            control.PostureRBDL_FT(qSens, dqSens, modelRBDLUB, modelRBDLUB2, 0.0001, 1, qPelvis, forceRightHand, forceLeftHand, forceRightHand0, forceLeftHand0, torqueRightHand, torqueLeftHand, trans);

            qInit[2] = -qPelvis[0];
            qInit[1] = qPelvis[1];

            tauDes[19] = tauDesUB[15];
            tauDes[15] = tauDesUB[15];

            // control.SaveVars(controlOutput, onlyUpperBody);
            // control.SaveCustVars(controlOutput);
        }

        keep_init_pos(tme, qInitOld, qInit, qSens, dqSens, tauDesLevel, valsLevel, control.whichComan_);

        tauDes[1] = tauDesLevel[1];
        tauDes[2] = tauDesLevel[2];

        tauDes[26] = tauDesLevel[26];
        tauDes[27] = tauDesLevel[27];
        tauDes[28] = tauDesLevel[28];
        tauDes[23] = tauDesLevel[23];
        tauDes[24] = tauDesLevel[24];
        tauDes[25] = tauDesLevel[25];
    }
}

void CarryingPlugin::Loop2() // Loop for coman 2
{
    double qPelvis[2] = {};

    double qPelvisRBDL[2] = {};
    double comUB[3] = {};
    control.PostureRBDL_FT(qSens, dqSens, modelRBDLUB, modelRBDLUB2, 0.001, 0, qPelvis, forceRightHand, forceLeftHand, forceLeftHand, forceRightHand0, torqueRightHand, torqueLeftHand, trans);

    if (tme >= TIME2WALK && notInitialized)
    {
        for (int i = 0; i < 3; i++)
        {
            forceRightHand0[i] = forceRightHand[i];
            forceLeftHand0[i] = forceLeftHand[i];
        }
        notInitialized = false;
    }

    control.UpperBody(tauDesUB, tme, forceRightHand0, forceLeftHand0, qSens, dqSens, forceRightHand, forceLeftHand, torqueRightHand, torqueLeftHand, Q0, qInit,
                      LoadModelFlag, eigRobotStateMainARM, modelARM, kinDynCompARM, onlyUpperBody);
    if (tme < TIME2WALK)
    {
        init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);
    }
    else
    {
        //Lower Body Controller

        std::cerr << "SECOND CONDITON" << std::endl;
        control.SetOrientation3(tme, "right", qDes, valsLevel, trans, qSens, dqSens, modelRBDLright);
        qInit[23] = qDes[0];
        qInit[24] = qDes[1];
        qInit[25] = qDes[2];

        control.SetOrientation3(tme, "left", qDes, valsLevel, trans, qSens, dqSens, modelRBDLleft);
        qInit[26] = qDes[0];
        qInit[27] = qDes[1];
        qInit[28] = qDes[2];

        init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);

        if (tme > TIME2WALK + 2)
        {
            oscillate(tme, Q0, qInit, qSens, dqSens, tauDesOscillate, valsOscillate, control.whichComan_);
            control.LowerBody(tme, Q0, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,
                              trans, imuAngRates, imuAccelerations, h, dh, hD, dhD, tauDes, vals, euler);

            std::cerr << "I WAAS HERE 1" << std::endl;
            init_pos(tme, Q0, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);
            // tauDes[19] = tauDesOscillate[15];
            // tauDes[15] = tauDesOscillate[15];
            tauDes[0] = tauDesOscillate[0];

            std::cerr << "I WAAS HERE 2" << std::endl;
            control.SaveVars(controlOutput, onlyUpperBody);
            std::cerr << "I WAAS HERE 3" << std::endl;
        }

        keep_init_pos(tme, Q0, qInit, qSens, dqSens, tauDesLevel, valsLevel, control.whichComan_);
        std::cerr << "I WAAS HERE 4" << std::endl;
        tauDes[26] = tauDesLevel[26];
        tauDes[27] = tauDesLevel[27];
        tauDes[28] = tauDesLevel[28];
        tauDes[23] = tauDesLevel[23];
        tauDes[24] = tauDesLevel[24];
        tauDes[25] = tauDesLevel[25];
    }
}

void CarryingPlugin::Loop3()
{
    virtualSensoryCal(inputfile, tme, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand, torqueRightHand, torqueLeftHand, trans, imuAngRates, imuAccelerations, ignoreVar);

    if (tme >= TIME2WALK && notInitialized)
    {
        for (int i = 0; i < 3; i++)
        {
            forceRightHand0[i] = forceRightHand[i];
            forceLeftHand0[i] = forceLeftHand[i];
        }
        notInitialized = false;
    }
    std::cout << "Force 0 right hands: " << forceRightHand0[0] << " | " << forceRightHand0[1] << " | " << forceRightHand0[2] << std::endl;
    std::cout << "Force 0 left hands: " << forceLeftHand0[0] << " | " << forceLeftHand0[1] << " | " << forceLeftHand0[2] << std::endl;

    control.UpperBody(tauDesUB, tme, forceRightHand0, forceLeftHand0, qSens, dqSens, forceRightHand, forceLeftHand, torqueRightHand, torqueLeftHand, Q0, qInit,
                      LoadModelFlag, eigRobotStateMainARM, modelARM, kinDynCompARM, onlyUpperBody);

    double qPelvis[2] = {};
    double qPelvisRBDL[2] = {};
    double comUB[3] = {};

    std::cout << "SECOND CONDITON" << std::endl;
    control.SetOrientation3(tme, "right", qDes, valsLevel, trans, qSens, dqSens, modelRBDLright);
    qInit[23] = qDes[0];
    qInit[24] = qDes[1];
    qInit[25] = qDes[2];

    control.SetOrientation3(tme, "left", qDes, valsLevel, trans, qSens, dqSens, modelRBDLleft);
    qInit[26] = qDes[0];
    qInit[27] = qDes[1];
    qInit[28] = qDes[2];

    std::cout << "Initpos + setorientation: " << comNum << std::endl;

    init_pos(tme, qInitOld, qInit, qSens, dqSens, tauDes, vals, control.whichComan_);

    if (tme > TIME2WALK + 2)
    {
        std::cout << "Lowerbody: " << comNum << std::endl;
        control.LowerBody(tme, Q0, qSens, qSensAbs, dqSens, tauSens, forceRightAnkle, forceLeftAnkle, torqueRightAnkle, torqueLeftAnkle, forceRightHand, forceLeftHand,
                          trans, imuAngRates, imuAccelerations, h, dh, hD, dhD, tauDes, vals, euler);

        control.PostureRBDL_FT(qSens, dqSens, modelRBDLUB, modelRBDLUB2, 0.0001, 1, qPelvis, forceRightHand, forceLeftHand, forceRightHand0, forceLeftHand0, torqueRightHand, torqueLeftHand, trans);

        qInit[2] = -qPelvis[0];
        qInit[1] = qPelvis[1];

        tauDes[19] = tauDesUB[15];
        tauDes[15] = tauDesUB[15];

        // control.SaveCustVars(controlOutput);
    }

    keep_init_pos(tme, qInitOld, qInit, qSens, dqSens, tauDesLevel, valsLevel, control.whichComan_);

    tauDes[1] = tauDesLevel[1];
    tauDes[2] = tauDesLevel[2];

    tauDes[26] = tauDesLevel[26];
    tauDes[27] = tauDesLevel[27];
    tauDes[28] = tauDesLevel[28];
    tauDes[23] = tauDesLevel[23];
    tauDes[24] = tauDesLevel[24];
    tauDes[25] = tauDesLevel[25];
}

// Initialization

void CarryingPlugin::Initialize()
{
    //

    std::cerr << "- Initializing" << std::endl;

    // Setting the initial pose depending on whichcoman

    control.whichComan_ = 1;

    if (comNum == 1)
    {
        for (int i = 0; i < N; i++)
        {
            qInit[i] = qInitCOMAN1[i];
            qInitOld[i] = qInitCOMAN1[i];
        }
        control.comNum = 1;
    }
    else
    {
        for (int i = 0; i < N; i++)
        {
            qInit[i] = qInitCOMAN2[i];
            qInitOld[i] = qInitCOMAN2[i];
        }
        control.comNum = 2;
    }

    // Gait

    control.T = dT;            // 0.5
    control.STEP_LENGTH = dSL; // 0.025

    // PID

    std::cerr << "PID parameters: ";
    for (int i = 0; i < 4; i++)
    {
        std::cerr << pid_params[i] << " | ";
        control.walkingController3.parameters[i] = pid_params[i];
    }
    std::cerr << std::endl;

    //

    tmeOld = 0;
    control.SetInitPos(qInit);

    // Initialize forces for UpperBody controller

    notInitialized = true;
    for (int i = 0; i < 3; i++)
    {
        forceRightHand0[i] = 0;
        forceLeftHand0[i] = 0;
    }

    // Load models

    std::cerr << "-- Loading models" << std::endl;

    bool ok = mdlLoader.loadModelFromFile(modelFile);    // right arm
    bool ok1 = mdlLoader1.loadModelFromFile(modelFile1); // upper body
    bool ok2 = mdlLoader2.loadModelFromFile(modelFile2); // all robot without forearm
    bool ok3 = mdlLoader3.loadModelFromFile(modelFile3); // all robot
    bool ok4 = kinDynComp.loadRobotModel(mdlLoader1.model());
    bool ok5 = kinDynCompARM.loadRobotModel(mdlLoader.model());
    bool ok6 = kinDynComp2.loadRobotModel(mdlLoader2.model()); // all robot without forearm
    bool ok7 = kinDynComp3.loadRobotModel(mdlLoader3.model()); // all robot
    model = kinDynComp.model();
    modelARM = kinDynCompARM.model();
    model2 = kinDynComp2.model(); // all robot without forearm
    model3 = kinDynComp3.model(); // all robot

    eigRobotStateMain.resize(model.getNrOfDOFs());
    eigRobotStateMain.random();
    eigRobotStateMainARM.resize(modelARM.getNrOfDOFs());
    eigRobotStateMainARM.random();
    eigRobotState2.resize(model2.getNrOfDOFs());
    eigRobotState2.random();
    eigRobotState3.resize(model3.getNrOfDOFs());
    eigRobotState3.random();

    rbdl_check_api_version(RBDL_API_VERSION);

    modelRBDLright = new Model();
    modelRBDLleft = new Model();
    modelRBDLleg = new Model();
    modelRBDLUB = new Model();
    modelRBDLUB2 = new Model();

    std::cerr << "-- Reading URDFs" << std::endl;

    std::cerr << "--- File 1 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_right_arm_form_IMU.urdf", modelRBDLright, false))
    {
        std::cerr << "Error loading model ./coman_right_arm_form_IMU.urdf" << std::endl;
        abort();
    }

    std::cerr << "--- File 2 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_left_arm_form_IMU.urdf", modelRBDLleft, false))
    {
        std::cerr << "Error loading model ./coman.urdf" << std::endl;
        abort();
    }

    std::cerr << "--- File 3 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_right_leg.urdf", modelRBDLleg, false))
    {
        std::cerr << "Error loading model ./coman_right_leg.urdf" << std::endl;
        abort();
    }

    std::cerr << "--- File 4 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_upper_body.urdf", modelRBDLUB, false))
    {
        std::cerr << "Error loading model ./coman_upper_body.urdf" << std::endl;
        abort();
    }

    std::cerr << "--- File 5 " << std::endl;

    if (!Addons::URDFReadFromFile("/home/biorob/ModelComanURDF/coman_upper_body.urdf", modelRBDLUB2, false))
    {
        std::cerr << "Error loading model ./coman_upper_body.urdf" << std::endl;
        abort();
    }

    // Set torques to 0

    initTorques();

    std::cerr << "- Initializing ended" << std::endl;
}

// Get the joints in the same order as in the controller
// The function model::GetJoints() doesn't return joints in the same order as in the urdf

void CarryingPlugin::GetJointsInOrder()
{
    std::vector<std::string> jointsNames =
        {
            "WaistYaw", "WaistSag", "WaistLat", "RHipSag", "LHipSag", "RHipLat", "RHipYaw", "RKneeSag", "RAnkSag", "RAnkLat", "LHipLat", "LHipYaw", "LKneeSag", "LAnkSag", "LAnkLat", "RShSag", "RShLat", "RShYaw", "RElbj", "LShSag", "LShLat", "LShYaw", "LElbj", "RForearmPlate", "RWrj1", "RWrj2", "LForearmPlate", "LWrj1", "LWrj2", "r_handj", "l_handj"};

    for (int i = 0; i < jointsNames.size(); i++)
    {
        joints.push_back(gazModel->GetJoint(jointsNames[i]));
    }
}

// Read imu's data

void CarryingPlugin::ReadImu()
{
    // Reading from the imu sensor

    ignition::math::Vector3d imuAngRate = imuSensor->AngularVelocity();
    ignition::math::Vector3d imuLinAcc = imuSensor->LinearAcceleration();
    ignition::math::Quaterniond imuOrient = imuSensor->Orientation();

    // Quaternions to euler's angles and trans matrix

    double QW = imuOrient.W();
    double QX = imuOrient.Z();
    double QY = imuOrient.X();
    double QZ = imuOrient.Y();

    trans[0][0] = 2 * ((QX * QX) + (QW * QW)) - 1;
    trans[0][1] = 2 * ((QX * QY) - (QW * QZ));
    trans[0][2] = 2 * ((QX * QZ) + (QW * QY));
    trans[1][0] = 2 * ((QX * QY) + (QW * QZ));
    trans[1][1] = 2 * ((QW * QW) + (QY * QY)) - 1;
    trans[1][2] = 2 * ((QY * QZ) - (QW * QX));
    trans[2][0] = 2 * ((QX * QZ) - (QW * QY));
    trans[2][1] = 2 * ((QY * QZ) + (QW * QX));
    trans[2][2] = 2 * ((QW * QW) + (QZ * QZ)) - 1;

    imuData.get_Orientation(trans, cdPR, euler);

    // Angular acceleration

    imuAngRates[0] = imuAngRate.Z();
    imuAngRates[1] = imuAngRate.X();
    imuAngRates[2] = imuAngRate.Y();

    imuData.get_AngRates(imuAngRates, cdPR, imuAngRates);

    // Linear acceleration (TODO: Not useful for now, but must check for later)

    imuAccelerations[0] = imuLinAcc.Z();
    imuAccelerations[1] = imuLinAcc.X();
    imuAccelerations[2] = imuLinAcc.Y();

    double gravArray[3] = {0, 0, -9.81};

    imuData.get_Accelerations(gravArray, cdPR, gravArray);

    imuData.get_Accelerations(imuAccelerations, cdPR, imuAccelerations);

    imuAccelerations[0] += gravArray[0];
    imuAccelerations[1] += gravArray[1];
    imuAccelerations[2] += gravArray[2];
}

// Function that read and store Force/Torque sensors

void CarryingPlugin::ReadFT()
{
    // Ankles

    RightAnkleWrench = gazModel->GetJoint("RAnkSag")->GetForceTorque(0);
    LeftAnkleWrench = gazModel->GetJoint("LAnkSag")->GetForceTorque(0);

    vecForceRightAnkle = RightAnkleWrench.body2Force;
    vecForceLeftAnkle = LeftAnkleWrench.body2Force;

    vecTorqueRightAnkle = RightAnkleWrench.body2Torque;
    vecTorqueLeftAnkle = LeftAnkleWrench.body2Torque;

    // Arms

    bool useSensors = true; // Use integrated SDF sensors ? (Instead of gazebo's reading of joints efforts)

    if (!useSensors)
    {
        gazebo::physics::JointWrench RightForearmWrench = gazModel->GetJoint("RForearmPlate")->GetForceTorque(0);
        gazebo::physics::JointWrench LeftForearmWrench = gazModel->GetJoint("LForearmPlate")->GetForceTorque(0);
        vecForceRightHand = RightForearmWrench.body2Force;
        vecForceLeftHand = LeftForearmWrench.body2Force;
        vecTorqueRightHand = RightForearmWrench.body2Torque;
        vecTorqueLeftHand = LeftForearmWrench.body2Torque;
    }
    else
    {
        vecForceRightHand = rightArmSensor->Force();
        vecForceLeftHand = leftArmSensor->Force();
        vecTorqueRightHand = rightArmSensor->Torque();
        vecTorqueLeftHand = leftArmSensor->Torque();
    }

    //
    bool startPace = true;
    double factor = 1;

    if (true) // Without noise if true
    {

        if (tme < 24.5 && false) // Changing starting foot
        {
            forceRightAnkle[0] = vecForceRightAnkle.X();
            forceRightAnkle[1] = vecForceRightAnkle.Y();
            forceRightAnkle[2] = vecForceRightAnkle.Z() + ((startPace) ? -50 : ((comNum == 1) ? -50 : 0));

            forceLeftAnkle[0] = vecForceLeftAnkle.X();
            forceLeftAnkle[1] = vecForceLeftAnkle.Y();
            forceLeftAnkle[2] = vecForceLeftAnkle.Z() + ((startPace) ? 0 : ((comNum == 1) ? 0 : -50));
        }
        else
        {
            forceRightAnkle[0] = vecForceRightAnkle.X();
            forceRightAnkle[1] = vecForceRightAnkle.Y();
            forceRightAnkle[2] = vecForceRightAnkle.Z();

            forceLeftAnkle[0] = vecForceLeftAnkle.X();
            forceLeftAnkle[1] = vecForceLeftAnkle.Y();
            forceLeftAnkle[2] = vecForceLeftAnkle.Z();
        }

        forceRightHand[0] = vecForceRightHand.X() - forceROffset[0];
        forceRightHand[1] = vecForceRightHand.Y() - forceROffset[1];
        forceRightHand[2] = vecForceRightHand.Z() - forceROffset[2];

        forceLeftHand[0] = vecForceLeftHand.X() - forceLOffset[0];
        forceLeftHand[1] = vecForceLeftHand.Y() - forceLOffset[1];
        forceLeftHand[2] = vecForceLeftHand.Z() - forceLOffset[2];

        torqueRightAnkle[0] = vecTorqueRightAnkle.X();
        torqueRightAnkle[1] = vecTorqueRightAnkle.Y();
        torqueRightAnkle[2] = vecTorqueRightAnkle.Z();

        torqueLeftAnkle[0] = vecTorqueLeftAnkle.X();
        torqueLeftAnkle[1] = vecTorqueLeftAnkle.Y();
        torqueLeftAnkle[2] = vecTorqueLeftAnkle.Z();

        torqueRightHand[0] = vecTorqueRightHand.X();
        torqueRightHand[1] = vecTorqueRightHand.Y();
        torqueRightHand[2] = vecTorqueRightHand.Z();

        torqueLeftHand[0] = vecTorqueLeftHand.X();
        torqueLeftHand[1] = vecTorqueLeftHand.Y();
        torqueLeftHand[2] = vecTorqueLeftHand.Z();
    }
    else
    {
        if (tme < 24.2 && false) // Changing starting foot
        {
            forceRightAnkle[0] = vecForceRightAnkle.X() + dist(generator);
            forceRightAnkle[1] = vecForceRightAnkle.Y() + dist(generator);
            forceRightAnkle[2] = vecForceRightAnkle.Z() + dist(generator) + ((comNum == 1) ? 75 : 0);

            forceLeftAnkle[0] = vecForceLeftAnkle.X() + dist(generator);
            forceLeftAnkle[1] = vecForceLeftAnkle.Y() + dist(generator);
            forceLeftAnkle[2] = vecForceLeftAnkle.Z() + dist(generator) + ((comNum == 2) ? 0 : -75);
        }
        else
        {
            forceRightAnkle[0] = vecForceRightAnkle.X() + dist(generator);
            forceRightAnkle[1] = vecForceRightAnkle.Y() + dist(generator);
            forceRightAnkle[2] = vecForceRightAnkle.Z() + dist(generator);

            forceLeftAnkle[0] = vecForceLeftAnkle.X() + dist(generator);
            forceLeftAnkle[1] = vecForceLeftAnkle.Y() + dist(generator);
            forceLeftAnkle[2] = vecForceLeftAnkle.Z() + dist(generator);
        }

        forceRightHand[0] = vecForceRightHand.X() + dist(generator) / factor;
        forceRightHand[1] = vecForceRightHand.Y() + dist(generator) / factor;
        forceRightHand[2] = vecForceRightHand.Z() + dist(generator) / factor;

        forceLeftHand[0] = vecForceLeftHand.X() + dist(generator) / factor;
        forceLeftHand[1] = vecForceLeftHand.Y() + dist(generator) / factor;
        forceLeftHand[2] = vecForceLeftHand.Z() + dist(generator) / factor;

        torqueRightAnkle[0] = vecTorqueRightAnkle.X() + dist(generator);
        torqueRightAnkle[1] = vecTorqueRightAnkle.Y() + dist(generator);
        torqueRightAnkle[2] = vecTorqueRightAnkle.Z() + dist(generator);

        torqueLeftAnkle[0] = vecTorqueLeftAnkle.X() + dist(generator);
        torqueLeftAnkle[1] = vecTorqueLeftAnkle.Y() + dist(generator);
        torqueLeftAnkle[2] = vecTorqueLeftAnkle.Z() + dist(generator);

        torqueRightHand[0] = vecTorqueRightHand.X() + dist(generator);
        torqueRightHand[1] = vecTorqueRightHand.Y() + dist(generator);
        torqueRightHand[2] = vecTorqueRightHand.Z() + dist(generator);

        torqueLeftHand[0] = vecTorqueLeftHand.X() + dist(generator);
        torqueLeftHand[1] = vecTorqueLeftHand.Y() + dist(generator);
        torqueLeftHand[2] = vecTorqueLeftHand.Z() + dist(generator);

        std::cerr << "Noise: " << dist(generator) << std::endl;
    }

    // Filtering the hand forces

    int filterWindow = 10;

    std::cerr << "Force before filtering: " << forceRightHand[0] << std::endl;
    filterFile << tme << " " << forceRightHand[0] << " ";

    MeanFilter(vecForceRightHandX, forceRightHand[0], forceRightHand[0], filterWindow);
    MeanFilter(vecForceRightHandY, forceRightHand[1], forceRightHand[1], filterWindow);
    MeanFilter(vecForceRightHandZ, forceRightHand[2], forceRightHand[2], filterWindow);
    MeanFilter(vecForceLeftHandX, forceLeftHand[0], forceLeftHand[0], filterWindow);
    MeanFilter(vecForceLeftHandY, forceLeftHand[1], forceLeftHand[1], filterWindow);
    MeanFilter(vecForceLeftHandZ, forceLeftHand[2], forceLeftHand[2], filterWindow);

    std::cerr << "Force after filtering: " << forceRightHand[0] << std::endl;
    filterFile << forceRightHand[0] << std::endl;
}

// Read and store joints states

void CarryingPlugin::ReadJS()
{
    for (int i = 0; i < joints.size(); i++)
    {
        qSens[i] = joints[i]->Position();
        dqSens[i] = joints[i]->GetVelocity(0);
    }
}

// Write joints commands

void CarryingPlugin::WriteJC()
{
    for (int i = 0; i < joints.size() - 2; i++)
    {
        joints[i]->SetForce(0, tauDes[i]);
    }
}

// Initialise torques to zero

void CarryingPlugin::initTorques()
{
    for (int i = 0; i < N; i++)
    {
        tauDes[i] = 0.0;
    }
}

// Set to a joint position

void CarryingPlugin::WriteJP(const double q[31])
{
    for (int i = 0; i < joints.size(); i++)
    {
        joints[i]->SetPosition(0, q[i]);
    }
}

void CarryingPlugin::ReadEnergy()
{
    torsoPos = torsoLink->WorldInertialPose().Pos();

    rForearmPos = rForearmLink->WorldInertialPose().Pos() - torsoPos;
    lForearmPos = lForearmLink->WorldInertialPose().Pos() - torsoPos;

    energy = ((precRPosition - rForearmPos).Dot(vecForceRightHand) + (precLPosition - lForearmPos).Dot(vecForceLeftHand)) / 2;

    precRPosition = rForearmPos;
    precLPosition = lForearmPos;
}

void CarryingPlugin::ReadPos()
{
    auto modelPose = gazModel->WorldPose().Pos();
    xPos = modelPose.X();
    yPos = modelPose.Y();
    zPos = modelPose.Z();
}