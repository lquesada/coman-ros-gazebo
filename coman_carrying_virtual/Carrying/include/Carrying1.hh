#pragma once
#ifdef REAL_ROBOT
#include <comanepfl/robot.hh>
#else
//#include <coman/webots/robotwebots.hh>
#endif

#include <cmath>
#include <iostream>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <iDynTree/Model/FreeFloatingState.h>
#include <iDynTree/KinDynComputations.h>
#include <iDynTree/ModelIO/ModelLoader.h>
#include <iDynTree/Model/FreeFloatingState.h>
#include <iDynTree/Model/FreeFloatingMatrices.h>
#include <iDynTree/Core/EigenHelpers.h>
//Optoforce
#include "MinimalOpto.hpp"

#include <ignition/math/Vector3.hh>
#include <MeanFilter.hh>

using namespace std;
using namespace Eigen;
using namespace RigidBodyDynamics;
using namespace RigidBodyDynamics::Math;

class VirtualController
{

public:
    VirtualController();
    void Load();
    void Controller();
    void Initialize();

    // Virtual files

    std::ifstream inputfile;
    std::ofstream outputfile;
    std::ofstream controlOutput;

private:

std::ofstream filterFile;

EigenRobotState eigRobotStateMain;
EigenRobotState eigRobotStateMainARM;
EigenRobotState eigRobotState2;
EigenRobotState eigRobotState3;
Eigen::Vector3d handRightPos;
bool notInitialized = true;
double forceRightHand0[3] = {0};
double forceLeftHand0[3] = {0};

double vals[N] = {}, valsPosture[N] = {}, valsOscillate[N] = {}, valsUpperBody[N] = {}, valsLevel[N] = {}, valsUpperBodyOscillate[N] = {};
bool onlyUpperBody = false;

double Q0[N] = {};
double qSens[N] = {}, dqSens[N] = {}, tauSens[N] = {}, qSensAbs[N] = {};
double trans[3][3] = {};
double imuAngRates[3] = {}, imuAccelerations[3] = {};
double forceRightAnkle[3] = {}, torqueRightAnkle[3] = {}, forceLeftAnkle[3] = {}, torqueLeftAnkle[3] = {}, forceRightHand[3] = {}, forceLeftHand[3] = {}, torqueRightHand[3] = {}, torqueLeftHand[3] = {};
std::vector<double> vecForceRightHandX;
std::vector<double> vecForceRightHandY;
std::vector<double> vecForceRightHandZ;
std::vector<double> vecForceLeftHandX;
std::vector<double> vecForceLeftHandY;
std::vector<double> vecForceLeftHandZ;

double h[N] = {}, dh[N] = {}, hD[N] = {}, dhD[N] = {};
const double TIME2WALK = 10; //10;
double qDes[4] = {};

RigidBodyDynamics::Model *modelRBDLright;
RigidBodyDynamics::Model *modelRBDLleft;
RigidBodyDynamics::Model *modelRBDLleg;
RigidBodyDynamics::Model *modelRBDLUB;
RigidBodyDynamics::Model *modelRBDLUB2;

Control control;
bool LoadModelFlag = true;
double cdPR[3][3] = {}, euler[3] = {};

// Torques variables

double tauDes[N] = {}, tauDesUB[N] = {}, tauDesLevel[N] = {}, tauDesPost[N] = {}, tauDesOscillate[N] = {}, tauDesLB[N] = {};

//

std::ofstream customOutput;
std::ofstream outputFile;

//

iDynTree::ModelLoader mdlLoader;
iDynTree::ModelLoader mdlLoader1;
iDynTree::ModelLoader mdlLoader2;
iDynTree::ModelLoader mdlLoader3;
iDynTree::KinDynComputations kinDynComp;
iDynTree::KinDynComputations kinDynCompARM;
iDynTree::KinDynComputations kinDynComp2;
iDynTree::KinDynComputations kinDynComp3;
//NB PAY ATTENTION TO THE URDF CONVENTION: CHECK EVERYTIME WHETHER THE SEQUENCE OF JOINTS IN THE URDF IS THE SAME AS IT IS IN YOU REAL ROBOT!!!!
std::string modelFile = "/home/biorob/ModelComanURDF/coman_right_arm.urdf";   // to be replaced
std::string modelFile1 = "/home/biorob/ModelComanURDF/coman_upper_body.urdf"; // to be replaced
std::string modelFile2 = "/home/biorob/ModelComanURDF/iit-coman-no-forearms-CUT/model.urdf";
std::string modelFile3 = "/home/biorob/ModelComanURDF/coman.urdf";

iDynTree::Model model;
iDynTree::Model modelARM;
iDynTree::Model model2; // all robot without forearm
iDynTree::Model model3; // all robot

bool firstTime = true;
double tmeLoop = 0;

// Time

double prevTime = 0;
double tme = 0;
double begin_time = -1;
double tmeOld = 0;
double tme_0 = 0;

//

int comNum = 2;
bool enterFirst = true;
double tmeRead = 0;

// Initiale positions

double qknee0 = 0.2, qP0 = -0.1, qR0 = 0.055 * 1;
const double RIGHT_ELBOW_YAW = 0, LEFT_ELBOW_YAW = 0, HIP_YAW = 0;

double qInit[N] = {};
double qInitOld[N] = {};

double bWaistYaw = 0;
double bWaistSag = 0;
double bWaistLat = 0;
double bhipSag = qP0;
double bhipLat = -qR0;
double bhipYaw = 0;
double bkneeSag = qknee0;
double bankSag = qP0 * 1;
double bankLat = qR0;
double bshSag = 0.01;
double bshLat = -0.23;
double bshYaw = 0.015;
double belbj = -0.3;
double bforearmPlate = 0.06;
double bWrj1 = -0.001;
double bWrj2 = -0.06481;

double fWaistYaw = 0;
double fWaistSag = 0.175;
double fWaistLat = 0; //0
double fhipSag = qP0;
double fhipLat = -qR0;
double fhipYaw = 0;
double fkneeSag = qknee0;
double fankSag = qP0 * 1;
double fankLat = qR0;
double fshSag = 0.2;
double fshLat = -0.2;
double fshYaw = -0.06398;
double felbj = -0.46973;
double fforearmPlate = 0.1725;
double fWrj1 = 0.2;
double fWrj2 = 0.05;

double qInitCOMAN1[N] = {bWaistYaw, bWaistSag, bWaistLat, bhipSag, bhipSag, bhipLat, bhipYaw, bkneeSag, bankSag, bankLat, -bhipLat, bhipYaw, bkneeSag, bankSag, -bankLat, bshSag, bshLat, bshYaw, belbj, bshSag, -bshLat, -bshYaw, belbj, bforearmPlate, bWrj1, bWrj2, -bforearmPlate, bWrj1, -bWrj2, 0, 0};
double qInitCOMAN2[N] = {fWaistYaw, fWaistSag, fWaistLat, fhipSag, fhipSag, fhipLat, fhipYaw, fkneeSag, fankSag, fankLat, -fhipLat, fhipYaw, fkneeSag, fankSag, -fankLat, fshSag, fshLat, fshYaw, felbj, fshSag, -fshLat, -fshYaw, felbj, fforearmPlate, fWrj1, fWrj2, -fforearmPlate, fWrj1, -fWrj2, 0, 0};

// Automation variable

double dT = 0;
double dSL = 0;

std::ofstream outputLogFile;
std::ifstream inputIteration;
std::ifstream inputParams;

// Automation PID

double pid_params[8] = {};
std::ofstream outputData;
std::ifstream inputPID;

// Force offset

double forceROffset[3] = {};
double forceLOffset[3] = {};

// Shift

double xPos;
double yPos;
double zPos;

// Multi threading

int threadNb =0;

// Additional virtual variables

double forceSensors[3] = {};
double ignoreVar = 0;
};

int main(int argc, char *argv[]);