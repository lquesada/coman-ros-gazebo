
#pragma once // do not remove!!!!
#ifdef REAL_ROBOT
#include <comanepfl/robot.hh>
#else
// #include <coman/webots/robotwebots.hh>
#endif
#include <iDynTree/Model/FreeFloatingState.h>
// Eigen headers
#include </usr/include/eigen3/Eigen/Dense>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <ctime>
#include <Eigen/Geometry>
#include <Eigen/Core>
#include <iDynTree/Model/FreeFloatingState.h>
#include <iDynTree/KinDynComputations.h>
#include <iDynTree/ModelIO/ModelLoader.h>
#include <iDynTree/Model/FreeFloatingState.h>
#include <iDynTree/Model/FreeFloatingMatrices.h>
#include <iDynTree/Core/EigenHelpers.h>
#include <sys/time.h>
#include <rbdl/rbdl.h>
#include <rbdl/addons/urdfreader/urdfreader.h>

#include "Classifier.hh"
#include "structures.hh"

#include </usr/include/eigen3/Eigen/Dense>
#include <Eigen/Eigen>

#include "WalkingController3.hh"
#include "imu_data.hh"

using namespace Eigen;
using namespace RigidBodyDynamics;
using namespace RigidBodyDynamics::Math;
using namespace RigidBodyDynamics::Utils;
//! The class of Cmatrix abreviated from Eigen
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> Cmatrix;

#include "AvgFilter.hh"
#include "init_pos.hh"
//! The class of Cmatrix abreviated from Eigen
//! The class of Cvector abreviated from Eigen VectorXd
#define Cvector Eigen::VectorXd
//! The class of Cvector3 abreviated from Eigen Vector3d
#define Cvector3 Eigen::Vector3d
#define Cvector4 Eigen::Vector4d
#define zero_v3 Cvector3(0.0, 0.0, 0.0)

const int M = 50;
const int M2 = 10;
const int M3 = 15;
const int M4 = 10;
const int M5 = 5;
const int AirTresh = 50;
const int LEFT = 0;
const int RIGHT = 1;
const int LOWER_BODY_N = 15;
const int UPPER_BODY_N = 16;
const int nFeat = 3;

const int N = 31;

class Control
{

public:
  Control() : classifier(1) {}
  void UpperBody(double vals[N], double time, double ForceRightHand0[3], double ForceLeftHand0[3], double Pos_sens[], double Vel_sens[], double forceRightHand[3], double forceLeftHand[3], double torqueRightHand[3], double torqueLeftHand[3], double Q0[], double Q1[], bool LoadModelFlag, EigenRobotState eigRobotStateMainARM, const iDynTree::Model modelMainARM, iDynTree::KinDynComputations &kinDynCompMainARM, bool onlyUpperBody);
  void Posture(double prova[], double Pos_sens[], double Vel_sens[], double qPelvis[], double comUB[], EigenRobotState eigRobotStateMain, const iDynTree::Model &model, iDynTree::KinDynComputations &kinDynCompMain, double accuracy, double filt);
  void PostureRBDL(double posSens[], double velSens[], Model *modelRBDL, Model *modelRBDLJac, double accuracy, double filt, double qPelvis[]);
  void PostureRBDL_FT(double posSens[], double velSens[], Model *modelRBDL, Model *modelRBDLJac, double accuracy, double filt, double qPelvis[], double forceRightHand[3], double forceLeftHand[3], double forceRightHand0[3], double forceLeftHand0[3], double torqueRightHand[3], double torqueLeftHand[3], double trans[][3]);
  void LowerBody(double tm, double *Q0, double *qSens, double *qSensAbs, double *dqSens, double *tauSens, double *forceRightAnkle, double *forceLeftAnkle, double *torqueRightAnkle, double *torqueLeftAnkle, double *forceRightHand, double *forceLeftHand, double trans[][3], double *imuAngRates, double *aPelvis, double *h, double *dh, double *hD, double *dhD, double *tauDes, double *vals, double euler[3]);
  void IntentionDetection(double tm, double &handRightVelFilt, Eigen::VectorXf &handRightPos, double forceSensors[3], bool &stop, double &speedCommandAvg);
  int testing();
  void SetInitPos(double qInit[N]);
  void SetOrientation(double time, const char *whichArm, double *qDes, double valsUpperBody[N], double trans[][3], double posSens[], double velSens[], Model *modelRBDL);
  void SetOrientation2(double time, const char *whichArm, double *qDes, double valsUpperBody[N], double trans[][3], double posSens[], double velSens[], Model *modelRBDL);
  void SetOrientation3(double time, std::string whichArm, double *qDes, double valsUpperBody[N], double trans[][3], double posSens[], double velSens[], Model *modelRBDL);
  void Leveling(double time, const char *whichArm, double *qDes, double valsUpperBody[N], double trans[][3], double posSens[], double velSens[], Model *modelRBDL, EigenRobotState eigRobotState3, const iDynTree::Model model3, iDynTree::KinDynComputations &kinDynComp3, Model *modelRBDLleg);
  void ClassifyInit(int x, std::string d_LDA, std::string d_FeatLabel, std::string d_Classes);
  void ForceEndEffector(double *fMapping, std::string whichArm, double posSens[], double velSens[], double forceRightHand[6], double forceLeftHand[6], EigenRobotState eigRobotState2, EigenRobotState eigRobotState3, const iDynTree::Model model2, const iDynTree::Model model3, iDynTree::KinDynComputations &kinDynComp2, iDynTree::KinDynComputations &kinDynComp3, double forceSensors[3]);
  void ComputeRotMatrix(double qq[], double qqd[], std::string from, std::string to, EigenRobotState eigRobotState, const iDynTree::Model model, iDynTree::KinDynComputations &kinDynComp, Eigen::Matrix<double, 3, 3> &rotMatrixEig, Eigen::VectorXd &relPos);
  iDynTree::FreeFloatingGeneralizedTorques ComputeGravity(double qq[], double qqd[], EigenRobotState eigRobotState, const iDynTree::Model model, iDynTree::KinDynComputations &kinDynComp);
  void EraseVectors();
  std::string ClassifyStart(double forceDiff, double velRel, double posRel, double vel, double Time, bool &simpleThreshold);
  void ComputeMomentum(VectorNd Q, double forceRightHand[3], double forceLeftHand[3], double torqueRightHand[3], double torqueLeftHand[3], Model *modelRBDL, Math::Vector3d &comDesMomentum, Math::Vector3d &torqueRForearm2BaseFrame, Math::Vector3d &torqueLForearm2BaseFrame, Math::Vector3d &comDesMomentumR, Math::Vector3d &comDesMomentumL);

  Eigen::Vector3d shoulderPositionR_Abs = {0, 0, 0};
  Eigen::Vector3d elbowPositionR_Abs = {0, 0, 0};
  Eigen::Vector3d basePositionR_Abs = {0, 0, 0};
  Eigen::Vector3d handPositionR_Abs = {0, 0, 0};
  double qUB[N_JOINT_ARM * 2 + 3] = {};
  double qPelvisFilt[2] = {};
  bool firstTimeR = true;
  bool firstTimeL = true;
  bool firstTime2R = true;
  bool firstTime2L = true;
  bool firstTime = true;
  timeval start, end;
  double orientationHandsRRoll = 0;
  double orientationHandsRPitch = 0;
  double orientationHandsRYaw = 0;
  double orientationHandsLRoll = 0;
  double orientationHandsLPitch = 0;
  double orientationHandsLYaw = 0;
  double orientationCompR[3] = {};
  double orientationCompL[3] = {};

  VectorNd prevQR = VectorNd::Zero(10);
  VectorNd prevQL = VectorNd::Zero(10);
  double verifOrientR[3] = {};
  double verifOrientL[3] = {};
  double rotHand[3][3] = {};

  //Eigen::ArrayXXf outputtest;

  struct state_vars
  {
    //        std::ofstream &outputFile;
    double tm_;
    double qLevelDes1R;
    double qLevelDes2R;
    double qLevelDes1L;
    double qLevelDes2L;
    unsigned int n_;
    double *qSens_;
    double *qDesOrientL_;
    double *qDesOrientR_;
    double *qSensAbs_;
    double *dqSens_;
    double *pPelvis_;
    double *pPelvisAbs_;
    double *vPelvis_;
    double *vPelvisAbs_;
    double *aPelvis_;
    double *forceRightAnkle_;
    double *forceLeftAnkle_;
    double *torqueRightAnkle_;
    double *torqueLeftAnkle_;
    double *forceRightHand_;
    double *forceLeftHand_;
    double *torqueRightHand_;
    double *torqueLeftHand_;
    double *orSwFt_;
    double *dorSwFt_;
    double *pSwFtInH_;
    double *angRates_;
    double *h_;
    double *hD_;
    double *pPelvisFK_;
    double *pPelvisTest_;
    double *vPelvisFk_;
    double *tauSens_;
    double *tauDes_;
    double *tauAnkTorque_;
    double k_;
    double s_;
    double tInStep_;
    double deltaX_;
    double deltaY_;
    double thp_;
    double thpF_init_;
    double thr_;
    unsigned int side_;
    double vxFK_;
    double kv_;
    double px0_;
    double kOrg_;
    double vxDes_;
    double x0_;
    double T_;
    double trans_[3][3];
    double *imuAngRates_;
    double *imuAccelerations_;
    double kR_;
    double kL_;
    double avgFreq_;
    double f0_;
    double last_step_freq_;
    double frontalBias_;
    double vxAbsF_;
    double *qSensAbsMed_;
    double *pPelvisAbsMed_;
    double handRightPos_;
    double handPosR_;
    double handPosL_;
    double handPosDesR_;
    double handPosDesL_;
    double forceSensors_;
    double *qSt_;
    int *indexSt_;
    double inCommands_;
    double handRightVelFilt_;
    double velRel_;
    double forceDiff_;
    double velSimpleThreshold_;
    bool simpleThreshold_;
    double posRel_;
    double speedCommandAvg_;
    double *orientationDesR_;
    double *orientationDesL_;
    double *qDesLevR_;
    double *qDesLevL_;
    double errLevelsR_;
    double errLevelsL_;
    double orientationRollR_;
    double orientationRollL_;
    double orientationRollDesR_;
    double orientationRollDesL_;
    double orientationPitchR_;
    double orientationPitchL_;
    double orientationPitchDesR_;
    double orientationPitchDesL_;
    double orientationYawR_;
    double orientationYawL_;
    double orientationYawDesR_;
    double orientationYawDesL_;
    double errorConvergenceR_;
    double errorConvergenceL_;
    double errorConvergenceLevR_;
    double errorConvergenceLevL_;
    double *orientationErrorR_;
    double *orientationErrorL_;

    double pxAbsOld_;
    double *pPelvisAbsF_;
    double *position_;
    double *p_left_w_;
    double *p_right_w_;
    double *p_pelvis_right_;
    double *p_pelvis_left_;
    double pPelvisAbsTmpX_;
    double *seOrientation_;
    double *velocity_;
    double *velocity_straight_;
    double rightStance_;
    double handPosWRTFootR_;
    double handPosWRTFootL_;
    double handPos_;
    double footPosR_;
    double footPosL_;
    Eigen::VectorXd fExt_;
    Eigen::VectorXd fInt_;
    Eigen::VectorXd fElbowHand_;
    std::vector<double> velTimeWind_;
    double thyF_;
    double yaw_curr_;
    double yawangle_;
    double comUBx_;
    double comUBy_;
    double comUBz_;
    double comUBxDes_;
    double comUByDes_;
    double forceHandR0_;
    double forceHandR1_;
    double forceHandR2_;
    double forceHandL0_;
    double forceHandL1_;
    double forceHandL2_;
    double forceHandRBaseFrame0_;
    double forceHandRBaseFrame1_;
    double forceHandRBaseFrame2_;
    double forceHandLBaseFrame0_;
    double forceHandLBaseFrame1_;
    double forceHandLBaseFrame2_;
    double torqueHandRBaseFrame0_;
    double torqueHandRBaseFrame1_;
    double torqueHandRBaseFrame2_;
    double torqueHandLBaseFrame0_;
    double torqueHandLBaseFrame1_;
    double torqueHandLBaseFrame2_;
    double comDesMomentum0_;
    double comDesMomentum1_;
    double comDesMomentum2_;
    double hM0_;
    double hM1_;
    int itRBDL_;
    int itIdyntree_;
    double *qPelvis_;
    double hNormPostureRBDL_;
    double hNormPostureIdyntree_;
    Math::Vector3d torqueRForearm2BaseFrame_;
    Math::Vector3d torqueLForearm2BaseFrame_;
    Math::Vector3d comDesMomentumR_;
    Math::Vector3d comDesMomentumL_;
    Math::Vector3d comDesMomentumWeight_;
    
    double forceRAnkleZF_;
    double forceLAnkleZF_;
    double energy_;
    double xPos_;
    double yPos_;
    double zPos_;

    Math::Vector3d posRArm_;
    Math::Vector3d forceRArm_;

    state_vars() : tm_(), n_(), qSens_(), qSensAbs_(), dqSens_(), pPelvis_(), pPelvisAbs_(), vPelvis_(), vPelvisAbs_(), aPelvis_(), forceRightAnkle_(), forceLeftAnkle_(), torqueRightAnkle_(),
                   torqueLeftAnkle_(), forceRightHand_(), forceLeftHand_(), torqueRightHand_(), torqueLeftHand_(), orSwFt_(), dorSwFt_(), pSwFtInH_(), angRates_(), h_(), hD_(), pPelvisFK_(), pPelvisTest_(), vPelvisFk_(), tauSens_(),
                   tauDes_(), tauAnkTorque_(), k_(), s_(), tInStep_(), deltaX_(), deltaY_(), thp_(), thpF_init_(), thr_(), side_(), vxFK_(), kv_(), px0_(), kOrg_(), vxDes_(), x0_(), T_(), trans_(),
                   imuAngRates_(), imuAccelerations_(), kR_(), kL_(), avgFreq_(), f0_(), last_step_freq_(), frontalBias_(), vxAbsF_(), qSensAbsMed_(), pPelvisAbsMed_(), handRightPos_(), forceSensors_(), inCommands_(),
                   handRightVelFilt_(), qSt_(), indexSt_(), velRel_(), forceDiff_(), velSimpleThreshold_(), velTimeWind_(), simpleThreshold_(), posRel_(), speedCommandAvg_(), fExt_(), fInt_(), fElbowHand_(), orientationDesR_(),
                   orientationDesL_(), orientationRollR_(), orientationRollL_(), orientationPitchR_(), orientationPitchL_(), orientationRollDesR_(), orientationRollDesL_(), orientationPitchDesR_(), orientationPitchDesL_(), orientationErrorR_(),
                   orientationErrorL_(), orientationYawR_(), orientationYawL_(), orientationYawDesR_(), orientationYawDesL_(), errorConvergenceR_(), errorConvergenceL_(), qDesOrientL_(), qDesOrientR_(), errLevelsR_(), errLevelsL_(), qDesLevR_(),
                   qDesLevL_(), handPosR_(), handPosL_(), handPosDesR_(), handPosDesL_(), qLevelDes1R(), qLevelDes2R(), qLevelDes1L(), qLevelDes2L(), pxAbsOld_(), pPelvisAbsF_(), position_(), p_left_w_(), p_right_w_(), p_pelvis_right_(), p_pelvis_left_(), pPelvisAbsTmpX_(), seOrientation_(), velocity_straight_(), rightStance_(),
                   handPosWRTFootR_(), handPosWRTFootL_(), footPosL_(), footPosR_(), handPos_(), errorConvergenceLevR_(), errorConvergenceLevL_(), thyF_(), yaw_curr_(), yawangle_(), comUBx_(), comUBy_(), qPelvis_(), hNormPostureRBDL_(), hNormPostureIdyntree_(),
                   itRBDL_(), itIdyntree_(), comUBxDes_(), comUByDes_(), forceHandR0_(), forceHandR1_(), forceHandR2_(), forceHandL0_(), forceHandL1_(), forceHandL2_(), forceHandRBaseFrame0_(), forceHandRBaseFrame1_(), forceHandRBaseFrame2_(), forceHandLBaseFrame0_(), forceHandLBaseFrame1_(), forceHandLBaseFrame2_(),
                   torqueHandRBaseFrame0_(), torqueHandRBaseFrame1_(), torqueHandRBaseFrame2_(), torqueHandLBaseFrame0_(), torqueHandLBaseFrame1_(), torqueHandLBaseFrame2_(), comDesMomentum0_(), comDesMomentum1_(), comDesMomentum2_(),
                   hM0_(), hM1_(), torqueRForearm2BaseFrame_(), torqueLForearm2BaseFrame_(), comDesMomentumR_(), comDesMomentumL_(), comDesMomentumWeight_(), comUBz_(), forceRAnkleZF_(), forceLAnkleZF_(), energy_(), xPos_(), yPos_(), zPos_(), posRArm_(), forceRArm_() {}
  };

  state_vars varsOut;
  //    state_vars lb_vars;
  void SaveVars(std::ofstream &outputFile, bool onlyUpperBody);
  void SaveCustVars(std::ofstream &outputFile);

  const double EPSILON = 0.000001;
  unsigned int whichComan_ = 2;
  double kR = 0, kL = 0, kF = 0;
  Math::Matrix3d rotImuGlobal;
  double STEP_LENGTH = 0.025; // = 0.025
  double T = 0.5;             // T = 0.5
  double simuTime = 0;
  bool stopSimulation = false;

  WalkingController3 walkingController3;
  double comNum = 2;

private:
  /*loadModel is a method that allows you to import the robot model (in urdf format) that you need. In this controller it is used to import the model of the full arm of COMAN robot*/
  void computeDynKin(double qq[], double qqd[], double ForceHand[], std::string whichPart, EigenRobotState eigRobotStateMainARM, const iDynTree::Model modelMainARM, iDynTree::KinDynComputations &kinDynCompMainARM);
  std::vector<double> filter(double S1[], double S2[], double S3[], std::vector<double> result, int MM);
  Eigen::Vector3d get_absolute_position(std::string link, const iDynTree::Model &model, iDynTree::KinDynComputations &kinDynCompMainARM);

  // Variables from the beginning of the file

  double qknee0 = 0.2, qP0 = -0.1, qR0 = 0.055 * 1;
  double RIGHT_ELBOW_YAW = 0, LEFT_ELBOW_YAW = 0;
  int count1 = 0, count2 = 0, count3 = 0, count4 = 0, count5 = 0;

  //useful variables

  // Walking Q
  // double Q_INIT[N] = {0, 0.0, 0, 0.2, -0.1, -0.055 * 1, 0, 0.2, -0.1 * 1, 0.055, 0.055 * 1, 0, 0.2, -0.1 * 1, -0.055 * 1, 0.01, -0.23, 0.015, -0.3, 0.01, 0.17, -0.015, -0.3, 0.06, -0.001, 0.06481, 0.06, -0.001, -0.06481, 0, 0};
  // Oscillation Q
  double Q_INIT[N] = {0, 0.2, 0, qP0, qP0, -qR0, 0, qknee0, qP0 * 1, qR0, qR0 * 1, 0, qknee0, qP0 * 1, -qR0 * 1, 0.2, -0.16, -0.06398, -0.46973, 0.2, 0.2, -0.06398, -0.46973, -0.01725, 0.2, 0.05, 0.01725, 0.2, -0.05, 0, 0};

  float r11 = 0;
  float r12 = 0;
  float r13 = 0;
  float r21 = 0;
  float r22 = 0;
  float r23 = 0;
  float r31 = 0;
  float r32 = 0;
  float r33 = 0;
  Eigen::MatrixXd eigJacobian;
  Eigen::MatrixXf JacobTrasp;
  Eigen::MatrixXf JacobTrasp2;
  Eigen::MatrixXf Jacob;
  Eigen::MatrixXf Jacob2;
  iDynTree::FreeFloatingGeneralizedTorques gGF;
  unsigned int DOFsize = 0;
  EigenRobotState eigRobotState;
  iDynTreeRobotState idynRobotState;
  iDynTreeRobotState idynRobotStateMainARM;
  Eigen::Vector3d comVel;
  Eigen::Vector3d com;
  Eigen::Vector3d comFilt;
  Eigen::Vector3d comDes;
  //        Eigen::Vector3d handPositionR_Abs={0,0,0};
  Eigen::Vector3d handPositionR_Rel;
  Eigen::Vector3d handOrientationR;
  //        Eigen::Vector3d shoulderPositionR_Abs={0,0,0};
  //        Eigen::Vector3d elbowPositionR_Abs={0,0,0};
  //        Eigen::Vector3d basePositionR_Abs={0,0,0};

  Eigen::Vector3d handPositionL_Abs;
  Eigen::Vector3d handPositionL_Rel;
  Eigen::Vector3d handPositionTemp_Abs;
  Eigen::Vector3d shoulderPositionTemp_Abs;
  Eigen::Vector3d elbowPositionTemp_Abs;
  Eigen::Vector3d basePositionTemp_Abs;
  Eigen::Vector3d torsoPositionTemp_Abs;
  Eigen::MatrixXd eigMassMatrix;
  Eigen::MatrixXd eigCOMJacobian;
  EigenRobotAcceleration eigRobotAcc;
  iDynTreeRobotAcceleration idynRobotAcc;
  Eigen::VectorXd jntTorques;
  int var = 0;
  int loop = 0;
  std::string RightLeft;
  Eigen::VectorXd gravityCompensation;
  // iDynTree::KinDynComputations kinDynComp;
  bool notInitialized = true;
  std::vector<double> S_x_R;
  std::vector<double> S_y_R;
  std::vector<double> S_z_R;
  std::vector<double> S_x_L;
  std::vector<double> S_y_L;
  std::vector<double> S_z_L;
  double conta = 0;
  std::vector<double> filtered_force_R;
  std::vector<double> filtered_force_L;

  double g[N_JOINT_ARM] = {};
  double g1[N_JOINT_ARM] = {};
  double g2[N_JOINT_ARM] = {};
  double torqueDesRight[7] = {};
  double torqueALLRight[7] = {};
  double torqueRealRight[7] = {};
  double torqueGravityRight[7] = {};
  double torqueDesLeft[7] = {};
  double torqueALLLeft[7] = {};
  double torqueRealLeft[7] = {};
  double torqueGravityLeft[7] = {};
  double Pos_sens[N] = {}, Vel_sens[N] = {};
  double ForceRightHand[3] = {}, ForceLeftHand[3] = {};
  double forceSensorsFilt = 0;
  double forceDiffFilt = 0;
  double y_des[N] = {};  // desired output to be driven to zero
  double dy_des[N] = {}; // derivative of the desired output to be driven to zero
  double tau_des[N] = {}, Kp[N] = {}, Kd[N] = {}, I[N] = {};
  double pos_des = 0;
  double vel_des = 0;
  double kRaw = 0;

  // Classifier stuff
  Classifier classifier;
  std::string d_ZstandStop;
  std::string d_ClassesStop;
  std::string d_LDAStop;
  std::string d_PCAStop;
  std::string d_FeatLabelStop;

  std::string staterob;

  // Lower body private variables
  double yaw_curr = 0;

  // Lower-body variables
  double pxDes = 0, forceRightAnkleFx = 0;
  double pPelvis[3] = {}, vPelvis[3] = {};
  double pSwFtInH[3] = {}, vSwFtInH[3] = {};
  double orSwFt[3] = {}, dorSwFt[3] = {};
  double qSt[6] = {}, qStAbs[6] = {}, qStAbsMed[6] = {}, dqSt[6] = {}, qSw[6] = {}, qSensAbsMed[N] = {}, tauAnkTorque[2] = {};
  double imuOrientation[3] = {}, angRates[3] = {}, aPelvis[3] = {}, cdPR[3][3] = {}, thr = 0, thp = 0, thy = 0;
  double pPelvisTest[3] = {}, pPelvisAbs[3] = {}, pPelvisAbsMed[3] = {}, vPelvisAbs[3] = {}, pPelvisFK[3] = {}, vPelvisFk[3] = {};
  double t = 0, s = 0, tInStep = 0, tmeTemp = 0;
  double kv = 0, x0 = 0, vxDes = 0, vxOld = 0;
  double dVx = 0; //to check
  std::vector<double> qSensAbsVec[N];
  //std::vector<double> orientationVec[3], qDesVec[3];
  double lM = 0;
  double forceRightAnkleF[3] = {}, forceLeftAnkleF[3] = {}, torqueRightAnkleF[3] = {}, torqueLeftAnkleF[3] = {};
  //double kRaw;
  std::vector<double> pyMidVec;
  double thPelvis[3] = {}, swFtPos[3] = {}, dswFtPos[3] = {};
  double TIME_WALK = 12, ZERO_S = 0.01, TIME_REWALK = 10, DTm = 0; // (T = 0.5; TIME_WALK = 12)

  imu_data imuData;
  double pyEnd = 0;
  double f_d = 2;
  bool flagStopCommand = false;
  double f0AtStop = f_d;
  double timeAtStop = 0, f_dAtStop = f_d;
  double timeInAir = 0;
  bool flagStart = false;
  int sg = -1;
  int stepNumber = 1;
  int indexSt[6] = {4, 10, 11, 12, 13, 14}, indexSw[6] = {3, 5, 6, 7, 8, 9};
  unsigned int side = 0, oldSide = side;
  double qSensInit[N] = {};
  double thpF = 0, thrF = 0, thyF = 0, thpF_init = 0, thrF_init = 0, thyF_init = 0, thPelvisInit[3] = {}, swFtPosInit[3] = {}, orSwFtInit[3] = {};
  double px = 0, px0 = 0, py = 0, pxswf = 0, pyswf = 0, vx = 0, vy = 0, vxswf = 0, vyswf = 0, ax = 0, px_old = 0, py_old = 0, pxswf_old = 0, pyswf_old = 0, dthpF = 0, dthrF = 0, dthyF = 0, swFtPitch = 0, swFtPitch_old = 0, dswFtPitch = 0, swFtRoll = 0, swFtRoll_old = 0, dswFtRoll = 0;
  double k_vec[M5] = {}, sumk = 0, forceLeftAnkleZMed = 0, forceRightAnkleZMed = 0, imuMed[3] = {};
  double vec_px[M] = {}, vec_py[M] = {}, vec_pxswf[M5] = {}, vec_pyswf[M5] = {}, vec_swFtPitch[M3] = {}, vec_swFtRoll[M3] = {};
  double k = 0, pxAbs = 0, pxAbsMed = 0, pyAbs = 0, vxAbs = 0, vxAbsF = 0, vyAbs = 0, pxAbs_old = 0, pyAbs_old = 0, vec_pxAbs[M] = {}, vec_pyAbs[M] = {}, vxFK = 0, avgFreq = f_d;
  double deltaX = 0, deltaY = 0;
  double t0 = 0;
  int inAir = 0;
  bool flagMidStep = false;
  double frontalBias = 0;
  double QswKMid_INIT = 0.8, QswKMid = QswKMid_INIT;
  int firstStopN = 300, stepsToStop = 1;
  std::vector<double> freqVec{f_d, f_d};
  std::vector<double> yawVec{0, 0};
  double SIDE_INITIAL = 0;
  double f0 = 0;
  bool startWalkingFlag = false;
  double last_step_freq = f0;
  double qDesAvgR[3] = {}, qDesAvgL[3] = {}, qDesAvg2R[2] = {}, qDesAvg2L[2] = {};
  double qDesTemp[3] = {};
  //double handRightPosFilt; // to check if the value persists

  double pPelvisAbsF[3] = {};

  double yawangle = 0;

  // State Estimation-only variables

  std::vector<double> accVec[3];
  double avgAcc[3] = {};
  double avgOr[3] = {};
  double meanAccNorm = 0;
  double position[3] = {};
  double p_left_w[3] = {};
  double p_right_w[3] = {};
  double tm_1 = {}, variance[3] = {};
  double P[21][21] = {};
  double velocity[3] = {};
  double biasAcceleration[3] = {0.0011, 0.0120, -0.0016};
  double biasOrientation[3] = {0.00662207, 0.0109228, -0.00523774};
  double rotOrient[3][3] = {
      {1, 0, 0},
      {0, 1, 0},
      {0, 0, 1}};
  double stateEstOrientation[4] = {0, 0, 0, 1};

  double p_pelvis_left[3] = {};
  double p_pelvis_right[3] = {};
  double velocity_straight[3] = {};

  // Variables from EraseVectors

  std::vector<double> pxVec, pyVec, pxswfVec, pyswfVec, vxVec, vyVec, vxswfVec, vyswfVec, axVec, thpFVec, thrFVec, dthpFVec, dthrFVec, kVec, pxAbsVec, pyAbsVec,
      vxFkVec, vxAbsVec, vyAbsVec, swFtPitchVec, dswFtPitchVec, swFtRollVec, dswFtRollVec, vecPxAbs, pxAbsMedVec, vxAbsVecF, frcRxVec,
      frcRyVec, frcRzVec, frcLxVec, frcLyVec, frcLzVec, trqRxVec, trqRyVec, trqRzVec, trqLxVec, trqLyVec, trqLzVec;
  std::vector<double> FRxVec, FRyVec, FRzVec, FLxVec, FLyVec, FLzVec, pHand, vHandVec, pHandVectTemp, relPosVec, sideVec, relVelVec, velTimeWind, intForceVec, ForceVecRx, ForceVecRy, ForceVecRz, TorqueVecRx, TorqueVecRy, TorqueVecRz, ForceVecLx, ForceVecLy, ForceVecLz, TorqueVecLx, TorqueVecLy, TorqueVecLz, sensorExtForceVec, forceDiffvec;
  std::vector<double> orientationVec[3], qDesVecR[3], qDesVecL[3], qDesVec2R[2], qDesVec2L[2], speedCommandVec, pPelvisAbsVec[3], pPelvisAbsFVec[3], forceLeftAnkleZVec, imuMedVec[3], forceRightAnkleZVec, tmVec;
  std::vector<double> frcRVec[3], frcLVec[3], trqRVec[3], trqLVec[3], qUBVec[2], qPelvisVec[2], thyFVec, dthyFVec;

  // LowerBody variables

  double begsteptime = TIME_WALK;

  // Leveling variables

  double handPosDesR = 0, handPosDes = 0;
  double handPosDesL = 0;

  // Set orientation variables (1/2/3)

  double orientationDesR[3] = {};
  double orientationDesL[3] = {};

  // Force and effector variables

  double forceLeftHandGenFilt[6] = {}, forceRightHandGenFilt[6] = {};

  // Intention detection variables

  double handRightPosFilt = 0;
  std::string cc;
  bool simpleThreshold = true;
  std::string CC = "stop";
  double T0 = 0;
  double T1 = 0;
  std::vector<double> vSupport;
  double initPosArm = 0;
  bool primaVolta = true;

  // Posture variables

  bool firstLoop = true;

  // Compute momentum variables

  double forceLeftHandFilt[3] = {}, forceRightHandFilt[3] = {}, torqueLeftHandFilt[3] = {}, torqueRightHandFilt[3] = {};
  bool firstTime2 = true;
  Math::Vector3d comDesMomentum0 = Math::Vector3d(0, 0, 0);

  // Count the number of times the resolution failed

  int overNum = 0;

  // PostureRBDL_Ft variables
};