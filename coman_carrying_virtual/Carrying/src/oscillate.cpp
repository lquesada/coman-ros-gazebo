#include <math.h>
#define N 31
#include <iostream>

using namespace std;

void oscillate(double time, double Pos_sens0[], double Qfinal[], double Pos_sens[], double Vel_sens[], double tauDes[], double vals[], int whichComan)
{
    double y[N];  // desired output to be driven to zero
    double dy[N]; // derivative of the desired output to be driven to zero
    double Kp[N], Kd[N], I[N];
    double pos_des;
    double vel_des;
    double freq = 0.5; // per f=1, AMP=0.05
    double AMP = 0.5, AMP2 = 0.5;
    double ALPHA = 0.01; // How fast exp approaches zero.

    for (int i = 0; i < N; i++)
    {
        pos_des = Qfinal[i] + (AMP - AMP * exp(-ALPHA * time)) * sin(freq * 2 * M_PI * time); //Qfinal[i];
        vel_des = AMP * cos(freq * 2 * M_PI * time) * freq * 2 * M_PI;
        //pos_des = Qfinal[i]+ AMP* sin(freq*2*M_PI*time);//Qfinal[i];
        //vel_des = AMP * cos(freq*2*M_PI*time)*freq*2*M_PI;
        y[i] = Pos_sens[i] - pos_des;
        dy[i] = Vel_sens[i] - vel_des;
        if (i == 16 || i == 17 || i == 20)
        {
            pos_des = Qfinal[i] + (AMP2 - AMP2 * exp(-ALPHA * time)) * sin(freq * 2 * M_PI * time); //Qfinal[i];
            vel_des = AMP2 * cos(freq * 2 * M_PI * time) * freq * 2 * M_PI;
            y[i] = Pos_sens[i] - pos_des;
            dy[i] = Vel_sens[i] - vel_des;
        }
    }

    for (int i = 0; i < 23; i++)
    {
        Kp[i] = 300;
        Kd[i] = 3;
    }

    Kp[23] = 5;
    Kd[23] = 0;
    Kp[24] = 5;
    Kd[24] = 0;
    Kp[25] = 5;
    Kd[25] = 0;
    Kp[26] = 0;
    Kd[26] = 0;
    Kp[27] = 5;
    Kd[27] = 0;
    Kp[28] = 5;
    Kd[28] = 0;
    Kp[29] = 0;
    Kd[29] = 0;
    Kp[30] = 0;
    Kd[30] = 0;

    for (int i = 0; i < N; i++)
    {
        tauDes[i] = -Kp[i] * (y[i]) - Kd[i] * (dy[i]);
    }

    //    cout << vals[24] << " " << Pos_sens[24] << " " << Vel_sens[24] << endl;
}
