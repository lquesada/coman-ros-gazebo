#include "StiffJoint.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/gazebo.hh"
#include "ignition/math4/ignition/math.hh"
#include <iostream>
#include <math.h>

StiffJoint::StiffJoint(gazebo::physics::LinkPtr link1_, gazebo::physics::LinkPtr link2_)
{
    this->link1 = link1_;
    this->link2 = link2_;
}

void StiffJoint::Initialize()
{
    initialPosOffset = link2->WorldPose().Pos() - link1->WorldPose().Pos();
    initialRotOffset = link1->WorldPose().Rot() * link2->WorldPose().Rot().Inverse();
}

void StiffJoint::Apply()
{
    // Linear offset

    currentPosOffset = link2->WorldPose().Pos() - link1->WorldPose().Pos();
    offsetPosOffset = currentPosOffset - initialPosOffset;
    forceStiff = offsetPosOffset;

    linearVelocityLink1 = link1->WorldLinearVel();
    linearVelocityLink2 = link2->WorldLinearVel();

    forceVel1 = dampingLawLinear(linearVelocityLink1);
    forceVel2 = dampingLawLinear(linearVelocityLink2);

    forceStiff = stiffnessLawLinear(forceStiff);

    // Rotationnal offset

    rotLink1 = link1->WorldPose().Rot();
    rotLink2 = link2->WorldPose().Rot();

    currentRotOffset = rotLink1 * rotLink2.Inverse();
    offsetRotOffset = initialRotOffset * currentRotOffset.Inverse();

    offsetRotOffset.Normalize();

    torqueStiff = offsetRotOffset.Euler();

    angularVel1 = link1->WorldAngularVel();
    angularVel2 = link2->WorldAngularVel();

    torqueStiff = stiffnessLawRotational(torqueStiff);
    torqueVelLink1 = dampingLawRotational(angularVel1);
    torqueVelLink2 = dampingLawRotational(angularVel2);

    // Frame alignments

    alignedForceStiffLink1 = rotLink1.RotateVectorReverse(forceStiff);
    alignedForceStiffLink2 = -rotLink2.RotateVectorReverse(forceStiff);
    alignedForceVelLink1 = rotLink1.RotateVectorReverse(forceVel1);
    alignedForceVelLink2 = rotLink2.RotateVectorReverse(forceVel2);

    forceLink1 = alignedForceStiffLink1 + alignedForceVelLink1;
    forceLink2 = alignedForceStiffLink2 + alignedForceVelLink2;

    // alignedTorqueStiffLink1 = rotLink1.RotateVectorReverse(torqueStiff);
    // alignedTorqueStiffLink2 = rotLink2.RotateVectorReverse(-torqueStiff);
    alignedTorqueVelLink1 = rotLink1.RotateVectorReverse(torqueVelLink1);
    alignedTorqueVelLink2 = rotLink2.RotateVectorReverse(torqueVelLink2);

    alignedTorqueStiffLink1 = torqueStiff;
    alignedTorqueStiffLink2 = -torqueStiff;
    // alignedTorqueVelLink1 = torqueVelLink1;
    // alignedTorqueVelLink2 = torqueVelLink2;

    torqueLink1 = alignedTorqueStiffLink1 + alignedTorqueVelLink1;
    torqueLink2 = alignedTorqueStiffLink2 + alignedTorqueVelLink2;

    // Force and torques applications

    link1->AddLinkForce(forceLink1);
    link2->AddLinkForce(forceLink2);

    link1->AddTorque(torqueLink1);
    link2->AddTorque(torqueLink2);

    //

    std::cout << "Initial offset: " << initialPosOffset << std::endl;
    std::cout << "Pos link1: " << link1->WorldPose() << std::endl;
    std::cout << "Pos link2: " << link2->WorldPose() << std::endl;
    std::cout << "Offset offset: " << offsetPosOffset << std::endl;
    std::cout << "Force link 1: " << forceLink1 << std::endl;
    std::cout << "Force link 2: " << forceLink2 << std::endl;
    std::cout << "-----" << std::endl;

    //

    std::cout << "Initial rot offset: " << initialRotOffset << std::endl;
    std::cout << "Rot link1: " << rotLink1 << std::endl;
    std::cout << "Rot link2: " << rotLink2 << std::endl;
    std::cout << "Current offset: " << currentRotOffset << std::endl;
    std::cout << "Offset offset: " << offsetRotOffset.Euler() << std::endl;
    std::cout << "Torque link 1: " << torqueLink1 << std::endl;
    std::cout << "Torque link 2: " << torqueLink2 << std::endl;
}

ignition::math::Vector3d StiffJoint::stiffnessLawLinear(ignition::math::Vector3d offsetVec)
{
    double k = 100000000;
    return k * offsetVec;
}

ignition::math::Vector3d StiffJoint::dampingLawLinear(ignition::math::Vector3d velocityVec)
{
    double c = -100;
    return c * velocityVec;
}

ignition::math::Vector3d StiffJoint::stiffnessLawRotational(ignition::math::Vector3d offsetVec)
{
    double k = 0;
    ignition::math::Vector3d torqueVec;

    for (int i = 0; i < 3; i++)
    {
        torqueVec[i] = k * offsetVec[i] / 1.57;
    }

    return torqueVec;
}

ignition::math::Vector3d StiffJoint::dampingLawRotational(ignition::math::Vector3d velocityVec)
{
    double c = -100;
    ignition::math::Vector3d torqueVec;

    for (int i = 0; i < 3; i++)
    {
        torqueVec[i] = c * velocityVec[i] / 1.57;
    }

    return torqueVec;
}