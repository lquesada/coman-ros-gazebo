
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "Classifier.hh"

#include <Eigen/Core>

// iDynTree headers
#include <iDynTree/Model/FreeFloatingState.h>
#include <iDynTree/KinDynComputations.h>
#include <iDynTree/ModelIO/ModelLoader.h>
#include <iDynTree/Model/FreeFloatingState.h>
#include <iDynTree/Model/FreeFloatingMatrices.h>

// Helpers function to convert between
// iDynTree datastructures
#include <iDynTree/Core/EigenHelpers.h>

#include <math.h>




Classifier::Classifier(int x)
{
  this->type_of_node = x;
  if(type_of_node==1){// stopping
      //std::cout << "NO HOLA :(" << std::endl;
      this-> real_feat_label = {"Sh","Elb","CM","Force"};
//      this->size_RFL = this->real_feat_label.size();
      this-> n_classes = 4;
      this-> n_postPCA_feat = 2; // final numeber of feature
      this -> n_features = 2;
      this -> n_total_features = 70;
//      this -> slidingWinSize = 5;
//      this->ST = {3, 3.6,4,4.2,4.4,4.6,4.8,5};
      this->LDA = Eigen::ArrayXXf::Zero(this->n_classes,this->n_postPCA_feat+1);}
  else if(type_of_node==2){ // walking
     // std::cout <<"HOLAAAAxD" << std::endl;
      this-> real_feat_label = {"vHa_diffAllInterval","ForceM_diffAllInterval"};
//      this->size_RFL = 3;
      this-> n_classes = 4;
      this-> n_postPCA_feat = 2;
      this -> n_features = 2;
      this -> n_total_features =2;
//      this -> slidingWinSize = 3;//2;jess
//      this->ST = {0.01,0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8,2.0};
       this->LDA = Eigen::ArrayXXf::Zero(6,this->n_postPCA_feat+1);
  }


      this->pc = Eigen::ArrayXXf::Zero(this->n_features,this->n_features);
      this->zStand = Eigen::ArrayXXf::Zero(this->n_total_features,2);
      this->valorX = 3;


      // VALUES FOR CLASSIFICATION
      
      this->rowsize = 5;
      this->colssize = 2;
      this->OldData = Eigen::ArrayXXf::Zero(rowsize,2);
      this->NewData = Eigen::ArrayXXf::Zero(rowsize,2);
      this->Feat = Eigen::VectorXf::Zero(n_postPCA_feat); //num of features
      this->mean_values = Eigen::VectorXf::Zero(4);
      this-> length_bool = 0;
      this-> v.reserve(n_postPCA_feat);

//     std::cout<<"siamo qui alleluja alleluja"<<std::endl;

  

}

int Classifier::test(){
  int x = 30;
  return x;
}

void Classifier::load( std::string d_LDA , std::string d_FeatLabel , std::string d_Classes)
{
   // OPEN MATRCIES FROM MATLAB
    std::string line;
    std::string::size_type sz;     // alias of size_t
    int r = 0;

  // LDA
   //std::string d_LDA = "/home/julen/catkin_ws/src/classifier/data/LDA.dat";
   std::ifstream  data3(d_LDA);

    while(std::getline(data3,line))
    {

        std::stringstream  lineStream(line);
        std::string        cell;
        //std::cout << line << "meeeeeh!"<< std::endl;
        int c = 0;
        while(std::getline(lineStream,cell,','))
        {
            //std::cout << cell << "meeeeeh!"<< std::endl;
            //std::cout << "QUE PASA 2= "<< c<<"  "<<r << std::endl;
            double d = std::stod (cell,&sz);
            this->LDA(r,c) = d;
            c = c+1;
            
        }
        r = r+1;
    }

  // LABELS
   //std::string d_FeatLabel = "/home/julen/catkin_ws/src/classifier/data/Features_label.csv";
   std::ifstream  data4(d_FeatLabel);
   r = 0;
   
    while(std::getline(data4,line))
    {

        std::stringstream  lineStream(line);
        std::string        cell;
        //std::cout << line << "meeeeeh!"<< std::endl;
        int c = 0;
        while(std::getline(lineStream,cell,','))
        {
            //std::cout << cell << "meeeeeh!"<< std::endl;
            //std::cout << cell << std::endl;
            this->v.push_back(cell);
            c = c+1;
        }
//        std::cout << c<< std::endl;
        r = r+1;
    }
//    std::cout << r<< std::endl;
   for(int i=0; i<this->n_total_features ; i++){
    for(int j = 0; j<this->real_feat_label.size();j++){
      if (v[i].find(real_feat_label[j]) != std::string::npos && v[i].find("v"+real_feat_label[j]) == std::string::npos ) {

          n_feats.push_back(i);
//          std::cout << v[i] << std::endl;
      } 
    }
   }
//   std::cout << n_feats.size() << std::endl;

   // Classes
   std::ifstream  data5(d_Classes);
//    std::cout<<"sono in load"<<std::cout;
   r = 0;
   
    while(std::getline(data5,line))
    {

        std::stringstream  lineStream(line);
        std::string        cell;
        int c = 0;
        while(std::getline(lineStream,cell,','))
        {

            this->Class.push_back(cell);
            c = c+1;
        }
        r = r+1;
    }
   
}

std::string Classifier::Classify(double featVect[], double Time)
{
  
  // Main Featured Data : 
    this->Feat(0) = featVect[0];
    this->Feat(1) = featVect[1];
   // this->Feat(2) = featVect[2];
    this->LinearDiscriminant(Feat);
    return this->outval;

}

void Classifier::slidingWindow(double Time)
{

    // GET THE LAST X seconds window of the Features

    this->OldData.resize(rowsize,colssize+1);
    this->OldData.block(0,0,rowsize,colssize) = NewData.block(0,0,rowsize,colssize);
    this->colssize = this->colssize+1;


    this->OldData.rightCols(1) << Time,Feat(0),Feat(1),Feat(2),Feat(3);
    //std::cout << OldData<< std::endl;
    int j = -1;
    int break_bol = 0;
    while(break_bol==0)
    {
      j= j+1;
      if(this->OldData(0,j)-Time+this->slidingWinSize>0)
        {break_bol = 1;}
    }

   
    this->NewData.resize(this->rowsize,this->colssize-j);
    this->colssize = colssize-j;
    //std::cout << OldData<< std::endl;
    this->NewData.block(0,0,rowsize,colssize) = this->OldData.topRightCorner(rowsize,colssize);
   

}

Eigen::ArrayXXf Classifier::getFeatures(double Time)
{
    //std::cout << NewData << std::endl;
   // Obtain the features

    
    this-> Features =  Eigen::ArrayXXf::Zero(rowsize, ST.size());
   
    int i;
    //std::cout << NewData.block(1,0,rowsize-1,0) << std::endl;
    
    // divide the new data information in ST samples
    for (i=0;i<NewData.cols();i++)
    {
      int j=0;
      bool break_while= 0;
      while(j<ST.size() && break_while ==0)
      {
        break_while = add_element(i,j,Time);
        j = j+1;
      }
    }
    
    // GET MEAN;

    this->get_means();
    for(i=0;i<ST.size();i++)
    {
      //std::cout << Features(0,i)<< std::endl;
      if(Features(0,i) != 0)
      {
        for(int j=1;j<rowsize;j++){
            if(i == 0){
              Features.block(j,i,1,1) = Features.block(j,i,1,1)/Features.block(0,i,1,1);
               //std::cout <<"means = "<<this->mean_values(j-1)<<" & Feat(0,0) = " << Features.block(j,i,1,1)<<std::endl;
               }
            else {Features.block(j,i,1,1) = Features.block(j,i,1,1)/Features.block(0,i,1,1) - this->mean_values(j-1);}
            
       }
      }
    }


    //std::cout << "MEANS = \n"<<this->mean_values(0) <<" " << this->mean_values(1) <<" "<< this->mean_values(2) <<" "<< this->mean_values(3)<< std::endl;

    Eigen::ArrayXXf Features2 = Eigen::ArrayXXf::Zero(rowsize-1, ST.size()-1);
    
    Features2.block(0,0,rowsize-1,ST.size()-1) = Features.block(1,1,rowsize-1, ST.size()-1);
        

    if(type_of_node==1){
        
        return Features2;
    }
    else if(type_of_node==2){
       // std::cout << "CheckPoint 3"<< std::endl;
        this->extra_feats = this->extra_features(); // maximum values walking states as feature
        //std::cout << "HOLAAAAAAA"<< std::endl;
   
        return Features2;

  }
   
}
bool Classifier::add_element(int i, int j, double Time)
{
  //std::cout <<"EL SAMPLING ESTA AQUI = " <<this->NewData(0,i)-Time+5<<std::endl;
  if(this->NewData(0,i)-Time+slidingWinSize<ST[j])
      {
        //std::cout << "HOLA"<<std::endl;
        this->Features(0,j)= this->Features(0,j)+1;
        this->Features.block(1,j,rowsize-1,1)=this->Features.block(1,j,rowsize-1,1)+NewData.block(1,i,rowsize-1,1);
        return 1;
        
      }
  else{return 0;}

}

Eigen::VectorXf Classifier::norm_pc(Eigen::ArrayXXf Features2)
{


    
    // Z-STANDARDIZATION

    Eigen::ArrayXXf Feat_vec = zStandarization(Features2);
    
    //std::cout << Feat_vec<< std::endl;
    // PC ANAlYSIS

    Eigen::VectorXf Feat_vec_pc(n_postPCA_feat,1);
   

    Eigen::MatrixXf pc2(n_postPCA_feat,n_features);
    pc2 = this->pc.block(0,0,n_features,n_postPCA_feat).transpose();
    Eigen::VectorXf Feat_vec2(n_features,1);
    Feat_vec2 = Feat_vec.transpose();
   

    Feat_vec_pc = pc2*Feat_vec2; //transformation (PCA)
     //std::cout << Feat_vec_pc<< std::endl;
    //std::cout << "CheckPoint 6" << std::endl;
    return Feat_vec_pc;

}

Eigen::ArrayXXf Classifier::zStandarization(Eigen::ArrayXXf Features2)
{
  int count = 0;
 if(type_of_node==1){
            for(int i=0;i<ST.size()-1;i++)
            {
              for(int j=0;j<size_RFL;j++)
              {
               //std::cout<<  " N_FEATS NOOOOOO = "<<n_feats[count] << std::endl;
                Features2(j,i) = (Features2(j,i)-zStand(n_feats[count],0))/zStand(n_feats[count],1);
                count = count+1;
              }
            }
 }
    else if(type_of_node==2)
 {
      for(int i=0;i<ST.size()-1;i++)
      {
        for(int j=0;j<4;j++)
        {
           if(j !=1){
                 //std::cout<< " N_FEATS = "<<n_feats[count] << std::endl;
                  Features2(j,i) = (Features2(j,i)-zStand(n_feats[count],0))/zStand(n_feats[count],1);
                  count = count+1;
           }
        }
      }


   }
     // std::cout <<"FEATURES _ NORMALIZED ="<< zStand(0,1)<<" \n" << Features2 << std::endl;

      //this->outputtest = Features2;

      Eigen::ArrayXXf Feat_vec = Eigen::ArrayXXf::Zero(1,n_features);

      //std::cout <<"Creo que muere aqui! = "<< ST.size()<< std::endl;
      Feat_vec.block(0,0,1,ST.size()-1) = Features2.block(0,0,1,ST.size()-1);

     // std::cout <<"Feat_vec "<< Feat_vec<< std::endl;

 if(type_of_node==1){

            Feat_vec.block(0,ST.size()-1,1,ST.size()-1) = Features2.block(1,0,1,ST.size()-1);
            Feat_vec.block(0,2*(ST.size()-1),1,ST.size()-1) = Features2.block(2,0,1,ST.size()-1);
            Feat_vec.block(0,3*(ST.size()-1),1,ST.size()-1) = Features2.block(3,0,1,ST.size()-1);


 }else if(type_of_node==2) {
              //std::cout << "CAWEN!"<< std::endl;
              for(int i=0;i<3;i++)
              {
                  //std::cout << "pre ="<<extra_feats(i)<< std::endl;
                  this->extra_feats(i) = (this->extra_feats(i)-zStand(n_feats[count],0))/zStand(n_feats[count],1);
                  //std::cout << "after ="<<extra_feats(i)<< std::endl;
                 // std::cout << this->extra_feats(i)<< std::endl;
                  count = count+1;
              }

              Feat_vec.block(0,ST.size()-1,1,ST.size()-1) = Features2.block(2,0,1,ST.size()-1);

             Feat_vec.block(0,2*(ST.size()-1),1,ST.size()-1) = Features2.block(3,0,1,ST.size()-1);
             //std::cout << "CheckPoint 4"<< std::endl;
             Feat_vec(0,30) = this->extra_feats(0);
             Feat_vec(0,31) = this->extra_feats(1);
             Feat_vec(0,32) = this->extra_feats(2);
             //std::cout << "CheckPoint 5"<< std::endl;



      }
      //std::cout << Feat_vec << std::endl;
      return Feat_vec;

}



void Classifier::comb(int N, int K)
{
    std::string bitmask(K, 1); // K leading 1's
    bitmask.resize(N, 0); // N-K trailing 0's

    // print integers and permute bitmask
    do {
        for (int i = 0; i < N; ++i) // [0..N-1] integers
        {
            if (bitmask[i]) std::cout << " " << i;
        }
        std::cout << std::endl;
    } while (std::prev_permutation(bitmask.begin(), bitmask.end()));
}




void Classifier::LinearDiscriminant(Eigen::VectorXf Feat_vec_pc)
{

     int iter = Classifier::factorial(n_classes-1);

     //Test Jessica
 /*    int whichClass[iter][2];
     std::vector<int> allClass{0, 0, 0, 0};
     double whichClassContainer[iter][2];
     int conta=0;
     //std::cout<<"iter"<<iter<<std::endl;
     for (int i=0;i<n_classes-1; i++){ //classes combinations
       for (int j=i+1; j<n_classes; j++){
            whichClass[conta][0]=i;
            whichClass[conta][1]=j;
    //std::cout<<whichClass[conta][0]<<"  "<<whichClass[conta][1]<<std::endl;
            conta=conta+1;

         }
     }
     for (int i=0; i<iter; i++){
             whichClassContainer[i][0]=0;
             whichClassContainer[i][1]=0;
     }
    //std::cout<<"Wich Class Container"<<std::endl;
    for (int i=0; i<iter; i++){
        if(OUT[i]>0){
            whichClassContainer[i][0]=whichClassContainer[i][0]+1;
        }
        else {
           whichClassContainer[i][1]=whichClassContainer[i][1]+1;
        }
//        std::cout<<whichClassContainer[i][0]<<"  "<<whichClassContainer[i][1]<<std::endl;

    }
    int index=0;
    for (int i=0; i<iter; i++){
        if( whichClassContainer[i][0]> whichClassContainer[i][1]){
            index=whichClass[i][0];
               }
        else{
            index=whichClass[i][1];
        }
        allClass[index]= allClass[index]+1;

    }
//     std::cout<<allClass[0]<<" "<<allClass[1]<<"  "<<allClass[2]<<"   "<<allClass[3]<<std::endl;
    std::vector<int>::iterator result;
    result= std::max_element(allClass.begin(), allClass.end());
    int winnerClass= std::distance(allClass.begin(), result);
    std::cout<< "win: Class #"<<winnerClass+1<<std::endl;
*/
     int numFeat=2;
     std::vector<double> OUT(iter);
     for(int i=0;i<iter;i++)
    {
      Eigen::VectorXf LDA_vec(numFeat+1);
      LDA_vec = LDA.block(i,0,1,numFeat+1).transpose();// each line of the LDA matrix in converted in a column vector
      OUT[i]=LDA_vec.segment(0,numFeat).transpose()*Feat_vec_pc +LDA_vec(numFeat);
    }

    /***************DECISION PROCESS****************/
    std::vector<std::vector<int>> arr(2);
    arr[0].resize(iter);
    arr[1].resize(iter);

    int f = 0;
    for(int i=0;i<n_classes-1;i++)
    {

      for (int j = i+1;j<n_classes;j++){          
          arr[0][f+j-(i+1)] = i; // WHICH CLASSES ARE COMPARED
          arr[1][f+j-(i+1)] = j;
      }
      f = f+ n_classes-i-1;
      
    }

    std::vector<int> WIN(n_classes,0); 
    for(int i = 0; i<iter; i++)
    {
      if(OUT[i]>0){WIN[arr[0][i]]=WIN[arr[0][i]]+1;}
      else {WIN[arr[1][i]]=WIN[arr[1][i]]+1;}
    
    }

    int H = 0;
    for(int i = 1; i<n_classes;i++){
        if(WIN[H]<WIN[i]){
          H=i;}
    }

    if(WIN[n_classes-1]==WIN[n_classes-2]){ // I CHECK IF THERE ARE SAME VALUES
        this->outval = "Uncertainty";
    }
    else
      {

        this->outval = Class[H]; // h REFERS TO THE POSITION
       }
       
}

int Classifier::factorial(int n)
{
  return (n == 1 ) ? 1 : this->factorial(n - 1) + n;
}

Eigen::VectorXf Classifier::extra_features()
{   
    Eigen::VectorXf new_features(3);
    int count=0;
    for(int i=1; i<rowsize;i++)
    {
        if(i !=2){

      double H =  this->NewData.block(i,0,1,colssize).maxCoeff()-this->mean_values(i-1);
      new_features(count) = H;
        //std::cout <<"H= " << H << std::endl;
      count=count+1;

        }

    }
   // std::cout <<"extraFeat = " <<new_features << std::endl;
    return new_features;

}

void Classifier::get_means()
{

  for(int i= 0; i<4 ; i++)
    
  {
   if(type_of_node==1){
            if(Features(0,0) != 0){
              //std::cout << "ESTE MOLA!"<< std::endl;
            this->mean_values(i) = this->Features(i+1,0)/this->Features(0,0);
            }
       }else if(type_of_node==2){
            double F = 0;
            double N = 0;
            for(int j=0;j<6;j++)
            {
                if(Features(0,j) != 0){
                        F = F+ this->Features(i+1,j);
                        N = N + this->Features(0,j);


                }
                this->mean_values(i) = F/N;

            }

        }
  }

  


}

