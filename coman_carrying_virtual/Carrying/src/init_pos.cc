#include "bezier.hh"
#include <math.h>
#define N 31
#include <iostream>

using namespace std;

void init_pos(double time, double Pos_sens0[], double Qfinal[], double Pos_sens[], double Vel_sens[], double tauDes[], double vals[], int whichComan)
{
    double y[N] = {};  // desired output to be driven to zero
    double dy[N] = {}; // derivative of the desired output to be driven to zero
    double pos_des = 0;
    double vel_des = 0;
    double Kp[N] = {}, Kd[N] = {};
    double ALPHA = 1;

    // Gains

    for (int i = 0; i < 15; i++)
    {
        Kp[i] = 300;
        Kd[i] = 2;
    }

    for (int i = 15; i < 31; i++)
    {
        Kp[i] = 10;
        Kd[i] = 0;
    }

    // Applying the PIDs

    for (int i = 0; i < N; i++)
    {
        pos_des = Qfinal[i] + (Pos_sens0[i] - Qfinal[i]) * exp(-ALPHA * time);
        vel_des = -ALPHA * (Pos_sens0[i] - Qfinal[i]) * exp(-ALPHA * time);
        y[i] = Pos_sens[i] - pos_des;
        dy[i] = Vel_sens[i] - vel_des;

        tauDes[i] = -Kp[i] * (y[i]) - Kd[i] * (dy[i]);
    }
    // tauDes[21] = 0;
}
