//#include "Parameters.hh"
#include <vector>
std::vector<double> pxVec, pyVec, pxswfVec, pyswfVec, vxVec, vyVec, vxswfVec, vyswfVec, axVec, thpFVec, thrFVec, dthpFVec, dthrFVec, kVec, pxAbsVec, pyAbsVec,
    vxFkVec, vxAbsVec, vyAbsVec, swFtPitchVec, dswFtPitchVec, swFtRollVec, dswFtRollVec, vecPxAbs, pxAbsMedVec, vxAbsVecF, frcRxVec,
    frcRyVec, frcRzVec, frcLxVec, frcLyVec, frcLzVec, trqRxVec, trqRyVec, trqRzVec, trqLxVec, trqLyVec, trqLzVec;
std::vector<double> FRxVec, FRyVec, FRzVec, FLxVec, FLyVec, FLzVec, pHand, vHandVec, pHandVectTemp, relPosVec, sideVec, relVelVec, velTimeWind, intForceVec, ForceVecRx, ForceVecRy, ForceVecRz, TorqueVecRx, TorqueVecRy, TorqueVecRz, ForceVecLx, ForceVecLy, ForceVecLz, TorqueVecLx, TorqueVecLy, TorqueVecLz, sensorExtForceVec, forceDiffvec;
std::vector<double> orientationVec[3], qDesVecR[3], qDesVecL[3], qDesVec2R[2], qDesVec2L[2], speedCommandVec, pPelvisAbsVec[3], pPelvisAbsFVec[3], forceLeftAnkleZVec, imuMedVec[3], forceRightAnkleZVec, tmVec;
std::vector<double> frcRVec[3], frcLVec[3], trqRVec[3], trqLVec[3], qUBVec[2], qPelvisVec[2], thyFVec, dthyFVec;

void EraseVectors()
{
    pxVec.clear();
    pxVec.clear();
    pyVec.clear();
    pxswfVec.clear();
    pyswfVec.clear();
    //    vxVec.clear();
    //    vyVec.clear();
    vxswfVec.clear();
    vyswfVec.clear();
    axVec.clear();
    thpFVec.clear();
    thrFVec.clear();
    thyFVec.clear();
    dthpFVec.clear();
    dthrFVec.clear();
    dthyFVec.clear();
    kVec.clear();
    pxAbsVec.clear();
    pxAbsMedVec.clear();
    pyAbsVec.clear();
    //    vxFkVec.clear();
    //    vxAbsVec.clear();
    //    vyAbsVec.clear();
    swFtPitchVec.clear();
    dswFtPitchVec.clear();
    swFtRollVec.clear();
    dswFtRollVec.clear();
    vecPxAbs.clear();
    for (int i(0); i < 3; i++)
    {
        pPelvisAbsVec[i].clear();
        pPelvisAbsFVec[i].clear();
    }
}
