// Output class

class Output
{
    public:
        Output(int n_degree, double h_in, double dh_in);
        Output(int n_degree, double h_st_in, double dh_st_in, double h_sw_in, double dh_sw_in, double k);
        void Set_outputD(double coeff[], double s);
        void Set_outputD(double coeff_st[], double coeff_sw[], double k, double s);
        void Set_outputD(double hD_st, double dhD_st, double hD_sw, double dhD_sw, double k);
        double h;
        double dh;
        double hD;
        double dhD;
    private:
        double Lin_comb(double xst, double xsw, double k);
        int n;
};